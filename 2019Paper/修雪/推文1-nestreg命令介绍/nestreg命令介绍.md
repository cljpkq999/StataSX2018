>Stata新命令：nestreg——嵌套模型统计

[![连享会-空间计量现场班 (2019年6月27-30日，西安)](http://upload-images.jianshu.io/upload_images/7692714-26cfc907a456a153.jpeg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "连享会-空间计量现场班 (2019年6月27-30日，西安)")](https://gitee.com/arlionn/Course/blob/master/Spatial.md)


&emsp;

> 作者：修雪 (哈尔滨工程大学)      
>     
> Stata 连享会： [知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn) | [CSDN](https://blog.csdn.net/arlionn)

&emsp;
  
- Stata连享会 [精品专题](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文](https://github.com/arlionn/stata/blob/master/README.md)

[![点击此处-查看完整推文列表](https://images.gitee.com/uploads/images/2019/0511/123125_939b8094_1522177.png "连享会(公众号: StataChina)推文列表")](https://gitee.com/arlionn/Course/blob/master/README.md)


&emsp;

# 1.nestreg命令简介
在回归分析中，如果解释变量间相关性较强，“解释变量无关的独立性”假设得不到满足，这时我们会将相似的解释变量归入一组，允许同组内的变量相关，但不同组的变量相互独立，这就形成了嵌套式的树形结构。多元回归模型可以将嵌套关系的数据置于阶层结构的模型中，不再将群组效应同个体效应混合在一起，可以分层计算不同水平解释变量对被解释变量的影响。因此在一些复杂的嵌套关系下，我们可以采用分层多元回归分析法，对两个或多个回归模型进行比较。

在用stata做分层多元回归分析时，通常应用nestreg命令，该命令允许用户估计嵌套模型的序列。nesterg通过顺序添加变量块来拟合嵌套模型，然后报告嵌套模型之间的比较测试。nestreg提供了丰富的信息，可以打印每个中间模型的结果，并存储它们的估计值以供以后使用。特别有用的是，nestreg对模型的贡献有几种衡量标准：Wald统计，似然比卡方，R2和变异R2统计，以及贝叶斯信息准则（BIC）和Akaike信息准则（AIC）测量可用于每个中间模型。 这些措施提供了多种方法来评估添加到模型中的每个变量或变量集的重要性和效果。

## 1.1 nestreg的语法结构
根据不同的估计方法，nestreg的语法结构有以下两种：

**标准估计命令语法**
```stata
nestreg [, options ] : command_name depvar (varlist) [(varlist) ...] [if] [in] [weight] [, command_options]
```
**调查估计命令语法**
```stata
nestreg [, options] : svy [vcetype] [, svy_options] : command_name depvar (varlist) [(varlist) ...] [if] [in] [, command_options]
```

**常用选项有：**

waldtable：报告wald测试结果（默认）

lrtable：报告似然比测试结果

quietly：禁止命令名的任何输出

store(stub)：存储嵌套估计结果

waldtable指定报告wald测试结果表，waldtable是默认值；lrtable指定报告似然比测试表，如果指定了pweights，vce（robust）选项、vce（cluster clustvar）选项或者使用svy前缀则不允许使用lrtable选项；quietly禁止显示命令名称的任何输出。store（stub）指定由nestreg拟合的每个模型都存储在名称_est_stub＃下，其中＃是从第一个到最后一个的嵌套顺序。

## 1.2 nestreg语法应用举例
顺序拟合嵌套（分层）模型，首先包括协变量x1和x2，然后添加x3和x4
```stata
nestreg: regress y (x1 x2) (x3 x4)
```
同样适用于三个模型，第三个模型变量包括d1，d2和d3
```stata
nestreg: regress y (x1 x2) (x3 x4) (d1 d2 d3)
```
报告似然比测试表
```stata
nestreg, lrtable: regress y (x1 x2) (x3 x4) (d1 d2 d3)
```
适合嵌套模型，并使用svyset数据调整复杂的测量设计
```stata
nestreg: svy: regress y (x1 x2) (x3 x4) (d1 d2 d3)
```
**注意：**以上的应用举例中，regress可以更换为任何nestreg支持的命令。（nestreg支持以下Stata命令：betareg, clogit, cloglog, glm, intreg, logistic, logit, nbreg, ologit, oprobit, poisson, probit, qreg, regress, scobit, stcox, stcrreg, stintreg, streg, and tobit）

#2.Stata范例
##2.1 Wald tests

使用nestreg来测试预测变量块的重要性，一次构建一个块的回归模型。 使用来自http://www.stata-press.com/data/r15/census4的数据，我们希望测试以下出生率预测因子的显着性：medage，medagesq和region（已经分为四个指示变量：reg1，reg2，reg3和reg4）。
```stata
use http://www.stata-press.com/data/r15/census4
nestreg: regress brate (medage) (medagesq) (reg2-reg4)
```
```stata
Block  1: medage

      Source |       SS           df       MS      Number of obs   =        50
-------------+----------------------------------   F(1, 48)        =    164.72
       Model |  32675.1044         1  32675.1044   Prob > F        =    0.0000
    Residual |  9521.71561        48  198.369075   R-squared       =    0.7743
-------------+----------------------------------   Adj R-squared   =    0.7696
       Total |    42196.82        49  861.159592   Root MSE        =    14.084

------------------------------------------------------------------------------
       brate |      Coef.   Std. Err.      t    P>|t|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
      medage |  -15.24893   1.188141   -12.83   0.000    -17.63785   -12.86002
       _cons |   618.3935   35.15416    17.59   0.000     547.7113    689.0756
------------------------------------------------------------------------------

Block  2: medagesq

      Source |       SS           df       MS      Number of obs   =        50
-------------+----------------------------------   F(2, 47)        =    158.75
       Model |  36755.8524         2  18377.9262   Prob > F        =    0.0000
    Residual |  5440.96755        47  115.765267   R-squared       =    0.8711
-------------+----------------------------------   Adj R-squared   =    0.8656
       Total |    42196.82        49  861.159592   Root MSE        =    10.759

------------------------------------------------------------------------------
       brate |      Coef.   Std. Err.      t    P>|t|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
      medage |  -109.8925   15.96663    -6.88   0.000    -142.0132    -77.7718
    medagesq |   1.607332   .2707228     5.94   0.000     1.062708    2.151956
       _cons |   2007.071   235.4316     8.53   0.000     1533.444    2480.698
------------------------------------------------------------------------------

Block  3: reg2 reg3 reg4

      Source |       SS           df       MS      Number of obs   =        50
-------------+----------------------------------   F(5, 44)        =    100.63
       Model |   38803.419         5  7760.68381   Prob > F        =    0.0000
    Residual |  3393.40095        44  77.1227489   R-squared       =    0.9196
-------------+----------------------------------   Adj R-squared   =    0.9104
       Total |    42196.82        49  861.159592   Root MSE        =     8.782

------------------------------------------------------------------------------
       brate |      Coef.   Std. Err.      t    P>|t|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
      medage |  -109.0957   13.52452    -8.07   0.000    -136.3526   -81.83886
    medagesq |   1.635208   .2290536     7.14   0.000     1.173581    2.096835
        reg2 |   15.00284   4.252068     3.53   0.001     6.433365    23.57233
        reg3 |   7.366435   3.953336     1.86   0.069    -.6009898    15.33386
        reg4 |   21.39679   4.650602     4.60   0.000     12.02412    30.76946
       _cons |    1947.61   199.8405     9.75   0.000     1544.858    2350.362
------------------------------------------------------------------------------

  +-------------------------------------------------------------+
  |       |          Block  Residual                     Change |
  | Block |       F     df        df   Pr > F       R2    in R2 |
  |-------+-----------------------------------------------------|
  |     1 |  164.72      1        48   0.0000   0.7743          |
  |     2 |   35.25      1        47   0.0000   0.8711   0.0967 |
  |     3 |    8.85      3        44   0.0001   0.9196   0.0485 |
  +-------------------------------------------------------------+
```
调用一次nestreg命令运行了三次回归，每次回归按顺序添加一个变量块，如同按顺序运行一下三个回归：
```stata
regress brate medage
regress brate medage medagesq
regress brate medage medagesq reg2-reg4
```
nestreg收集了相应的预测变量块的F统计量和每个模型拟合的模型R^2统计量。第一个区块的F统计量164.72用于测试第一个变量块的联合显著性，它只是brate对medage回归的F统计量。 第二个块的F统计量为35.25，用于测试第一个变量块和第二个变量块的回归中第二个变量块的联合有效性。 在我们的例子中，它是在medage和medagesq上的brate回归中的medagesq的F检验。 类似地，第三个块的F统计量为8.85，对应于最终回归中reg2，reg3和reg4的联合测试。

## 2.2 Likelihood-ratio tests
nestreg命令提供了一种简单的语法，用于执行嵌套模型规范的似然比检验。 使用来自 http://www.stata-press.com/data/r15/lbw的数据，我们希望联合测试以下低出生体重预测因子的显著性：age, lwt, ptl 和 ht。
```stata
use http://www.stata-press.com/data/r15/lbw
xi: nestreg, lr: logistic low (i.race smoke ui) (age lwt ptl ht)
```
```stata
i.race            _Irace_1-3          (naturally coded; _Irace_1 omitted)

Block  1: _Irace_2 _Irace_3 smoke ui

Logistic regression                             Number of obs     =        189
                                                LR chi2(4)        =      18.80
                                                Prob > chi2       =     0.0009
Log likelihood = -107.93404                     Pseudo R2         =     0.0801

------------------------------------------------------------------------------
         low | Odds Ratio   Std. Err.      z    P>|z|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
    _Irace_2 |   3.052746   1.498087     2.27   0.023     1.166747    7.987382
    _Irace_3 |   2.922593   1.189229     2.64   0.008     1.316457    6.488285
       smoke |   2.945742   1.101838     2.89   0.004     1.415167    6.131715
          ui |   2.419131   1.047359     2.04   0.041     1.035459    5.651788
       _cons |   .1402209   .0512295    -5.38   0.000     .0685216    .2869447
------------------------------------------------------------------------------
Note: _cons estimates baseline odds.

Block  2: age lwt ptl ht

Logistic regression                             Number of obs     =        189
                                                LR chi2(8)        =      33.22
                                                Prob > chi2       =     0.0001
Log likelihood =   -100.724                     Pseudo R2         =     0.1416

------------------------------------------------------------------------------
         low | Odds Ratio   Std. Err.      z    P>|z|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
    _Irace_2 |   3.534767   1.860737     2.40   0.016     1.259736    9.918406
    _Irace_3 |   2.368079   1.039949     1.96   0.050     1.001356    5.600207
       smoke |   2.517698    1.00916     2.30   0.021     1.147676    5.523162
          ui |     2.1351   .9808153     1.65   0.099     .8677528      5.2534
         age |   .9732636   .0354759    -0.74   0.457     .9061578    1.045339
         lwt |   .9849634   .0068217    -2.19   0.029     .9716834    .9984249
         ptl |   1.719161   .5952579     1.56   0.118     .8721455    3.388787
          ht |   6.249602   4.322408     2.65   0.008     1.611152    24.24199
       _cons |   1.586014   1.910496     0.38   0.702     .1496092     16.8134
------------------------------------------------------------------------------
Note: _cons estimates baseline odds.


  +----------------------------------------------------------------+
  | Block |        LL       LR     df  Pr > LR       AIC       BIC |
  |-------+--------------------------------------------------------|
  |     1 |  -107.934    18.80      4   0.0009  225.8681  242.0768 |
  |     2 |  -100.724    14.42      4   0.0061   219.448  248.6237 |
  +----------------------------------------------------------------+
```
完整模型的估计结果保留在e（）中，因此我们可以使用estat和其他postestimation命令。
```stata
. estat gof

Logistic model for low, goodness-of-fit test

       number of observations =       189
 number of covariate patterns =       182
            Pearson chi2(173) =       179.24
                  Prob > chi2 =       0.3567
```
#3.参考文献

1.help nestreg

2.Acock, A. C. 2016. A Gentle Introduction to Stata. 5th ed. College Station, TX: Stata Press.

3.Lindsey, C., and S. J. Sheather. 2015. Best subsets variable selection in nonnormal regression models. Stata Journal
15: 1046–1059.

4.Stata tip 46: Step we gaily, on we go.The Stata Journal (2007)7, Number 2, pp. 272–274

5.邵辉, 张敏强, 王小婷, et al. 复杂嵌套关系下的多水平回归模型[C]// 第十七届全国心理学学术会议. 0.（http://www.doc88.com/p-6601258261021.html）

6.Church J T , Kim A C , Erickson K M , et al. Pushing the boundaries of ECLS: Outcomes in <, 34 week EGA neonates[J]. Journal of Pediatric Surgery, 2017:S0022346817302038.（https://www.sciencedirect.com/science/article/pii/S0022346817302038）
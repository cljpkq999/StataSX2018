
> 已认领：晏发发 `2019/8/5 16:42`

> **任务：** 重现如下论文，附完整数据和 Stata dofiles
- 下载作者提供的数据和 dofiles，重现论文中的结果；
- 写一篇推文介绍这篇文章，呈现论文中的核心思想、模型设定和主要结果及讨论
- 查阅引用这篇文章的相关论文，说明后续进展

#### 参考资料：
- Broockman, D. E., 2014, Do female politicians empower women to vote or run for office? A regression discontinuity approach, Electoral Studies, 34: 190-204. [[PDF]](http://web.stanford.edu/~dbroock/published%20paper%20PDFs/broockman%20female%20rd.pdf), [[重现程序和数据]](https://dataverse.harvard.edu/dataset.xhtml?persistentId=doi:10.7910/DVN/JTSYV3)


&emsp;

> 作者：xxx (xxx大学)      
>     
> Stata 连享会： [知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn) | [CSDN](https://blog.csdn.net/arlionn)

&emsp;
  

## 1.简介
我用 Stata 已经 15 年了 (数字和英文字符两侧要保留一个空格)。
## 2. xxxx

### 2.1 xxx

### 2.2 xxx

#### aaaa

#### bbbb

## 3. xxx
后续章节可以自行添加

## 4. 参考资料

**Note：** 关键文献请通过 Google 学术 或 百度学术搜索全文，并按如下格式附上链接。
如果无法找到原文，可以采用 `[[PDF]](https://sci-hub.tw/doi)` 格式提供原文链接 (如何一篇有 DOI 的论文都可以通过 DOI 前面添加 `https://sci-hub.tw/` 网址获得其原文链接)。

- Dumitrescu, E.-I., and C. Hurlin. 2012. Testing for Granger non-causality in heteroge­ neous panels. _Economic Modelling_ 29: 1450-1460. [[PDF]](http://www.univ-orleans.fr/deg/masters/ESA/CH/Causality_20111.pdf)
- Hadri, K. 2000. Testing for stationarity in heterogeneous panel data. _Econometrics Journal_ 3: 148-161. [[PDF]](https://sci-hub.tw/10.1111/1368-423X.00043)
- Dumitrescu, E.-I., and C. Hurlin. 2012. Testing for Granger non-causality in heteroge­ neous panels. _Economic Modelling_ 29: 1450-1460. [[PDF]](http://www.univ-orleans.fr/deg/masters/ESA/CH/Causality_20111.pdf)
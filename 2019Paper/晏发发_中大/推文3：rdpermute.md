>Stata新命令：rdpermute——RDD断点回归和拐点回归的组合检验

&emsp;
> 作者：晏发发 (中山大学)， yanff@mail2.sysu.edu.cn 
>     
> Stata 连享会： [知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn) | [CSDN](https://blog.csdn.net/arlionn)

&emsp;
  
## 1 rdpermute命令简介

### 1.1 rdpermute命令产生的背景简介

在断点回归设计(RDD)中，通常通过检验基准协变量的均值在运行变量的断点(或阈值)上是否发生变化来评估设计的可信度。这一做法的部分动机是Lee（2008）提出在某些条件下，RDD中的基准协变量在断点上必须是连续的。

Ivan（2018）提出了一种基于叫做诱导有序统计量的组合检验方法，这种检验方法的零假设为在断点上基准协变量分布是连续的；并引入了一种新的渐近框架来分析其性质。渐近框架旨在近似一个小样本现象：即使观测的总数n可能很大，但断点邻域内的有效观测数往往很小。因此，虽然传统的渐近解在RDD中需要断点邻域内的样本的越来越多（当n趋于无穷时），而Ivan（2018）的框架将断点邻域内的观测数q固定（当n趋于无穷时）。这种新的检验在弱条件下易于实现且渐近有效，在比渐近有效性所需的条件更强的条件下表现出有限的样本有效性，并且相对于基于均值的检验而言，它的势具有良好的特性。

### 1.2 rdpermute命令功能简介

rdpermute是对断点回归(RD)或拐点回归(RK)设计进行组合检验，由Ganong和J ger(2018)开发。RD或RK的代码估计了一系列预设的安慰剂断点或拐点，并计算渐近和基于随机的p值。它检验了该政策对结果没有影响的精准零假设，并提供带宽选择、估计和推理过程，包括Calonico, CattaneoTitiunik（2014a,b）开发的rdrobuf(Cattaneo和Titiunik)。

## 2 rdpermute的语法结构
### 2.1 rdpermute命令的语法结构

rdpermute命令的语法结构如下：
```Stata
rdpermute depvar runvar, placebo_disconts(numlist) true_discont(string) [position_true_discont(#) deriv_discont(#) bw(#)) linear quad cubic skip_install   filename(#) save_path(#) dgp(#) bw_manual(#) fg_bandwidth_scaling(# #) fg_bias_porder(#) fg_f_0(#) fg_density_porde ) fg_num_bins(#) cct_bw_par(#) cct_reg_par(#) silent ]
```
### 2.2 rdpermute命令的选项介绍
#### 2.2.1 rdpermute命令的必须选项

**placebo_disconts**：定义安慰剂拐点的位置。关于如何选择placebo_disconts的讨论，见Ganong和J ger(2017年)第3.3节。

**true_discont**：定义真正的拐点或断点所在的整数。此值必须出现在集合placebo_disconts中。如果不是手动生成placebo_disconts，而是自动生成(例如，循环)，则true_discont的二进制表示与placebo_disconts中的相应值不同。在这种情况下，可以使用参数location_true_discont代替。除非rdpermute打印错误，否则不需修改。

#### 2.2.2 rdpermute命令的其他选项

**position_true_discont(integer -1)**：预期不连续true_discont 在向量placebo_disconts中的位置。在二进制表示错误的情况下，此参数替换true_discont。

**deriv_discont(integer 1)**：设定是实现断点回归(0)还是拐点回归(1)设计。默认是拐点回归设计。

**bw(string)**：定义带宽选择的方法。 如果没有设定其他选择方法，则默认为fg_aic可能的带宽选择方法如下:

        cct:使用在calonico,Cattaneo和Titiunik（2014 a，b）中开发的rdbwselect包中的过程和函数作为子程序。rdbwselect的参数可以使用参数{cmd:cc }更改。
        fg:Fan and Gijbels (1996)提出的带宽选择。 附加参数(fg_bias_p_order, fg_density_p_order, fg_num_bins, fg_f0, 和fg_bandwidth_scaling) 可用于改变计算。
        fg_aic:Fan and Gijbels (1996) 带宽，用 fg_bias_p_order自动选择。 附加参数 (fg_density_p_order, fg_num_bins，fg_f0, 和 fg_bandwidth_scaling) 可以用于改变计算。
        manual:手动选择一个固定的带宽。可通过manual_bw选择带。.

**linear/quad/cubic**：设定使用线性、二次或三次模型。rdpermute将计算每个设定模型的p值。如果不设定线性、四次或三次，rdpermute将自动计算它们的p值。

**skip_install**：跳过所需软件包的安装。rdpermute将尝试使用稳定的预定义版本自动安装所有依赖的包。这可能并不总是可能的或期望的。Skip_install取消所有安装。注意：如果没有安装相关的程序包，代码的某些子程序和部分可能无法工作。

**filename(string)**：最终的dta格式数据的名字。只有提供了filename的时候，这个数据才会保存。

**save_path(string)**：Path最终的dta格式数据的路径。如果save_path is没有提供，那么结果自动保存在工作路径下。

**dgp(string)**：在dta数据中添加一列指标变量。

**bw_manual(real 1)**：是带宽选择方法 bw(manual)的数值。这个值是带宽，该带宽计算所有placebo_disconts的p值。

**fg_bandwidth_scaling(numlist)**：设定采用Fan和Gijbels（1996）提出的拇指法则带宽计算公式计算的依赖于模型的常数。可能必须用其他值而不是预设线性、二次和三次。fg_bandwidth_scaling[1]描述预因素prefactor, fg_bandwidth_scaling[2] 使用指数. 参数fg_bandwidth_scaling必须包含两个项目的值. fg_bandwidth_scaling 中的其他所有项目are omit 都被省略，公式的具体描述参看 Fan and Gijbels (1996)。

**fg_bias_porder(integer 4)**：设定用于估计m^2、m^3和m^4的多项式的最大阶数的带宽选择bs(fg)。只有在所选方法为fg而不是bw(fg_aic)时，才需要此参数。警告：fg_bias_p_order可能会导致使用的回归不稳定，而Stata不会指出这一点。选项bw=“fg_aic”将自动防止此类错误，因此被设置为默认值。

**fg_f_0(real 0)**：设定选择bw(fg)的组的位置。如果不设置fg_num_bins，则将在运行变量取值范围内使用50个等距的组。对于fg_f_0的自动选取，建议让此参数为空。如果希望使用手动值，可以在fg_f_0中定义一个数值。

**fg_density_porder(integer 3)**：设定用于密度估计的多项式的阶数，这意味着它通过回归来选择用于估计bw(fg)的x^p的最大指数。警告：高fg_density_p_order可能会导致在fg_bias_p_order中的问题。建议使用预设值。

**fg_num_bins(integer 50)**：设定用于估计fg_f_0的选择bw(fg) 和 fg_f_0(0) 的等距组的数量。

**cct_bw_par(string)**：设定用于选择bw(cct)的子程序rdbwselect 的附加参数或替代参数。除： x, p, q, deriv外，可以更改rdbwselect的所有参数。要更改选项，请在字符串内的html标签内定义nded 值。例子: cct_bw_par("<kernel>epa</kernel><bwselect>cerrd</bwselect>")。

**cct_reg_par(string)**：设定用于选择bw(cct)的子程序rdbwselect 的附加参数或替代参数。除： x, p, q, deriv和h外，可以更改rdbwselect的所有参数。更改如 {cmd:cct_b

**silent**：在运行时产生较少的输出结果。

## 3 Stata范例
### 3.1 Example 1:  RD 

Lee(2008)使用断点回归设计，以选票份额差额作为运行变量，估计在职对下一次选举获胜可能性的影响。这个阴谋提供了一个直观的证据，民主党赢得选举谨慎地增加了民主党人赢得下一次选举的可能性。

为了进行组合检验，将数据看成给定的，并将断点看成一个随机变量来处理。在给定的安慰剂断点集合时，我们计算RD系数，这是民主党不同的投票取值，将这些安慰剂的估计值与真实断点的估计值进行了比较。设定以下选项：

placebo_disconts(-50(1)49)意味着使用了100种安慰剂，从-50到4；

true_discont(0)表示真正的断点是在0处；

deriv_discont(0)意味着正在寻找截距线性均值的变化，使用局部线性回归。

```stata
use lee_election, clear
rdpermute y x, placebo_disconts(-50(1)49)  true_discont(0)  deriv_discont(0)  linear
```
**注**：附上数据下载地址，请直接点击[lee_election.dta](https://github.com/ganong-noel/rdpermute/blob/master/example_data/lee_election.dta)。

```stata
Block  1: Example 1:  RD
__________________________________________________
Operating in verbose mode. Add option <silent> to suppress intermediate output.
Evaluating placebo regression discontinuities
Using Imbens and Kalyanaraman bandwidth selection.

Progress:
|....|....|....|....|....|....|....|....|....|....|....|....|....|....|....|....|....|....|..
> ..|....| 
|********************************************************************************************
> ********
pvalues for local linear model
Bandwidth at true discontinuity using fg_aic: 2.2048778
Coef at true discontinuity is .51172216
p-value asymptotic: <.0001
p-value random: .02
__________________________________________________
```

渐近检验和组合检验得出的结论是一致的：在这两种方法下，都拒绝在职不影响未来选举胜利的零假设。

### 3.2 Example 2: RK with kink 
用一个拐点明显在0模拟数据。

设定以下选项：

placebo_disconts(-.98(.02)1)意味着使用了100种安慰剂，从-.98到1； 

true_discont(0)表示真正的断点是在0处；

linear意味着使用的是局部线性回归；

deriv_discont(1)采用默认意味着在寻找斜率的变化。

```stata
use sim1, clear
rdpermute y x, placebo_disconts(-.98(.02)1) true_discont(0) linear
```
**注**：附上数据下载地址，请直接点击[sim1.dta](https://github.com/ganong-noel/rdpermute/blob/master/example_data/sim1.dta)。

```stata
Block  2:  Example 2: RK with kink
__________________________________________________
Operating in verbose mode. Add option <silent> to suppress intermediate output.
Evaluating placebo regression kinks
Using AIC to choose polynomial order for bias estimate in FG bw choice

Progress:
|....|....|....|....|....|....|....|....|....|....|....|....|....|....|....|....|....|....|....|....| 
|****************************************************************************************************
pvalues for local linear model
Bandwidth at true discontinuity using fg_aic: 1.0969452
Coef at true discontinuity is 19.981462
p-value asymptotic: <.0001
p-value random: .02
__________________________________________________
```
检验与得出的结论是一致的：在政策拐点，两者都表现出高度显著的斜率变化。

## 4 参考文献

1. [help rdpermute](http://fmwww.bc.edu/RePEc/bocode/r/)

2. Calonico, S., Cattaneo, M. D., and Titiunik, R. "[Robust data-driven inference in the regression-discontinuity design](https://cattaneo.princeton.edu/papers/Calonico-Cattaneo-Titiunik_2014_Stata.pdf)".Stata Journal 14.4: 909-946 (2014a).

3. Calonico, S., Cattaneo, M. D., and Titiunik, R. "[Robust Nonparametric Confidence Intervals for Regression-Discontinuity Designs](https://onlinelibrary.wiley.com/doi/abs/10.3982/ECTA11757)" .Econometrica, 82(6):2295-2326 (2014b).

4. Fan, J. and Gijbels, I. "Local Polynomial Modelling and Its Applications", volume 66. Chapman and Hall (1996).

5. Ganong, P. and Jäger, S. "[A permutation test for the regression kink design](http://economics.mit.edu/files/13020)". Journal of the American Statistical Association, 113(522), pp.494-504 (2018).

6. Nichols, A. "[RD 2.0: Revised Stata module for regression discontinuity estimation](https://ideas.repec.org/c/boc/bocode/s456888.html)" (2011).

7. Ivana Canay,"[Approximate permutation tests and induced order statistics in the regression discontinuity design](http://faculty.wcas.northwestern.edu/~iac879/wp/RDDPermutations.pdf)". Review of Economics Studies,85,1577-1608 (2019).
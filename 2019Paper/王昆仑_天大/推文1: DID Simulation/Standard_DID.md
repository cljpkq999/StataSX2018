/// 非 ddid 命令的作用，使用更加可控的数据生成过程，如将多期处理效应的值分别设置为2,4,6
/// 样本模拟数据生成

///20190806 01:30 更新DID模拟数据
```
///设定60个观测值，设定随机数种子
  clear all
  set obs 60 
. set seed 10101
. gen id =_n
/// 每一个数值的数量扩大11倍，再减去前六十个观测值，即60*11-60 = 600，为60个体10年的面板数据
. expand 11
. drop in 1/60
. count
///以id分组生成时间标识
. bys id: gen time = _n+1999
. xtset id time
///生成协变量和处理变量，以及个体和时间效应
. gen x1 = rnormal(1,7)
. gen x2 = rnormal(2,5)
. sort time id
. by time: gen ind = _n
. sort id time
. by id: gen T = _n

. bysort id: gen x3 =_n
. bysort id: gen x4 = _n * 3
. gen D = 0 
. replace D = 1 if  id > 29
. gen post = 0
. replace post = 1 if time >= 2005

///处理效应的生成

///Standard DID的模拟'
///模拟1：这份模拟仅仅是政策效果发生在当期，整个线向上平移
bysort id: gen y0 = 10  + 5 * x1 + 3 * x2 + T + ind*D + rnormal()
bysort id: gen y1 = 10  + 5 * x1 + 3 * x2 + T + ind*D + rnormal() if time < 2005

bysort id: replace y1 = 10  + 5 * x1 + 3 * x2 + 10 + T + ind*D + rnormal() if time >= 2005

gen y = y0 + D * (y1 - y0)

binscatter y time,  line(connect) by(D)

binscatter y time,  line(connect) by(D)  controls( x1 x2)


xtreg y x1 x2 , fe r
predict e,ue
binscatter e time, line(connect) by(D)

xtreg y x1 x2 , fe r
predict e1,e
binscatter e1 time, line(connect) by(D)

///多种估计方法，待添加...
xtreg y c.D#c.post x1 x2 i.time, r fe
xtreg y c.D#c.post x1 x2 i.time, r re
areg y c.D#c.post x1 x2 i.time, a(id) robust

///模拟2：不再是整条线向上平移，而可以冲击随时间而增加

drop y* 

bysort id: gen y0 = 10  + 5 * x1 + 3 * x2 + T + ind  + rnormal()
bysort id: gen y1 = 10  + 5 * x1 + 3 * x2 + T + ind  + rnormal() if time < 2005

bysort id: replace y1 = 10  + 5 * x1 + 3 * x2 + T * 4 + ind   + rnormal() if time >= 2005

gen y = y0 + D * (y1 - y0)

binscatter y time,  line(connect) by(D) controls( x1 x2)

drop e*
xtreg y x1 x2 , fe r
predict e,ue
binscatter e time, line(connect) by(D)

xtreg y x1 x2 , fe r
predict e1,e
binscatter e1 time, line(connect) by(D)


///多种估计方法，待添加...
///此时，实验组的斜率发生改变，实际效果应该为(18, 21, 24, 27, 30,avg24)

xtreg y c.D#c.post x1 x2 i.time, r fe
xtreg y c.D#c.post x1 x2 i.time, r re
areg y c.D#c.post x1 x2 i.time, a(id) robust

///模拟2的ESA的回归和图示法，其有两个作用，一方面检验parallel trend，一方面可以看到dynamic effect
xtreg y i.D##i.time x1 x2, r fe
tab time, gen(year)
xtreg y c.D#c.year2 c.D#c.year3 c.D#c.year4 c.D#c.year5 c.D#c.year6 c.D#c.year7 c.D#c.year8 c.D#c.year9 c.D#c.year10 x1 x2 i.time, r fe

///图示法
coefplot, ///
   keep(c.D#c.year2 c.D#c.year3 c.D#c.year4 c.D#c.year5 c.D#c.year6 c.D#c.year7 c.D#c.year8 c.D#c.year9 c.D#c.year10)  ///
   coeflabels(c.D#c.year2 = "-4"   ///
   c.D#c.year3 = "-3"           ///
   c.D#c.year4 = "-2"           ///
   c.D#c.year5 = "-1"           ///
   c.D#c.year6  = "0"             ///
   c.D#c.year7  = "1"              ///
   c.D#c.year8  = "2"              ///
   c.D#c.year9  = "3"              ///
   c.D#c.year10 = "4")            ///
   vertical                             ///
   yline(0)                             ///
   ytitle("Y")                 ///
   xtitle("Time passage relative to year of adoption of implied contract exception") ///
   addplot(line @b @at)                 ///
   ciopts(recast(rcap))                 ///
   ///rescale(100)                         ///
   scheme(s1mono)
///

///ESA的标准做法,这个也等于在做有一直在Control Group的个体，如何进行ESA的设计
gen event = time - 2005 if D == 1
tab event, gen(eventt)

forvalues i = 1/10{
	replace eventt`i' = 0 if eventt`i' == .

}

drop eventt1

xtreg y eventt* x1 x2  i.time, r fe


///图示法
coefplot, ///
   keep(event*)  ///
   coeflabels(eventt2 = "-4"   ///
   eventt3 = "-3"           ///
   eventt4 = "-2"           ///
   eventt5 = "-1"           ///
   eventt6  = "0"             ///
   eventt7  = "1"              ///
   eventt8  = "2"              ///
   eventt9  = "3"              ///
   eventt10 = "4")            ///
   vertical                             ///
   yline(0)                             ///
   ytitle("Y")                 ///
   xtitle("Time passage relative to year of adoption of implied contract exception") ///
   addplot(line @b @at)                 ///
   ciopts(recast(rcap))                 ///
   ///rescale(100)                         ///
   scheme(s1mono)
```
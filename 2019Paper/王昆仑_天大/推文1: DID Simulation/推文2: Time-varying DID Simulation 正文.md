
**导入**

本系列的上一篇文章对 DID 模型的估计，平行趋势的检验以及政策的动态效果的展示，通过模拟的方式给出了较为详尽的解答。
但是上一篇文章仅仅针对实施时点为统一的年份将样本划分为实验组和控制组的 Standard DID 模型。本文作为本系列的第二篇文章，将对政策
实施时点更加灵活的 DID 形式进行介绍，其主要内容结构和方法与上一篇尽量保持一致，以达到本系列开篇时所说的用一套模拟方法
将Standard DID 和 Time-varying DID 模型整合在一起的目的。

上一篇文章的内容，点击[此处](https://gitee.com/Stata002/StataSX2018/blob/master/2019Paper/%E7%8E%8B%E6%98%86%E4%BB%91_%E5%A4%A9%E5%A4%A7/%E6%8E%A8%E6%96%871:%20DID%20Simulation/%E6%8E%A8%E6%96%871:%20Standard%20DID%20Simulation%20%E6%AD%A3%E6%96%87.md)

**一、引言**

标准 DID 模型一般针对政策实施时点为同一个时期，且接受干预的状态将一直持续下去，否则 $Treat _i * Post _t$ 的交互项设置将会严重违背平行趋势的假设，从而导致交互项的估计系数有偏。由于现实世界中很多的政策试点地区和时间都不尽相同，而且也容易发生个体是否接受政策干预的状态在不停地发生改变，因此，本文将介绍多时点 DID 方法（Time-varying DID）来使得 DID 模型更加具有一般性。这类模型也被称为渐进 DID。

高铁开通、官员晋升以及多阶段试点政策等主题往往应用渐进 DID 方法作为其主要方法。说到渐进 DID 的相关论文，不得不提到的就是发表在 The Journal of Finance 上的 [Beck, Levine & Levkov(2010)](http://sci-hub.tw/https%3A%2F%2Fdoi.org%2F10.1111%2Fj.1540-6261.2010.01589.x) 这篇文章。 它运用渐进 DID 方法对银行去管制对收入分配的影响，并且给出渐进 DID 模型的平行趋势检验的方法。这篇不失为一篇利用渐进 DID 的模版。关于 Beck & Levkov(2010) 更多的讨论，请参见经管之家[黄河泉老师的帖子](https://bbs.pinggu.org/thread-5587630-1-1.html)，这篇文章的代码和数据也可以从这个帖子中下载得到。

下面对Standard DID 和 Time-varying DID 的模型设定予以简要的介绍。在双重固定效应（Two-Way Fixed Effects）的估计框架下，Standard DID 的一般化方程是 $$ Y_{it} = \beta_0 + \beta_1 * Treat_i * Post_t + \beta * \Sigma Z_{it} +  \mu_i + \tau_t +  \epsilon_{it} (1) $$

与之相对应的Time-varying DID 的一般化模型设定是 $$ Y_{it} = \beta_0 + \beta_1 * Treat_{it} + \beta * \Sigma Z_{it} +  \mu_i + \tau_t +  \epsilon_{it} (2) $$
其中，$\Sigma Z_{it}$ 表示随时间和个体变化的控制变量，$\mu_i$ 表示个体固定效应，$\tau_t$ 表示时间固定效应，$\epsilon_{it}$ 表示标准残差项，$ i = 1,2,3,...,N; t = 1,2,3,...,T$ 。公式(1)和公式(2)中最重要的区别就是 $Treat_i * Post_t$和$Treat_{it}$ ,换句话说，Time-varying DID 用一个随时间和个体变化的处理变量代替 Standard DID 中常用的交互项。

由于 Time-varying DID 的模型设定存在一个重大的差异，如果样本中所有个体在样本期末没有完全进入处理组，那么此时，Time-varying DID 和 Event Study Approach 方法的结合存在一定的差异，因此，为和 Beck, Levine & Levkov(2010) 文章设定保持一致。本文的模拟内容为样本中所有个体在末期都进入处理组，并且在本文的第五节对 Beck, Levine & Levkov(2010) 的 Figure 3 进行复现。关于上述的二者之间的差异交由后续的推文加以介绍。


**二、Time-varyig DID Simulation: 政策效果不随时间发生变化**

**2.1 模拟数据的生成**

再次仿照[第一篇文章](https://gitee.com/Stata002/StataSX2018/blob/master/2019Paper/%E7%8E%8B%E6%98%86%E4%BB%91_%E5%A4%A9%E5%A4%A7/%E6%8E%A8%E6%96%871:%20DID%20Simulation/%E6%8E%A8%E6%96%871:%20Standard%20DID%20Simulation%20%E6%AD%A3%E6%96%87.md)
生成基础的数据结构，依然为60个体*10年=600个观察值的平衡面板数据。Time-varying DID 的设置体现在，我们使得 id 编号为 1-20 的个体在 2004 年接收政策干预，编号 21-40 的个体在 2006 年接受干预，编号为 41-60 的个体在 2008 年接受干预。因此，三组个体接受政策干预的时长分别为 6 年，4 年和 2年。

```
///设定60个观测值，设定随机数种子
. clear all
. set obs 60 
. set seed 10101
. gen id =_n

/// 每一个数值的数量扩大11倍，再减去前六十个观测值，即60*11-60 = 600，为60个体10年的面板数据
. expand 11
. drop in 1/60
. count

///以id分组生成时间标识
. bys id: gen time = _n+1999
. xtset id time

///生成协变量以及个体和时间效应
. gen x1 = rnormal(1,7)
. gen x2 = rnormal(2,5)

. sort time id
. by time: gen ind = _n
. sort id time

. by id: gen T = _n
. gen y = 0

///生成处理变量,此时D为Dit,设定1-20在2004年接受冲击，21-40为2006年，36-60为2008年
. gen D = 0
. gen birth_date = 0
forvalues i = 1/20{
	replace D = 1 if id  == `i' & time >= 2004
	replace birth_date = 2004 if id == `i'
}

forvalues i = 21/40{
	replace D = 1 if id  == `i' & time >= 2006
	replace birth_date = 2006 if id == `i'
}

forvalues i = 41/60{
	replace D = 1 if id  == `i' & time >= 2008
	replace birth_date = 2008 if id == `i'
}
```

生成结果变量，设定接受干预个体的政策效果为 10。利用固定效应模型消除个体效应和 x1、x2 两个变量对结果变量的影响，得到残差，画出更加干净分组的结果变量的时间趋势图。这个图形可以作为验证平行趋势的一个旁证。

```
///Y的生成，使得接受冲击的个体的政策真实效果为10
bysort id: gen y0 = 10  + 5 * x1 + 3 * x2 + T + ind  + rnormal()

bysort id: gen y1 = 10  + 5 * x1 + 3 * x2 + T + ind  + 10 + rnormal() if time >= 2004 & id >= 1 & id <= 20

bysort id: replace y1 = 10  + 5 * x1 + 3 * x2 + T + ind  + 10 + rnormal() if time >= 2006 & id >= 21 & id <= 40

bysort id: replace y1 = 10  + 5 * x1 + 3 * x2 + T + ind  + 10 + rnormal() if time >= 2008 & id >= 41 & id <= 60

bysort id: replace y1 = 10  + 5 * x1 + 3 * x2 + T + ind  + rnormal() if y1 == .

replace y = y0 + D * (y1 - y0)

///去除个体效应和协变量对Y的影响，得到残差并画图
xtreg y x1 x2 , fe r
predict e, ue
binscatter e time, line(connect) by(D)

```
![图1](https://images.gitee.com/uploads/images/2019/0901/004811_1a10389e_2270897.png "推文2_图片1.png")

**2.2 多种估计结果的呈现**

继续使用双向固定效应(Two-Way Fixed Effects)的方法设定方程，使用`reg`,`xtreg`,`areg`,`reghdfe`等四个`Stata`命令进行估计。

```
. reg y c.D x1 x2 i.time i.id, r
+------------------------------------------------------------------------+
Linear regression		                Number of obs   =      600
		                                F(71, 528)      = 18823.40
		                                Prob > F        =   0.0000
		                                R-squared       =   0.9996
		                                Root MSE        =   .96295

+------------------------------------------------------------------------+
      |              Robust
    y |     Coef.   Std. Err.	  t	     P>t     [95% Conf.	Interval]
+------------------------------------------------------------------------+		
    D |  9.884974   .1571734	62.89	0.000     9.576212	10.19374
   x1 |  4.995274   .0056514	883.90	0.000     4.984172	5.006376
   x2 |  2.998722   .0083092	360.89	0.000     2.982399	3.015045
+------------------------------------------------------------------------+
Note: 限于篇幅，此处汇报结果省略个体虚拟变量，时间虚拟变量和常数项的估计系数
```

```
. xtreg y c.D x1 x2 i.time, r fe
+------------------------------------------------------------------------+
Fixed-effects (within) regression	    Number of obs     =        600
Group variable: id	                    Number of groups  =         60

R-sq:	                                Obs per group:
    within  = 0.9994	                              min =         10
    between = 0.2832	                              avg =       10.0
    overall = 0.8349	                              max =         10

	                                    F(12,59)          =   82929.59
corr(u_i, Xb)  = -0.0004	            Prob > F          =     0.0000

                               (Std. Err. adjusted for 60 clusters in id)
+------------------------------------------------------------------------+
      |              Robust
    y |     Coef.   Std. Err.      t	 P>t      [95% Conf. Interval]
+------------------------------------------------------------------------+	
    D |  9.884974   .1389533    71.14	0.000      9.60693    10.16302
   x1 |  4.995274   .0055989   892.19	0.000     4.984071    5.006478
   x2 |  2.998722   .0088727   337.97	0.000     2.980968    3.016476
+------------------------------------------------------------------------+
Note: 限于篇幅，此处汇报结果省略时间虚拟变量和常数项的估计系数
```

```
. areg	y c.D x1 x2 i.time, absorb(id) robust
+-----------------------------------------------------------------------------+
Linear	regression, absorbing indicators	Number of obs     =            600
		                                    F(  12,    528)   =       73256.92
		                                    Prob > F          =         0.0000
		                                    R-squared         =         0.9996
		                                    Adj R-squared     =         0.9995
		                                    Root MSE          =         0.9630
+-----------------------------------------------------------------------------+		
       |              Robust
     y |     Coef.   Std. Err.      t	      P>t     [95% Conf. Interval]
+-----------------------------------------------------------------------------+
     D |   9.884974   .1571734    62.89	    0.000     9.576212    10.19374
    x1 |   4.995274   .0056514   883.90	    0.000     4.984172    5.006376
    x2 |   2.998722   .0083092   360.89	    0.000     2.982399    3.015045
+-----------------------------------------------------------------------------+		
    id |  absorbed	                                        (60 categories)
+-----------------------------------------------------------------------------+
Note: 限于篇幅，此处汇报结果省略时间虚拟变量和常数项的估计系数
```


```
. reghdfe y c.D x1 x2, absorb(id time) vce(robust)
+-----------------------------------------------------------------------------+
(converged in 3 iterations)

HDFE Linear regression                            Number of obs   =	       600
Absorbing 2 HDFE groups                           F(   3,    528) =  282958.86
Statistics robust to heteroskedasticity           Prob > F        =	    0.0000
                                                  R-squared       =	    0.9996
                                                  Adj R-squared   =	    0.9995
                                                  Within R-sq.    =	    0.9994
                                                  Root MSE        =	    0.9630
+-----------------------------------------------------------------------------+	
     |               Robust
    y|      Coef.   Std. Err.      t     P>t     [95% Conf.	Interval]
+-----------------------------------------------------------------------------+
    D|   9.884974   .1571734    62.89   0.000     9.576212	10.19374
   x1|   4.995274   .0056514   883.90   0.000     4.984172	5.006376
   x2|   2.998722   .0083092   360.89   0.000     2.982399	3.015045
+-----------------------------------------------------------------------------+
	
Absorbed degrees of freedom:
+--------------------------------------------------------+	
Absorbed FE|  Num. Coefs.  =   Categories  -   Redundant      
+--------------------------------------------------------+	
        id |          60              60              0      
      time |           9              10              1      
+--------------------------------------------------------+	

```

```
reg y c.D x1 x2 i.time i.id, r
eststo reg
xtreg y c.D x1 x2 i.time, r fe
eststo xtreg_fe
areg y c.D x1 x2 i.time, absorb(id) robust
eststo areg
reghdfe y c.D x1 x2, absorb(id time) vce(robust)
eststo reghdfe

estout *, title("The Comparison of Actual Parameter Values") ///
		 cells(b(star fmt(%9.3f)) se(par)) ///
		 stats(N N_g, fmt(%9.0f %9.0g) label(N Groups)) ///
		 legend collabels(none) varlabels(_cons Constant) keep(x1 x2 D)

+--------------------------------------------------------------------------------+
                            The Comparison of Actual Parameter Values
+--------------------------------------------------------------------------------+		
       |              reg           xtreg_fe	     areg	    reghdfe   
+--------------------------------------------------------------------------------
     D |            9.885***        9.885***	    9.885***	9.885***
       |            (0.157)         (0.139)	        (0.157)     (0.157)   
    x1 |            4.995***        4.995***	    4.995***	4.995***
       |            (0.006)         (0.006)	        (0.006)	    (0.006)   
    x2 |            2.999***        2.999***	    2.999***	2.999***
       |            (0.008)         (0.009)	        (0.008)	    (0.008)   
+--------------------------------------------------------------------------------+		
N      |              600             600	            600	        600   
Groups |                               60		               
+--------------------------------------------------------------------------------+	
* p<0.05, ** p<0.01, *** p<0.001
+--------------------------------------------------------------------------------+

```

$D_{it}$ 的四个估计系数都为`9.885`，其95%CI明显包含真实效果 10, 因此方程的设定不存在问题。从表格展示的结果可以知道，四个命令估计的系数大小是一致的，唯一的区别在于固定效应模型的估计系数其标准误略有变化。再次验证了这四个命令在估计Two-Way Fixed Effects 上是等价的。


**2.3 ESA方法的应用**

**这里需要写清楚Time-varying DID 和 ESA 结合的思路**

```
gen event = time - birth_date
replace event = -4 if event <= -4
tab event, gen(eventt)
drop eventt1

xtreg y eventt* x1 x2 i.time, r fe

coefplot, ///
   keep(event*)  ///
   coeflabels(eventt2 = "-3"   ///
   eventt3 = "-2"           ///
   eventt4 = "-1"           ///
   eventt5 = "0"           ///
   eventt6  = "1"             ///
   eventt7  = "2"              ///
   eventt8  = "3"              ///
   eventt9  = "4"              ///
   eventt10 = "5")            ///
   vertical                             ///
   yline(0)                             ///
   ytitle("Y")                 ///
   xtitle("Time passage relative to year of adoption of implied contract exception") ///
   addplot(line @b @at)                 ///
   ciopts(recast(rcap))                 ///
   scheme(s1mono)

```
![图2](https://images.gitee.com/uploads/images/2019/0831/221244_94f679f1_2270897.png "推文2_图片2.png")


**三、Time-varyig DID Simulation: 政策效果随时间发生变化**

**3.1 模拟数据的生成**

基础的数据结构依然是60*10=600个观测值的平衡面板数据。依然设定 id 编号为 1-20 的个体在 2004 年接收政策干预，编号 21-40 的个体在 2006 年接受干预，编号为 41-60 的个体在 2008 年接受干预。因此，三组个体接受政策干预的时长分别为 6 年，4 年和 2年。

```
///设定60个观测值，设定随机数种子
. clear all
. set obs 60 
. set seed 10101
. gen id =_n

/// 每一个数值的数量扩大11倍，再减去前六十个观测值，即60*11-60 = 600，为60个体10年的面板数据
. expand 11
. drop in 1/60
. count

///以id分组生成时间标识
. bys id: gen time = _n+1999
. xtset id time

///生成协变量以及个体和时间效应
. gen x1 = rnormal(1,7)
. gen x2 = rnormal(2,5)

. sort time id
. by time: gen ind = _n
. sort id time

. by id: gen T = _n
. gen y = 0

///生成处理变量,此时D为Dit,设定1-20在2004年接受冲击，21-40为2006年，36-60为2008年
. gen D = 0
. gen birth_date = 0
forvalues i = 1/20{
	replace D = 1 if id  == `i' & time >= 2004
	replace birth_date = 2004 if id == `i'
}

forvalues i = 21/40{
	replace D = 1 if id  == `i' & time >= 2006
	replace birth_date = 2006 if id == `i'
}

forvalues i = 41/60{
	replace D = 1 if id  == `i' & time >= 2008
	replace birth_date = 2008 if id == `i'
}
```

结果变量的生成与 2.1 节略有变化，我们将真实的政策效果设定为在政策发生当期个体的结果变量增加三个单位，以后每多一期，再增加三个单位。
然后利用固定效应模型去除个体效应和协变量x1,x2的影响，画出更加干净的结果变量的图像，作为验证平行趋势的旁证。

```
///Y的生成，设定真实的政策效果为当年为3，并且每年增加3
bysort id: gen y0 = 10  + 5 * x1 + 3 * x2 + T + ind + rnormal()

bysort id: gen y1 = 10  + 5 * x1 + 3 * x2 + T + ind  + (time - birth + 1 ) * 3 + rnormal() if time >= 2004 & id >= 1 & id <= 20

bysort id: replace y1 = 10  + 5 * x1 + 3 * x2 +  T + ind  + (time - birth + 1 ) * 3  + rnormal() if time >= 2006 & id >= 21 & id <= 40

bysort id: replace y1 = 10  + 5 * x1 + 3 * x2 +  T + ind  + (time - birth + 1 ) * 3  + rnormal() if time >= 2008 & id >= 41 & id <= 60

bysort id: replace y1 = 10  + 5 * x1 + 3 * x2 +  T + ind  + rnormal() if y1 == .

replace y = y0 + D * (y1 - y0)

///去除个体效应和协变量对Y的影响，得到残差并画图
xtreg y x1 x2 , fe r
predict e, ue
binscatter e time, line(connect) by(D)

```
![图3](https://images.gitee.com/uploads/images/2019/0901/004954_d692d6f9_2270897.png "推文2_图片3.png")

**3.2 多种估计结果的呈现**


继续使用双向固定效应(Two-Way Fixed Effects)的方法设定方程，使用`reg`,`xtreg`,`areg`,`reghdfe`等四个`Stata`命令进行估计。

```
. reg y c.D x1 x2 i.time i.id, r
+------------------------------------------------------------------------+
Linear regression		        Number of obs     =	           600
		                        F(71, 528)        =        3270.68
		                        Prob > F          =	        0.0000
		                        R-squared         =	        0.9973
		                        Root MSE          =	        2.3386
+------------------------------------------------------------------------+
      |              Robust
    y |     Coef.   Std. Err.	 t	         P>t     [95% Conf.	Interval]
+------------------------------------------------------------------------+		
    D |    2.69066   .3612682	7.45	    0.000     1.980961	3.40036
    x1|   4.987876   .0138214	360.88	    0.000     4.960725	5.015028
    x2|   3.026438   .0201725	150.03	    0.000      2.98681	3.066066
+------------------------------------------------------------------------+
Note: 限于篇幅，此处汇报结果省略个体虚拟变量，时间虚拟变量和常数项的估计系数
```


```
. xtreg y c.D x1 x2 i.time, r fe
+------------------------------------------------------------------------+
Fixed-effects (within) regression	    Number of obs     =        600
Group variable: id	                    Number of groups  =         60

R-sq:	                                Obs per group:
    within  = 0.9966	                              min =         10
    between = 0.3929	                              avg =       10.0
    overall = 0.8615	                              max =         10

	                                    F(12,59)          =   14181.09
corr(u_i, Xb)  = 0.0290	                Prob > F          =     0.0000

                             (Std. Err.adjusted for 60 clusters in id)
+------------------------------------------------------------------------+	
      |               Robust
    y |     Coef.    Std. Err.    t	         P>t     [95% Conf. Interval]
+------------------------------------------------------------------------+
    D |    2.69066   .1803508    14.92	    0.000     2.329779    3.051541
    x1|   4.987876   .0128302   388.76	    0.000     4.962203     5.01355
    x2|   3.026438   .0202308   149.60	    0.000     2.985957     3.06692
+------------------------------------------------------------------------+
Note: 限于篇幅，此处汇报结果省略时间虚拟变量和常数项的估计系数
```

```
. areg	y c.D x1 x2 i.time, absorb(id) robust
+-----------------------------------------------------------------------------+

Linear	regression, absorbing indicators	Number of obs     =            600
		                                    F(  12,    528)   =       12994.75
		                                    Prob > F          =         0.0000
		                                    R-squared         =         0.9973
		                                    Adj R-squared     =         0.9970
		                                    Root MSE          =         2.3386
+-----------------------------------------------------------------------------+
      |               Robust
    y |     Coef.    Std. Err.      t	     P>t       [95% Conf. Interval]
+-----------------------------------------------------------------------------+
    D |    2.69066   .3612682     7.45	    0.000     1.980961     3.40036
    x1|   4.987876   .0138214   360.88	    0.000     4.960725    5.015028
    x2|   3.026438   .0201725   150.03	    0.000      2.98681    3.066066
+-----------------------------------------------------------------------------+		
    id    absorbed	                                        (60 categories)
+-----------------------------------------------------------------------------+
Note: 限于篇幅，此处汇报结果省略时间虚拟变量和常数项的估计系数
```

```
. reghdfe y c.D x1 x2, absorb(id time) vce(robust)
+-----------------------------------------------------------------------------+
(converged in 3 iterations)

HDFE Linear regression                            Number of obs   =	     600
Absorbing 2 HDFE groups                           F(   3,    528) =	49885.71
Statistics robust to heteroskedasticity           Prob > F        =	  0.0000
                                                  R-squared       =	  0.9973
                                                  Adj R-squared   =	  0.9970
                                                  Within R-sq.    =	  0.9964
                                                  Root MSE        =	  2.3386

+-----------------------------------------------------------------------------+	
      |              Robust
    y |     Coef.   Std. Err.      t         P>t       [95% Conf. Interval]
+-----------------------------------------------------------------------------+	
    D |    2.69066   .3612682     7.45       0.000     1.980961	3.40036
    x1|   4.987876   .0138214   360.88       0.000     4.960725	5.015028
    x2|   3.026438   .0201725   150.03       0.000      2.98681	3.066066
+-----------------------------------------------------------------------------+
	
Absorbed degrees of freedom:
+--------------------------------------------------------+	
Absorbed FE|  Num. Coefs.  =   Categories  -   Redundant      
+--------------------------------------------------------+	
        id |          60              60              0      
      time |           9              10              1      
+--------------------------------------------------------+	

```

```
reg y c.D x1 x2 i.time i.id, r
eststo reg
xtreg y c.D x1 x2 i.time, r fe
eststo xtreg_fe
areg y c.D x1 x2 i.time, absorb(id) robust
eststo areg
reghdfe y c.D x1 x2, absorb(id time) vce(robust)
eststo reghdfe

estout *, title("The Comparison of Actual Parameter Values") ///
		 cells(b(star fmt(%9.3f)) se(par)) ///
		 stats(N N_g, fmt(%9.0f %9.0g) label(N Groups)) ///
		 legend collabels(none) varlabels(_cons Constant) keep(x1 x2 D)


+--------------------------------------------------------------------------------+
The Comparison of Actual Parameter Values
+--------------------------------------------------------------------------------+		
          |             reg        xtreg_fe	        areg	    reghdfe   
+--------------------------------------------------------------------------------+		
        D |         2.691***        2.691***	    2.691***	2.691***
          |         (0.361)         (0.180)	        (0.361)	    (0.361)   
        x1|         4.988***        4.988***	    4.988***	4.988***
          |         (0.014)         (0.013)	        (0.014)	    (0.014)   
        x2|         3.026***        3.026***	    3.026***	3.026***
          |         (0.020)         (0.020)	        (0.020)	    (0.020)   
+--------------------------------------------------------------------------------+		
N         |           600             600	            600	    600   
Groups    |                            60		               
+--------------------------------------------------------------------------------+
* p<0.05, ** p<0.01, *** p<0.001
+--------------------------------------------------------------------------------+

```
$D_{it}$的四个估计系数都为`2.691`，每年的平均处理效应见 3.3 节的图形。从表格展示的结果可以知道，四个命令估计的系数大小是一致的，唯一的区别在于固定效应模型的估计系数其标准误略有变化。再次验证了这四个命令在估计Two-Way Fixed Effects 上是等价的。


**3.3 ESA方法的应用**

```
gen event = time - birth_date
replace event = -4 if event <= -4
tab event, gen(eventt)
drop eventt1

xtreg y eventt* x1 x2 i.time, r fe

coefplot, ///
   keep(event*)  ///
   coeflabels(eventt2 = "-3"   ///
   eventt3 = "-2"           ///
   eventt4 = "-1"           ///
   eventt5 = "0"           ///
   eventt6  = "1"             ///
   eventt7  = "2"              ///
   eventt8  = "3"              ///
   eventt9  = "4"              ///
   eventt10 = "5")            ///
   vertical                             ///
   yline(0)                             ///
   ytitle("Y")                 ///
   xtitle("Time passage relative to year of adoption of implied contract exception") ///
   addplot(line @b @at)                 ///
   ciopts(recast(rcap))                 ///
   scheme(s1mono)
```
![图4](https://images.gitee.com/uploads/images/2019/0831/223921_579d7553_2270897.png "推文2_图片4.png")




**四、Beck & Levkov(2010) Figure 3 的重现**

原图如下：
![图5](https://images.gitee.com/uploads/images/2019/0831/224456_1895bfa0_2270897.png "推文2_图片5.png")

[Beck, Levine & Levkov(2010)](http://sci-hub.tw/https%3A%2F%2Fdoi.org%2F10.1111%2Fj.1540-6261.2010.01589.x) 原文中所给的绘制 Figure 3 的代码有点繁琐，经过调整和合并，形成本文下面代码。其优点在于，一方面利用 `coefplot`命令省略大量需要从回归方程中手动提取的系数和%95置信区间端点的取值；另一方面使用循环和 `Factor Variables` 尽量减少需要生成的过程的变量。对比本节的两张图可知，这部分代码几乎完整地将 [Beck, Levine & Levkov(2010)](http://sci-hub.tw/https%3A%2F%2Fdoi.org%2F10.1111%2Fj.1540-6261.2010.01589.x)的 Figure 3 重现，并且这部分代码具有普适和易于改造的性质。

提醒：本文并没有提供 Beck, Levine & Levkov(2010) 原文的代码和数据集，本部分的运行结果仅做生成图形对比。感兴趣的同学可以去引言中提到的经管之家[黄河泉老师的帖子](https://bbs.pinggu.org/thread-5587630-1-1.html)下载相关内容进行操作。


```
xtset statefip wrkyr
gen bb = 0
gen aa = 11
gen event = wrkyr - branch_reform

replace event  = -10 if event <= -10
replace event = 15 if event >= 15

///tab event, gen(eventt)
gen event1 = event + 10
gen y = log(gini)

qui xtreg y ib10.event1 i.wrkyr, fe r 

///生成前十期系数均值x
forvalues i = 0/9{
	gen b_`i' = _b[`i'.event1]

}

gen avg_coef = (b_0+b_9+b_8+b_7+b_6+b_5+b_4+b_3+b_2+b_1)/10
su avg_coef 
return list

coefplot, baselevels ///
   drop(_cons event1  *.wrkyr ) ///
   if(@at!=11) ///去除当年的系数点
   coeflabels(0.event1 = "-10"   ///
   1.event1 = "-9"           ///
   2.event1 = "-8"           ///
   3.event1 = "-7"           ///
   4.event1  = "-6"             ///
   5.event1  = "-5"              ///
   6.event1  = "-4"              ///
   7.event1  = "-3"       ///
   8.event1  = "-2" ///
   9.event1  = "-1" ///
   10.event1 = "0" ///
   11.event1  = "1" ///
   12.event1 = "2" ///
   13.event1  = "3" ///
   14.event1  = "4" ///
   15.event1  = "5" ///
   16.event1  = "6" ///
   17.event1  = "7" ///
   18.event1  = "8" ///
   19.event1 = "9" ///
   20.event1  = "10" ///
   21.event1  = "11" ///
   22.event1 = "12" ///
   23.event1  = "13" ///
   24.event1 = "14" ///
   25.event1 = "15")  ///更改系数的label
   vertical                             ///转置图形
   yline(0, lwidth(vthin) lpattern(dash) lcolor(teal)) ///加入y=0这条虚线
   ylabel(-0.06(0.02)0.06) ///
   xline(11, lwidth(vthin) lpattern(dash) lcolor(teal)) ///
   ytitle("Percentage Changes", size(small)) ///加入Y轴标题,大小small
   xtitle("Years relative to branch deregulation", size(small)) ///加入X轴标题，大小small
   transform(*=@-r(mean)) ///去除前十期的系数均值，更好看
   addplot(line @b @at  if @at < 11, lcolor(gs0)  || line @b @at if @at>11, lcolor(gs0) lwidth(thin))   ///增加点之间的连线
   ciopts(lpattern(dash) recast(rcap) msize(medium))  ///CI为虚线上下封口
   msymbol(circle_hollow) ///plot空心格式
   scheme(s1mono)

```
![图6](https://images.gitee.com/uploads/images/2019/0831/224509_56e5f94e_2270897.png "推文2_图片6.png")


**五、总结和拓展**

本文的主要内容是，通过模拟的方法将Time-varying DID 方法的估计，平行趋势的假设以及动态政策效果的展示完整地运行了一遍。通过模拟的方法，我们能更加清楚地了解 Time-varying DID 方程设置的逻辑。由于此时政策时点的不统一，这使得 Standard DID 直观上的事前和事后的比较发生分组的困难，也进一步难以利用政策发生前的时点进行对最关键的平行趋势的检验。然而，Time-varying DID 的这些困难恰好给了我们应用者了解 DID 基础假设的机会。这时候，我们需要从每期的变化入手来理解 DID 模型的事前/事后和实验/控制组的区别，即此时我们无法找到一个统一时点来划分事前事后，但是由于对于每一个个体来说，政策发生的时点是确定，因此必然存在对于每一个的个体的事前N期和事后N期对照基准时期所发生的改变，即差分值。举例说明即，虽然没有统一的政策干预前的样本区间，但是都存在政策干预前一期，前两期，前三期等等，政策干预后一期，后二期，后三期等等。因此，这就再次验证了 DID 模型实际上是差分形式的匹配方法，或者说 DID 模型的识别假设是结果变量的差分形式均值独立于政策干预，即在差分意义上满足随机实验的要求。进一步地，如本文模拟的例子，如果在样本的结束时，所有的个体都接受了政策干预，那么在与基准时期差分的意义上，这就可以应用标准的 Event Stduy Approach 的方法来进行估计。因为恰好此时若原来水平值满足平行趋势假设，那么差分值就满足 ESA 方法所得到时间窗口期前的估计系数应该都不显著异于零。



**参考资料**
1. [Beck, T., Levine, R., & Levkov, A. (2010). Big bad banks? The winners and losers from bank deregulation in the United States. The Journal of Finance, 65(5), 1637-1667.](http://sci-hub.tw/https%3A%2F%2Fdoi.org%2F10.1111%2Fj.1540-6261.2010.01589.x)
2. [黄河泉老师的帖子](https://bbs.pinggu.org/thread-5587630-1-1.html)
3. [多期DID：平行趋势检验图示](https://mp.weixin.qq.com/s/xFggRCjawcXiTe8fpzgS6Q)

**相关代码**

情形1： 政策效果不随时间发生变化
```
///设定60个观测值，设定随机数种子
. clear all
. set obs 60 
. set seed 10101
. gen id =_n

/// 每一个数值的数量扩大11倍，再减去前六十个观测值，即60*11-60 = 600，为60个体10年的面板数据
. expand 11
. drop in 1/60
. count

///以id分组生成时间标识
. bys id: gen time = _n+1999
. xtset id time

///生成协变量以及个体和时间效应
. gen x1 = rnormal(1,7)
. gen x2 = rnormal(2,5)

. sort time id
. by time: gen ind = _n
. sort id time

. by id: gen T = _n
. gen y = 0

///生成处理变量,此时D为Dit,设定1-20在2004年接受冲击，21-40为2006年，36-60为2008年
. gen D = 0
. gen birth_date = 0
forvalues i = 1/20{
	replace D = 1 if id  == `i' & time >= 2004
	replace birth_date = 2004 if id == `i'
}

forvalues i = 21/40{
	replace D = 1 if id  == `i' & time >= 2006
	replace birth_date = 2006 if id == `i'
}

forvalues i = 41/60{
	replace D = 1 if id  == `i' & time >= 2008
	replace birth_date = 2008 if id == `i'
}

///Y的生成，使得接受冲击的个体的政策真实效果为10
bysort id: gen y0 = 10  + 5 * x1 + 3 * x2 + T + ind  + rnormal()

bysort id: gen y1 = 10  + 5 * x1 + 3 * x2 + T + ind  + 10 + rnormal() if time >= 2004 & id >= 1 & id <= 20

bysort id: replace y1 = 10  + 5 * x1 + 3 * x2 + T + ind  + 10 + rnormal() if time >= 2006 & id >= 21 & id <= 40

bysort id: replace y1 = 10  + 5 * x1 + 3 * x2 + T + ind  + 10 + rnormal() if time >= 2008 & id >= 41 & id <= 60

bysort id: replace y1 = 10  + 5 * x1 + 3 * x2 + T + ind  + rnormal() if y1 == .

replace y = y0 + D * (y1 - y0)

///去除个体效应和协变量对Y的影响，得到残差并画图
xtreg y x1 x2 , fe r
predict e, ue
binscatter e time, line(connect) by(D)

///多种回归形式及对比展示
reg y c.D x1 x2 i.time i.id, r
eststo reg
xtreg y c.D x1 x2 i.time, r fe
eststo xtreg_fe
areg y c.D x1 x2 i.time, absorb(id) robust
eststo areg
reghdfe y c.D x1 x2, absorb(id time) vce(robust)
eststo reghdfe

estout *, title("The Comparison of Actual Parameter Values") ///
		 cells(b(star fmt(%9.3f)) se(par)) ///
		 stats(N N_g, fmt(%9.0f %9.0g) label(N Groups)) ///
		 legend collabels(none) varlabels(_cons Constant) keep(x1 x2 D)

///Time-varying DID 的 ESA 应用以及coefplot图形化展示
gen event = time - birth_date
replace event = -4 if event <= -4
tab event, gen(eventt)
drop eventt1

xtreg y eventt* x1 x2 i.time, r fe

coefplot, ///
   keep(event*)  ///
   coeflabels(eventt2 = "-3"   ///
   eventt3 = "-2"           ///
   eventt4 = "-1"           ///
   eventt5 = "0"           ///
   eventt6  = "1"             ///
   eventt7  = "2"              ///
   eventt8  = "3"              ///
   eventt9  = "4"              ///
   eventt10 = "5")            ///
   vertical                             ///
   yline(0)                             ///
   ytitle("Y")                 ///
   xtitle("Time passage relative to year of adoption of implied contract exception") ///
   addplot(line @b @at)                 ///
   ciopts(recast(rcap))                 ///
   scheme(s1mono)

```


情形2：政策效果随时间发生变化
```
///设定60个观测值，设定随机数种子
. clear all
. set obs 60 
. set seed 10101
. gen id =_n

/// 每一个数值的数量扩大11倍，再减去前六十个观测值，即60*11-60 = 600，为60个体10年的面板数据
. expand 11
. drop in 1/60
. count

///以id分组生成时间标识
. bys id: gen time = _n+1999
. xtset id time

///生成协变量以及个体和时间效应
. gen x1 = rnormal(1,7)
. gen x2 = rnormal(2,5)

. sort time id
. by time: gen ind = _n
. sort id time

. by id: gen T = _n
. gen y = 0

///生成处理变量,此时D为Dit,设定1-20在2004年接受冲击，21-40为2006年，36-60为2008年
. gen D = 0
. gen birth_date = 0
forvalues i = 1/20{
	replace D = 1 if id  == `i' & time >= 2004
	replace birth_date = 2004 if id == `i'
}

forvalues i = 21/40{
	replace D = 1 if id  == `i' & time >= 2006
	replace birth_date = 2006 if id == `i'
}

forvalues i = 41/60{
	replace D = 1 if id  == `i' & time >= 2008
	replace birth_date = 2008 if id == `i'
}

///Y的生成，设定真实的政策效果为当年为3，并且每年增加3
bysort id: gen y0 = 10  + 5 * x1 + 3 * x2 + T + ind + rnormal()

bysort id: gen y1 = 10  + 5 * x1 + 3 * x2 + T + ind  + (time - birth + 1 ) * 3 + rnormal() if time >= 2004 & id >= 1 & id <= 20

bysort id: replace y1 = 10  + 5 * x1 + 3 * x2 +  T + ind  + (time - birth + 1 ) * 3  + rnormal() if time >= 2006 & id >= 21 & id <= 40

bysort id: replace y1 = 10  + 5 * x1 + 3 * x2 +  T + ind  + (time - birth + 1 ) * 3  + rnormal() if time >= 2008 & id >= 41 & id <= 60

bysort id: replace y1 = 10  + 5 * x1 + 3 * x2 +  T + ind  + rnormal() if y1 == .

replace y = y0 + D * (y1 - y0)

///去除个体效应和协变量对Y的影响，得到残差并画图
xtreg y x1 x2 , fe r
predict e, ue
binscatter e time, line(connect) by(D)

///多种回归形式及对比展示
reg y c.D x1 x2 i.time i.id, r
eststo reg
xtreg y c.D x1 x2 i.time, r fe
eststo xtreg_fe
areg y c.D x1 x2 i.time, absorb(id) robust
eststo areg
reghdfe y c.D x1 x2, absorb(id time) vce(robust)
eststo reghdfe

estout *, title("The Comparison of Actual Parameter Values") ///
		 cells(b(star fmt(%9.3f)) se(par)) ///
		 stats(N N_g, fmt(%9.0f %9.0g) label(N Groups)) ///
		 legend collabels(none) varlabels(_cons Constant) keep(x1 x2 D)

///Time-varying DID 的 ESA 应用以及coefplot图形化展示
gen event = time - birth_date
replace event = -4 if event <= -4
tab event, gen(eventt)
drop eventt1

xtreg y eventt* x1 x2 i.time, r fe

coefplot, ///
   keep(event*)  ///
   coeflabels(eventt2 = "-3"   ///
   eventt3 = "-2"           ///
   eventt4 = "-1"           ///
   eventt5 = "0"           ///
   eventt6  = "1"             ///
   eventt7  = "2"              ///
   eventt8  = "3"              ///
   eventt9  = "4"              ///
   eventt10 = "5")            ///
   vertical                             ///
   yline(0)                             ///
   ytitle("Y")                 ///
   xtitle("Time passage relative to year of adoption of implied contract exception") ///
   addplot(line @b @at)                 ///
   ciopts(recast(rcap))                 ///
   scheme(s1mono)

```

重现Figure 3的代码
```
///本文并没有提供 Beck, Levine & Levkov(2010) 原文的代码和数据集，本部分的运行结果仅做生成图形对比。感兴趣的同学可以去引言中提到的经管之家黄河泉老师的帖子下载相关内容进行操作。

xtset statefip wrkyr
gen bb = 0
gen aa = 11
gen event = wrkyr - branch_reform

replace event  = -10 if event <= -10
replace event = 15 if event >= 15

///tab event, gen(eventt)
gen event1 = event + 10
gen y = log(gini)

qui xtreg y ib10.event1 i.wrkyr, fe r 

///生成前十期系数均值x
forvalues i = 0/9{
	gen b_`i' = _b[`i'.event1]

}

gen avg_coef = (b_0+b_9+b_8+b_7+b_6+b_5+b_4+b_3+b_2+b_1)/10
su avg_coef 
return list

coefplot, baselevels ///
   drop(_cons event1  *.wrkyr ) ///
   if(@at!=11) ///去除当年的系数点
   coeflabels(0.event1 = "-10"   ///
   1.event1 = "-9"           ///
   2.event1 = "-8"           ///
   3.event1 = "-7"           ///
   4.event1  = "-6"             ///
   5.event1  = "-5"              ///
   6.event1  = "-4"              ///
   7.event1  = "-3"       ///
   8.event1  = "-2" ///
   9.event1  = "-1" ///
   10.event1 = "0" ///
   11.event1  = "1" ///
   12.event1 = "2" ///
   13.event1  = "3" ///
   14.event1  = "4" ///
   15.event1  = "5" ///
   16.event1  = "6" ///
   17.event1  = "7" ///
   18.event1  = "8" ///
   19.event1 = "9" ///
   20.event1  = "10" ///
   21.event1  = "11" ///
   22.event1 = "12" ///
   23.event1  = "13" ///
   24.event1 = "14" ///
   25.event1 = "15")  ///更改系数的label
   vertical                             ///转置图形
   yline(0, lwidth(vthin) lpattern(dash) lcolor(teal)) ///加入y=0这条虚线
   ylabel(-0.06(0.02)0.06) ///
   xline(11, lwidth(vthin) lpattern(dash) lcolor(teal)) ///
   ytitle("Percentage Changes", size(small)) ///加入Y轴标题,大小small
   xtitle("Years relative to branch deregulation", size(small)) ///加入X轴标题，大小small
   transform(*=@-r(mean)) ///去除前十期的系数均值，更好看
   addplot(line @b @at  if @at < 11, lcolor(gs0)  || line @b @at if @at>11, lcolor(gs0) lwidth(thin))   ///增加点之间的连线
   ciopts(lpattern(dash) recast(rcap) msize(medium))  ///CI为虚线上下封口
   msymbol(circle_hollow) ///plot空心格式
   scheme(s1mono)
```
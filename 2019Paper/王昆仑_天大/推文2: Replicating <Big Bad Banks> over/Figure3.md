*++++++++++++++++++++++++++++++++++++++++++++

*Authors: Beck, T., Levine, R., & Levkov, A. (2010). 

*Paper: Big bad banks? The winners and losers from bank deregulation in the United States. 

*Journal: The Journal of Finance, 65(5), 1637-1667.

*Figure3 The Dynamic Impact of Deregulation on the Gini Coefficient of Income Inequality

*++++++++++++++++++++++++++++++++++++++++++++
```
set more off
clear all
use "E:\论文\双重差分法\bbb\macro_workfile.dta"
xtset statefip wrkyr

gen event = wrkyr - branch_reform

replace event  = -10 if event <= -10
replace event = 15 if event >= 15

tab event, gen(eventt)
drop eventt11
gen y = log(gini)

xtreg y eventt* i.wrkyr, fe i(statefip) robust cluster(statefip) 
xtreg y eventt* i.wrkyr, fe r

///生成前十期系数均值x
forvalues i = 1/10{
	gen b_`i' = _b[eventt`i']

}
gen avg_coef = (b_10+b_9+b_8+b_7+b_6+b_5+b_4+b_3+b_2+b_1)/10
su avg_coef
return list

coefplot, ///
   keep(eventt*)  ///
   coeflabels(eventt1 = "-10"   ///
   eventt2 = "-9"           ///
   eventt3 = "-8"           ///
   eventt4 = "-7"           ///
   eventt5  = "-6"             ///
   eventt6  = "-5"              ///
   eventt7  = "-4"              ///
   eventt8  = "-3"       ///
   eventt9  = "-2" ///
   eventt10  = "-1" ///
   eventt12  = "1" ///
   eventt13 = "2" ///
   eventt14  = "3" ///
   eventt15  = "4" ///
   eventt16  = "5" ///
   eventt17  = "6" ///
   eventt18  = "7" ///
   eventt19  = "8" ///
   eventt20 = "9" ///
   eventt21  = "10" ///
   eventt22  = "11" ///
   eventt23 = "12" ///
   eventt24  = "13" ///
   eventt25 = "14" ///
   eventt26 = "15")  ///更改系数的label
   vertical                             ///转置图形
   yline(0, lwidth(vthin) lpattern(dash) lcolor(teal)) ///加入y=0这条虚线
   ylabel(-0.06(0.02)0.06) ///
   xline(10.5, lwidth(vthin) lpattern(dash) lcolor(teal)) ///
   ytitle("Percentage Changes", size(small)) ///加入Y轴标题,大小small
   xtitle("Years relative to branch deregulation", size(small)) ///加入X轴标题，大小small
   transform(*=@-r(mean)) ///去除前十期的系数均值，更好看
   addplot(line @b @at)                 ///增加点之间的连线
   ciopts(lpattern(dash) recast(rcap) msize(medium))  ///CI为虚线上下封口
   msymbol(circle_hollow) ///plot空心格式
   scheme(s1mono)
```
========================================================

========================================================

========================================================


///第二种实现方式，利用类别变量的baselevels,可以将实施当年的政策效果在图上画出，但是没有其CI
*++++++++++++++++++++++++++++++++++++++++++++

*Authors: Beck, T., Levine, R., & Levkov, A. (2010).
 
*Paper: Big bad banks? The winners and losers from bank deregulation in the United States. 

*Journal: The Journal of Finance, 65(5), 1637-1667.

*Figure3 The Dynamic Impact of Deregulation on the Gini Coefficient of Income Inequality

*++++++++++++++++++++++++++++++++++++++++++++

```
set more off
clear all
use "E:\论文\双重差分法\bbb\macro_workfile.dta"
xtset statefip wrkyr

gen event = wrkyr - branch_reform

replace event  = -10 if event <= -10
replace event = 15 if event >= 15

gen event1 = event + 10
gen y = log(gini)

xtreg y ib10.event1 i.wrkyr, fe r 

///生成前十期系数均值x
forvalues i = 0/9{
	gen b_`i' = _b[`i'.event1]

}

gen avg_coef = (b_0+b_9+b_8+b_7+b_6+b_5+b_4+b_3+b_2+b_1)/10
su avg_coef 

coefplot, baselevels ///
   drop(*.wrkyr _cons event1) ///
   coeflabels(0.event1 = "-10"   ///
   1.event1 = "-9"           ///
   2.event1 = "-8"           ///
   3.event1 = "-7"           ///
   4.event1  = "-6"             ///
   5.event1  = "-5"              ///
   6.event1  = "-4"              ///
   7.event1  = "-3"       ///
   8.event1  = "-2" ///
   9.event1  = "-1" ///
   10.event1 = "0" ///
   11.event1  = "1" ///
   12.event1 = "2" ///
   13.event1  = "3" ///
   14.event1  = "4" ///
   15.event1  = "5" ///
   16.event1  = "6" ///
   17.event1  = "7" ///
   18.event1  = "8" ///
   19.event1 = "9" ///
   20.event1  = "10" ///
   21.event1  = "11" ///
   22.event1 = "12" ///
   23.event1  = "13" ///
   24.event1 = "14" ///
   25.event1 = "15")  ///更改系数的label
   vertical                             ///转置图形
   yline(0, lwidth(vthin) lpattern(dash) lcolor(teal)) ///加入y=0这条虚线
   ylabel(-0.06(0.02)0.06) ///
   xline(11, lwidth(vthin) lpattern(dash) lcolor(teal)) ///
   ytitle("Percentage Changes", size(small)) ///加入Y轴标题,大小small
   xtitle("Years relative to branch deregulation", size(small)) ///加入X轴标题，大小small
   transform(*=@-r(mean)) ///去除前十期的系数均值，更好看
   addplot(line @b @at)                 ///增加点之间的连线
   ciopts(lpattern(dash) recast(rcap) msize(medium))  ///CI为虚线上下封口
   msymbol(circle_hollow) ///plot空心格式
   scheme(s1mono)
```

+++++++第三种作图的方法，也是最接近原图的一种

```
su avg_coef 

coefplot, baselevels ///
   drop(_cons event1  *.wrkyr ) ///
   if(@at!=11) ///
   coeflabels(0.event1 = "-10"   ///
   1.event1 = "-9"           ///
   2.event1 = "-8"           ///
   3.event1 = "-7"           ///
   4.event1  = "-6"             ///
   5.event1  = "-5"              ///
   6.event1  = "-4"              ///
   7.event1  = "-3"       ///
   8.event1  = "-2" ///
   9.event1  = "-1" ///
   10.event1 = "0" ///
   11.event1  = "1" ///
   12.event1 = "2" ///
   13.event1  = "3" ///
   14.event1  = "4" ///
   15.event1  = "5" ///
   16.event1  = "6" ///
   17.event1  = "7" ///
   18.event1  = "8" ///
   19.event1 = "9" ///
   20.event1  = "10" ///
   21.event1  = "11" ///
   22.event1 = "12" ///
   23.event1  = "13" ///
   24.event1 = "14" ///
   25.event1 = "15")  ///更改系数的label
   vertical                             ///转置图形
   yline(0, lwidth(vthin) lpattern(dash) lcolor(teal)) ///加入y=0这条虚线
   ylabel(-0.06(0.02)0.06) ///
   xline(11, lwidth(vthin) lpattern(dash) lcolor(teal)) ///
   ytitle("Percentage Changes", size(small)) ///加入Y轴标题,大小small
   xtitle("Years relative to branch deregulation", size(small)) ///加入X轴标题，大小small
   transform(*=@-r(mean)) ///去除前十期的系数均值，更好看
   addplot(line @b @at  if @at < 11, lcolor(gs0)  || line @b @at if @at>11, lcolor(gs0) lwidth(thin))     ///增加点之间的连线
   ciopts(lpattern(dash) recast(rcap) msize(medium))  ///CI为虚线上下封口
   msymbol(circle_hollow) ///plot空心格式
   scheme(s1mono)

```


&emsp;

> 作者：徐萌萌 (厦门大学)      
>     &emsp;     
> Stata 连享会： [知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn) | [CSDN](https://blog.csdn.net/arlionn)

&emsp;
[![点击查看完整推文列表](https://images.gitee.com/uploads/images/2019/0918/113702_87a12f7a_1522177.jpeg)](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw)

> Stata连享会 &emsp; [计量专题](https://gitee.com/arlionn/Course/blob/master/README.md)  || [精品课程](https://mp.weixin.qq.com/s/hWtncj56PeFNL4yg2-va0Q) || [简书推文](https://www.jianshu.com/p/de82fdc2c18a)  || [公众号合集](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw)








&emsp;


  

&emsp;
Roland Fryer 曾说过 **「数据指向哪，我就去哪，从不犹豫」**。

在这里给大家总结一些常用数据集的获取方式，希望可以提高大家的科研工作效率。


---
## 教科书范例数据集
-  [Introductory Econometrics: A Modern Approach (1st and 2d eds.) by Jeffrey Wooldridge](http://fmwww.bc.edu/gstat/examples/wooldridge/wooldridge.html)
- [Econometric Analysis of Cross Section and Panel Data by Jeffrey Wooldridge]( https://www.stata.com/texts/eacsap/) 

## 经济数据
- [DataHub]() >> [Collections](https://datahub.io/collections) A collection of economic indicators available on DataHub
-  [The Economics Network](https://www.economicsnetwork.ac.uk/data_sets) 汇集了全球各类经济学方面的数据，偏宏观
-  [World Bank Open Data]( https://data.worldbank.org.cn/) 免费并公开获取世界各国的发展数据
-  [OECD]( http://stats.oecd.org/) 涵盖经合组织国家的相关统计信息
-  [World Population Prospects]( http://esa.un.org/unpd/wpp/Excel-Data/population.htm) 涵盖各国家和地区的关键人口指标
-  [Human Development Reports]( http://hdr.undp.org/en) 联合国开发计划署发布的人类发展报告
-  [Doing Business]( http://www.doingbusiness.org/) 世界银行集团营商环境报

## 商业数据
-  [CEIC]( https://www.ceicdata.com/zh-hans) 涵盖全球超过195个国家的宏观经济数据
-  [Choice]( http://choice.eastmoney.com/) 提供基础信息、公告、财务数据等金融数据
-  [Wind]( https://www.wind.com.cn/newsite/edb.html) 整合全球宏观和行业统计数据
-  [GTA]( http://www.gtarsc.com/Home) 一个公司全局性的金融数据库，具有数据关联性和实时发布机制

## 问卷调查数据集

- Sebastian Bauhoff,  [Household datasets for development economic research](https://scholar.harvard.edu/bauhoff/datalinks.html), 提供了各类家庭调查类数据库的链接
#### [panelwhiz](http://www.panelwhiz.eu/index.html)
涵盖了美国、澳大利亚、英国、新西兰、俄罗斯、韩国等国家的居民收入、医疗、人口等社会经济家庭面板数据集。可以根据提示安装 `pwupdate.ado` 命令 ([点击下载](http://www.panelwhiz.eu/pwupdate.ado))，以自动下载和更新这些数据集。所有数据均可免费下载。
*    Australia
 [HILDA](https://melbourneinstitute.unimelb.edu.au/hilda): Household Income Labour Dynamics of Australia   |   [MABEL](https://melbourneinstitute.unimelb.edu.au/mabel/home): Medicine in Australia   |   [LSAC](https://growingupinaustralia.gov.au/): Longitudinal Survey of Australian Children   |   [LSIC](https://www.dss.gov.au/about-the-department/publications-articles/research-publications/longitudinal-data-initiatives/footprints-in-time-the-longitudinal-study-of-indigenous-children-lsic): Longiduinal Survey of Indigenous Children   |   [BNLA](http://www3.aifs.gov.au/bnla/): Building a New Life in Austrlia   |   [ABS-SIH](http://meteor.aihw.gov.au/content/index.phtml/itemId/657468): ABS Survey of Income and Housing
*  United States
 [CPS-NBER](http://www.nber.org/cps/):Current Population Survey (CPS)   |   [RAND-HRS](https://www.rand.org/labor/aging/dataprod/hrs-data.html): Health and Retirement Study (HRS)   |   [SIPP-NBER](http://www.nber.org/data/survey-of-income-and-program-participation-sipp-data.html): Survey of Income and Program Participation (SIPP)
*  United Kingdom
[BHPS](https://www.iser.essex.ac.uk/bhps): British Household Panel Survey   |   [UKUS](https://www.understandingsociety.ac.uk/): UK Understanding Society
* Netherlands
[DHS](https://www.centerdata.nl/en/databank/dhs-data-access): DNB Household Survey   |   [LISS](https://www.surveydata.nl/liss-panel-data-archive): Longitudinal Internet Studies for the Social sciences
* Russia
[RLMS](http://www.cpc.unc.edu/projects/rlms-hse): HSE Russia Longitudinal Monitoring Survey
* South Korea
[KLIPS](https://www.kli.re.kr/klips_eng/index.do): Korean Labor and Income Panel Study

## 高校维护的数据库
-  [UCLA Data Archives Home](http://www.sscnet.ucla.edu/issr/da/)：Holds some older surveys, often US political polls, election studies and attitude data
-  [CHFS](https://chfs.swufe.edu.cn/)：由西南财经大学中国家庭金融调查与研究中心建立的一个具有全国代表性的家庭层面的金融数据库
-  [CFPS](http://www.isss.edu.cn/cfps/)：北京大学中国社会科学调查中心建立的中国家庭追踪调查
-  [CHIPS](http://www.ciidbnu.org/index.asp)：北京师范大学中国收入分配研究院建立的中国家庭收入调查
-  [CHARLS](http://charls.pku.edu.cn/zh-CN)：北京大学国家发展研究院建立的中国健康与养老追踪调查
-  [CGSS](http://www.chinagss.org/)：中国人民大学社会学系、香港科技大学社会科学部建立的中国综合社会调查
-  [CLDS](http://css.sysu.edu.cn/data/list)：中山大学社会科学调查中心建立的中国劳动力动态调查
-  [CRS](http://crs.ruc.edu.cn/)：中国人民大学哲学院与中国人民大学中国调查与数据中心建立的中国宗教调查
&emsp;
> #### [连享会计量方法专题……](https://gitee.com/arlionn/Course/blob/master/README.md)

## 中国数据专题
-  [国家统计局普查数据]( http://www.stats.gov.cn/tjsj/pcsj/)
-  [全国流动人口动态监测调查数据]( http://www.chinaldrk.org.cn/data/)
-  [世界银行中国企业调查数据]( http://www.enterprisesurveys.org/data)
-  [中国专利数据库]( http://new.ccerdata.cn/Home/Special#h3)
-  [中国工业企业数据库]( http://www.allmyinfo.com/data/zggyqysjk.asp)
-  [中国海关数据]( http://new.ccerdata.cn/Home/Special)

### 相关链接
- [DataHub](https://datahub.io/) 
- [美国政府公开数据网](https://catalog.data.gov/dataset)
- [IMF Data](https://data.imf.org/datasets) 国际货币基金组织
- [Economy - OECD Data](https://data.oecd.org/economy.htm)
- [Yale Law School - 75 Dataset Links](https://library.law.yale.edu/news/75-sources-economic-data-statistics-reports-and-commentary)
- [Agricultural and Economic - Catalog Data Gov](https://catalog.data.gov/dataset?tags=agricultural-economics) 
- [Harvard Dataderve]( https://dataverse.harvard.edu/) 



> #### [连享会：内生性问题及估计方法专题](https://zhuanlan.zhihu.com/p/81310667)

[![连享会-内生性专题现场班-2019.11.14-17](https://images.gitee.com/uploads/images/2019/0918/113702_c59d48bf_1522177.png)](https://zhuanlan.zhihu.com/p/81310667)

&emsp;

>#### 关于我们

- **「Stata 连享会」** 由中山大学连玉君老师团队创办，定期分享实证分析经验， 公众号：**StataChina**。
- 公众号推文同步发布于 [CSDN](https://blog.csdn.net/arlionn) 、[简书](http://www.jianshu.com/u/69a30474ef33) 和 [知乎Stata专栏](https://www.zhihu.com/people/arlionn)。可在百度中搜索关键词 「Stata连享会」查看往期推文。
- 点击推文底部【阅读原文】可以查看推文中的链接并下载相关资料。
- **欢迎赐稿：** 欢迎赐稿。录用稿件达 **三篇** 以上，即可 **免费** 获得一期 Stata 现场培训资格。
- **E-mail：** StataChina@163.com
- 往期推文：[计量专题](https://gitee.com/arlionn/Course/blob/master/README.md)  || [精品课程](https://mp.weixin.qq.com/s/hWtncj56PeFNL4yg2-va0Q) || [简书推文](https://www.jianshu.com/p/de82fdc2c18a)  || [公众号合集](https://mp.weixin.qq.com/s/AWxDPvTuIrBdf6TMUzAhWw) 
[![点击此处-查看完整推文列表](https://images.gitee.com/uploads/images/2019/0918/113702_23288753_1522177.png)](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw)

---
![在这里插入图片描述](https://images.gitee.com/uploads/images/2019/0918/113702_3838bb59_1522177.png)

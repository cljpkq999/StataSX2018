[![连享会-空间计量现场班 (2019年6月27-30日，西安)](https://gitee.com/uploads/images/2019/0426/092109_587bfe19_1522177.jpeg "连享会-空间计量现场班 (2019年6月27-30日，西安)")](https://gitee.com/arlionn/Course/blob/master/Spatial.md)

&emsp;

> **Source:** [Rizaudin Sahlan](http://rizaudinsahlan.blogspot.com/) → [Impulse Response Function with Stata (time series)](http://rizaudinsahlan.blogspot.com/2017/08/)

> 编译：许梦洁 (中山大学)      
>     
> Stata 连享会： [知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn)

> Stata连享会时间序列专题：
>*   [Stata: VAR - 模拟、估计和推断](https://www.jianshu.com/p/d9ac957af894)
>*   [Stata: VAR (向量自回归) 模型简介](https://www.jianshu.com/p/4a72c8f594f6)
>*   [Stata: 单位根检验就这么轻松](https://www.jianshu.com/p/ac9ffc716ff9)
>*   [Stata: 协整还是伪回归?](https://www.jianshu.com/p/edde2a4ca653)
>*   [协整：醉汉牵着一条狗](https://links.jianshu.com/go?to=https%3A%2F%2Flink.zhihu.com%2F%3Ftarget%3Dhttp%253A%2F%2Fwww.jianshu.com%2Fp%2F08e194e14b7a)


&emsp;

在这篇推文中，我们讨论 VAR 模型中的脉冲响应函数 (IRFs)。  

### 模型推导

脉冲响应函数反映了当 VAR 模型某个变量受到"外生冲击"时，模型中其他变量受到的动态影响。我们会根据这些变量受到此冲击后的一段时间内的动态变化画出脉冲响应图形。

脉冲响应函数是一种条件预测，更确切地说，是一种点估计，只不过我们会估计冲击发生后不同时点的值。

类似于 AR 模型有 MA 表达形式，VAR 模型也有 VMA 表达形式。 VMA 表达形式有助于我们探求在 VAR 系统中变量收到冲击后的随时间变化的路径。考虑一个由 $y_t$ 和 $z_t$ 构成的VAR 系统：

$$
\left[ \begin{array} { c } { y _ { t } } \\ { z _ { t } } \end{array} \right] = \left[ \begin{array} { c } { a _ { 10 } } \\ { a _ { 20 } } \end{array} \right] + \left[ \begin{array} { c c } { a _ { 11 } } & { a _ { 12 } } \\ { a _ { 21 } } & { a _ { 22 } } \end{array} \right] \left[ \begin{array} { c } { y _ { t - 1 } } \\ { z _ { t - 1 } } \end{array} \right] + \left[ \begin{array} { c } { e _ { 1 t } } \\ { e _ { 2 t } } \end{array} \right]\tag{1}
$$

该模型也可以写成如下形式：

$$
\left[ \begin{array} { c } { y _ { t } } \\ { z _ { t } } \end{array} \right] = \left[ \begin{array} { c } { \overline { y } } \\ { \overline { z } } \end{array} \right] + \sum _ { i = 0 } ^ { \infty } \left[ \begin{array} { c c } { a _ { 11 } } & { a _ { 12 } } \\ { a _ { 21 } } & { a _ { 22 } } \end{array} \right] ^ { i } \left[ \begin{array} { c } { e _ { 1 t - i } } \\ { e _ { 2 t - i } } \end{array} \right] \quad \tag{2}
$$

方程 (2) 使用 {$e_{ 1 t }$} 和 {$e_{ 2 t }$} 序列表示了 $y_t$ 和 $z_t$ 。根据 [Enders(2014, p286)](https://gitee.com/xuswufe/twitter_references/blob/master/Applied_Econometric_Time_Series_4e.pdf)，{$e_{ 1 t }$} 和 {$e_{ 2 t }$} 可以写成：

$$
\left[ \begin{array} { c } { e _ { 1 t } } \\ { e _ { 2 t } } \end{array} \right] =  \frac{1}{ 1 - b _ { 12 } b _ { 21 }}  \left[ \begin{array} { c c } { 1 } & { - b _ { 12 } } \\ { - b _ { 21 } } & { 1 } \end{array} \right] \left[ \begin{array} { c } { \varepsilon _ { y t } } \\ { \varepsilon _ { zt } } \end{array} \right]\tag{3}
$$

将 (3) 式代入 (2) 式中可得：

$$
\left[ \begin{array} { c } { y _ { t } } \\ { z _ { t } } \end{array} \right] = \left[ \begin{array} { c } { \overline { y } } \\ { \overline { z } } \end{array} \right]+ \frac{1}{ 1 - b _ { 12 } b _ { 21 }}  \sum _ { i = 0 } ^ { \infty } \left[ \begin{array} { c c } { a _ { 11 } } & { a _ { 12 } } \\ { a _ { 21 } } & { a _ { 22 } } \end{array} \right] ^ { i } \left[ \begin{array} { c c } { 1 } & { - b _ { 12 } } \\ { - b _ { 21 } } & { 1 } \end{array} \right] \left[ \begin{array} { c } { \varepsilon _ { y t } } \\ { \varepsilon _ { z t } } \end{array} \right] \tag { 4 }
$$

由于 (4) 式不太简洁，我们可以通过定义一个 $2\times2$ 的矩阵 $\phi_i$ 来简化它：

$$
\phi _ { i } =  \frac{A _ { 1 } ^ { i } }{ 1 - b _ { 12 } b _ { 21 }  } \left[ \begin{array} { c c } { 1 } & { - b _ { 12 } } \\ { - b _ { 21 } } & { 1 } \end{array} \right]\tag{5}
$$

将 (5) 式代入 (4) 式可得：

$$
\left[ \begin{array} { l } { y _ { t } } \\ { z _ { t } } \end{array} \right] = \left[ \begin{array} { c } { \overline { y } } \\ { \overline { z } } \end{array} \right] + \sum _ { i = 0 } ^ { \infty } \left[ \begin{array} { c c } { \phi _ { 11 } ( i ) } & { \phi _ { 12 } ( i ) } \\ { \phi _ { 21 } ( i ) } & { \phi _ { 22 } ( i ) } \end{array} \right] ^ { i } \left[ \begin{array} { c } { \varepsilon _ { y t - i } } \\ { \varepsilon _ { zt - i } } \end{array} \right]\tag{6}
$$

显然 (6) 式可以写成更简洁的形式：

$$
x _ { t } = \mu + \sum _ { i = 0 } ^ { \infty } \phi _ { i } \varepsilon _ { t - i }\tag{7}
$$

以上 VMA 形式的表出对于我们理解  $y_t$ 和 $z_t$ 构成的VAR 系统格外有帮助。其中系数矩阵 $\phi_i$ 即为来自 $\varepsilon_{yt}$ 和  $\varepsilon_{zt}$ 的冲击对 $y_t$ 和 $z_t$ 序列的影响。

很显然 $\phi_{jk} (0)$ 是冲击的即时影响。比如，$\phi_{12} (0)$ 表示 1 个单位  $\varepsilon_{zt}$ 的冲击会使当期的  $y_t$ 变化多少个单位。类似地，$\phi_{11} (1)$ 和 $\phi_{12} (1)$ 则分别表示来自一个单位  $\varepsilon_{yt-1}$ 和一个单位   $\varepsilon_{zt-1}$ 的冲击对  $y_t$ 产生的影响。

$\phi_{11} (1)$ 和 $\phi_{12} (1)$ 也可以表示来自一个单位  $\varepsilon_{yt}$ 和  $\varepsilon_{zt}$ 的冲击对  $y_{t+1}$ 产生的影响。以此类推，来自一个单位  $\varepsilon_{yt}$ 和  $\varepsilon_{zt}$ 的冲击对后续各期的  $y_t$ 和 $z_t$ 产生的影响可以通过对脉冲响应函数进行适当的汇总得到。比如来自一个单位  $\varepsilon_{zt}$ 的冲击对 {$y_{t}$} 序列在后续第 $n$ 期产生的影响即为 $\phi_{12} (n)$ ，则在此  $\varepsilon_{zt}$ 冲击对 $y_{t}$ 序列在冲击发生后的 $n$ 期内产生的总影响为：

$$
\sum _ { i = 0 } ^ { n } \phi _ { 12 } ( n )
$$

令 $n$ 趋于无穷，可以得到冲击对  $y_t$ 和 $z_t$ 产生的长期影响。由于 {$y_{t}$} 和 {$z_{t}$} 被假定为是平稳的，易得对于任意 $j$  和 $k$ ：

$$
\sum _ { i = 0 } ^ { \infty } \phi _ { j k } ^ { 2 } ( i )\text{ 在 $i\rightarrow \infty$ 时收敛}
$$

四个系数集 $\phi_ { 11 } ( i )$，$\phi _ { 12 } ( i )$，$\phi _ { 21 } ( i )$ 和 $\phi _ { 22 } ( i )$ 被称为脉冲响应函数。绘制脉冲响应函数的图形是观察冲击  $\varepsilon_{yt}$ 和  $\varepsilon_{zt}$ 对系统内变量  $y_t$ 和 $z_t$ 影响的实用手段，也是观察来自  $\varepsilon_{yt}$ 和  $\varepsilon_{zt}$ 冲击造成的时变影响的有力工具。

### 使用 Stata 估计脉冲响应函数

在 `Stata` 中， 我们可以使用 **irf create** 命令得到脉冲响应函数，这个命令可以估计五种脉冲响应函数(IRFs)：简单脉冲响应函数(simple IRFs)、正交脉冲响应函数(orthogonalized IRFs)、累积脉冲响应函数(cumulative IRFs)、累积正交脉冲响应函数(cumulative orthogonalized IRFs)以及结构脉冲响应函数(structural IRFs)。

具体的操作思路为：首先拟合 VAR 模型，然后使用 **irf create** 命令估计脉冲响应函数并将其存储到文件中，最后使用 **irf graph** 或其他的 **irf** 分析命令检验结果。

本篇推文使用的数据集为 **Data09.dta**，可以点击[这里](<https://gitee.com/xuswufe/twitter_references/blob/master/Data09.dta>)下载。

我们想估计的 VAR 模型中包含的变量有：对数固定资本形成总额 **(lrgrossinv)** ，对数实际家庭消费支出额 **(lrconsump)** 以及对数 GDP (**lrgdp**)。

在估计脉冲响应函数之前，我们首先需要知道该 VAR 模型的最优滞后阶数：

```stata
 varsoc lrgrossinv lrconsump lrgdp, max (12)

   Selection-order criteria
   Sample:  1962q1 - 2010q3                     Number of obs      =       195
  +---------------------------------------------------------------------------+
  |lag |    LL      LR      df    p      FPE       AIC      HQIC      SBIC    |
  |----+----------------------------------------------------------------------|
  |  0 |  721.692                      1.3e-07   -7.3712  -7.35081  -7.32085  |
  |  1 |  1979.02  2514.7    9  0.000  3.5e-13  -20.1746   -20.093  -19.9731  |
  |  2 |  2022.28  86.525    9  0.000  2.4e-13   -20.526  -20.3833* -20.1735* |
  |  3 |  2030.07  15.571    9  0.076  2.5e-13  -20.5135  -20.3096    -20.01  |
  |  4 |  2036.31  12.492    9  0.187  2.5e-13  -20.4853  -20.2202  -19.8307  |
  |  5 |  2044.65  16.669    9  0.054  2.6e-13  -20.4784  -20.1522  -19.6728  |
  |  6 |  2056.46  23.622    9  0.005  2.5e-13  -20.5073  -20.1199  -19.5505  |
  |  7 |  2070.89  28.858*   9  0.001  2.4e-13*  -20.563* -20.1144  -19.4552  |
  |  8 |  2074.97  8.1629    9  0.518  2.5e-13  -20.5125  -20.0028  -19.2537  |
  |  9 |  2078.94  7.9406    9  0.540  2.6e-13  -20.4609  -19.8901   -19.051  |
  | 10 |  2083.77  9.6582    9  0.379  2.7e-13  -20.4181  -19.7861  -18.8572  |
  | 11 |  2088.52  9.5076    9  0.392  2.9e-13  -20.3746  -19.6814  -18.6626  |
  | 12 |     2094  10.959    9  0.279  3.0e-13  -20.3385  -19.5841  -18.4754  |
  +---------------------------------------------------------------------------+
   Endogenous:  lrgrossinv lrconsump lrgdp
    Exogenous:  _cons
```

以上结果显示，根据 AIC 准则，模型的最优滞后阶数为 7 阶。使用 7 阶滞后重新估计 VAR 模型：

```stata
quietly var lrgrossinv lrconsump lrgdp,lags(1/7)dfk small
```

并基于估计结果估计脉冲响应函数：

```stata
 irf create order1, step(10) set(myirf1) replace
(file myirf1.irf created)
(file myirf1.irf now active)
irfname order1 not found in myirf1.irf
(file myirf1.irf updated)
```

在 `Stata` 中，多个脉冲响应的结果可以被存储在同一个文件中，并被标记为不同的名字。上述命令表示，此次脉冲响应估计的结果被存储在 **myirf1** 文件中，其名字被标记为 **order1**。**order1** 中存储了前面提到的该 VAR 模型的全部五种脉冲响应函数的估计结果。

得到了脉冲响应函数的估计结果后，我么就可以使用 **irf graph** 命令来绘制秒冲响应图形了：

```stata
irf graph oirf, impulse(lrgrossinv lrconsump lrgdp) response(lrgrossinv lrconsump lrgdp)  yline (0,lcolor(black)) byopts(yrescale)
```

![脉冲响应函数](https://gitee.com/uploads/images/2019/0425/213857_c52fbd4a_2272981.png)

脉冲响应图形中，每一行为同种冲击对不同变量造成的影响，每一列为不同冲击对同一变量造成的影响。

横坐标的刻度单位为 VAR 模型估计的单位时间(在这个案例中为每一季度)。这张图形中显示的是冲击在 10 个季度内造成的影响。纵坐标以每个变量自己的单位来衡量，由于我们估计的模型所有变量的单位均为百分比，因此纵坐标表示百分比的变化。

第一行展示了家庭消费 **(lrconsump)** 受到一个单位标准差的冲击对 VAR 系统造成的影响：家庭消费 **(lrconsump)** 会瞬间全部吸收冲击，并增加相应单位的百分比，此影响甚至到 10 个季度之后都没有消退。GDP (**lrgdp**) 受到该冲击的影响会在4个季度内有所上升，在第四个季度达到峰值，随后该影响慢慢衰退。固定资本形成额 **(lrconsump)** 与 GDP 呈现相似的变化。

第二行展示了GDP (**lrgdp**) 受到一个单位标准差的冲击对 VAR 系统造成的影响：家庭消费 **(lrconsump)** 和固定资本形成额 **(lrconsump)** 都在前 5 个季度轻微下降，之后回升。

第三行展示了固定资本形成额 **(lrconsump)** 受到一个标准差的冲击对 VAR 系统造成的影响：家庭消费 **(lrconsump)** 和 GDP (**lrgdp**) 都出现了高度持续的下降。


&emsp;

[![连享会-Python爬虫与文本分析现场班 (2019年5月17-19日，太原)](http://upload-images.jianshu.io/upload_images/7692714-fe9847853cf613fe.jpeg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "连享会-Python爬虫与文本分析现场班 (2019年5月17-19日，太原)")](https://gitee.com/arlionn/Course/blob/master/Python_text.md)

>#### 关于我们

- 【**Stata 连享会(公众号：StataChina)**】由中山大学连玉君老师团队创办，旨在定期与大家分享 Stata 应用的各种经验和技巧。
- 公众号推文同步发布于 [CSDN-Stata连享会](https://blog.csdn.net/arlionn) 、[简书-Stata连享会](http://www.jianshu.com/u/69a30474ef33) 和 [知乎-连玉君Stata专栏](https://www.zhihu.com/people/arlionn)。可以在上述网站中搜索关键词`Stata`或`Stata连享会`后关注我们。
- 点击推文底部【阅读原文】可以查看推文中的链接并下载相关资料。
- Stata连享会 [精品专题](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文](https://github.com/arlionn/stata/blob/master/README.md)

>#### 联系我们

- **欢迎赐稿：** 欢迎将您的文章或笔记投稿至`Stata连享会(公众号: StataChina)`，我们会保留您的署名；录用稿件达`五篇`以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。
- **意见和资料：** 欢迎您的宝贵意见，您也可以来信索取推文中提及的程序和数据。
- **招募英才：** 欢迎加入我们的团队，一起学习 Stata。合作编辑或撰写稿件五篇以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。
- **联系邮件：** StataChina@163.com

>#### 往期精彩推文
- [Stata连享会推文列表](https://www.jianshu.com/p/de82fdc2c18a) 
- Stata连享会 [精品专题](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文](https://github.com/arlionn/stata/blob/master/README.md)

[![](https://gitee.com/uploads/images/2019/0426/092109_36ee2a97_1522177.png)](https://gitee.com/arlionn/Course/blob/master/README.md)


---
![欢迎加入Stata连享会(公众号: StataChina)](http://wx1.sinaimg.cn/mw690/8abf9554gy1fj9p14l9lkj20m30d50u3.jpg "扫码关注 Stata 连享会")

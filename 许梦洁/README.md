### 许梦洁的项目主页
Stata 连享会高级成员

**Note：** 写作之前请务必先查看 [「Wiki」](https://gitee.com/Stata002/StataSX2018/wikis/Home) 页面中的相关建议和要求，尤其是 [「Stata连享会推文格式要求」](https://gitee.com/Stata002/StataSX2018/wikis/00_Stata%E8%BF%9E%E4%BA%AB%E4%BC%9A%E6%8E%A8%E6%96%87%E6%A0%BC%E5%BC%8F%E8%A6%81%E6%B1%82.md?sort_id=953475)


### 日志

- `2018/11/7 9:37` 已完成推文 [Stata中的单位根检验](https://gitee.com/Stata002/StataSX2018/blob/master/%E8%AE%B8%E6%A2%A6%E6%B4%81/Stata%E4%B8%AD%E7%9A%84%E5%8D%95%E4%BD%8D%E6%A0%B9%E6%A3%80%E9%AA%8C.md)
- `2018/11/7 9:52` 新任务：[Stata: 协整分析](https://gitee.com/Stata002/StataSX2018/blob/master/%E8%AE%B8%E6%A2%A6%E6%B4%81/Stata:%20%E5%8D%8F%E6%95%B4%E5%88%86%E6%9E%90.md)。Note: 格式上参考上一篇已经完成的推文，具体要求参见：[「Stata连享会推文格式要求」](https://gitee.com/Stata002/StataSX2018/wikis/00_Stata%E8%BF%9E%E4%BA%AB%E4%BC%9A%E6%8E%A8%E6%96%87%E6%A0%BC%E5%BC%8F%E8%A6%81%E6%B1%82.md?sort_id=953475)





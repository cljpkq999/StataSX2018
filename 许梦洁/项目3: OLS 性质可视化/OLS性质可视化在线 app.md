
### 1. OLS 回顾
#### 1.1 估计系数的决定因素
采用矩阵形式将线性回归模型表示如下：

$$
y = x\beta + u  \tag{1}
$$

若假设解释变量外生 (同时满足其它基本假设条件)，即 $E(x'u)=0$，则 OLS 估计式如下：

$$
\hat{\beta} = (x'x)^{-1}x'y  \tag{2}
$$

进一步假设 $u \sim N(0, \sigma^2)$，则 $\hat{\beta}$ 为：

$$
Var(\hat{\beta}) = \sigma^2(x'x)^{-1}  \tag{3}
$$

显然，若想提高 $\hat{\beta}$ 估计的准确程度，我们希望 $Var(\hat{\beta})$ 尽可能小一点。从公式 (3) 中可以看出，这取决于两个因素：
- **要点1：**  干扰项的变异程度。显然，$\sigma^2$ 越小越好：这意味着原始模型 (1) 中干扰项的方差 ($Var(u)=\sigma_u^2$) 越小越好。实证分析中，若模型设定中遗漏了重要的变量，或是某些不可观测的个体或时间特征没有得到很好的控制，它们都会**跑到**干扰项中，从而导致 $\sigma^2$ 过大。一个比较常用的处理方式就是在模型中加入反应个体或时间固定效应的虚拟变量。
- **要点2：** 解释变量的变异程度。若把 $(x'x)$ 视为 $Var(x)=\sigma_x^2$，则 $(x'x)^{-1}$ 可以视为 $1/Var(x)$。换言之，解释变量的离散程度越高，则越容易更为准确地识别 $\beta$ 的参数估计值。实证分析中，在收集数据环节，就要尽可能保证样本的来源多样化，能够较好的反应母体的特征。
#### 1.2 拟合优度的决定因素
当模型设定正确即回归模型与真实模型一致时，拟合优度 $R^2$ 是解释变量方差 $\sigma^2_x$ 的增函数，是扰动项方差 $\sigma^2_u$ 的减函数。这一点可以由下面的简单推导中得到：  

假设真实模型为 $y=\beta_0+\beta_1 x+u$，若回归模型与真实模型一致，则估计结果为$\hat{y}=\hat{\beta_0}+\hat{\beta_1} x+\hat{u}$，两边取期望有$\bar{y}=\hat{\beta_0}+\hat{\beta_1}\bar{x}+\bar{u}$。易得：  

$$
\hat{u_i}=u_i-\bar{u}-(\hat{\beta_1}-\beta_1)(x_i-\bar{x}) \tag{4}
$$  

则有：

<div align="center"><img src="http://latex.codecogs.com/gif.latex?$$
\sum_{i=1}^{n}\hat{u}_i^2=\sum_{i=1}^{n} (u_i-\bar{u})^2+(\hat{\beta_1}-\beta_1)^2(x_i-\bar{x}) ^2-2(\hat{\beta_1}-\beta_1)\sum_{i=1}^{n}(x_i-\bar{x}) u_i 
$$ \tag{5}" /></div>

其中，由$\hat{\beta_1}=\sum_{i=1}^{n} (x_i-\bar{x})y_i/\sum_{i=1}^{n} (x_i-\bar{x})^2$易得：

$$
\hat{\beta_1}-\beta_1=\frac{\sum_{i=1}^{n}(x_i-\bar{x}) u_i}{\sum_{i=1}^{n} (x_i-\bar{x})^2} \tag{5}
$$ 

将公式 (5) 代入$\sum_{i=1}^{n}\hat{u}_i^2$中可得：

<div align="center"><img src="http://latex.codecogs.com/gif.latex?$$  
E \left[\sum_{i=1}^{n}\hat{u}_i^2 \right]=(n-1)\sigma_u^2+\sigma_u^2-2\sigma_u^2=(n-2)\sigma_u^2
$$   " /></div>

则拟合优度 $R^2$ 为(注意$\beta'\beta$为常数)：  

$$
R^2=1-SSR/SST=1-\frac{\hat{u}'\hat{u}}{(X\beta+u)'(X\beta+u)}=1-\frac{(n-2)\sigma_u^2}{(\beta'\beta)\sigma_x^2+\sigma_u^2} \tag{6}
$$  

由公式 (6) 易得：拟合优度 $R^2$ 是解释变量方差 $\sigma^2_x$ 的增函数，是扰动项方差 $\sigma^2_u$ 的减函数。

### 2. OLS 性质的可视化模拟

#### 2.1 概览

为了让大家对参数和模型的设定如何影响 OLS 回归结果有一个直观地感受，本次推文将介绍一个可以直接调整各项参数并得到可视化OLS结果的[A Shiny App for Playing with OLS](http://www.econometricsbysimulation.com/2013/11/a-shiny-app-for-playing-with-ols.html) 。这个 APP 对解释变量和扰动项的假设与经典 OLS 假设一致 ，并给定 $x$ 的期望为2， $u$ 的期望为0。用户可以调整解释变量 $x$ 和误差项 $u$ 的标准差、随机种子的数量、样本规模、样本生成模型以及 OLS 回归模型，并可以在调整参数的同时看到样本点、拟合情况和对应的回归统计量的实时变化，如下图：  

![OLS_app-01.gif](https://upload-images.jianshu.io/upload_images/7692714-b2b924c437a2dda6.gif?imageMogr2/auto-orient/strip)

#### 2.2 样本规模的影响

首先来看样本规模对 OLS 回归的影响，其他条件不变，在回归模型设定正确的前提下，样本量越大模型的拟合优度越高。这个预判可以从下图的操作中得到验证，这里真实数据生成过程为 $y=2+3x+u$ ，回归模型与真实模型一致，为$y = \alpha+x\beta + u$。随着样本量由10逐渐扩大到496，拟合优度也呈现由0.26扩大到0.31的非减变化。

![OLS_app-02-sample-size.gif](https://upload-images.jianshu.io/upload_images/7692714-6147367eb0432c7b.gif?imageMogr2/auto-orient/strip)

#### 2.3 解释变量标准差的影响

模型设定正确的前提下，其他条件不变，解释变量标准差越大，拟合效果越好。如下图，当解释变量 $x$ 的标准差由0.25逐渐增加到3.5时，拟合优度也呈现了大幅度地增长，由0.29 增加到了0.99。

![OLS_app-03-varX.gif](https://upload-images.jianshu.io/upload_images/7692714-9c483effb7bd7c95.gif?imageMogr2/auto-orient/strip)

#### 2.4 解释变量标准差和误差项标准差的交互影响
当同时考虑解释变量和误差项的标准差变化时，我们将能更清楚地看到解释变量和误差项标准差对拟合效果不同方向的影响以及影响程度的相对大小。模拟显示，保持 $x$ 的标准差为0.25，$u$ 的标准差由1增加到7时，模型的拟合优度将由0.28骤降到0。保持 $u$ 的标准差为5，$x$ 的标准差由0.25增加到4.25时，模型的拟合优度将由0.01增加到0.83。

![OLS_app-04-varU.gif](https://upload-images.jianshu.io/upload_images/7692714-19f481238f8c5e16.gif?imageMogr2/auto-orient/strip)

#### 2.5 回归模型设定的影响

前面的模拟我们都直接设定回归模型与真实模型一致，如果回归模型设定不符合真实模型会发生什么呢？这里我们设定真实模型为 $y=2+3x-5x^2+u$，当回归模型只包含一次项时拟合优度为0.30；只包含二次项$x^2$时，由于抛物线的开口方向相反，拟合优度仅为0.16；而同时包含一次项和二次项即与真实模型一致时，拟合优度增加到了0.49；而往模型中加入更高次项时，拟合优度却并没有改善。显然以上模拟结果证实了当回归模型与真实模型一致时，OLS 的拟合效果最好。  

![OLS_app-05-NLS.gif](https://upload-images.jianshu.io/upload_images/7692714-9915764e7cbc503d.gif?imageMogr2/auto-orient/strip)


### 3. Stata 模拟 OLS

以上模拟过程同样也可以用 Stata 操作，事实上仅需几行代码即可完成：

```stata
cd "D:\stata15\ado\personal\Jianshu\OLS_simu_APP"
clear
set obs 151
set seed 1
gen x = rnormal(0, 0.25)
gen u = rnormal(0,1)
gen y = 2 + 3*x + u
*-图示
twoway (scatter y x) (lfit y x), ///
       scheme(tufte) legend(off)
*-更为简洁的做法
aaplot y x
graph export OLS_APP_01.png, replace
reg y x
```

![Stata连享会-OLS蒙特卡洛模拟-使用 aaplot 命令制图](https://upload-images.jianshu.io/upload_images/7692714-bee209e5eac767cd.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)






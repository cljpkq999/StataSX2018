> 作者：连玉君 | 许梦洁  ([知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn))    
>
> #### Stata连享会 [「2018.11.23-25 内生性专题研讨班」](https://gitee.com/arlionn/stata_training/blob/master/README.md) 

最小二乘估计( OLS )是经管类学生学术道路上的第一个拦路虎，也是最重要的垫脚石。复杂的估计参数、模型设定以及各种统计量总是让初学者们望而却步，同时如何清楚明白地向同学们讲清楚复杂的 OLS 系统对于老师们来说也是一个不小的挑战。因此一个能够直观反映 OLS 系统抽象性质的工具想必不仅能帮助同学们对抽象的 OLS 系统有一个直观地体验，而且也能极大地方便老师们的教学工作。在这篇推文中，我们就将为大家介绍这样一个福利APP：[A Shiny App for Playing with OLS](http://www.econometricsbysimulation.com/2013/11/a-shiny-app-for-playing-with-ols.html)。

### 1. OLS 回顾

采用矩阵形式将线性回归模型表示如下：

$$
y = x\beta + u  \tag{1}
$$

若假设解释变量外生 (同时满足其它基本假设条件)，即 $E(x'u)=0$，则 OLS 估计式如下：

$$
\hat{\beta} = (x'x)^{-1}x'y  \tag{2}
$$

进一步假设 $u \sim N(0, \sigma^2)$，则 $\hat{\beta}$ 为：

$$
Var(\hat{\beta}) = \sigma^2(x'x)^{-1}  \tag{3}
$$

显然，若想提高 $\hat{\beta}$ 估计的准确程度，我们希望 $Var(\hat{\beta})$ 尽可能小一点。从公式 (3) 中可以看出，这取决于两个因素：
- **要点1：**  干扰项的变异程度。显然，$\sigma^2$ 越小越好：这意味着原始模型 (1) 中干扰项的方差 ($Var(u)=\sigma_u^2$) 越小越好。实证分析中，若模型设定中遗漏了重要的变量，或是某些不可观测的个体或时间特征没有得到很好的控制，它们都会**跑到**干扰项中，从而导致 $\sigma^2$ 过大。一个比较常用的处理方式就是在模型中加入反应个体或时间固定效应的虚拟变量。
- **要点2：** 解释变量的变异程度。若把 $(x'x)$ 视为 $Var(x)=\sigma_x^2$，则 $(x'x)^{-1}$ 可以视为 $1/Var(x)$。换言之，解释变量的离散程度越高，则越容易更为准确地识别 $\beta$ 的参数估计值。实证分析中，在收集数据环节，就要尽可能保证样本的来源多样化，能够较好的反应母体的特征。

### 2. OLS 性质的可视化模拟

#### 2.1 概览

为了让大家对参数和模型的设定如何影响 OLS 回归结果有一个直观地感受，本文与大家分享一个在线的可视化 APP —— [A Shiny App for Playing with OLS](http://www.econometricsbysimulation.com/2013/11/a-shiny-app-for-playing-with-ols.html)。

这个 APP 使我们可以直接修改 DGP (数据生成过程，Data Generation Process) 中各项参数，以便了解 OLS 的估计性质受那些因素影响，从而为实证分析中合理设定模型提供依据。

具体而言，在进行基本参数设定时，该 APP 对解释变量和扰动项的假设与经典 OLS 假设一致 ，并给定 $x$ 的期望为 2， $u$ 的期望为0。用户可以调整解释变量 $x$ 和干扰项 $u$ 的标准差、随机模拟的种子值、样本数、DGP 以及 OLS 回归模型，并可以在调整参数的同时看到样本点、拟合情况和对应的回归统计量的实时变化，如下图：  

![OLS_app-01-Stata连享会.gif](https://upload-images.jianshu.io/upload_images/7692714-b2b924c437a2dda6.gif?imageMogr2/auto-orient/strip)

#### 2.2 样本规模的影响

首先，我们来看样本规模对 OLS 回归的影响。其他条件不变，在回归模型设定正确的前提下，样本量越大模型的拟合优度越高。这个预判可以从下图的操作中得到验证，这里真实数据生成过程 (DGP) 为 $y=2+3x+u$ ，回归模型与真实模型一致，为$y = \alpha+x\beta + u$。随着样本量由 10 逐渐扩大到 496，拟合优度由 0.26 增加到 0.31。

![OLS_app-02-sample-size-Stata连享会.gif](https://upload-images.jianshu.io/upload_images/7692714-6147367eb0432c7b.gif?imageMogr2/auto-orient/strip)

#### 2.3 解释变量标准差的影响

模型设定正确的前提下，其他条件不变，解释变量标准差越大，拟合效果越好。如下图，当解释变量 $x$ 的标准差由 0.25 逐渐增加到 3.5 时，拟合优度也呈现了大幅度地增加，由 0.29 增加到了 0.99。

![OLS_app-03-varX.gif](https://upload-images.jianshu.io/upload_images/7692714-9c483effb7bd7c95.gif?imageMogr2/auto-orient/strip)

#### 2.4 解释变量标准差和误差项标准差的交互影响

当同时考虑解释变量和误差项的标准差变化时，我们将能更清楚地看到解释变量和误差项标准差对拟合效果不同方向的影响以及影响程度的相对大小。

模拟显示，保持 $x$ 的标准差为 0.25，$u$ 的标准差由 1 增加到 7 时，模型的拟合优度将由 0.28 骤降到 0。保持 $u$ 的标准差为 5，$x$ 的标准差由 0.25 增加到 4.25时，模型的拟合优度将由 0.01增加到 0.83。

![OLS_app-04-varU-Stata连享会.gif](https://upload-images.jianshu.io/upload_images/7692714-19f481238f8c5e16.gif?imageMogr2/auto-orient/strip)

#### 2.5 回归模型设定的影响

在上述模拟中，我们都直接设定回归模型与真实模型一致。然而，实际分析中，我们并不知道真实的 DGP，也就无从知道真实的模型形式。那么，当回归模型设定不符合真实模型会发生什么呢？

这里，我们设定真实模型为 $y=2+3x-5x^2+u$，当回归模型只包含一次项时，拟合优度为 0.30；只包含二次项 $x^2$ 时，由于抛物线的开口方向相反，拟合优度仅为 0.16；而同时包含一次项和二次项 (与真实模型一致) 时，拟合优度增加到了 0.49；但当我们在模型中进一步添加高次项时，拟合优度并没有改善。

上述模拟结果表明：**当回归模型与真实模型一致时，OLS 的拟合效果最好。** 

这意味着，在实证分析中，我们可以使用 R<sup>2</sup>  (更多的时候是使用 **残差平方和** RSS)，来作为判别模型优劣的指标。（需要说明的是，这只是一个必要条件，而不是充分条件。因为，当存在严重共线性或伪回归问题时，也会表现出 R<sup>2</sup> 很高的特征。参见 [「Stata: 协整还是伪回归?」](https://www.jianshu.com/p/edde2a4ca653)）

![OLS_app-05-NLS-Stata连享会.gif](https://upload-images.jianshu.io/upload_images/7692714-9915764e7cbc503d.gif?imageMogr2/auto-orient/strip)


### 3. 自己动手：用 Stata 模拟 OLS 的性质

以上模拟过程同样也可以用 Stata 操作，事实上仅需几行代码即可完成：

```stata
cd "D:\stata15\ado\personal\Jianshu\OLS_simu_APP"
clear
set obs 151
set seed 1
gen x = rnormal(2, 0.25)
gen u = rnormal(0,1)
gen y = 2 + 3*x + u
*-图示
twoway (scatter y x) (lfit y x), ///
       scheme(tufte) legend(off)
*-更为简洁的做法
aaplot y x
graph export OLS_APP_01.png, replace
reg y x
```

我们可以在上述 dofile 中修改相应的参数来对比 OLS 性质的变化。

如果想让模拟分析过程更有效，可以把上述代码改写为一个可以反复执行的 **ado** 文件，并以此为基础定义一个 Stata 命令，通过选项来设定模拟过程中各个参数的数值。具体实现过程参见：[「Stata：编写ado文件自动化执行常见任务」](https://www.jianshu.com/p/3d7eb86c5a92)。  

![Stata连享会-OLS蒙特卡洛模拟-使用 aaplot 命令制图](https://upload-images.jianshu.io/upload_images/7692714-bee209e5eac767cd.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)  

### 结语

在这篇推文中，我们为大家介绍了一个根据用户自己设置的参数生成样本点并进行 OLS 回归的 APP，并通过对这个 APP 的简单操作对 OLS 回归中样本规模、解释变量标准差、误差项标准差以及各个参数之间的交互作用对回归结果的影响给出了一个直观的体验，希望对大家有所帮助。需要注意的是，在试图使用 `Stata` 等其他统计软件对这个 APP 计算的统计量进行对比和验证时需要牢记这个 APP 默认 $x$ 的期望为2，$u$ 的期望为 0 ，否则可能会出现与预期不一致的结果。这里我们也给出了使用 `Stata` 实现同样回归效果的代码，作为对这个 APP 的一个补充和对照。


>#### 关于我们

- 【**Stata 连享会(公众号：StataChina)**】由中山大学连玉君老师团队创办，旨在定期与大家分享 Stata 应用的各种经验和技巧。
- 公众号推文同步发布于 [CSDN-Stata连享会](https://blog.csdn.net/arlionn) 、[简书-Stata连享会](http://www.jianshu.com/u/69a30474ef33) 和 [知乎-连玉君Stata专栏](https://www.zhihu.com/people/arlionn)。可以在上述网站中搜索关键词`Stata`或`Stata连享会`后关注我们。
- 点击推文底部【阅读原文】可以查看推文中的链接并下载相关资料。
- Stata连享会 [精彩推文1](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文2](https://github.com/arlionn/stata/blob/master/README.md)

>#### 联系我们

- **欢迎赐稿：** 欢迎将您的文章或笔记投稿至`Stata连享会(公众号: StataChina)`，我们会保留您的署名；录用稿件达`五篇`以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。
- **意见和资料：** 欢迎您的宝贵意见，您也可以来信索取推文中提及的程序和数据。
- **招募英才：** 欢迎加入我们的团队，一起学习 Stata。合作编辑或撰写稿件五篇以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。
- **联系邮件：** StataChina@163.com

>#### 往期精彩推文
- [Stata连享会推文列表1](https://www.jianshu.com/p/de82fdc2c18a) 
- [Stata连享会推文列表2](https://github.com/arlionn/Stata_Blogs/blob/master/README.md)
- Stata连享会 [精彩推文1](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文2](https://github.com/arlionn/stata/blob/master/README.md)
![](https://upload-images.jianshu.io/upload_images/7692714-8b1fb0b5068487af.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)



---
![欢迎加入Stata连享会(公众号: StataChina)](http://wx1.sinaimg.cn/mw690/8abf9554gy1fj9p14l9lkj20m30d50u3.jpg "扫码关注 Stata 连享会")
#### 写在前面(目的)

##### 方案 1：（针对 Stata 15 以后的用户）
- 参考 [Creating Excel tables with putexcel, part 2: Macro, picture, matrix, and formula expressions](https://blog.stata.com/2017/01/24/creating-excel-tables-with-putexcel-part-2-macro-picture-matrix-and-formula-expressions/)。这篇文章是介绍如何把图形写入 Excel 文档的，我们可以仿照其思路，使用 `putdocx` 命令中的 `putdocx image` 子命令来实现在 Word 文档中自动添加图片的目的。
- **高级任务：** 可以参考`吴水亭`那个推文中的`reg2docx`命令，写一个程序，名称暂定为 `gr2docx` 或 `fig2docx`，用来输出图片。

##### 方案 2：（针对 Stata 14 以前的用户）
- 介绍 `graphto` 和 `graphout` 命令。主要用途在于自动输出 Stata 图形到 Word 文档中。是吴水亭此前介绍的 `reg2docx` 命令的补充。
- 重点在于介绍易于读者使用的操作模式：如何保证输出到 Word 文档中的图片可以直接用，或者尽可能少做二次手动调整。
- 参考资料：Wolfe, J. D., Bauldry, S. Collecting and organizing Stata graphs [J]. Stata Journal, 2014, 14 (4): 965-974. [--点击下载 PDF--](http://www.researchgate.net/profile/Shawn_Bauldry/publication/270582154_Collecting_and_Organizing_Stata_Graphs/links/54d7ecb60cf246475818afea.pdf)


- 输入如下命令，可以下载该命令以及作者提供的 dofiles
```stata
view net sj 14-4 gr0060
```

#### Stata 范例

- 如下是 `help graphout` 帮助文件中提供的Stata 范例：
```stata
. graphsto, clear
. sysuse auto
. graph twoway scatter price mpg, scheme(sj)
. graphsto Ex1.png, replace title(Price by MPG)
. graphout using Ex1.rtf, replace
```
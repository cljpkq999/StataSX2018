
- `2019/1/2 17:24` 新增
  - 平滑性假设检验的命令，`ssc install rdcont, replace`, 详情查看帮助文件，附带有Stata 范例数据和例子 (可以使用 `findit rdcont` 搜索后，在弹出页面中下载)。
```stata
  use "table_two_final.dta", clear 
  //Test of continuity on the running variable
  rdcont difdemshare if use==1
```



> **任务：** 参考 [mostly-harmless-replication](https://github.com/vikjam/mostly-harmless-replication/blob/master/06%20Getting%20a%20Little%20Jumpy/06%20Getting%20a%20Little%20Jumpy.md) 第 6 章中的例子，上述连接中已经提供了 dofile。


# 06 Getting a Little Jumpy
## 6.1 Sharp RD

### Figure 6-1-1

Completed in [Stata](Figure%206-1-1.do), [R](Figure%206-1-1.r), [Python](Figure%206-1-1.py) and [Julia](Figure%206-1-1.jl)

![Figure 6-1-1 in Stata](https://github.com/vikjam/mostly-harmless-replication/blob/master/06%20Getting%20a%20Little%20Jumpy/Figure%206-1-1-Stata.png?raw=true)

### Figure 6-1-2

Completed in [Stata](Figure%206-1-2.do), [R](Figure%206-1-2.r), [Python](Figure%206-1-2.py) and [Julia](Figure%206-1-2.jl)

![Figure 6-1-2 in R](https://github.com/vikjam/mostly-harmless-replication/blob/master/06%20Getting%20a%20Little%20Jumpy/Figure%206-1-2-R.png?raw=true)

### Figure 6-2-1

Completed in [Stata](Figure%206-2-1.do), [R](Figure%206-2-1.r), [Python](Figure%206-2-1.py) and [Julia](Figure%206-1-1.jl)

![Figure 6-2-1 in Julia](https://github.com/vikjam/mostly-harmless-replication/blob/master/06%20Getting%20a%20Little%20Jumpy/Figure%206-2-1-Julia.png?raw=true)

----
## Stata dofiles

```stata
clear all
set more off

/* Set random seed for replication */
set seed 1149

/* Number of random variables */
local nobs = 100

set obs `nobs'

gen x         = runiform()
gen y_linear  = x + (x > 0.5) * 0.25 + rnormal(0, 0.1)
gen y_nonlin  = 0.5 * sin(6 * (x - 0.5)) + 0.5 + (x > 0.5) * 0.25 + rnormal(0, 0.1)
gen y_mistake = 1 / (1 + exp(-25 * (x - 0.5))) + rnormal(0, 0.1)

graph twoway (lfit y_linear x if x < 0.5, lcolor(black))                        ///
             (lfit y_linear x if x > 0.5, lcolor(black))                        ///
             (scatter y_linear x, msize(vsmall) msymbol(circle) mcolor(black)), ///
                title("A. Linear E[Y{sub:0i}|X{sub:i}]")                        ///
                ytitle("Outcome")                                               ///
                xtitle("x")                                                     ///
                xline(0.5, lpattern(dash))                                      ///
                scheme(s1mono)                                                  ///
                legend(off)                                                     ///
                saving(y_linear, replace)

graph twoway (qfit y_nonlin x if x < 0.5, lcolor(black))                        ///
             (qfit y_nonlin x if x > 0.5, lcolor(black))                        ///
             (scatter y_nonlin x, msize(vsmall) msymbol(circle) mcolor(black)), ///
                title("B. Nonlinear E[Y{sub:0i}|X{sub:i}]")                     ///
                ytitle("Outcome")                                               ///
                xtitle("x")                                                     ///
                xline(0.5, lpattern(dash))                                      ///
                scheme(s1mono)                                                  ///
                legend(off)                                                     ///
                saving(y_nonlin, replace)

graph twoway (lfit y_mistake x if x < 0.5, lcolor(black))                        ///
             (lfit y_mistake x if x > 0.5, lcolor(black))                        ///
             (function y = 1 / (1 + exp(-25 * (x - 0.5))), lpattern(dash))       ///
             (scatter y_mistake x, msize(vsmall) msymbol(circle) mcolor(black)), ///
                title("C. Nonlinearity mistaken for discontinuity")              ///
                ytitle("Outcome")                                                ///
                xtitle("x")                                                      ///
                xline(0.5, lpattern(dash))                                       ///
                scheme(s1mono)                                                   ///
                legend(off)                                                      ///
                saving(y_mistake, replace)

graph combine y_linear.gph y_nonlin.gph y_mistake.gph, ///
    col(1)                                             ///
    xsize(4) ysize(6)                                  ///
    graphregion(margin(zero))                          ///
    scheme(s1mono)
graph export "Figure 6-1-1-Stata.png", replace

/* End of file */
exit
```
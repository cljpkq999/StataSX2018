
---
> **导言**    
>
> 这些推文来自一个老外的个人博客，介绍了 Stata 应用过程中的方方面面，我们可以选择感兴趣的主题进行翻译，进而加入一些我们自己的理解，尽量保证每篇推文能够比较全面的把一个主题讲清楚。
>
> **Source:** [Stata Blogs - Econometrics By Simulation](http://www.econometricsbysimulation.com/p/stata.html)

---

### Programming
#### Basics
- [commenting](http://www.econometricsbysimulation.com/2012/06/commenting-in-stata.html)
- [wildcards](http://www.econometricsbysimulation.com/2012/06/stata-wildcards-and-shortcuts.html)
- [forvalues](http://www.econometricsbysimulation.com/2012/06/stata-programming-basics-forvalues.html)
- [foreach](http://www.econometricsbysimulation.com/2012/06/stata-coding-basics-foreach.html)
- [macros 1](http://www.econometricsbysimulation.com/2012/06/stata-programming-basics-macros.html)
- [macros 2](http://www.econometricsbysimulation.com/2012/09/fun-with-macros.html)
- [capture](http://www.econometricsbysimulation.com/2012/06/stata-programming-basics-capture.html)
- [predict](http://www.econometricsbysimulation.com/2012/07/predict.html)
- [return](http://www.econometricsbysimulation.com/2012/07/the-return-command.html)
- [logical operators](http://www.econometricsbysimulation.com/2012/07/the-joy-of-logic-stata-logical-operators.html)
- [If statements and relational operators](http://www.econometricsbysimulation.com/2012/07/if-statements-and-relational-operators.html)[](http://www.blogger.com/)
[](http://www.econometricsbysimulation.com/2012/06/stata-programming-basics-macros.html)
- [7 ways to Speed up your do files](http://www.econometricsbysimulation.com/2012/07/7-ways-to-speed-up-your-do-files.html)
- [Formatting Guide](http://www.econometricsbysimulation.com/2013/03/my-not-so-brief-stata-formatting-guide.html)

#### Random Variables
- [A note on generating random numbers](http://www.econometricsbysimulation.com/2012/05/note-on-generating-random-numbers.html)
- [Generating 'random' variables drawn from any distribution](http://www.econometricsbysimulation.com/2012/06/generating-random-variables-drawn-from.html)
- [Drawing jointly distributed non-normal random variables](http://www.econometricsbysimulation.com/2012/09/drawing-jointly-distributed-non-normal.html)
- [Generate rank correlated variables](http://www.econometricsbysimulation.com/2012/06/how-to-generate-variables-that-are-rank.html)
- [Draw Multinomial Random Variables](http://www.econometricsbysimulation.com/2012/11/draw-multinomial-random-variables.html)

#### Mata
- [Matrix operations in Mata](http://www.econometricsbysimulation.com/2012/09/matrix-operations-in-mata.html)
- [Mata speed gains over Stata](http://www.econometricsbysimulation.com/2012/09/mata-speed-gains-over-stata.html)
- [Write your own System OLS in Mata](http://www.econometricsbysimulation.com/2012/05/write-your-own-system-ols-in-mata.html)
- [Mata: program your own IV Estimator](http://www.econometricsbysimulation.com/2012/08/mata-program-your-own-iv-estimator.html)

#### Maximum Likelihood (write your own commands)
- [Quantile regression](http://www.econometricsbysimulation.com/2012/05/program-your-own-quantile-regression-v1.html)
- [3 ways to write a probit command](http://www.econometricsbysimulation.com/2012/07/3-ways-to-write-probit-command.html)
- [Linear models with heteroskedastic errors](http://www.econometricsbysimulation.com/2012/08/linear-models-with-heteroskedastic.html)
- [ML and initial values](http://www.econometricsbysimulation.com/2013/03/ml-and-initial-values.html)

#### Programs
- [all missing in SPSS to Stata missing](http://www.econometricsbysimulation.com/2012/04/easily-recode-all-missing-values-in.html)
- [capture_retry](http://www.econometricsbysimulation.com/2012/06/progamming-in-stata-captureretry-cap2.html)
- [Remove a subset from a global - Stata](http://www.econometricsbysimulation.com/2012/07/remove-subset-from-global-stata.html)
- [Recursive programming in Stata](http://www.econometricsbysimulation.com/2012/07/recursive-programming-in-stata.html)
- [Inverting a Binomial Distribution in Stata - harder than it sounds](http://www.econometricsbysimulation.com/2012/11/inverting-binomial-distribution-in.html)

#### Simulation Tools
- [bootstrapped draws - merge method](http://www.econometricsbysimulation.com/2012/07/bootstrapped-draws-for-simulating.html)
[](http://www.econometricsbysimulation.com/2012/07/use-bootstrapped-draws-for-simulating.html)- [bootstrapped draws - expand method](http://www.econometricsbysimulation.com/2012/07/use-bootstrapped-draws-for-simulating.html)
- [asim command](http://www.econometricsbysimulation.com/2013/03/asim-command.html)
- [msimulate command](http://www.econometricsbysimulation.com/2013/03/msimulate-command.html)

#### Programming
- [Return Text](http://www.econometricsbysimulation.com/2013/01/return-text.html)
- [Stata Syntax Command's Syntax](http://www.econometricsbysimulation.com/2013/01/stata-syntax-commands-syntax.html)

## Data Management

### Data Organization
- [Three ways to Create Unique Identifiers](http://www.econometricsbysimulation.com/2012/09/three-ways-to-create-unique-identifiers.html)
- [Use Expand to Manually Reshape Data](http://www.econometricsbysimulation.com/2013/01/use-expand-to-manually-reshape-data.html)
- [Pre and Post Test Data Merge/Append](http://www.econometricsbysimulation.com/2012/11/pre-and-post-test-data-mergeappend.html) 已认领
- [Moving Averages & Data Cleaning](http://www.econometricsbysimulation.com/2012/11/moving-averages-data-cleaning.html)

### Data Imputation
- [Hot Decking!](http://www.econometricsbysimulation.com/2012/09/hot-decking.html)

#### Data Powertools-?Increasing your workload Efficiency
- [Easy recode all missing SPSS or SAS into Stata](http://www.econometricsbysimulation.com/2012/04/recode-all-missing-spss-into-stata.html)
- [Turn factor variables into a list of dummy variables](http://www.econometricsbysimulation.com/2012/06/convert-factor-variables-dummy-lists.html)
- [3 Ways of Loading SPSS (sav) files into Stata](http://www.econometricsbysimulation.com/2012/06/3-ways-of-loading-spss-sav-files-into.html)
- [Stata Fuzzy match command](http://www.econometricsbysimulation.com/2012/06/stata-fuzzy-match-command.html)
- [Search all of the data in a directory for a variable](http://www.econometricsbysimulation.com/2012/06/search-through-all-of-files-in.html)

## Econometrics

### Probability
- [Central Limit Theorem](http://www.econometricsbysimulation.com/2012/08/central-limit-theorem.html)
- [Independence and Bayes Rule](http://www.econometricsbysimulation.com/2012/06/probability-theory-by-example.html)
- [Jensen's inequality](http://www.econometricsbysimulation.com/2012/07/jensens-inequality.html)
- [The Delta Method](http://www.econometricsbysimulation.com/2012/07/delta-method.html)
- [Law of Iterative Expectations/Law of Total Expectations](http://www.econometricsbysimulation.com/2012/08/law-of-iterative-expectationslaw-of.html)
- [t-tests and F-tests and rejection rates](http://www.econometricsbysimulation.com/2012/11/t-tests-and-f-tests-and-rejection-rates.html)
- [A note on correlated variables](http://www.econometricsbysimulation.com/2012/08/a-note-on-correlated-variables.html)
- [Write your own bootstrap command](http://www.econometricsbysimulation.com/2012/07/write-your-own-bootstrap-command.html)
- [Survival Rates and Negative Binomial](http://www.econometricsbysimulation.com/2012/07/survival-rates-and-negative-binomial.html)已认领
- [The Problem with Probabilities I - how many rolls does it take to get a 1?](http://www.econometricsbysimulation.com/2012/07/problem-with-probabilities-how-many.html)
- [The Problem with Probabilities II - Monte Hall's Problem](http://www.econometricsbysimulation.com/2012/07/problem-with-probilities-monte-halls.html)
- [The Problem with Probabilities III - Settlers of Catan - probability of bandit attack](http://www.econometricsbysimulation.com/2012/07/settlers-of-catan-probability-of-bandit.html)
- [The Delta method to estimate standard errors from a non-linear transformation](http://www.econometricsbysimulation.com/2012/12/the-delta-method-to-estimate-standard.html)

#### Classical Problems- 
- [OLS w Correlation between x and u](http://www.econometricsbysimulation.com/2012/09/ols-w-correlation-between-x-and-u.html)
- [The problem of near multicollinearity](http://www.econometricsbysimulation.com/2012/07/problem-of-near-multicollinearity.html)
- [The randomly walking currency market](http://www.econometricsbysimulation.com/2012/06/he-randomly-walking-currency-market.html)
- [Selection and Bias](http://www.econometricsbysimulation.com/2012/08/selection-and-bias.html)
- [Linear models with heteroskedastic errors](http://www.econometricsbysimulation.com/2012/08/linear-models-with-heteroskedastic.html)
- [Calculating Mutually Exclusive Fixed Effects](http://www.econometricsbysimulation.com/2012/08/calculating-mutually-exclusive-fixed.html)
- [Test Rejection Power for Failure of Normality Assumption](http://www.econometricsbysimulation.com/2012/08/test-rejection-power-for-failure-of.html)
- [Modeling Heteroskedasticity](http://www.econometricsbysimulation.com/2012/11/modeling-heteroskedasticity.html)
- [Measurement Error](http://www.econometricsbysimulation.com/2012/11/measurement-error.html)
- [Model Selection Tests](http://www.econometricsbysimulation.com/2013/01/tests-of-model-fit.html)
- [Efficiency of LAD vs OLS](http://www.econometricsbysimulation.com/2013/01/efficiency-of-lad-vs-ols.html)
- [Checking for differences in estimates by group](http://www.econometricsbysimulation.com/2012/12/checking-for-differences-in-estimates.html)
- [Chamberlain Mundlak Device and the CLuster Robust Hausman Test](http://www.econometricsbysimulation.com/2013/02/chamberlain-mundlak-device-and-cluster.html)
- [Endogenous Binary Regressors](http://www.econometricsbysimulation.com/2013/02/endogenous-binary-regressors.html)
- [Regression with Endogenous Explanatory Variables](http://www.econometricsbysimulation.com/2013/02/regression-with-endogenous-explanatory.html)
- [Potential IV Challenges even with RCTs](http://www.econometricsbysimulation.com/2013/02/potential-iv-challenges.html)

#### System Ordinary Least Squares
- [Wr](http://www.econometricsbysimulation.com/2012/05/write-your-own-system-ols-in-mata.html)[ite your own System OLS in Mata](http://www.econometricsbysimulation.com/2012/05/write-your-own-system-ols-in-mata.html)
- [Seemingly Unrelated Regression - with Correlated Errors](http://www.econometricsbysimulation.com/2012/04/seemingly-unrelated-regression-with.html)
- [R-squared](http://www.econometricsbysimulation.com/2012/11/r-squared.html)
- [Multivariate Least Squares - Multi-Step Estimator and Correlated Explanatory Variables](http://www.econometricsbysimulation.com/2012/12/multivariate-least-squares-multi-step.html)

#### Unobserved Effect Models
- [](http://www.econometricsbysimulation.com/2012/05/unobserved-fixed-effects-model.html)[Unobserved fixed effects model](http://www.econometricsbysimulation.com/2012/05/unobserved-fixed-effects-model.html)
- [Unobserved effects model - extensions](http://www.econometricsbysimulation.com/2012/05/unobserved-effects-model-extensions.html)
- [A brief look at fe vs re](http://www.econometricsbysimulation.com/2012/08/a-brief-look-at-fe-vs-re.html)
- [Robust Hausman Test](http://www.econometricsbysimulation.com/2012/09/robust-hausman-test.html)
- [Robust Hausman Test Fail?](http://www.econometricsbysimulation.com/2012/09/robust-hausman-test-fail.html)
- [Interpreting the Control Function Coefficient](http://www.econometricsbysimulation.com/2013/02/interpreting-control-function.html)

### Correlated Random Coefficients
- [Average Treatement Effects and Correlated Random Coefficients](http://www.econometricsbysimulation.com/2012/05/average-treatement-effects-and.html)
- [Random Coefficients with IFGLS and MLE](http://www.econometricsbysimulation.com/2012/11/random-coefficients-with-ifgls-and-mle.html)

#### Random Coefficients
- [HLM comparison with OLS - 2 levels, random coefficient on constant](http://www.econometricsbysimulation.com/2013/01/xtmixed-exploration-2-levels.html)
- [Hierarchical linear modelling](http://www.econometricsbysimulation.com/2012/10/hierarchical-linear-modelling.html)
- [Choosing an appropriate level of analysis](http://www.econometricsbysimulation.com/2013/01/choosing-appropriate-level-of-analysis.html)

#### Instrumental Variables
- [Many Forms of Instrumental Variables](http://www.econometricsbysimulation.com/2012/04/many-forms-of-instrumental-variables.html)
- [The Weak Instrument Problem](http://www.econometricsbysimulation.com/2012/05/weak-instrument-problem.html)
- [Testing the Rank Condition of IV Estimators](http://www.econometricsbysimulation.com/2012/05/testing-rank-condition-of-iv-estimators.html)
- [2SQreg IVqreg Cfqreg - zombies](http://www.econometricsbysimulation.com/2012/06/2sqreg-ivqreg-cfqreg-zombies.html)

### Censored Regression
- [Dependent variable is bottom coded](http://www.econometricsbysimulation.com/2012/05/dependent-variable-is-bottom-coded.html)?
- [Asymmetric Error with Right and Left Sensoring](http://www.econometricsbysimulation.com/2012/05/asymmetric-error-with-right-and-left.html)
- [Tobit Normality Assumption Fail - Tobit Still Works](http://www.econometricsbysimulation.com/2012/05/tobit-normality-assumption-fail-tobit.html)

### Quantile Regression
- [Program your own quantile regression v1 - Maximum Likelihood](http://www.econometricsbysimulation.com/2012/05/program-your-own-quantile-regression-v1.html)?
- [Quantile Regression Fail](http://www.econometricsbysimulation.com/2012/05/quantile-regression-fail.html)
- [Oh wait! Quantile regression wins!](http://www.econometricsbysimulation.com/2012/05/oh-wait-quantile-regression-wins.html)
- [2SQreg IVqreg Cfqreg - zombies](http://www.econometricsbysimulation.com/2012/06/2sqreg-ivqreg-cfqreg-zombies.html)
- [Quantile Regression (qreg) is invariant to non-decreasing transformations](http://www.econometricsbysimulation.com/2012/06/least-absolute-deviation-regression.html)

### Random Coefficients
- [Estimating Random Coefficients on X](http://www.econometricsbysimulation.com/2012/05/estimating-random-coefficients-on-x.html)?(using xtmixed)
- [Estimating Random Coefficients on X](http://www.econometricsbysimulation.com/2012/11/estimating-random-coefficients-on-x.html)?(using NormalReg -preferred)
- [Estimating Random Coefficients in a panel data](http://www.econometricsbysimulation.com/2012/05/estimating-random-coefficients-in-panel.html)

### Binary Response Models
- [Reverse Engineering a Probit](http://www.econometricsbysimulation.com/2012/05/reverse-engineering-probit.html)
- [Average Partial Effects](http://www.econometricsbysimulation.com/2012/05/average-partial-effects.html)
- [Probit vs Logit](http://www.econometricsbysimulation.com/2012/07/probit-vs-logit.html)
- [Simulating Multinomial logit in Stata](http://www.econometricsbysimulation.com/2012/07/simulating-multinomial-logit-in-stata.html)

### Non-linear least squares
- [Non-Linear Least Squares (M-estimation)](http://www.econometricsbysimulation.com/2012/06/non-linear-least-squares-m-estimation.html)
- [R vs Stata Non-linear least squares!](http://www.econometricsbysimulation.com/2012/06/r-vs-stata-non-linear-least-squares.html)
- [Power Analysis with Non-Linear Least Squares: A Simulation Approach](http://www.econometricsbysimulation.com/2012/11/power-analysis-with-non-linear-least.html)

### Simultaneous Equations
- [Demand Simulation and Estimation](http://www.econometricsbysimulation.com/2012/05/estimating-supply.html)

### Bootstrapping
- [Write your own estimator and bootstrap the standard errors](http://www.econometricsbysimulation.com/2012/07/write-your-own-estimator-and-bootstrap.html)
- [Write your own bootstrap command](http://www.econometricsbysimulation.com/2012/07/write-your-own-bootstrap-command.html)
- [Stata - Write your own "fast" bootstrap](http://www.econometricsbysimulation.com/2012/10/stata-write-your-own-fast-bootstrap.html)
- [Bootstrapping Percentile Count Vs. Standard Error Approximation](http://www.econometricsbysimulation.com/2012/10/bootstrapping-percentile-count-vs.html)

### Regression Discontinuity (RDD)
- [Sharp Regression Discontinuity Example and Limitations](http://www.econometricsbysimulation.com/2012/08/sharp-regression-discontinuity-example.html)
- [Non-Parametric Regression Discontinuity](http://www.econometricsbysimulation.com/2013/02/non-parametric-regression-discontinuity.html)已认领

### Non-random Selection
- [Inverse Probability?Weighting?to Correct for Sample Selection/Missing Data](http://www.econometricsbysimulation.com/2012/08/inverse-probability-wieghting-to.html)
My Own Tools (No Theory Backing Them)
- [Breaking down R2 by variable](http://www.econometricsbysimulation.com/2012/08/breaking-down-r2-by-variable.html)
- [Sticky Probit - clustered bootstrapped standard errors](http://www.econometricsbysimulation.com/2013/01/sticky-probit-clustered-bootstrapped.html)
- [Non-Parametric PDF Fit Test](http://www.econometricsbysimulation.com/2013/02/non-parametric-pdf-fit-test.html)
- [Test of PDF Fit - Does not work](http://www.econometricsbysimulation.com/2013/02/test-of-pdf-fit-does-not-work.html)

### Power Analysis
- [Power Analysis with Non-Linear Least Squares: A Simulation Approach](http://www.econometricsbysimulation.com/2012/11/power-analysis-with-non-linear-least.html)

### Micro Simulations:
- [Selection Bias in the use of Fertilizer](http://www.econometricsbysimulation.com/2012/06/selection-bias-in-use-of-fertilizer.html)
- [Estimating Supply](http://www.econometricsbysimulation.com/2012/05/estimating-supply.html)
- [Demand Simulation and Estimation](http://www.econometricsbysimulation.com/2012/05/estimating-supply.html)
- [Estimating demand (AIDS model)](http://www.econometricsbysimulation.com/2012/05/estimating-supply.html)
- [Misspecified Cobb Douglas Demand Model](http://www.econometricsbysimulation.com/2012/05/estimating-supply.html)
- [Production Possibility Frontier: Simulation & Estimation](http://www.econometricsbysimulation.com/2012/04/production-possibility-frontier.html)
- [Commodity Demand Estimation - Fish Demand](http://www.econometricsbysimulation.com/2012/11/commodity-demand-estimation-fish-demand.html)

### Logistic Regression
- [Logistic or Logit or doesn't matter](http://www.econometricsbysimulation.com/2012/12/logistic-or-logit-or-doesnt-matter.html)
- [Simulating Multinomial Logit](http://www.econometricsbysimulation.com/2012/07/simulating-multinomial-logit-in-stata.html)

### Path Analysis
- [Path Analysis](http://www.econometricsbysimulation.com/2012/12/path-analysis.html)

## Miscellaneous Topics

### Special Topics
- [Value-added modelling iPad example](http://www.econometricsbysimulation.com/2012/05/value-added-modelling-stata-simulation.html)
- [Estimating VAMs Value Added Methods (models)](http://www.econometricsbysimulation.com/2012/06/estimating-vams-value-added-methods.html)
- [Social network analysis simulation - contagious depression](http://www.econometricsbysimulation.com/2012/06/social-network-analysis-simulation.html)
- [2012 Election! Simulation and predictions. Obama wins 91% of simulated outcomes](http://www.econometricsbysimulation.com/2012/11/2012-election-simulation-and.html)

### Dynamic Simulations:
- [Spatial malaria epidemiology infection model](http://www.econometricsbysimulation.com/2012/06/spatial-malaria-epidemiology-infection.html)
- [Zombie Invasion - spatial multi-agent system simulation](http://www.econometricsbysimulation.com/2012/06/zombie-invasion-spatial-multi-agent.html)
- [Werewolf attack - spatial multi-agent system basic artificial intelligence](http://www.econometricsbysimulation.com/2012/06/werewolf-attack-spatial-multi-agent.html)

### Exporting Results for Publication
- [Creating Professional Tables Using Estpost](http://www.econometricsbysimulation.com/2012/11/creating-professional-tables-using.html)
- [Professional Post Estimation Tables Using Estout](http://www.econometricsbysimulation.com/2012/11/professional-post-estimation-tables.html)

## Psychometrics

### Classical Methods
- [Cronbach's alpha](http://www.econometricsbysimulation.com/2012/08/cronbachs-alpha.html)
- [The True Test Score World](http://www.econometricsbysimulation.com/2012/09/the-true-test-score-world.html)
- [Relating Classics Test Theory Parameters to IRT Parameters](http://www.econometricsbysimulation.com/2012/09/relating-classics-test-theory.html)
- [Strictly Parallel Tests Classical Test Theory Results](http://www.econometricsbysimulation.com/2012/09/strictly-parallel-tests-classical-test.html)

## R & Stata Bridge

### Commands in Stata and R
- [7 commands in R & Stata](http://www.econometricsbysimulation.com/2012/07/7-commands-in-stata-and-r.html)
- [R vs Stata Non-linear least squares!](http://www.econometricsbysimulation.com/2012/06/r-vs-stata-non-linear-least-squares.html)?
- [Nested functions in R and Stata](http://www.econometricsbysimulation.com/2012/08/nested-functions-in-r-and-stata.html)
- [Stata is to Accounting as R is to Tetris](http://www.econometricsbysimulation.com/2012/09/stata-is-to-accounting-as-r-is-to-tetris.html)

`2017/11/10 8:36`

## 写在前面

本文介绍滚动回归的 Stata 实现方法。

## 何谓滚动回归

- 介绍滚动回归的基本思想；
- 应用场景（简要介绍几篇 JF，JFE 等 Top 期刊上的论文）

## Stata 实现

### Stata 相关命令概览
请使用 `findit` 和 `github search` 命令搜索相关命令，自行测试各个命令的使用效果后，有选择地进行介绍。
- `help rolling` //Rolling-window and recursive estimation
- `help rolling2` //


### 应用实例

### 目的：
目前，实证分析领域非常重视**论文结果的可复制性**。  

为了达到这一目标，在完成一篇实证分析论文过程中，我们必须合理安排所有相关文档。基本思想就是分门别类地存储不同类型的文件。

比如，所有的数据文件存储在一个，名称为 data 的文件夹里；所有参考文献和相关介绍性文档统一存储在一个名称为 refs 的文件夹中；
而论文中的所有输出结果，包括基本统计量、相关系数，以及回归分析的表格，都统一存放在名称为 out 的文件夹中。


---
### 推文思路和结构

- 先呈现一份简单的 dofile。
- 进而解释每一块代码的含义和作用。
  - 介绍全局暂元和局部暂元的概念。
  - 介绍几个基本的，有关stata文件夹设定的概念和命令
    - `help sysdir`
    - `help creturn`, `help creturn##directories`
    - `cret list`
- 截图呈现每个文件夹中存放的文件
- 最后一部分呈现一些扩展性的内容。介绍大型课题的文档设置，以及学位论文，比如博士论文的，文档结构的设定。此时每一章会有一个独立的文件夹。每个文件夹下也依次设定与单篇文章相似的子文件夹结构。

**说明：** 如果采用截图的方式呈现效果不佳，可以考虑采用思维导图。

---

### 背景介绍

### 范例：一个结构清晰的 Stata dofile 及其文件结构

在一篇都稳当的开头部分，我们可以加入如下语句：
```stata
  global path "D:\stata15\ado\personal\MyPaper\ToAER" //定义主文件夹所在路径
  global D    "$path\data"       //范例数据
  global R    "$path\refs"       //参考文献
  global T    "$path\Table_Fig"  //存储图形和表格
  adopath +   "$path\adofiles"   //自编程序 
  cd "$D"
  set scheme s2color  //彩色图片
  set scheme s1mono   //黑白图片
  set cformat  %4.3f  //回归结果中系数的显示格式
  set pformat  %4.3f  //回归结果中 p 值的显示格式      
  set sformat  %4.2f  //回归结果中 se值的显示格式    
```
上述语句的主要作用是，将每一个文件夹对应的文件路径存放在一个名称非常简单的全局暂元中，以便于后续引用。

### 关键命令解读

  - 介绍全局暂元和局部暂元的概念。
  - 介绍几个基本的，有关stata文件夹设定的概念和命令
    - `help sysdir`
    - `help creturn`, `help creturn##directories`
    - `cret list`



## 简介
请看如下公式
$$y_{it} = \beta x_{it} + \varepsilon_{it}$$

其中，$\varepsilon_{it}$ 为干扰项。

`regress` 命令很好用。 

### 图片测试

插入一个图片
![](http://file.lianxh.cn/images/20191108/6ea94ac9b6f815dd89af120827dae1c6.png)
代码如下：
```stata
![](http://file.lianxh.cn/images/20191108/6ea94ac9b6f815dd89af120827dae1c6.png)
```#
### 任务

翻译 [xtabond cheat sheet](https://blog.stata.com/2015/11/12/xtabond-cheat-sheet/)

### 建议
- 阅读 `help xtabond`，参考电子手册中的例子和说明；
- 从网上查找其他介绍动态面板的 blog 和相关介绍，丰富这篇推文的内容；如下供参考：
  - [Stata官网：Dynamic panel-data (DPD) analysis](https://www.stata.com/features/overview/dynamic-panel-data/)
  - [Dynamic Panel Data - Econometrics Tutorial for Stata](http://econometricstutorial.com/2015/04/dynamic-panel-data-stata/)
  -  [econometrics - Practical issues with dynamic panel data modeling](https://stats.stackexchange.com/questions/108351/practical-issues-with-dynamic-panel-data-modeling)
  - PPT [Lindner, 21010, Dynamic Panel Data Models](https://homepage.univie.ac.at/robert.kunst/pan2010_pres_lindner.pdf)
- 推文中使用 Stata 官方提供的范例数据，使用 `webuse abdata.dta, clear` 或 `webuse nlswork.dta` 这两份经典的面板数据。



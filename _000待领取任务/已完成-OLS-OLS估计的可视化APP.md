### 任务概览
- 根据 [A Shiny App for Playing with OLS](http://www.econometricsbysimulation.com/2013/11/a-shiny-app-for-playing-with-ols.html) 写一篇中文推文，介绍这个有趣的 APP。
  - [APP 链接](https://econometricsbysimulation.shinyapps.io/OLS-App/)
  - [APP 项目主页-R语言-Github](https://github.com/EconometricsBySimulation/OLS-demo-App/blob/master/server.R)
- 可以根据自己的理解加入一些 OLS 的公式和相关的解释及说明。


--- 
> 以下是已经完成部分：

>作者：连玉君 ([知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn))    

> Stata连享会 [精彩推文1](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文2](https://github.com/arlionn/stata/blob/master/README.md)
  
> 娱乐中窥视 OLS 的基本性质！[【点击开启欢乐之旅】](http://econometricsbysimulation.shinyapps.io/OLS-App/)

- 根据 [A Shiny App for Playing with OLS](http://www.econometricsbysimulation.com/2013/11/a-shiny-app-for-playing-with-ols.html) 写一篇中文推文，介绍这个有趣的 APP。
  - [APP 链接](https://econometricsbysimulation.shinyapps.io/OLS-App/)
  - [APP 项目主页-R语言-Github](https://github.com/EconometricsBySimulation/OLS-demo-App/blob/master/server.R)
- 可以根据自己的理解加入一些 OLS 的公式和相关的解释及说明。
- 可以用 LiceCap 软件做屏幕截图。

### 1. OLS 回顾
采用矩阵形式将线性回归模型表示如下：
$$
y = x\beta + \epsilon  \qquad  (1)
$$
若假设解释变量外生 (同时满足其它基本假设条件)，即 $E(x'\epsilon)=0$，则 OLS 估计式如下：
$$
\hat{\beta} = (x'x)^{-1}x'y  \qquad  (2)
$$
进一步假设 $\epsilon \sim N(0, \sigma^2)$，则 $\hat{\beta}$ 为：
$$
Var(\hat{\beta}) = \sigma^2(x'x)^{-1}  \qquad  (3)
$$
显然，若想提高 $\hat{\beta}$ 估计的准确程度，我们希望 $Var(\hat{\beta})$ 尽可能小一点。从公式 (3) 中可以看出，这取决于两个因素：
- **要点1：**  干扰项的变异程度。显然，$\sigma^2$ 越小越好：这意味着原始模型 (1) 中干扰项的方差 ($Var(\epsilon)=\sigma^2$) 越小越好。实证分析中，若模型设定中遗漏了重要的变量，或是某些不可观测的个体或时间特征没有得到很好的控制，它们都会**跑到**干扰项中，从而导致 $\sigma^2$ 过大。一个比较常用的处理方式就是在模型中加入反应个体或时间固定效应的虚拟变量。
- **要点2：** 解释变量的变异程度。若把 $(x'x)$ 视为 $Var(x)$，则 $(x'x)^{-1}$ 可以视为 $1/Var(x)$。换言之，解释变量的离散程度越高，则越容易更为准确地识别 $\beta$ 的参数估计值。实证分析中，在收集数据环节，就要尽可能保证样本的来源多样化，能够较好的反应母体的特征。

### 2. 蒙特卡洛模拟分析
#### 2.1 概览
```stata
cd "D:\stata15\ado\personal\Jianshu\OLS_simu_APP"
clear
set obs 151
set seed 1
gen x = rnormal(0, 0.25)
gen u = rnormal(0,1)
gen y = 2 + 3*x + u
*-图示
twoway (scatter y x) (lfit y x), ///
       scheme(tufte) legend(off)
*-更为简洁的做法
aaplot y x
graph export OLS_APP_01.png, replace
reg y x
```

![Stata连享会-OLS蒙特卡洛模拟-使用 aaplot 命令制图](https://upload-images.jianshu.io/upload_images/7692714-bee209e5eac767cd.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![OLS_app-01.gif](https://upload-images.jianshu.io/upload_images/7692714-b2b924c437a2dda6.gif?imageMogr2/auto-orient/strip)

![OLS_app-02-sample-size.gif](https://upload-images.jianshu.io/upload_images/7692714-6147367eb0432c7b.gif?imageMogr2/auto-orient/strip)

![OLS_app-03-varX.gif](https://upload-images.jianshu.io/upload_images/7692714-9c483effb7bd7c95.gif?imageMogr2/auto-orient/strip)

![OLS_app-04-varU.gif](https://upload-images.jianshu.io/upload_images/7692714-19f481238f8c5e16.gif?imageMogr2/auto-orient/strip)

![OLS_app-05-NLS.gif](https://upload-images.jianshu.io/upload_images/7692714-9915764e7cbc503d.gif?imageMogr2/auto-orient/strip)



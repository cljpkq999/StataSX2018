
参见 [NJC STATA PLOTS](https://stats.idre.ucla.edu/stata/faq/graph/njc-stata-plots/)

这篇推文中介绍了几十个非常有用的 stata 绘图命令，我们可以直接翻译过来，并加入一些命令，如 `coefplot`, `catplot` 等，让这个推文变成一个可以速查 stata 常用绘图命令的手册类型的文档。
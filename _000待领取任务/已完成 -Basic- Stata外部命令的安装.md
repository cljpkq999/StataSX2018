
- **目的：** 介绍如何下载和安装 Stata 外部命令。要让读者通过这篇推文了解到下载和使用外部命令时各种情形下的处理方式。
- **写法：**
  - 提供几个链接
  - 介绍 Stata 执行命令的机制：先从内存中找是否有已经调入的同名命令 (即 `program dir` 列出的那些命令)，继而到 `adopath` 列示的路径下找同名的 **ado** 文件。如果找到，就先讲此命令的文件读入内存，进而执行之；若找不到，则以红色字体显示 **xxx unreganized**。
  - 介绍 `adopath` 命令：`adopath +`， `adopath -`
  - 介绍 `net` 相关的几个命令：`net set`, `net install` 等命令
  - 介绍 `github` 命令：`github search`, `github install` 等。
  - 或许需要另写一篇介绍 `profile` 的推文，构成这篇推文的基础
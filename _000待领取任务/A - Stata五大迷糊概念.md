
> 参见：The Stata Journal (2015), 15, Number 2, pp. 501–511, Top 10 Stata “gotchas” [PDF下载](https://gitee.com/Stata002/Hello/raw/master/002-References/gotchas-SJ-15-2.pdf)

- 要点：从这篇文章里边挑取五个，最重要的。增加一些我们自己的理解。
- 建议：
  - 2.2 Gotcha 9: Each execution of a do-file has its own local space
  - 2.3 Gotcha 8: The capture command hides all errors
  - 2.1 Gotcha 10: Invalid initial values become missing values
  - 2.5 Gotcha 6: Missing values are not always treated as large positive
values 这一部分除了作者介绍的内容以外，还需要对Stata中的缺失值做一些补充的解释。
  - 2.7 Gotcha 4: Distinct numerical values may be stored as the same
value
  - 2.9 Gotcha 2: Preserved data are automatically restored


- IV 经典范例 - 教育回报率的估计
  - [伍德里奇书中范例- Wooldrige-chp15-IV估计（工具变量法）](http://fmwww.bc.edu/gstat/examples/wooldridge/wooldridge15.html)，对应的 [PPT](https://faculty.newpaltz.edu/yuanwen/files/2SLS.pdf)
- 【香樟推文0833】剪不断，理还乱 ——如何argue IV的排他性假设 [Link](https://mp.weixin.qq.com/s?__biz=MzAwMTExMTI1Nw==&mid=2650123942&idx=1&sn=72bdaad065fb61183fd1cdb4a9c27e86&chksm=82dfab10b5a82206dc2ea6e220fc3d143fd565d76635155cbfa87d2d07330a6713a4285e5628&mpshare=1&scene=1&srcid=0731kLxaObT4ioNUvgRa6th1&key=0e40aeace6ff4dc8a92bdcd49a16015efca8f3b91222f281e8b322a8edf010c4ccdf1d9e76d513ba8aa7a060c6d005b8ea95f37cd13347a660a78bbd4a66e024fb2565890c537357ad104d1a5d5b49f8&ascene=1&uin=MjAyMjQwODM2NA%3D%3D&devicetype=Windows+XP&version=62040549&pass_ticket=1dOFlQpv9qDUjoO%2B9ISkMutDvGkze45UYn0dXW%2BBu33c3x8OVa3qlHvA2MFlc9SX&winzoom=1##)。重点讲述了 Nathan Nunn and Leonard Wantchekon, The Slave Trade and the Origins of Mistrust in Africa. American Economic Review, 2011, Vol 101, No 7, pp.3221-3252. [[PDF]](https://gitee.com/arlionn/stata_training/raw/master/PDF/Nunn_Wantchekon_2011_AER.pdf)，我这里有这篇文章的完整数据和 dofiles.
- [How do instrumental variables work?](https://acarril.github.io/posts/IV-intro)， `2019/11/28 17:06` 新增


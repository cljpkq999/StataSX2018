
## 说明

这是我现场班的讲义中截取的有关聚类标准误的 Stata codes，可以按需截取到推文中。

对于二维聚类标准误，推荐使用 `vce2way` 和 `cgmreg` 命令。

```Stata
*--------------------------
*-4.2.3 聚类调整后的标准误 

*  -- Petersen2009 --                 cluter2.ado
*-Petersen, M. A., 2009, 
*  Estimating standard errors in finance panel data sets: 
*  Comparing approaches, 
*  Review of Financial Studies, 22 (1): 435-480.
  shellout "$R\Petersen-2009.pdf"              //Petersen2009, 面板 SE
  * Stata commnd for 2way clutered S.E.:  cluster2

*  -- CM2015 --                       
*-Cameron, C. A., D. L. Miller, 2015, 
*  A practitioner’s guide to cluster-robust inference, 
*  Journal of Human Resources, 50 (2): 317-372.  
  shellout "$R\Cameron_2015_ClusterSE_JHR.pdf" //cluster SE 使用手册

*  -- CGM2011 --                      cgmreg.ado |  vce2way.ado
*-Cameron, A. C., J. B. Gelbach, D. L. Miller, 2011, 
*  Robust inference with multiway clustering, 
*  Journal of Business & Economic Statistics, 29 (2): 238-249.  
  shellout "$R\Cameron_2011_ClusterSE.pdf"     //多维
  shellout "$R\Cameron_2011_ClusterSE_PPT.pdf" // Robust SE PPT 
  help cgmreg 
  help vce2way

*  -- IM2010 --                       clustse.ado | clustbs.ado
*-Ibragimov, Rustam and Ulrich K. Muller. 2010. 
*  t-Statistic Based Correlation and Heterogeneity Robust Inference.
*  Journal of Business and Economic Statistics 28(4):453-468.
  shellout "$R\Ibragimov_2010_clustse.pdf"
  help clustse  // 基于 Bootstrap 的聚类标准误 
  
*  -- IM2010 --                       clustbs.ado 
*-Cameron, A., J. Gelbach and D. Miller. 2008. 
*  Bootstrap-based improvements for inference with clustered errors.
*  Review of Economics and Statistics 90(3): 414-427.
  shellout "$R\Cameron_2008_RES_bsClusterSE.pdf" // 基于 Bootstrap 的 SE
  help clustbs   

  
*----------
*-应用范例：
  
*-一维 clustered S.E.

  reg wage $x, vce(cluster industry)
  est store m3
  *-Note: 
  * (1)   同行业内的个体的干扰项之间彼此相关;
  * (2) 不同行业之间的个体的干扰项之间彼此不相关；
  * (3) 系数估计值仍然采用OLS估计值，因为它是无偏的
  
*-二维 clustered S.E.

  cluster2 wage $x, fcluster(industry) tcluster(occupation) // Petersen-2009, RFS
  *-该命令没有帮助文件，所有功能都可以用 cgmreg 和 vce2way 代替
  * 因此，建议日后不必使用该命令
  
  help cgmreg  // CGM2011, Mitchell Petersen's -cluster2.ado- 的升级版
  cgmreg wage $x, cluster(industry occupation)	
  est store m4
  
  help vce2way // CGM2011, 支持 Panel data, xtreg 等命令
  vce2way reg wage $x, cluster(industry occupation)	
  est store m5
  
*-对比
  local m  "m1 m2 m3 m4 m5"
  local mt "OLS Robust 1Clus 2_cgmreg 2_vce2way"
  esttab `m', mtitle(`mt') nogap b(%4.3f) se(%6.4f) brackets ///
	          star(* 0.1 ** 0.05 *** 0.01) s(N r2) compress
/*
------------------------------------------------------------------------
              (1)          (2)          (3)          (4)          (5)   
              OLS       Robust        1Clus     2_cgmreg    2_vce2way   
------------------------------------------------------------------------
hours       0.053***     0.053***     0.053**      0.053**      0.053** 
         [0.0113]     [0.0112]     [0.0177]     [0.0219]     [0.0223]   
ttl_exp     0.251***     0.251***     0.251***     0.251***     0.251***
         [0.0308]     [0.0300]     [0.0398]     [0.0605]     [0.0627]   
tenure      0.019        0.019        0.019        0.019        0.019   
         [0.0252]     [0.0247]     [0.0266]     [0.0327]     [0.0351]   
south      -1.530***    -1.530***    -1.530***    -1.530***    -1.530***
         [0.2311]     [0.2281]     [0.2399]     [0.3400]     [0.3400]   
collgrad    3.190***     3.190***     3.190***     3.190***     3.190***
         [0.2690]     [0.3018]     [0.3932]     [0.4719]     [0.4719]   
married    -0.213       -0.213       -0.213       -0.213       -0.213   
         [0.2399]     [0.2507]     [0.2653]     [0.2672]     [0.2673]   
_cons       2.593***     2.593***     2.593**      2.593***     2.593***
         [0.5364]     [0.4619]     [0.9229]     [0.9813]     [0.9813]   
------------------------------------------------------------------------
N        2209.000     2209.000     2209.000     2209.000     2209.000   
r2          0.150        0.150        0.150        0.150        0.150   
------------------------------------------------------------------------
Standard errors in brackets
* p<0.1, ** p<0.05, *** p<0.01                                         */ 

*-Comments:
* 1. 考虑异方差后的 SE 通常会比同方差(Homo)下的 SE 大一些; 但并不绝对如此;
* 2. 聚类调整后的 SE 要比 Homo SE 大, 如本例中 SE[_cons] 
* 3. 本例中，二维聚类 SE 约为 Homo SE 的两倍 
* 4. 在截面数据或面板数据中，通常都要使用聚类 SE
* 5. cluster(industry) 同时考虑了行业层面的异方差, 
*    以及行业内部不同公司之间的相关性;
* 6. Q: cluster(firm) 是什么含义？
 
 
*              (self-reading)
*-小样本下的聚类标准调整，参考文献请查阅帮助文件
*-Stata 14.2 以上版本  
 
  help reg_sandwich //small-sample corrections for cluster-robust SE

*-----------------------------------------Begin----------- 
preserve
  sysuse "nlsw88.dta", clear
	set seed 135
	sample 50, count  // 随机抽取50个观察值
	tab industry
	global x "hours ttl_exp tenure south collgrad married"
  *-官方命令，未作小样本纠偏  
	reg wage $x, cluster(industry) 
    est store m1	
  *-纠偏后	
	reg_sandwich wage $x, cluster(industry) 
	est store m2
restore
	esttab m1 m2, mtitle(reg reg_sandwich) nogap se(%6.4f)  ///
	       star(* 0.1 ** 0.05 *** 0.01)
*-----------------------------------------Over------------	 
  *-纠偏前的 SE 通常偏小，导致 t 值偏大
```
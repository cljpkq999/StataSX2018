

# 连享会 - 内生性专题开班啦！

2019 年 11 月 14 日，「[连享会 - 2019年内生性专题研讨会](https://mp.weixin.qq.com/s/T1pOk216yLTLPJg_3YU1xw)」在南京财经大学科技园隆重开班。本次研讨班由中山大学林建浩和南开大学王群勇作为主讲嘉宾进行授课，学员们的学习热情高涨，共有 150 余位来自全国各地的老师和学生参加了这一次培训。


![林建浩老师授课中](https://images.gitee.com/uploads/images/2019/1116/093103_0fae4c56_1522177.jpeg "林建浩老师授课中")

## 我们要学什么？

本次培训为期四天，主要有以下内容：
- [Day1](https://mp.weixin.qq.com/s/-dFPrGQMhH3JzA4tEc35kQ): 传统IV；**弱 IV** 和 **近似外生 IV**
- [Day2](https://mp.weixin.qq.com/s/-dFPrGQMhH3JzA4tEc35kQ): 面板数据模型和 **倍分法**
- [Day3](https://mp.weixin.qq.com/s/-dFPrGQMhH3JzA4tEc35kQ): **匹配分析**（matching）和 **断点回归**分析（RDD）
- [Day4](https://mp.weixin.qq.com/s/-dFPrGQMhH3JzA4tEc35kQ): 离散选择模型的内生性问题与估计；**内生处理效应** 模型

课程伊始，林建浩老师从 「为制度寻找工具变量」和「教育回报率」的经典问题研究出发给学员们介绍了计量模型中内生性问题的成因、本质、IV估计核心思想及一般步骤，并结合 Angrist and Krueger（1991）的研究引出弱工具变量。

第一天下午林老师依次详解传统IV估计和新近发展的弱IV及近似外生架构下的估计及统计推断方法。工具变量（IV）估计最核心的两个假设：一是工具变量与内生解释变量存在相关性；二是工具变量具有外生性。显然工具变量的“质量”会在很大程度上影响在有限样本下IV估计量的表现。然而完全满足上述假设的工具变量寻找非常困难。越来越多的计量经济学家开始放松传统IV估计的模型设定，提出“弱工具变量”（Weak Instruments）和 “近似外生工具变量”（plausibly / approximately / exogenous）下的稳健推断方法。

在授课过程中，为了使学员们更好了解工具变量估计模型的应用与 Stata 操作，林老师演示了上机代码操作并解读运行结果，重点对 IV 估计不可识别检验、弱工具变量检验、过度识别检验等进行讲解。林老师结合自身投稿经验提醒大家要严谨对待遗漏变量的问题。最后，在近似外生工具变量的实证检验中结束了一天的课程。

## 互动交流

为了更好的解答学员们在学习中遇到的疑问，在课堂结束后，林老师用半个小时专门为学员们解疑答惑，通过对学员问题的解答提高了学员对内生性问题的理解。

![林建浩老师答疑中-记者招待会](https://images.gitee.com/uploads/images/2019/1116/093103_ae7f7b33_1522177.jpeg "林建浩老师答疑中")

![林建浩老师答疑中-课后小灶](https://images.gitee.com/uploads/images/2019/1116/093103_0b2f4ee4_1522177.jpeg "林建浩老师答疑中-课后小灶")

一天的课程结束后，不少学员和老师进行沟通，讨论自身论文写作与投稿中遇到的问题，林老师给出行之有效的建议，引来其他学员的聆听。

---

&emsp;
> #### [2020寒假Stata现场班](https://mp.weixin.qq.com/s/-dFPrGQMhH3JzA4tEc35kQ) (北京, 1月8-17日，连玉君-江艇主讲)
> #### [「+助教招聘」](https://mp.weixin.qq.com/s/_T6venQ_CFHAUQL4glB2sA)
[![2020寒假Stata现场班](https://images.gitee.com/uploads/images/2019/1116/093103_4f2077a1_1522177.jpeg "2020寒假Stata现场班-扫码了解详情")](https://mp.weixin.qq.com/s/-dFPrGQMhH3JzA4tEc35kQ)

![2020寒假现场班全程班链接二维码](https://images.gitee.com/uploads/images/2019/1116/093103_b988dbda_1522177.png "2020寒假现场班全程班链接二维码")



&emsp;

>#### 关于我们

- **Stata连享会** 由中山大学连玉君老师团队创办，定期分享实证分析经验。
- 推文同步发布于 [CSDN](https://blog.csdn.net/arlionn) 、[简书](http://www.jianshu.com/u/69a30474ef33) 和 [知乎Stata专栏](https://www.zhihu.com/people/arlionn)。可在百度中搜索关键词 「Stata连享会」查看往期推文。
- 点击推文底部【阅读原文】可以查看推文中的链接并下载相关资料。
- **欢迎赐稿：** 欢迎赐稿。录用稿件达 **三篇** 以上，即可 **免费** 获得一期 Stata 现场培训资格。
- **E-mail：** StataChina@163.com
- [**往期精彩推文**：一网打尽](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw)

---
[![欢迎加入Stata连享会(公众号: StataChina)](https://images.gitee.com/uploads/images/2019/1116/093103_0bcfc034_1522177.png)](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw)






 

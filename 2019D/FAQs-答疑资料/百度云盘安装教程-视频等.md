我在百度云盘中存放了所有需要预先安装的软件，以及安装教程的 PDF 和视频资料，请各位预先安装，并详细记录你的安装过程，以便在学员答疑时可以自如应对。

**空间计量培训软件安装教程资料**

> 链接:  https://pan.baidu.com/s/1sVIPNbMEdXEqV-fjRhMcLQ&shfl=shareset          
> 提取码: pdcd 

**杨海生老师此前提供的参考文献：**

> 链接： https://pan.baidu.com/s/1AKGHYE-HcfDfm5keCGF8xQ&shfl=sharepset           
> 提取码：mz80 

&emsp;

> 作者：韩少真(西北大学) || 展金永（对外经济贸易大学）
>       
> Stata连享会 [计量专题](https://mp.weixin.qq.com/s/hWtncj56PeFNL4yg2-va0Q)  || [公众号合集](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw)

[![点击查看完整推文列表](https://upload-images.jianshu.io/upload_images/7692714-0e9627ca839252d1.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw)


[![](https://upload-images.jianshu.io/upload_images/7692714-0e75288940706b45.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)](https://mp.weixin.qq.com/s/aU6B9HZaf2BSHF7lSLe6LA)


&emsp;

> 或许你已经感觉得到这个世界的变化：
>
> 古代侠客们都是终身一样兵器，或刀或剑，行走江湖；
>
> 现代的江洋大盗则是专业路线，紧身衣上配备了各种装备，来去自如。

言归正传，就学术研究而言，目前在很多 Top 期刊上发表的论文都会综合使用多种计量方法和软件来完成文中的实证分析工作。这意味着，我们需要一个工作平台，以便有效整合手头的 2-3 中软件和语言，不必频繁地地转场。

今天要介绍的 Anaconda 就是这么个玩意儿，其配置的 Jupyter Notebook 便是我们所需的分析和写作平台。你可以在一份文档中同时使用 Stata，Python，R 多种语言来执行实证分析，同时使用 Markdown 等写作，并将完成后的文档输出为 Word，PDF，HTML 甚至是幻灯片等多种格式。

## 1. 准备工作-关联前提

首先，请安装 Anaconda 和 Stata，并检验 Anaconda 和 Stata 是否已经成功安装在本机电脑上。详情参见 [Anaconda 的安装和使用](https://mp.weixin.qq.com/s/w7MsTIJXiCdQVXrgEr3Dww)，[Anaconda安装到详细介绍](https://mp.weixin.qq.com/s/RV806l5op0FbLwVdYxNaEQ)，以及 [Stata 15软件安装教程](https://mp.weixin.qq.com/s/qj0rLt3K37YJH5LI_TQrkQ)。

## 2. 将 stata 添加到命令行注册

根据下图步骤，以管理员身份运行 **Windows PowerShell** (Note: 按下 Wins 花键，在弹出菜单中下拉到 W 处即可找到)。**划重点：** 请务必以管理员身份运行。

![image](https://upload-images.jianshu.io/upload_images/7692714-dd2d5972209ac37f.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

打开 stata 的安装文件夹，根据下图获取 stata 的安装路径：

![image](https://upload-images.jianshu.io/upload_images/7692714-7924d81c027f610a.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

在 **Windows PowerShell** 执行 cd 命令，以进入 stata 程序安装的路径。cd 命令后接上步所获取的 stata 安装路径。根据个人电脑安装路径不同有所差异。路径请以英文引号包围，这样可以避免路径文件夹名称中包含空格导致无法顺利进入目标路径。

```stata
cd "D:\Stata15"
```

执行上述命令后，请根据下图提示，确认是否已进入stata安装路径：
![image](https://upload-images.jianshu.io/upload_images/7692714-d58c3ab4fc5f226e.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

当然，也可以在 **Windows PowerShell** 中执行 `dir` 命令，然后根据下图，观察当前路径的文件内容和stata安装路径的内容是否一致。如果两者一致，说明已成功进入 stata 安装路径。如果不一致，则应仔细检查操作流程，重新执行以确定进入 stata 安装路径。
![image](https://upload-images.jianshu.io/upload_images/7692714-05b8d36f8db9094e.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

在 **Windows PowerShell** 中执行 `.\StataMP-64.exe /Register` 命令，则可以成功将 stata 添加到命令行注册。**需要注意的是：** `.\StataMP-64.exe /Register` 中的 `.\StataMP-64.exe` 部分，根据个人电脑安装 stata15 版本有所差异。我电脑安装的是 MP 版，所以为 `.\StataMP-64.exe`。如果安装的是 SE 版，应该为 `.\StataSE-64.exe`。
```
.\StataMP-64.exe /Register
```
如果是 SE 版，则为:
```
.\StataSE-64.exe /Register
```

如果依然担心出错，还有一种简单办法可以确定 `.\StataMP-64.exe /Register` 中的`.\StataMP-64.exe` 部分。打开个人电脑中 stata 安装路径文件夹，找到 stata 程序的执行文件，然后按下图所示，将完整的带 **.exe** 的文件名复制下来，然后将其替换 `StataMP-64.exe' 部分。再进行命令行注册。
![image](https://upload-images.jianshu.io/upload_images/7692714-46e397865a6177d4.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

## 3. 在 Anaconda 中安装 **stata_kernel** 包

根据下图打开**Anaconda prompt**：

![image](https://upload-images.jianshu.io/upload_images/7692714-9ca97d7991181e93.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

在 **Anaconda prompt** 中逐行执行以下代码，第一行是为了升级 conda，第二行是升级 pip 包。

根据 **Anaconda prompt** 窗口的提示，可能要输入
`y`
来进行包的升级。如果已经是最新版本，则不会提醒升级，略过此步骤即可。升级包管理模块的目的是保证 anaconda 中的包管理模块是最新版本，这样可以降低后续安装 **stata_kernel** 包出错的概率。

```python
conda update conda
python -m pip install --upgrade pip
```

在 **Anaconda prompt** 逐行执行以下代码，安装 **stata_kernel** 包。安装结束后可根据**Anaconda prompt** 窗口的提示，判断是否成功安装。
```python
pip install  stata_kernel
python -m stata_kernel.install
```
- 为检验 **stata_kernel** 包是否已经成功安装，请在 **Anaconda prompt** 执行`conda list` 代码，这会在 **Anaconda prompt** 窗口展示当前 python 环境下安装的所有包，以英文字母排序。请查看是否包含 **stata_kernel** 包。如果包含，这说明 **stata_kernel** 包已经成功安装。
```python
conda list
```
具体如下图所示：
![image](https://upload-images.jianshu.io/upload_images/7692714-aa853bc5d4145620.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

## 4. 打开 Jupyter Notebook，新建 stata 语法格式的 notebook，执行 stata 代码，检验是否关联成功。

- 根据下图，打开`Anaconda Navigator`
![image](https://upload-images.jianshu.io/upload_images/7692714-3c34b5ef0da6e718.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

- 根据下图，在`Anaconda Navigator`窗口打开`jupyter notebook`。这会在浏览器中弹出一个类似于网页的窗口，就是`jupyter notebook`。
![image](https://upload-images.jianshu.io/upload_images/7692714-870a7c30d9f4f2a2.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

- 根据下图，在`jupyter notebook`中执行以下操作，新建一个支持stata语法的notebook。这会弹出一个新的网页标签。
![image](https://upload-images.jianshu.io/upload_images/7692714-ae78fb8113b377a4.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

- 在新建的notebook中，通过下图可以初步判断是否关联成功。
![image](https://upload-images.jianshu.io/upload_images/7692714-91f944916771e647.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

- 在命令行输入stata命令，并点击`运行`执行。如果关联成功，则会在命令的下方显示stata结果窗口的结果。具体如下图所示：

```stata
dis 1+3
sysuse auto,clear
reg price weight
scatter price weight
```
![image](https://upload-images.jianshu.io/upload_images/7692714-0f9c14502eca63f6.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

&emsp;
> #### [连享会计量方法专题……](https://gitee.com/arlionn/Course/blob/master/README.md)


>#### 关于我们

- 【**Stata 连享会(公众号：StataChina)**】由中山大学连玉君老师团队创办，旨在定期与大家分享 Stata 应用的各种经验和技巧。
- 公众号推文同步发布于 [CSDN-Stata连享会](https://blog.csdn.net/arlionn) 、[简书-Stata连享会](http://www.jianshu.com/u/69a30474ef33) 和 [知乎-连玉君Stata专栏](https://www.zhihu.com/people/arlionn)。可以在上述网站中搜索关键词`Stata`或`Stata连享会`后关注我们。
- 点击推文底部【阅读原文】可以查看推文中的链接并下载相关资料。
- Stata连享会 [计量专题](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文](https://github.com/arlionn/stata/blob/master/README.md)

>#### 联系我们

- **欢迎赐稿：** 欢迎将您的文章或笔记投稿至 StataChina@163.com，我们会保留您的署名；录用稿件达`五篇`以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。您也可以从 [连享会选题平台](https://gitee.com/Stata002/StataSX2018/wikis/Home) &rarr; [002_备选主题] 中选择感兴趣的题目来撰写推文，网址为：[https://gitee.com/Stata002/StataSX2018/wikis/Home](https://gitee.com/Stata002/StataSX2018/wikis/Home)。
- **招募英才：** 欢迎加入我们的团队，一起学习 Stata。合作编辑或撰写稿件五篇以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。
- **联系邮件：** StataChina@163.com

>#### 往期精彩推文
> Stata连享会 &emsp; [计量专题](https://gitee.com/arlionn/Course/blob/master/README.md)  || [精品课程](https://mp.weixin.qq.com/s/hWtncj56PeFNL4yg2-va0Q) || [简书推文](https://www.jianshu.com/p/de82fdc2c18a)  || [公众号合集](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw) 

[![点击查看完整推文列表](https://upload-images.jianshu.io/upload_images/7692714-b61ba30298396017.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)](https://gitee.com/arlionn/Course/blob/master/README.md)

---
![欢迎加入Stata连享会(公众号: StataChina)](https://upload-images.jianshu.io/upload_images/7692714-2178507e5a3f67a2.jpeg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "扫码关注 Stata 连享会")



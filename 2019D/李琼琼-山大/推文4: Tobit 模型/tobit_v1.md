## 1. Tobit 模型的介绍

### 1.1 受限数据：截断和截堵
实证研究中，我们获取的数据往往存在「截断 (Truncated)」或「截堵 (Censored)」特征。例如，在研究收入和消费的关系时，我们获得的 **个人收入** (Income) 数据往往并不是完全连续的。出于个人隐私的考虑，部分受访者 (高收入或低收入) 往往不愿意透漏自己的具体收入。此时，会产生两种情形：(1) **截断数据**。年收入超过 50 万元的受访者可能不愿意填写具体的收入数据，导致这部分人的 Income 观测值为缺失值 —— 我们的数据在 50 万元处被截断了；(2) **截堵数据**。年收入超过 50 万元的受访者只告诉我们「收入为 50 万元以上」，无奈之下只能记录为 50 万元 —— 收入超过 50 万元的观察值都堆堵在了 50 万元的位置。显然，后者的数据信息要丰富一些。

我们将上述情形称为「受限因变量」，对应地就衍生出 「**截断回归模型**」 (truncated regression models) 和 「**截堵回归模型**」(censored regression models)。文献中，后者的别名还包括：「**归并回归模型**」和「**审查回归模型**」。

显然，上述两个例子都属于 **右侧受限**，我们可以将其推广到 **左侧受限** 或 **双侧受限** 的情形。左侧的受限点称为 **LL** (Lower limit 或 Left Limit)，右侧的受限点称为 **UL** (Upper limit) 或 **RL** (Right limit)。

### 1.2 Tobit 模型设定
对于截堵数据，当 LL = 0，RL = 正无穷 时，此模型就是所谓的「规范审查回归模型」，又称为 Tobit 模型 (Tobin 1958，[[PDF]]()) 。模型设定如下：
$$
\begin{aligned} y_{i}^{*} &=\mathbf{x}_{i}^{\prime} \boldsymbol{\beta}+u_i  \\
u_{i} & \sim N\left(0, \sigma^{2}\right)  \end{aligned}
$$ $$
y_{i}=\left\{\begin{array}{cl}{y_{i}^{*}} & {\text { if } y_{i}^{ *}>0} \\ {0} & {\text { if } y_{i}^{ *}\leqslant0}\end{array}\right.$$
当潜变量 $y^{*}$ 小于等于 0 时，被解释变量 $y$ 等于 0； 当 $y^{*}$ 大于 0 时，被解释变量 $y$ 等于 $y^{*}$ 本身，同时假设扰动项 $u_i$ 服从均值为 0 ，方差为 $\sigma^{2}$ 正态分布。


### 1.3 Tobit 模型的估计
由于使用 OLS 对整个样本进行线性回归，其非线性扰动项将被纳入扰动项中，导致估计不一致，Tobit 提出用 **MLE** 对模型进行估计。

我们先对该混合分布的概率密度函数进行推导, 再写出其对数似然函数。

当 $y_i = 0 $时，
$$
\begin{aligned} \mathrm{P}(y_i=0 | \mathbf{x_i}) &=\mathrm{P}\left(y_i^{*}<0 | \mathbf{x_i}\right)=\mathrm{P}(u_{i}<-\mathbf{x_i}^{\prime} \boldsymbol{\beta} | \mathbf{x_i}) \\ &=\mathrm{P}(u_{i}/ \sigma<-\mathbf{x_i}^{\prime} \boldsymbol{\beta} / \sigma | \mathbf{x_i})=\Phi(-\mathbf{x_i}^{\prime} \boldsymbol{\beta} / \sigma)\end{aligned} $$
当 $y_i > 0 $时，
$$
\begin{aligned} \mathrm{P}(y_i>0 | \mathbf{x_i}) &=\mathrm{P}\left(y_i^{*}>0 | \mathbf{x_i}\right)=  1 - \mathrm{P}\left(y_i^{*}\leq0 | \mathbf{x_i}\right) \\
&=1-\mathrm{P}(u_{i}\leq-\mathbf{x_i}^{\prime} \boldsymbol{\beta} | \mathbf{x_i})=1 - \mathrm{P}(u_{i} / \sigma\leq-\mathbf{x_i}^{\prime} \boldsymbol{\beta} / \sigma | \mathbf{x_i})\\
&= 1-\Phi(-\mathbf{x_i}^{\prime} \boldsymbol{\beta} / \sigma)=\Phi(\mathbf{x_i}^{\prime} \boldsymbol{\beta} / \sigma) \end{aligned}
$$
**概率密度函数为：**
$$
f\left(y_{i} | \mathbf{x}_{i}\right)=\left[\Phi\left(-\frac{\mathbf{x}_{i}^{\prime} \boldsymbol{\beta}}{\sigma}\right)\right]^{I_{y_{i}=0}}\left[\frac{1}{\sigma} \phi\left(\frac{y_{i}-\mathbf{x}_{i}^{\prime} \boldsymbol{\beta}}{\sigma}\right)\right]^{I_{y_{i}=0}}
$$ 其中 $I$ 为示性函数，当下标所表示的条件正确时取值为 1，否则为 0。

**整个样本的对数似然函数为：**
$$
\log L=\sum_{i=1}^{n}
\left\{ I_{y_{i}=0} \ln \left[\Phi\left(-\frac{\mathbf{x}_{i}^{\prime} \boldsymbol{\beta}}{\sigma}\right)\right]
+I_{y_{i}>0} \ln \left[\frac{1}{\sigma} \phi\left(\frac{y_{i}-\mathbf{x}_{i}^{\prime} \boldsymbol{\beta}}{\sigma}\right)\right]
\right \}
$$
通过使 $\log L$ 最大化来求出 $\beta$ 和 $\sigma$。

### 1.4 Tobit 模型的假设检验
Tobit 模型的假设检验是通过**似然比检验** (Likelihood Ratio Test, LR) 来实现的，该检验的**原假设**为：
$$H_{0}: \boldsymbol{\beta}=\boldsymbol{\beta}_{0}$$
**LR 统计量**为：
$$
L R=-2\left(\ln L_{r}-\ln L_{u}\right) \sim \chi^{2}(j)
$$ 其中 $\ln L_{r}$ 是有约束的 ML 估计得到的似然函数值，$\ln L_{u}$ 为无约束 ML 得到的似然函数值，如果 $H_{0}$ 正确，则 $\ln L_{r}-\ln L_{u}\$ 不应该为很大。

### 1.5 边际效应及其推导过程
在 Probit 模型和  Logit 模型等非线性模型中，估计量 $\boldsymbol{\beta}_{MLE}$ 并非边际效应 (marginal effects)，需要进行一定的转换。Tobit 模型也是一个非线性模型，估计量 $\boldsymbol{\beta}$ 无法直接作为被解释变量 $y$ (相当于截堵型被解释变量 ) 的边际效应, 但可以作为潜变量 $y^{*}$的边际效应，因为 $\boldsymbol{\beta}$ 与潜变量 $y^{*}$ 是线性关系。此外，$\boldsymbol{\beta}$ 可以表示变量 $y|y>0$ (相当于截断型被解释变量) 的期望。 下面我们从期望和偏效应入手，推导 $\boldsymbol{\beta}$ 与三种变量 $y^*、 y  和  y|y>0$ 的边际效应的关系。

&ensp;**潜变量 $y^{*}$ 的期望和边际效应**
- 潜变量 $y^{*}$ 关于 $\mathbf{x}$ 期望：
$$
\mathrm{E}(y^{*} | \mathbf{x}) =\mathbf{x} \boldsymbol{\beta}
$$
- 变量 $x_j$ 对潜变量 $y^{*}$ 偏效应 (partial effects)
$$
\partial \mathrm{E}(y^{*} | \mathbf{x}) / \partial x_{j}=\beta_{j}
$$
&ensp;**截断型被解释变量 $y| y >0$ 的期望和边际效应**
- 被解释变量 $y$ 关于 $y>0,\mathbf{x}$ 的期望 (又称为 **"条件期望"** ):
$$
\begin{aligned}\mathrm{E}(y | y>0, \mathbf{x})
&=\mathbf{x} \boldsymbol{\beta}+\mathrm{E}(u | u>-\mathbf{x} \boldsymbol{\beta}) \\
&=\mathbf{x} \boldsymbol{\beta}+\sigma \mathrm{E}[(u / \sigma) |(u / \sigma)>-\mathbf{x} \boldsymbol{\beta} / \sigma ]\\
 &=\mathbf{x} \boldsymbol{\beta}+\sigma \phi(\mathbf{x} \boldsymbol{\beta} / \sigma) / \Phi(\mathbf{x} \boldsymbol{\beta} / \sigma) \\
&=\mathbf{x} \boldsymbol{\beta}+\sigma \lambda(\mathbf{x} \boldsymbol{\beta} / \sigma) \end{aligned}
$$
其中 $\lambda(c) =\phi(c) / \Phi(c)$ 被称为 **逆米尔斯比率** (inverse Mills ratio), 是标准正态 pdf 和标准正态 cdf 在 $c$ 处之比。

- 变量 $x_j$ 对变量 $y$ 在 $y>0,\mathbf{x}$ 条件下的偏效应 (partial effects)：
$$
\begin{aligned}\partial \mathrm{E}(y | y>0, \mathbf{x}) / \partial x_{j} &=\beta_{j}+ \sigma \cdot \frac{d \lambda}{d c}  \frac{d c}{d x_{j}} = \beta_{j}+ \beta_{j} \cdot \frac{d \lambda}{d c} \\
&= \beta_{j}\{1-\lambda(\mathbf{x} \boldsymbol{\beta} / \sigma)[\mathbf{x} \boldsymbol{\beta} / \sigma+\lambda(\mathbf{x} \boldsymbol{\beta} / \sigma)]\}
\end{aligned}$$
上式说明 $x_j$ 对变量 $y$ 在 $y>0,\mathbf{x}$ 条件下的偏效应不仅取决于 $\beta_j$，而且受到 $\{\cdot\}$ 项的影响。

&ensp;**截堵型被解释变量 $y$ 的期望和边际效应**
- 被解释变量 $y$ 关于 $\mathbf{x}$ 的期望 (又称为 **"无条件期望"** ):
$$
\mathrm{E}(y | \mathbf{x})=\mathrm{P}(y>0 | \mathbf{x}) \cdot \mathrm{E}(y | y>0, \mathbf{x})=\Phi(\mathbf{x} \boldsymbol{\beta} / \sigma) \cdot \mathrm{E}(y | y>0, \mathbf{x})
$$

- 变量 $x_j$ 对 $y$ 在 $\mathbf{x}$ 条件下的偏效应 (partial effects)：
$$
\frac{\partial \mathrm{E}(y | \mathbf{x})}{\partial x_{j}}=\frac{\partial \mathrm{P}(y>0 | \mathbf{x})}{\partial x_{j}} \cdot \mathrm{E}(y | y>0, \mathbf{x})+\mathrm{P}(y>0 | \mathbf{x}) \cdot \frac{\partial \mathrm{E}(y | y>0, \mathbf{x})}{\partial x_{j}}
$$
经过化简后可得：
$$
\frac{\partial \mathrm{E}(y | \mathbf{x})}{\partial x_{j}}=\beta_{j} \Phi(\mathbf{x} \boldsymbol{\beta} / \sigma)
$$

对以上三种边际效应进行总结：
| 解释变量的偏效应 | 函数形式  |
| :---:         |     :---:      |
|对潜变量 $y^{*}$ 的偏效应  |  $\partial \mathrm{E}(y^{*} \| \mathbf{x}) / \partial x_{j} = \beta_{j} $ |
|对变量 $y$ (左截断 0) 偏效应  |  $\partial \mathrm{E}(y \| y>0, \mathbf{x})/ \partial x_{j}  = \beta_{j}\{1-\lambda(c)[c+\lambda(c)]\} $ |
|对变量 $y$ (左截断 0) 偏效应  |  $\partial \mathrm{E}(y \| \mathbf{x})/ \partial x_{j}  = \beta_{j}\Phi(c) $ |
注：$c = \mathbf{x} \boldsymbol{\beta} / \sigma$


## 2. Stata 范例
### 2.1 模型估计的实现
Stata 提供 `tobit` 命令对归并回归模型进行估计。 在命令窗口中输入 `help tobit` 命令即可查看其完整帮助文件。`tobit` 命令的基本语法为：
```stata
 tobit depvar [indepvars] [if] [in] [weight]，11[(#)] ul[(#)] [options]
```
其中 `ll[(#)]` 表示左归并，# 是左侧受限点的具体值 ；`ul[(#)]` 表示右归并，# 是右侧受限点的具体值。在实际运用中，可以只选择左归并或者右归并，也可以同时选择。

下面以研究影响 **非住院医疗费用** 的因素为例，我们来对如何使用 Stata 做 **Tobit** 模型估计进行详细的介绍。

非住院医疗费用 (ambulatory expenditure，ambexp) 作为被解释变量，解释变量包括：年龄 (age), 是否为女性 (female), 教育年限 (educ) 以及  totchr, totchr 和 ins 等变量。

首先对被解释变量进行观察，
![图 1：非住院医疗费用的描述性统计](https://upload-images.jianshu.io/upload_images/18306645-40470c3f0ba2b661.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

从上图可以发现，有超过 10% 的比例的被解释变量其数值为0， 这个时候我们考虑进行线性 Tobit 模型 (linear tobit model) 估计。具体的命令和估计结果如下

```stata
use mus16data.dta, clear
global xlist age female educ blhisp totchr ins // 定义将所有的解释变量定义为全局变量 $xlist
tobit ambexp $xlist, ll(0)
```
![图 2: Tobit 模型回归结果](https://upload-images.jianshu.io/upload_images/18306645-b07f457498c3d7b5.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


### 2.2 偏效应估计
在做完回归之后，使用 `margins` 命令分别进行三种偏边际效应的估计

- 对潜变量 $y^{*}$ 的偏效应
```Stata
margins, dydx(*)
```
![图 3：对潜变量的平均边际效应 ](https://upload-images.jianshu.io/upload_images/18306645-78826780611f57cb.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

解释：以教育的为例，教育年限对在非住院医疗上的 **预期花费** 平均边际效应为 70.87。

- 对 $ y | y>0 $ 偏效应
```Stata
margins, dydx(*) predict(e(0,.))
```
![图 4：对变量 y 在 y > 0 的条件下的平均边际效应](https://upload-images.jianshu.io/upload_images/18306645-5ed513b29c74e7c2.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
解释：相当于截断模型的平均边际效应，在非住院医疗费用的实际支出大于 0 的样本中，教育年限对于非住院医疗费用的实际支出的平均边际效应为 33.34。


- 对 $ y $ 的偏效应
```Stata
margins, dydx(*) predict(  e(0,.))
```
![图 5：对变量 y 的平均边际效应](https://upload-images.jianshu.io/upload_images/18306645-3b483fd9788f7569.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

解释：教育年限对于非住院医疗费用的实际支出的平均边际效应为 45.44。


## 3. 结论
在做实证研究时，虽然拥有全部的观测数据， 但是部分观测数据的被解释变量 $y$ 都被压缩在 0 这一个点上。此时，无论是整个样本还是去掉 $y$ 为 0 的样本，都无法通过 OLS 得到一致估计。因此需要使用 Tobit 模型来解决数据的截堵问题。

## 参考文献
1.  Davidson R, MacKinnon J G. Econometric theory and methods[M]. New York: Oxford University Press, 2004. [[PDF]](https://quqi.gblhgk.com/s/880197/t8EZJgtFyp23kfHc)
2.  Wooldridge J M. Econometric analysis of cross section and panel data[M]. MIT press, 2010. [[PDF]](https://quqi.gblhgk.com/s/880197/1y6HA8CnmKjxbZxh)
3.  Wooldridge J M. Introductory econometrics: A modern approach[M]. Nelson Education, 2016. [[PDF]](https://quqi.gblhgk.com/s/880197/9yh5m8OtHdEcHnKh)
4.  Cameron A C, Trivedi P K. Microeconometrics Using Stata[J]. Stata Press books, 2010. [[PDF]](https://quqi.gblhgk.com/s/880197/Tts1JToBS6HPW4kh)

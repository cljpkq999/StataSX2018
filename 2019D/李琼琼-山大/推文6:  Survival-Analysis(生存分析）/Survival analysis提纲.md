生存分析
## 背景：



## 1.生存分析介绍
### 1.1 基本介绍
生存分析 (survival analysis) 又被称为久期分析 (duration analysis) 、转换分析(transition analysis)、风险分析 (hazrd analysis)、信度分析(reliability analysis)、失效时间分析(failure-time analysis)、历史事件分析(event history analysis)等，主要研究 **关注事件** 发生所需要的时间，具体可归纳为三类：
-	定性变量状态变化所花费的时间, 比如离婚、晋升、死亡
-	定量变量出现急剧变化所经历的时间, 比如人口总和出生率锐减  (见 Figure 1)
-	定量变量超过某个阈值所需要的时间, 比如将体重减到健康标准  (见 Figure 2)

![Figure 1: Quantitative variables change sharply](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Survival analysis_figure1.png)


![Figure2: Quantitative variables cross a threshold](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/survival analysis_figure2.png)

### 1.2生存分析的优势
传统模型存在归并问题(censoring)

## 2. 生存分析模型
风险函数等
## 3. 生存分析案例及Stata命令
### 3.1 生存分析命令的介绍
An Introduction to Survival Analysis Using Stata -Third Edition

### 3.2国内研究实例
地方官员的变更与企业的兴衰（刘海洋等，2017(1) 中国工业经济）
## 4. 结语

## 参考文献
## Appendix

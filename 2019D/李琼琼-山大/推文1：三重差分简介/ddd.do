clear all 
xtset company year
gen lnzjz=ln(zjz+1)
gen lnlabor=ln(labor+1)
gen lnzjtr=ln(zjtr+1)
gen lncapital=ln(capital1+1)
levpet lnzjz, free(lnlabor) proxy(lnzjtr) capital(lncapital)   // TFP估计过程
predict tfp,omega
gen lntfp=ln(tfp)
gen tt=time*treat
gen lnzc=ln(zc+1)
gen lnzlb=ln(zlb+1)
gen lnaj=ln(aj+1)
gen lnlabor=ln(labor+1)
gen ttt=tt*so2
gen times=time*so2
gen treats=treat*so2
gen tf1=tf*time
gen tf2=tf1*tf1
gen lnrev=ln(revenue+1)
gen tt1=treat*year04
gen tt2=treat*year05
gen tt3=treat*year06
gen tt5=treat*year08
gen tt6=treat*year09
gen tt7=treat*year10
gen tt8=treat*year11
gen tt9=treat*year12
gen tt10=treat*year13
gen tt11=treat*year14
gen tt12=treat*year15
reg lntfp tt i.year i.area i.ind if so2==1,cluster(area)
reg lntfp tt zcsy lf age owner sczy lnaj lnlabor lnzlb i.year i.area i.ind if ///
so2==1,cluster(area) 
xtreg lntfp tt i.year i.company if so2==1,cluster(area)
set matsize 10000
xtreg lntfp tt zcsy lf age owner sczy lnaj lnlabor lnzlb i.year i.company if ///
so2==1,cluster(area)


/*  三重差分 */
reg lntfp ttt tt treats times so2 i.year i.area i.ind ,cluster(area)
reg lntfp ttt tt treats times so2 zcsy lf owner age sczy lnaj lnlabor lnzlb i.year i.area ///
i.ind ,cluster(area)
xtreg lntfp ttt tt treats times so2 i.year i.company,cluster(area)
xtreg lntfp ttt tt treats times so2 zcsy lf owner age sczy lnaj lnlabor lnzlb i.year ///
i.company,cluster(area)
安慰剂检验
twoway (kdensity estimate) (scatter pvalue estimate, ytitle(p-value)
xtitle(estimates)xscale(titlegap(2)outergap(0)) graphregion(margin(l-2 r-0 t-3 b-3))
legend( label (1 "kdensity of estimates") label ( 2 "p value"))),yline(0.1)
xline(0.090,lpattern(line)) xlabel(-0.1 -0.05 0 0.05 0.090 0.1) clegend(on bexpand)
异质性检验
reg lntfp tt i.year i.area i.ind if so2==1&dzbq==0,cluster(area)
reg lntfp tt zcsy lf age owner sczy lnaj lnlabor lnzlb i.year i.area i.ind if
so2==1&dzbq==0,cluster(area)
xtreg lntfp tt i.year i.company if so2==1&dzbq==0,cluster(area)
xtreg lntfp tt zcsy lf age owner sczy lnaj lnlabor lnzlb i.year i.company if
so2==1&dzbq==0,cluster(area)
xtreg lntfp tt zcsy lf age sczy lnaj lnlabor lnzlb i.year i.company if
so2==1&owner==0,cluster(area)
xtreg lntfp tt zcsy lf age sczy lnaj lnlabor lnzlb i.year i.company if
so2==1&owner==1,cluster(area)
xtreg lntfp tt zcsy lf age sczy owner lnlabor lnzlb i.year i.company if
so2==1&ajj==0,cluster(area)
xtreg lntfp tt zcsy lf age sczy owner lnlabor lnzlb i.year i.company if
so2==1&ajj==1,cluster(area)
传导机制
gen ttroa=tt*roa
reg zlb tt zcsy zc lf iso lnlabor xfs sch i.year i.area i.ind if so2==1,cluster(area)
xtreg zlb tt zcsy zc lf iso lnlabor xfs sch i.year i.company if
so2==1,cluster(area)
reg lntfp tt zcsy lf age owner sczy lnaj lnlabor lnzlb i.year i.area i.ind if
so2==1,cluster(area)
xtreg lntfp tt zcsy lf age owner sczy lnaj lnlabor lnzlb i.year i.company if
so2==1,cluster(area)
reg tz ttroa roa tt age zlb lf sczy iso i.year i.area i.ind if so2==1,cluster(area)
xtreg tz ttroa roa tt age zlb lf sczy iso i.year i.company if so2==1,cluster(area)
GMM 测算全要素生产率
xtivreg2 lnzjz lnlabor (lncapital= l.lncapital) ,gmm2s fe robust
gen lntfp1=lnzjz-0.3324*lnlabor-0.5646*lncapital
reg lntfp1 tt i.year i.area i.ind if so2==1,cluster(area)
reg lntfp1 tt zcsy lf age owner sczy lnaj lnlabor lnzlb i.year i.area i.ind if
so2==1,cluster(area)
xtreg lntfp1 tt i.year i.company if so2==1,cluster(area)
xtreg lntfp1 tt zcsy lf age owner sczy lnaj lnlabor lnzlb i.year i.company if
so2==1,cluster(area)

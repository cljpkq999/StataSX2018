
&emsp;

> 作者：彭显耿 (中山大学)      
>     
> Stata 连享会： [知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn) | [CSDN](https://blog.csdn.net/arlionn)

&emsp;
  
- [连享会推文集锦](https://www.jianshu.com/p/de82fdc2c18a) 
- Stata连享会 [精品专题](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文](https://github.com/arlionn/stata/blob/master/README.md)
[![点击此处-查看完整推文列表](https://upload-images.jianshu.io/upload_images/7692714-d57d6beed2a23dc5.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)](https://gitee.com/arlionn/Course/blob/master/README.md)


&emsp;
> #### [连享会计量方法专题……](https://gitee.com/arlionn/Course/blob/master/README.md)



# 面板数据（Panel Data）与固定效应模型（Fixed-Effects Regression Model）

&emsp;&emsp;实证研究过程中，我们可以收集并整理得到三种数据结构，分别为横截面数据(Cross-Sectional Data)——不同个体($i$)的各个变量在固定时点的数据集合，时序数据(Time Series)——变量按发生时间的先后顺序($t$)进行排列得到的数据序列，以及面板数据（Panel Data），又称横截面与时间序列的混合数据。前两者皆是一维数据，第三者则是二维数据，其可以反映个体在不同时点被重复观测的情况，如图1和2所示为面板数据的基本结构和模拟回归图。显然，面板数据要更为贴近我们对生活世界的观察，其包含了更多的动态信息。利用面板数据建立模型并进行实证研究能够在增加e观测值的基础上得到对于固定效应的无偏、一致及有效的估计，而这在相当程度上可以处理“内生性”(Endogeneity)问题。

![图1·面板数据的基本结构](https://upload-images.jianshu.io/upload_images/18983672-6b9f473c915a4b09.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/500)

![固定效应模型的模拟回归](https://upload-images.jianshu.io/upload_images/18983672-e3ebce0f8f5564f0.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/500)

 &emsp;&emsp;基于面板数据的回归模型基本有3种，包括混合模型(Pooled Regression Model)、固定效性模型(Fixed-Effects Regression Model)和随机效应模型(Random Effect Model)；其中，固定效应又包括个体固定效应(Entity Fixed Effect Model)和和时间固定效应(Time Fixed Effect Model)；由此，是否控制这两种效应便可以产生3种组合。初识固定效应，我们的目标是理解“双向固定效应模型”，即

 &emsp;&emsp; &emsp;&emsp; &emsp;&emsp; &emsp;&emsp; &emsp;&emsp; &emsp;&emsp;$y_{it}=\alpha_i+x_{it}*\beta+\lambda_{t}+\epsilon_{it}$

&emsp;&emsp; 其中，$\alpha_{i}$为不随时间变化的个体$i$特征，包括可以观测的性别、性格、种族、籍贯、文化等等，以及不可观测的其他个体效应，代表个体异质性的截距项；同理，$\lambda_{t}$为不随个体变化的时间效应，代表第$t$期特有的截距项；此二者皆可参考上述图2加以理解。由此，对应于$n$个个体或$t$个时点即有$n$或$t$个截距项(暂仅讨论变截距模型)，其等价于在传统的线性回归模型中加入$N-1$个体或时间虚拟变量(Dummy Variable)来表现，而这恰是我们在学习固定效应的过程中容易忽视的关键环节。

&emsp;&emsp;由此，可以理解我们常说的“固定效应”，是要去控制或剔除的；我们所关心的是既随个体又随时间变化的$x_{it}$的$\beta$。

# 一个常见且不自觉的困惑：都是命令惹的祸

&emsp;&emsp;经常可以在各经济论坛或问答平台上看到诸如此类的问题，问“看到许多文章在其回归结果中声称自己控制和诸如行业、区域等固定效应，不知道他们是怎么控制的”，或“在xtreg的命令中加入i.industry、i.region等会被omitted，怎么在命令中加入其他研究问题所关心的虚拟变量”等，其中的症结在于还没有理解固定效应模型的计量原理，即如上述所言其估计等价于加入了虚拟变量的最小二乘回归（DVLS）。或许这也是无可厚非的，毕竟我们学习及用stata实操固定效应模型时就一直由“xtreg”这一命令陪伴的，“reg”则被我们狭隘地认为仅用于严格服从“高斯-马尔可夫假设”的传统线性回归模型，因此只要一提到固定效应模型，下意识就是使用“xtreg y x i.year,fe robust”。

&emsp;&emsp;那么，解决并理解这一问题可以从传统线性回归模型着手，即

 &emsp;&emsp;&emsp;&emsp;&emsp; &emsp;&emsp; &emsp;&emsp; &emsp;&emsp; &emsp;&emsp; &emsp;&emsp;$y_{it}=\alpha+x_{it}* \beta+\epsilon_{it}$

&emsp;&emsp;假设我们想知道什么因素会影响收入(wage),在设计模型的过程如果遗漏了“是否为工会成员”、“性格”或其他不可观测的变量，则将导致内生性问题。如有

 &emsp;&emsp; &emsp;&emsp; &emsp;&emsp; &emsp;&emsp; &emsp;&emsp; &emsp;&emsp;$y_{it}=\alpha+x_{1,it}*\beta+x_{2,i}*\beta+\epsilon_{1,it}$

 &emsp;&emsp; 假设$corr(x_{1,it},x_{2,i})\neq 0$，若遗漏了$x_{2,i}$, 模型将变为$y_{it}=\alpha+x_{1,it}*\beta+\epsilon_{2,it}$，其中$\epsilon_{2,it}+\epsilon_{1,it}+x_{2,i}$,则有$corr(x_{1,it},\epsilon_{2,it})\neq 0$。所以将$x_{2,i}$加入模型中即控制，或剔除了个体$i$的某一不随时间变化的变量的影响。理解了固定效应模型与传统线性回归模型通过虚拟变量而产生的等价联系，便可以理解为什么固定效应可以处理内生性问题，亦可以理解 “reg y x i.id i.year robust" 与 “xtreg y x i.year, fe robust” 的估计结果是一样的($R^2$会有区别)。

 &emsp;&emsp; 进一步地将问题一般化，我们实际上可以将$y_{it}=\alpha+x_{it}*\beta+\epsilon_{it}$中的$x_{it}*\beta$理解为由$u_{i}$、$v_{it}*\rho$和$λ_{t}$此三个变量组成；同理，亦可以将干扰项$\epsilon_{it}$理解为由$q_{i}$、$w_{it}*\theta$和$\eta_{t}$组成；前者即是双向固定效应模型，设$\alpha_{i}=\alpha+u_{i}$，则有$y_{it}=\alpha_i+v_{it}*\rho+\lambda_{t}+\epsilon_{it}$。由此，可见固定效应模型与随机效应模型的关键区别即在于代表个体效应的变量是否与解释变量相关，如图3示；所以，不妨将固定效应模型理解为“变量相关效应模型”，将随机效应理解为“非变量相关效应模型”。

![固定效应与随机效应.png](https://upload-images.jianshu.io/upload_images/18983672-63067d75324c92c1.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/500)

# 固定效应模型的估计方法和stata实现：比较DVLS和FE

## 1.虚拟变量最小二乘法（DVLS）： 
 &emsp;&emsp; 如上文所述，我们可以通过在传统的线性回归模型中加入$N-1$个体或时间虚拟变量来估计固定效应。以个体虚拟变量$D_{i}$为例，即如果为个体2，则有$D_{2}=1$,其他个体$i$ $(i=1、2、3......N)$则为$D_{2}=0$,即用DVLS估计如下方程： 
&emsp; &emsp;&emsp; &emsp;&emsp; &emsp;&emsp; &emsp;&emsp; &emsp;&emsp;&emsp;&emsp;$y_{it}=\alpha+x_{it}*\beta+\sum_{i=2}^{N}\gamma_{i}D_{i}+\epsilon_{it}$

                                                                                   
## 2.组内去心法（Within）：
 &emsp;&emsp;如上文所述，“固定效应”是我们要去控制或剔除的，我们关心的是既随个体又随时间变化的变量。以个体固定效应为例，考虑将固定效应模型$y_{it}=\alpha_i+x_{it}*\beta+\epsilon_{it}$的方程(1)两边对时间取平均得方程(2)$\bar{y_{i}}=\alpha_i+\bar{x}_{i} * \beta +\bar{\epsilon_{i}}$,即求得每个个体$i$的组内均值$\frac{1}{T}*\sum_{t=1}^{T} y_{it}$，以式(1)减去式子(2)得到模型的离差形式(3)$y_{it} - \bar{y_{i}}=(x_{it} - \bar{x_{i}}) * \beta + (\epsilon_{it} - \bar{\epsilon_{i}})$。设$\tilde{y}$为$y_{it} - \bar{y_{i}}$，$\tilde{x}$为$(x_{it} - \bar{x_{i}})$，$\tilde{\epsilon}$为$(\epsilon_{it} - \bar{\epsilon_{i}} )$，则可以用OLS估计如下方程：

&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;$\tilde{y}=\tilde{x}*\beta+\tilde{\epsilon}$

## 3.一阶差分法(FD)：
&emsp;&emsp;同样能够达到消去个体效应$\alpha_{i}$的方法有以$y_{it}$减去$y_{i,t-1}$，即有$y_{it}-y_{i,t-1}=(x_{it}-x_{i,t-1})*\beta+(\epsilon_{it}-\epsilon_{i,t-1})$。设$\Delta y_{it}$为$(y_{it}-y_{i,t-1})$,$\Delta x_{it}$为$(x_{it}-x_{i,t-1})$，$\Delta\epsilon_{it}$为$(\epsilon_{it}-\epsilon_{i,t-1})$，亦可以用OLS估计如下方程(但由$corr(\Delta\epsilon_{it},\Delta\epsilon_{i,t-1})\neq 0$,用广义最小二乘法（GLS）估计会得到更有效的结果)：

&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;$\Delta y_{it}=\Delta x_{it}*\beta+\Delta \epsilon_{it}$
                                                                                          
 &emsp;&emsp;我们可以通过模拟数据来实操并对比一下DVLS与Within的估计结果的差别，具体过程如下：

```
//生成模拟数据
    clear
      set obs 60
      set seed 13599

      egen id= seq(), from(1) to(3) block(20)
      bysort id : gen t = _n + 1990

     gen x1 = 3*rnormal() 
     gen e = 1*rnormal()

     gen y = .

     gen x = x1
     replace x = x1+4 if 1.id
     replace x = x1-5 if 3.id

     replace y = 5 + 0.4*x + e if id==1
     replace y = 10 + 0.4*x + e if id==2
     replace y = 15 + 0.4*x + e if id==3

     order id t y x
     tsset id t	keep id t y x
 /*
     save "FE_simudata.dta", replace
 */
```
```
//回归分析
        use "FE_simudata.dta", clear
        tab id, gen(dum)
        reg y x // Pooled OLS
              est store OLS
        reg y x dum2 dum3 // OLS with Dummy varibles 
              est store OLS_dum2
        reg y x dum1-dum3, nocons // OLS with Dummy varibles, no constant 
              est store OLS_dum3
         xtset id t
         xtdes
         xtreg y x, fe // FE (withi-group estimator)
              est store FE	local m "OLS OLS_dum2 OLS_dum3 FE"
         esttab `m', mtitle(`m') s(N r2) b(%6.3f) nogap ///
                           star(* 0.1 ** 0.05 *** 0.01)
```
```
--------------------------------------------------------------------------------------------------
                          (1)                 (2)                   (3)                (4) 
                          OLS               OLS_dum2              OLS_dum3              FE 
--------------------------------------------------------------------------------------------------
     x                 -0.311***            0.488***              0.488***           0.488***
                        (-3.52)             (11.78)               (11.78)            (11.78) 
 dum2                                       5.811***              10.399*** 
                                            (17.42)               (54.17) 
 dum3                                       10.575***             15.163*** 
                                            (24.85)               (60.55)  
 dum1                                                              4.588*** 
                                                                  (18.18)
_cons                    9.863***           4.588***                                  10.050***
                         (26.44)            (18.18)                                   (91.61) 
-------------------------------------------------------------------------------------------------
N                       60.000               60.000                60.000             60.000 
r2                      0.176                0.931                  0.994               0.712 
-------------------------------------------------------------------------------------------------
      t statistics in parentheses, * p<0.1, ** p<0.05, *** p<0.01
```
&emsp;&emsp; 可以发现，对于x 的系数而言，OLS+dummies 与 FE 结果无异。但若关注$R^2$, 则 OLS_dum2 对应的 $R^2$是最完整的，而 FE 的 $R^2$仅仅反映了 x 对 y 的解释力度, 没有考虑个体效应的贡献。

# 学习心得：回归计量本身

 &emsp;&emsp;经过实操的经验证明，固定效应的估计方法有很多，关键在于如何去掉个体效应；其中DVLS和组内去心的估计量是完全相同的；stata的“reg”和“xtreg”命令是同样可以达到估计固定效应的效果，且前者的用武之地要比后者更大。

 &emsp;&emsp;由此，我们可以理解计量本身和程序代码可以是分属两个完全不同的方面。若势要分个高低，那么仍是计量的思想规制着程序的设计和运行。这便提醒我们以“xx命令能够实现xx模型“或“xx模型要用xx命令”的方式来思考或提问是不得要领的。所以，回到计量本身，而非弥留于命令的魔咒。



>#### 关于我们

- 【**Stata 连享会(公众号：StataChina)**】由中山大学连玉君老师团队创办，旨在定期与大家分享 Stata 应用的各种经验和技巧。
- 公众号推文同步发布于 [CSDN-Stata连享会](https://blog.csdn.net/arlionn) 、[简书-Stata连享会](http://www.jianshu.com/u/69a30474ef33) 和 [知乎-连玉君Stata专栏](https://www.zhihu.com/people/arlionn)。可以在上述网站中搜索关键词`Stata`或`Stata连享会`后关注我们。
- 点击推文底部【阅读原文】可以查看推文中的链接并下载相关资料。
- Stata连享会 [精品专题](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文](https://github.com/arlionn/stata/blob/master/README.md)

>#### 联系我们

- **欢迎赐稿：** 欢迎将您的文章或笔记投稿至`Stata连享会(公众号: StataChina)`，我们会保留您的署名；录用稿件达`五篇`以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。您也可以从 [连享会选题平台](https://gitee.com/Stata002/StataSX2018/wikis/Home) &rarr; [002_备选主题] 中选择感兴趣的题目来撰写推文。
- **意见和资料：** 欢迎您的宝贵意见，您也可以来信索取推文中提及的程序和数据。
- **招募英才：** 欢迎加入我们的团队，一起学习 Stata。合作编辑或撰写稿件五篇以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。
- **联系邮件：** StataChina@163.com

>#### 往期精彩推文
- [Stata连享会推文列表](https://www.jianshu.com/p/de82fdc2c18a) 
- Stata连享会 [精品专题](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文](https://github.com/arlionn/stata/blob/master/README.md)

[![点击此处-查看完整推文列表](https://upload-images.jianshu.io/upload_images/7692714-8b1fb0b5068487af.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "连享会(公众号: StataChina)推文列表")](https://gitee.com/arlionn/Course/blob/master/README.md)





---
![欢迎加入Stata连享会(公众号: StataChina)](http://wx1.sinaimg.cn/mw690/8abf9554gy1fj9p14l9lkj20m30d50u3.jpg "扫码关注 Stata 连享会")

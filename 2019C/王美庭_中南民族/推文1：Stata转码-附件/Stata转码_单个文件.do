*---------------------------用户修改部分---------------------------------------
cd "D:\stata15\ado\personal\Net_course\"
local dta_file "gdpChina.dta"


*----------------------------无需变更部分--------------------------------------
use `dta_file', clear 

*将原dta文件备份至"backup files"文件夹中
cap mkdir "backup files"
copy `dta_file' "backup files\", replace

*对数据标签进行转码
local data_lbl: data label
local data_lbl = ustrfrom("`data_lbl'", "gb18030", 1)
label data "`data_lbl'"

*对变量名、变量标签、字符型变量取值转码
foreach v of varlist _all {
	* 对字符型变量取值进行转码
	local type: type `v'   //将变量的类型放入宏type中
	if strpos("`type'", "str") {
		replace `v' = ustrfrom(`v', "gb18030", 1)   //如果变量是字符型变量，使用ustrfrom()函数进行转码
	}

	* 对变量标签进行转码
	local lbl: var label `v'   //将变量的标签放入宏lbl中
	local lbl = ustrfrom("`lbl'", "gb18030", 1)   //使用ustrfrom()函数对`lbl'转码
	label var `v' `"`lbl'"'   //将转码后的字符串作为变量的标签

	* 对变量名进行转码
	local newname = ustrfrom(`"`v'"', "gb18030", 1)   //使用ustrfrom()函数将变量名字符串进行转码
	qui rename `v' `newname'   //将转码后的字符串重命名为变量名
}

*下面对数值标签进行转码
qui label save using label.do, replace   //将数值标签的程序保存到label.do中（如果原来dta数据是gb18030编码，则这里导出的do文件也是gb18030编码）
preserve //将do文件的内容用txt的方式导入一个变量之中
qui import delimited using label.do, ///
  varnames(nonames) delimiters("asf:d5f14d5d",asstring) encoding(gb18030) clear
qui describe
if r(N) == 0 {
		restore
		save `dta_file', replace
	}
	else {
		qui levelsof v1, local(v1_lev)
		restore
		foreach label_modify of local v1_lev {    //这个的foreach与前面的local(v_lev)对应，细节可查看帮助文件
			`label_modify'      //依次对原数据执行值标签替换操作
		} 
		save `dta_file', replace  //将转码好的dta文件替换原来的dta文件
	}
	
* 删除中间临时文件
erase label.do

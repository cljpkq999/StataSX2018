*------------------------用户修改部分--------------------------------
cd "D:\stata15\ado\personal\Net_course"


*-------------------------无需变更部分--------------------------------
*将当前文件夹下的所有dta文件进行转码（第一层）
do "D:\转码_单个文件夹.do"

*将当前子文件夹下的所有dta文件进行转码（第二层）
local dir_list : dir . dirs "*", respectcase
foreach subdir of local dir_list {
    if "`subdir'" == "backup files" {
        continue //保留备份的原文件不动
    }
    cd "`subdir'"
    do "D:\转码_单个文件夹.do"
    
    *将当前子子文件夹下的所有dta文件进行转码（第三层）
    local dir_list2 : dir . dirs "*", respectcase
    foreach subdir2 of local dir_list2 {
        if "`subdir2'" == "backup files" {
            continue //保留备份的原文件不动
        }
        cd "`subdir2'"
        do "D:\转码_单个文件夹.do"
        qui cd ..
    }
    qui cd ..   
}

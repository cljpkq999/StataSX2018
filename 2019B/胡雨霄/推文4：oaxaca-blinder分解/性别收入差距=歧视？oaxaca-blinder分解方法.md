

&emsp;

> 作者：胡雨霄 (伦敦政治经济学院)      
>      &emsp;       
> Stata 连享会： [知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn) | [CSDN](https://blog.csdn.net/arlionn)

&emsp;

- Stata连享会 [精品专题](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文](https://github.com/arlionn/stata/blob/master/README.md)

[![点击此处-查看完整推文列表](https://images.gitee.com/uploads/images/2019/0722/083230_8730c8a9_1522177.png)](https://gitee.com/arlionn/Course/blob/master/README.md)



&emsp;

 
####  [2019暑期“实证研究方法与经典论文”专题班-连玉君-江艇主讲](https://gitee.com/arlionn/Course/blob/master/2019Papers.md) 

[![2019暑期“实证研究方法与经典论文”专题班-连玉君-江艇主讲](https://images.gitee.com/uploads/images/2019/0722/083230_7c669996_1522177.png)](https://gitee.com/arlionn/Course/blob/master/2019Papers.md)

---



本推文介绍 Oaxaca - Blinder 分解方法及其 Stata 的实现命令。Oaxaca - Blinder 分解方法由 Oaxaca (1973) , Blinder (1973) 提出。对方法原理的介绍主要基于 [Jann(2008)](https://www.stata-journal.com/sjpdf.html?articlenum=st0151)。该方法被劳动经济学家广泛使用。

## 1. Oaxaca - Blinder 分解方法简介

以性别收入差距的实证例子来说明该方法的用途。事实上，即使观测到性别收入差距的存在，也无法轻易断言劳动市场存在性别歧视。性别收入差距产生的原因多样，有可能是由于性别歧视，也有可能是由于男性和女性的生产力条件不同。例如，假设女性受教育水平更低，那么性别收入差距可能反映的是性别受教育水平差距。因此，若想探究劳动市场是否存在性别歧视，需要构建一个反事实组 （couterfactual group)，即 “被视为男性的女性” 。若无歧视存在，那么该反事实组别（couterfactual group) 的收入水平不应显著与女性收入水平不同。这说明劳动市场对男性女性一视同仁。而若收入水平显著不同，那么则可视为与歧视相关。通俗来讲，也就是若女性被视为男性，那么她们会得到更高的劳动市场回报。

需要注意的是，该组别被称为 **“反事实 (counterfacutal) ”**，是因为在现实中，我们无法观测到这样的组别。而 Oaxaca-blinder 方法的优势就在于可以用简单的方法构建一个反事实组 （couterfactual group)，并将不同组别之间的差异分解为 **"可解释部分”** 以及 **“不可解释部分”**。“可解释部分” 为与生产力条件不同相关的收入差距，“不可解释部分” 为与生产力条件无关的收入差距，在实证中也常被理解为 “歧视”。

此外，仍需注意的是，通常来说，利用该方法无法进行因果推断 (causal inference)。一般而言，我们 **无法** 将 “可解释部分” 与 “不可解释部分” 理解为由生产力条件差异和歧视导致的收入差距。下文将进一步阐述。

## 2. Oaxaca - Blinder 分解方法原理

该部分仍以性别收入差距为例进行原理部分的阐述。女性收入记为 $Y^F$，男性收入记为 $Y^M$。我们认为，收入是由生产力条件决定的，即

$$ln Y^M = X_M \beta_M$$

$$ln Y^F = X_F \beta_F$$

其中，$X_M$和$X_F$ 分别表示男性和女性与生产力条件有关的因素 (determinant)，例如，教育水平，工作年限，工作时间等。$\beta_M $和$\beta_F$ 分别为劳动力市场上对男性和女性的工资回报系数。

我们无法将 $ln Y^M-ln Y^F$ 直接理解为性别歧视。若想探究性别歧视的存在，要构建反事实组 （couterfactual group)，即在劳动市场上 “被视为男性的女性” （$C$)。该组别收入记为 $Y^C$。对于 $Y^C$ 的基本设定为

$$Y^C = X_F \beta_M$$

即被视为男性的女性在劳动市场上收获的劳动报酬。

基于此，性别收入差距可进行分解。

$$ln Y^M-ln Y^F = (ln Y^M- ln Y^C) + (ln Y^C - ln Y^F) = \beta_M (X_M - X_F) + (\beta_M - \beta_F)X_F$$

其中，
- **“可解释部分”** 为 $ln Y^M- ln Y^C = \beta_M (X_M - X_F)$，即生产力条件不同 ( $X_M$ 不等于  $X_F$)，而产生的收入差距。
- **“不可解释部分”** 为 $ln Y^C - ln Y^F = (\beta_M - \beta_F)X_F$，即由于男性和女性回报系数不同 ($\beta_M$ 不等于 $\beta_F$)，而产生的收入差距。这部分可以理解为歧视。

上文提到，通常来说，利用该方法无法进行因果推断 (causal inference)。但上一段又使用了 “产生” 这样具有因果推断意义的词汇。此处的矛盾源于，在实证分析中，我们很难找到所有的生产力条件决定因素，或者很难论证已经找到了所有的生产力条件决定因素。

在这种情况下，我们无法保证 “不可解释部分” 真的排除了所有的 “可解释部分”。例如，基因条件也许会对人的劳动市场表现产生影响，但很难找到基因的数据。

因此，大部分使用 Oaxaca - Blinder 的文章旨在论证劳动市场存在歧视，而却谨慎断言歧视就是导致性别收入差距的原因。

## 3. Oaxaca - Blinder 分解方法 Stata 命令实现

### 3.1 命令的安装

```
ssc install oaxaca, replace 
```

### 3.2 基本命令

```
 oaxaca depvar [indepvars] [if] [in] [weight] , by(groupvar) [options ]
```

其中

- **depvar** 为我们关心的结果变量 (outcome variables)，比如收入水平。

- **indepvar** 为可能会影响结果变量的自变量。比如，教育水平、工作经验、工作时长都有可能会影响收入水平。

- **by(groupvar)** 明确被比较的两个组别。

- **[options]** 中可以明确分解的方式。**weight(1)** 表示以组别 1 作为基准组，**pooled** 则表示以整个样本作为基准组。上文的例子中，因为反事实组假设女性在劳动市场上被视作男性，所以男性为基准组。

### 3.3 数据导入

```stata
use "http://fmwww.bc.edu/RePEc/bocode/o/oaxaca.dta", clear
```

数据结构如下

```
. list in 1/10

     +---------------------------------------------------------------------------------------------------------------------------------------+
     |   lnwage   educ      exper     tenure   isco   female   lfp   age   agesq   single   married   divorced   kids6   kids714          wt |
     |---------------------------------------------------------------------------------------------------------------------------------------|
  1. |  3.73304      9   9.041667   3.416667      1        1     1    37    1369        1         0          0       0         0   .53029772 |
  2. | 3.600868      9       7.25   20.66667      1        0     1    62    3844        0         1          0       0         0   1.0605954 |
  3. | 3.159036   10.5        2.5   .0833333      1        1     1    40    1600        0         1          0       0         0   1.0605954 |
  4. | 3.393229     12       26.5   6.166667      1        0     1    55    3025        0         0          1       0         0   1.0605954 |
  5. | 3.805663     12   13.91667   1.333333      1        0     1    36    1296        0         1          0       1         1   1.0605954 |
     |---------------------------------------------------------------------------------------------------------------------------------------|
  6. | 3.272365   10.5       9.25          0      1        0     1    31     961        0         1          0       0         0   1.0605954 |
  7. | 3.528298   10.5   29.41667      28.75      1        0     1    50    2500        0         0          1       0         1   .53029772 |
  8. | 3.805663   17.5   10.08333       6.25      1        0     1    42    1764        0         0          1       1         0   2.1211909 |
  9. | 3.824012   17.5   2.041667   5.083333      1        0     1    36    1296        0         1          0       0         0   1.0605954 |
 10. | 3.382179   10.5   10.83333   8.583333      1        0     1    30     900        0         1          0       0         0   1.0605954 |
     +---------------------------------------------------------------------------------------------------------------------------------------+
```

### 3.4 Stata 命令实现

运用 ``oaxaca`` 命令对性别收入差距进行分解，结果如下。

```
. svyset [pw=wt]
. oaxaca lnwage educ exper tenure, by(female) weight(0) svy

Blinder-Oaxaca decomposition

Number of strata   =         1                  Number of obs     =      1,647
Number of PSUs     =     1,647                  Population size   = 1,657.1804
                                                Design df         =      1,646
                                                Model              =    linear
Group 1: female = 0                             N of obs 1         =       751
Group 2: female = 1                             N of obs 2         =       683

------------------------------------------------------------------------------
             |             Linearized
      lnwage |      Coef.   Std. Err.      t    P>|t|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
overall      |
     group_1 |   3.405696   .0226311   150.49   0.000     3.361307    3.450085
     group_2 |   3.193847   .0276463   115.53   0.000     3.139622    3.248073
  difference |   .2118488    .035728     5.93   0.000     .1417718    .2819259
   explained |   .1124494   .0227931     4.93   0.000     .0677429     .157156
 unexplained |   .0993994   .0294441     3.38   0.001     .0416476    .1571512
-------------+----------------------------------------------------------------
explained    |
        educ |   .0563018   .0149668     3.76   0.000     .0269457    .0856578
       exper |   .0441621   .0137384     3.21   0.001     .0172156    .0711086
      tenure |   .0119856   .0087921     1.36   0.173    -.0052592    .0292305
-------------+----------------------------------------------------------------
unexplained  |
        educ |  -.0759137   .1671952    -0.45   0.650    -.4038515    .2520241
       exper |  -.1036479   .0560732    -1.85   0.065    -.2136303    .0063345
      tenure |    .064437   .0353861     1.82   0.069    -.0049696    .1338436
       _cons |    .214524   .1948348     1.10   0.271    -.1676261    .5966742
------------------------------------------------------------------------------
```

通过设定 ``weight(0)``，该命令以男性作为基准组。其中

```
-------------+----------------------------------------------------------------
overall      |
     group_1 |   3.405696   .0226311   150.49   0.000     3.361307    3.450085
     group_2 |   3.193847   .0276463   115.53   0.000     3.139622    3.248073
  difference |   .2118488    .035728     5.93   0.000     .1417718    .2819259
   explained |   .1124494   .0227931     4.93   0.000     .0677429     .157156
 unexplained |   .0993994   .0294441     3.38   0.001     .0416476    .1571512
-------------+----------------------------------------------------------------
```
表示，男性 (**group_1**) 的 **logwage** 均值为 3.406，女性（**group_2**）的 **logwage** 均值为 3.194。二者差距 (**difference**) 为 0.212。其中，"可解释部分" **explained** 为 0.112，占 **difference** 的 52.8%。“不可解释部分” **unexplained** 为 0.099，占 **difference** 46.7%。

### 3.5 经济学含义

首先，男性和女性的收入水平存在差距。男性的收入水平比女性的收入水平高出 21.18%。

其次，该收入差距可以被分解为两部分。一部分为 “可解释部分”。因为男性和女性在教育水平 (**educ**），工作年限（**exper**）以及获得 tenure 的年限（**tenure**）上存在差异，所以即使假设女性在劳动市场上被视为男性，还是会与真正的男性存在收入差距。具体而言，收入差距的 52.8% 与男性和女性在这些方面的生产力条件差异有关。

另一部分为 “不可解释部分”。该部分为被视为男性的女性和真正的女性的收入差距。其比较对象均为女性，差异为劳动市场看待女性的视角，因此无法由男性和女性的生产力条件差异解释。具体而言，收入差距的 46.7% 与女性在劳动力市场的差别待遇或者性别歧视有关。

最后，该结果表明劳动市场可能存在性别歧视。但是由于该分解只涉及教育水平 (**educ**），工作年限（**exper**）以及获得 tenure 的年限（**tenure**），并未穷尽所有收入水平的决定因，因此无法肯定性别歧视是导致劳动市场性别收入差距的原因。

## 文献来源

[1] Blinder, A. S. (1973). Wage discrimination: reduced form and structural estimates. *Journal of Human resources*, 436-455. [[Link]](https://www.jstor.org/stable/144855?seq=1#page_scan_tab_contents)

[2] Jann, B. (2008). The Blinder–Oaxaca decomposition for linear regression models. *The Stata Journal*, 8(4), 453-479. [PDF](https://www.stata-journal.com/sjpdf.html?articlenum=st0151)

[3] Oaxaca, R. (1973). Male-female wage differentials in urban labor markets. *International Economic Review*, 693-709. [[PDF]](https://dataspace.princeton.edu/jspui/bitstream/88435/dsp012514nk49s/1/23.pdf)， [[Link]](https://www.jstor.org/stable/2525981)
 

&emsp;


>#### 关于我们

- 【**Stata 连享会(公众号：StataChina)**】由中山大学连玉君老师团队创办，旨在定期与大家分享 Stata 应用的各种经验和技巧。
- 公众号推文同步发布于 [CSDN-Stata连享会](https://blog.csdn.net/arlionn) 、[简书-Stata连享会](http://www.jianshu.com/u/69a30474ef33) 和 [知乎-连玉君Stata专栏](https://www.zhihu.com/people/arlionn)。可以在上述网站中搜索关键词`Stata`或`Stata连享会`后关注我们。
- 点击推文底部【阅读原文】可以查看推文中的链接并下载相关资料。
- Stata连享会 [精品专题](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文](https://github.com/arlionn/stata/blob/master/README.md)

>#### 联系我们

- **欢迎赐稿：** 欢迎将您的文章或笔记投稿至`Stata连享会(公众号: StataChina)`，我们会保留您的署名；录用稿件达`五篇`以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。您也可以从 [连享会选题平台](https://gitee.com/Stata002/StataSX2018/wikis/Home) &rarr; [002_备选主题] 中选择感兴趣的题目来撰写推文。
- **意见和资料：** 欢迎您的宝贵意见，您也可以来信索取推文中提及的程序和数据。
- **招募英才：** 欢迎加入我们的团队，一起学习 Stata。合作编辑或撰写稿件五篇以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。
- **联系邮件：** StataChina@163.com

>#### 往期精彩推文
- [Stata连享会推文列表](https://www.jianshu.com/p/de82fdc2c18a) 
- Stata连享会 [精品专题](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文](https://github.com/arlionn/stata/blob/master/README.md)

[![点击此处-查看完整推文列表](https://images.gitee.com/uploads/images/2019/0722/083230_fcedc95d_1522177.png)](https://gitee.com/arlionn/Course/blob/master/README.md)





---
> [![image](https://images.gitee.com/uploads/images/2019/0722/083230_7fdba605_1522177.jpeg)](https://gitee.com/arlionn/Course/blob/master/README.md)

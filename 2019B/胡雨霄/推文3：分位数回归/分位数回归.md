


> 作者：胡雨霄  (伦敦政治经济学院)      
>
> Stata 连享会： [知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn) | [CSDN](https://blog.csdn.net/arlionn)



&emsp;


#### [2019暑期“实证研究方法与经典论文”专题班-连玉君-江艇主讲](https://mp.weixin.qq.com/s/1zfVO2GvCaYSwARmugsLLQ)


[![2019暑期“实证研究方法与经典论文”专题班-连玉君-江艇主讲](https://images.gitee.com/uploads/images/2019/0703/220705_980133f3_1522177.png)](https://mp.weixin.qq.com/s/1zfVO2GvCaYSwARmugsLLQ)






&emsp;

最小二乘法 (OLS) 是在实证中最常用到的回归方法。标准的最小二乘线性回归，关注解释变量 x 对被解释变量 y 的 **条件均值** $E(Y|X)$ 的影响。OLS 回归关注的是因变量的条件均值函数。然而，实证中，研究者可能对 $y|x$ 分布的其它重要分位数感兴趣。例如，劳动经济学家研究性别收入差距不平等问题时，若想探究高收入女性是否更难获得升职机会，就需要更关注女性群体收入的 90% 分位数水平。

## 1. 分位数回归简介

## 1.1 分位数回归的实证价值

我们来举几个例子说明分位数回归在实证中的价值。

- [Okada & Samreth (2011)](https://www.sciencedirect.com/science/article/pii/S016517651100574X) 利用分位数回归研究外国援助 (foreign aid) 与接收国腐败程度的关系。作者在接收国腐败程度的不同分位数上做回归，并发现初始腐败程度越低的国家，外国援助对于降低其腐败程度的影响就越大。如果使用 OLS，那么可能研究者只可以得出外国援助与接收国腐败程度负向相关这一条结论，却无法展现更为丰富多样的结果。

- [Martins & Pereira(2003)](https://www.sciencedirect.com/science/article/abs/pii/S0927537103000885) 利用分位数回归研究教育是否对不同收入水平的工作者的收入影响不同。如果使用 OLS，那么研究者只可以看到均值水平上教育与收入水平的关系。然而，教育有可能对不同收入水平的工作者有不同的影响。比如，高收入水平工作者更有可能从事脑力劳动，那么教育水平对他们的影响可能更高。通过分位数回归，作者确实发现了上述观点的实证证据。以瑞典为例，OLS 的回归结果显示教育的平均收益率为 4%。然而分位数回归的结果则表明，对于收入水平在 10% 分位数上的工作者，教育的平均收益率为 4%。而对于收入水平在 90% 分位数上的工作者，教育的平均收益率则可以达到 6%。这样的结果显然比 OLS 的结果更加丰富且更具政策意义。

与 OLS 不同，分位数回归估计的是解释变量 x 与被解释变量 y 的 **分位数** 之间线性关系。OLS 回归以残差平方最小化 $\sum_ie_i^2$ 作为目标，中位数回归则目标最小化离差绝对值 $\sum_i|e_i|$。对于分位数回归，其目标为最小化非对称性绝对值残值。

以 $q$ 表示分位数水平，分位数回归估计量 $\beta_q$ 最小化目标函数

$$Q(\beta_q)=\sum^N_{i: y_i > x_i'\beta q}q|y_i - x_i'\beta_q|+\sum^N_{i: y_i < x_i'\beta q}q|y_i - x_i'\beta_q|$$

## 1.2 分位数回归的优点

相较于 OLS， 分位数回归的优点主要是：

- 更全面描述解释变量 x 和被解释变量 y 的关系。实际上，在不同分位数上，因为解释变量 x 对被解释变量 y 可能影响不同，因此分位数回归系数可能和 OLS 的回归系数不同。

- OLS 要求残差独立，同方差，且服从正态分布。而分位数回归对残差的要求弱于OLS，估计也更加稳健。

## 1.3 分位数回归结果分析

对于研究者，一个自然而然的问题是，该如何解释分位数回归的系数？

[UCLA Institue for Digital Research & Education](https://stats.idre.ucla.edu/stata/faq/how-do-i-interpret-quantile-regression-coefficients/) 给出的答案是 “The short answer is that you interpret quantile regression coefficients **just like** you do ordinary regression coefficients. The long answer is that you interpret quantile regression coefficients **almost just like** ordinary regression coefficients.” 也就是说，我们可以按照分析 OLS 回归的方法对分位数回归的结果进行分析。

而 Firpo et al. (2009) 则认为条件分位数回归 (conditional quantile regression) 的结果并不十分有趣，也无法用分析 OLS 回归结果类似的方法进行分析。相反，非条件分位数回归 (unconditional quantile regression) 的结果则更有实证价值，而且也可以用分析 OLS 回归结果类似的方法进行分析。

关于该问题很有意义的讨论请参考 https://stats.stackexchange.com/questions/80349/what-is-the-difference-between-conditional-and-unconditional-quantile-regression

下面，简要介绍一些利用 Stata 进行分位数回归的命令。

## 2. 分位数回归基本命令：`qreg` 

分位数回归最基本的命令为 `qreg`。

```
 qreg depvar [indepvars] [if] [in] [weight] [, qreg_options]
```

其中，

`depvar` 为被解释变量

`[indepvars]` 为解释变量

`[, qreg_options]` 具化分位数水平，例如 `quantile (.25)` 。如不特别设定，则 Stata 默认为中位数，即 `quantile (.50)`。

若想绘制不同分位数的回归系数，则可使用 `qreg` 命令。该命令的基本语法如下。

## 2. 分位数回归绘图命令： `grqreg` 

```
 grqreg [varlist] [weight], qmin(integer) qmax(integer)  qstep(integer) cons ci ols olsci
```
其中，

`varlist` 为选择的要被绘制的变量。若无特别设定，则默认为所有的估计系数。

`qmin` 为估计的分位数的最小值。若无特别设定，则默认为 50%。

`qmax` 为估计的分位数的最大值。若无特别设定，则默认为 95%。

`qstep` 为分位数间距。若无特别设定，则默认为 5%。

`cons` 为绘制截 (intercept) 的估计系数。若无特别设定，则默认为 off 。

`ci` 为绘制分位数回归的置信区间水平。若无特别设定，则默认为 off 。

`ols` 为绘制 OLS 的估计系数。若无特别设定，则默认为 off 。

`olsci` 为绘制 OLS 回归的置信区间水平。若无特别设定，则默认为 off 。

`title` 为命名参数。

- 举例
```
ssc install  grqreg
. qreg price mpg, q(0.50)
Iteration  1:  WLS sum of weighted deviations =  70552.192

Iteration  1: sum of abs. weighted deviations =    75229.5
Iteration  2: sum of abs. weighted deviations =   64766.75
Iteration  3: sum of abs. weighted deviations =  64760.833

Median regression                                   Number of obs =         74
  Raw sum of deviations  71102.5 (about 4934)
  Min sum of deviations 64760.83                    Pseudo R2     =     0.0892

------------------------------------------------------------------------------
       price |      Coef.   Std. Err.      t    P>|t|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
         mpg |  -135.6667   67.26576    -2.02   0.047    -269.7585   -1.574816
       _cons |   8088.667   1483.808     5.45   0.000     5130.749    11046.58
------------------------------------------------------------------------------

```
该结果展示了在 **price** 的中位数水平上，**price** 对 **mpg** 做回归的结果。**mpg** 的系数为 -135.667。这说明，在中位数水平上，当 **mpg** 增加一单位，那么 **price** 就减少 135.667。

如果想要直观展示分位数回归的结果，那么可以采用命令 `grqreg`。
```
grqreg, ci title(Fig.1a Fig.1b)
```

![3.png](https://images.gitee.com/uploads/images/2019/0720/184652_a61dd0be_5089004.png)


左图展示的是在 **price** 的不同分位数点上， **price** 对 **mpg** 包含置信区间的回归结果。

## 3. 广义分位数回归 (generalized quantile regression) 命令： `genqreg`

`genqreg` 命令基于广义的分位数回归 （ [Powell (2016)](https://works.bepress.com/david_powell/4/) ），可以用来估计非条件的分位数回归。

我们利用一个例子来说明为何要进行广义的分位数回归。假设我们想要研究培训 (**training**) 对收入 (**y**) 的影响。另外假设培训是随机的。如果想要探究培训对于收入水平 90% 分位数上的人的影响，那么可以通过命令 `qreg y training, q(90)` 进行实证分析。

但是，如果数据中包含劳动者能力 (**ability**) 变量，而研究者又想引入该变量作为控制变量，那么命令 `qreg y training ability, q(90)` 却不能反映培训对于收入水平 90% 分位数上的人的影响。对于这条命令的回归结果的解释应该是，给定劳动者的能力，在收入水平的 90% 分位数上，培训对于收入水平的影响。

此处的问题在于，当给定劳动者能力 (**ability**) 之后，收入水平的 90% 分位数并不是真正的 “收入水平的 90% 分位数” 。例如，对于能力较低的这部分劳动者，他们收入水平的 90% 分位数代表的人群可能是整个样本中的低收入人群。

而 `genqreg` 命令允许研究者进行广义分位数回归，从而规避上述问题。该命令的基本语法为

```
genqreg depvar indepvars [if] [in] [weight] , [quantile(#) \\\
               instruments(varlist) proneness(varlist) technique(string)      
```

`instruments(varlist)` 列举外生变量。如果没有特别设定，则认为所有的变量都是外生的。

`proneness(varlist) ` 允许加入一些额外的控制变量。

`technique(string)` 可以允许进行 Logit 或者 Probit 回归，`technique("logit")` 或者 `technique("probit")`，默认为 `technique("linear")`

- 例子

```
. ssc install genqreg 

. genqreg price mpg headroom //广义分位数回归
Nelder-Mead optimization
initial:       f(p) = -.02029463
rescale:       f(p) = -.02029463
Iteration 0:   f(p) = -.02029463  
Iteration 1:   f(p) = -.00089208  
Iteration 2:   f(p) = -.00089208  
Iteration 3:   f(p) = -.00089208  
 
Generalized Quantile Regression (GQR)
     Observations:                74
 
------------------------------------------------------------------------------
       price |      Coef.   Std. Err.      z    P>|z|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
         mpg |  -137.3333   64.03182    -2.14   0.032    -262.8334   -11.83326
    headroom |  -120.6667   327.3533    -0.37   0.712    -762.2673     520.934
       _cons |   8462.333   2063.802     4.10   0.000     4417.356    12507.31
------------------------------------------------------------------------------
No excluded instruments and no proneness variables.
--> Estimation is equivalent to standard quantile regression.
 
. qreg price mpg headroom  //分位数回归
Iteration  1:  WLS sum of weighted deviations =  70255.799

Iteration  1: sum of abs. weighted deviations =  70990.429
Iteration  2: sum of abs. weighted deviations =  67518.917
note:  alternate solutions exist
Iteration  3: sum of abs. weighted deviations =      64777
Iteration  4: sum of abs. weighted deviations =  64587.083
Iteration  5: sum of abs. weighted deviations =  64574.074
Iteration  6: sum of abs. weighted deviations =  64552.093
Iteration  7: sum of abs. weighted deviations =  64549.833

Median regression                                   Number of obs =         74
  Raw sum of deviations  71102.5 (about 4934)
  Min sum of deviations 64549.83                    Pseudo R2     =     0.0922

------------------------------------------------------------------------------
       price |      Coef.   Std. Err.      t    P>|t|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
         mpg |  -127.3333   60.78234    -2.09   0.040    -248.5299   -6.136791
    headroom |  -130.6667   415.6721    -0.31   0.754    -959.4933      698.16
       _cons |   8272.333   2158.205     3.83   0.000     3968.995    12575.67
------------------------------------------------------------------------------


```

我们可以看到，运行命令 `genqreg price mpg headroom` 后，**mpg** 的系数相较于之前的回归结果（`qreg price mpg ` ）并没有发生很大的改变。而运行命令 `qreg price mpg headroom` 后，**mpg** 的系数却产生了很大的改变。

这一实证例子可以浅显得说明为什么研究者认为条件分位数回归 (conditional quantile regression) 结果的实证价值相对较低。

## 4. 面板数据分位数回归命令：`qregpd`

接下来，介绍对面板数据进行分位数回归的两个命令，`qregpd` 以及 `xtrifreg`。这两个命令都允许加入固定效应。

该命令适用于面板数据，可参照 [Powell (2015)](https://works.bepress.com/david_powell/1/)。其具体命令为：

```
 qregpd depvar indepvars [if] [in] [weight] , [quantile(#)
               instruments(varlist) identifier(varlist) fix(varlist)
```

其中，

`identifier(varlist)` 类似于 `xtset` 中的 **panelvar**，是观测值单位的固定效应。

`fix(varlist)` 类似于 `xtset` 中的 **timevar**，是时间的固定效应。-

-例子

```
.  webuse nlswork
(National Longitudinal Survey.  Young Women 14-26 years of age in 1968)

. qregpd ln_wage union, q(0.5) id(idcode) fix(year)
Nelder-Mead optimization
initial:       f(p) = -203.79665
rescale:       f(p) = -1.6803243
Iteration 0:   f(p) = -1.6803243  
Iteration 1:   f(p) = -1.6803243  
Iteration 2:   f(p) = -1.6803243  
Iteration 3:   f(p) = -1.6803243  
Iteration 4:   f(p) = -1.6803243  
Iteration 5:   f(p) = -1.6803243  
Iteration 6:   f(p) = -1.6803243  
Iteration 7:   f(p) = -1.6803243  
Iteration 8:   f(p) = -1.6803243  
Iteration 9:   f(p) = -1.5901349  
Iteration 10:  f(p) = -5.645e-06  
Iteration 11:  f(p) = -5.645e-06  
Iteration 12:  f(p) = -5.645e-06  
Iteration 13:  f(p) = -5.645e-06  
Iteration 14:  f(p) = -5.645e-06  
Iteration 15:  f(p) = -5.645e-06  


Quantile Regression for Panel Data (QRPD)
     Number of obs:             19238
     Number of groups:           4150
     Min obs per group:             1
     Max obs per group:            12
------------------------------------------------------------------------------
     ln_wage |      Coef.   Std. Err.      z    P>|z|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
       union |    .110782   .0114776     9.65   0.000     .0882864    .1332776
------------------------------------------------------------------------------
No excluded instruments - standard QRPD estimation.
```
该回归结果表示，在 **ln_wage** 的中位数水平上，控制了个人和时间的固定效应之后，工会成员 (**union == 1**）的工资水平相较于非工会成员 (**union == 0**) 高出 11.08%。
 
## 5. 非条件分位数固定效应模型回归命令： `xtrifreg`

该命令可以用于估计非条件分位数回归固定效应模型。其基本命令如下

```
 xtrifreg depvar indepvars [if] [in] [weight], fe i(varname)  quantile(#)
```

其中，

`i(varname)` 可用于表明固定效应变量。

```
. findit xtrifreg

. xtrifreg ln_wage tenure union, fe i(idcode)

---------------------------------------------------------------------------------------------------------------------
Model UQR
---------------------------------------------------------------------------------------------------------------------

Fixed-effects (within) regression               Number of obs      =     19010
Group variable: idcode                          Number of groups   =      4134

R-sq:  within  = 0.0588                         Obs per group: min =         1
       between = 0.1609                                        avg =       4.6
       overall = 0.1100                                        max =        12

                                                F(2,4133)          =    192.70
corr(u_i, Xb)  = 0.1377                         Prob > F           =    0.0000

                             (Std. Err. adjusted for 4,134 clusters in idcode)
------------------------------------------------------------------------------
             |               Robust
      rif_50 |      Coef.   Std. Err.      t    P>|t|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
      tenure |   .0276753   .0015301    18.09   0.000     .0246755    .0306752
       union |   .1120068   .0154023     7.27   0.000     .0818101    .1422035
       _cons |   1.596251   .0070095   227.73   0.000     1.582509    1.609994
-------------+----------------------------------------------------------------
     sigma_u |  .45465567
     sigma_e |  .37483862
         rho |  .59534098   (fraction of variance due to u_i)
------------------------------------------------------------------------------
F test that all u_i=0: F(4133, 4133) = .                     Prob > F =      .
```

根据 Firpo et al. (2009) ，我们可以用分析 OLS 结果的方法对非条件的分位数回归进行分析。该回归结果则可解释为，控制了个体和时间的固定效应后，在 **ln_wage** 的中位数水平上，工作年限 (**tenure**) 每增加 1 年，那么收入水平会上涨 2.77%；而工会成员 (**union == 1**）的工资水平相较于非工会成员 (**union == 0**) 高出 11.20%。

## 6. 总结

本篇推文介绍了四个分位数回归命令，`qreg`, `genqreg`, `qregpd` 以及 `xtrifreg`。



## 参考文献

[1] Firpo, S. et al. (2009) "Unconditional Quantile Regressions", *Econometrica*, Vol. 77(3), pp. 953-973. 

[2] Martins, P. S., & Pereira, P. T. (2004). Does education reduce wage inequality? Quantile regression evidence from 16 countries. *Labour economics*, 11(3), 355-371.

[3] Okada, K., & Samreth, S. (2012). The effect of foreign aid on corruption: A quantile regression approach. *Economics Letters*, 115(2), 240-243.

[4] Powell, D. (2016). Quantile regression with nonadditive fixed effects. Quantile Treatment Effects, 1-28.



&emsp;


&emsp;





> #### [2019暑期Stata现场班，7.17-26日，北京，连玉君+刘瑞明 主讲](https://mp.weixin.qq.com/s/7heG1VrmUtuV74iWhEi93A)





&emsp;

[![2019暑期Stata现场班，7.17-26日，北京，连玉君+刘瑞明 主讲](https://images.gitee.com/uploads/images/2019/0716/235051_4ab345a4_1522177.png)](https://gitee.com/arlionn/Course/blob/master/2019%E6%9A%91%E6%9C%9FStata%E7%8E%B0%E5%9C%BA%E7%8F%AD.md)



&emsp;



>#### 关于我们

- 【**Stata 连享会(公众号：StataChina)**】由中山大学连玉君老师团队创办，旨在定期与大家分享 Stata 应用的各种经验和技巧。
- 公众号推文同步发布于 [CSDN-Stata连享会](https://blog.csdn.net/arlionn) 、[简书-Stata连享会](http://www.jianshu.com/u/69a30474ef33) 和 [知乎-连玉君Stata专栏](https://www.zhihu.com/people/arlionn)。可以在上述网站中搜索关键词`Stata`或`Stata连享会`后关注我们。
- 点击推文底部【阅读原文】可以查看推文中的链接并下载相关资料。
- Stata连享会 [精品专题](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文](https://github.com/arlionn/stata/blob/master/README.md)

>#### 联系我们

- **欢迎赐稿：** 欢迎将您的文章或笔记投稿至`Stata连享会(公众号: StataChina)`，我们会保留您的署名；录用稿件达`五篇`以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。您也可以从 [连享会选题平台](https://gitee.com/Stata002/StataSX2018/wikis/Home) &rarr; [002_备选主题] 中选择感兴趣的题目来撰写推文。
- **意见和资料：** 欢迎您的宝贵意见，您也可以来信索取推文中提及的程序和数据。
- **招募英才：** 欢迎加入我们的团队，一起学习 Stata。合作编辑或撰写稿件五篇以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。
- **联系邮件：** StataChina@163.com

>#### 往期精彩推文
- [Stata连享会推文列表](https://www.jianshu.com/p/de82fdc2c18a) 
- Stata连享会 [精品专题](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文](https://github.com/arlionn/stata/blob/master/README.md)

[![点击此处-查看完整推文列表](https://images.gitee.com/uploads/images/2019/0716/235051_5001f3db_1522177.png "连享会(公众号: StataChina)推文列表")](https://gitee.com/arlionn/Course/blob/master/README.md)



---
> [![image](http://upload-images.jianshu.io/upload_images/7692714-e52cb14e1928d3f1.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "扫码关注 Stata 连享会")](https://gitee.com/arlionn/Course/blob/master/README.md)





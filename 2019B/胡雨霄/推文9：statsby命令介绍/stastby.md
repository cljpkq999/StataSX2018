

本篇推文介绍命令 `statsby`，该命令被广泛用于分组汇报单值 (scalar) 或者系数。

## 1. statsby 命令简介

首先，安装命令。

```
ssc install statsby 
```

在介绍 `statsby` 命令前，先简要介绍如下两个概念，作为铺垫，以便更好理解 `statsby` 命令。

- Stata 中的返回值概念

Stata 命令主要可以分为四种类型：（1）**r-class** 与模型估计无关的命令，如，`summary`; （2）**e-class** 与模型估计有关的命令，如，`regress`；（3）**s-class** 其他命令，如，`list`；（4）**c-class** 存储系统参数。

相应地，显示留存值的方法也分别为：`return list`,`ereturn list`,`sreturn list`,以及 `creturn list`。

留存值也可分为四种类型：（1）单值，如，`r(mean)`, `r(max)`, `r(N)`, `e(r2)`, `e(F)`；（2）矩阵，如，`e(b)`, `e(V)`；（3）暂元，如，`e(cmd)`, `e(depvar)`; （4）函变量，如，`e(sample)`。

- Stata 循环语句

相关介绍请参照推文 [普林斯顿Stata教程 - Stata编程](https://www.jianshu.com/p/916ddc3948d6)。

`statsby` 命令的基本语法十分简单。

```
statsby [exp_list], by(): command
```

其中，`by()` 规定了分组方式。

`[exp_list]` 规定留存值

## 2. 实例 1：输出 `r-class` 留存值

```
sysuse auto, clear
statsby mean=r(mean) sd=r(sd) size=r(N), by(rep78): summarize mpg
```

`statsby` 命令保存了不同 **rep78** 组别的 **mpg** 的平均值、标准误以及观察值个数。

```
. list

     +------------------------------------+
     | rep78       mean         sd   size |
     |------------------------------------|
  1. |     1         21    4.24264      2 |
  2. |     2     19.125   3.758324      8 |
  3. |     3   19.43333   4.141325     30 |
  4. |     4   21.66667    4.93487     18 |
  5. |     5   27.36364   8.732385     11 |
     +------------------------------------+
```

最后的数据结构如上所示。

## 2. 实例 2：输出不同组别的系数

实例 1 所介绍的 `statsby` 命令的应用其实并不常用。一个更具有实证价值的应用为输出不同组别的系数。 Opler et al. (1999, JFE, [[PDF]](https://cpb-us-w2.wpmucdn.com/u.osu.edu/dist/0/30211/files/2016/05/Determinants_cash-holdings-1w4ptsv.pdf)) 中图 3 的绘制就是一个很好的例子。 

![coefficient.png](https://upload-images.jianshu.io/upload_images/18387775-c55cef9ed236d113.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

该文研究资本结构的动态调整速度。动态资本结构理论认为企业的最优资本结构并不是固定的股权债务比例，而是一个变化的股权债务比例范围，企业设定目标资本结构，并依据各种条件的变化不断地进行着资本结构的调整。

其基本模型为：

$$y_{it}-y_{i,t-1}=\lambda(y^*_{it}-y_{i,t-1}) $$

$$y^*_{it}=\theta x_{it}$$

$y_{it}$ 为公司 $i$ 在 $t$ 期的资本结构，$y^*_{it}$ 为目标资本结构。我们可以将 $\lambda$ 理解为每家公司的调整系数。

设定数据模拟情形

$$y_{it}=0.6*y_{i,t-1}+1.3*x_{it}+a_i+e_{it}$$

$$x_{it}=0.2*x_{i,t-1}+v_{it}$$

$$corr(x_{it},a_i) = 1 $$

命令如下：

```
 xtarsim y x eta, n(1000) t(10) gamma(0.6) beta(1.3) rho(0.2) ///
                    one(corr 3) sn(9) seed(1234) 
```

数据结构如下

```
. list in 1/10

     +--------------------------------------------------+
     | ivar   tvar           y            x         eta |
     |--------------------------------------------------|
  1. |    1      1   13.862633    2.5646089   2.6616445 |
  2. |    1      2   8.3913755   -2.0077635   2.6616445 |
  3. |    1      3   11.940121    2.6233153   2.6616445 |
  4. |    1      4    14.46718    3.0163007   2.6616445 |
  5. |    1      5   10.069268   -.78471857   2.6616445 |
     |--------------------------------------------------|
  6. |    1      6   12.524141    2.3777554   2.6616445 |
  7. |    1      7   14.268779    2.3195181   2.6616445 |
  8. |    1      8   15.289451    2.9322523   2.6616445 |
  9. |    1      9   10.236962   -1.5005251   2.6616445 |
 10. |    1     10   8.8389802    .74967746   2.6616445 |
     +--------------------------------------------------+
```

接下来，运用 `statsby` 命令估计每家公司的调整系数

```
xtset ivar tvar
statsby _b[L.D.y], by(ivar) saving("$D\Lev_speed", replace): ///
            reg D.y L.D.y
```

该命令将回归 `reg D.y L.D.y` 的系数（**_b[L.D.y]**）存储在新数据集中。为了输出 Opler et al. (1999, JFE, [PDF]) 图 3 类似的图形，采取如下的命令。

```
use "$D\Lev_speed.dta", clear
    rename _stat_1 speed
    sum speed, d
    histogram speed
```

**speed** 表示资本动态结构调整的速度，其描述性统计结果如下。

```
.         sum speed, d

                          _b[L.D.y]
-------------------------------------------------------------
      Percentiles      Smallest
 1%    -.8351784      -1.170866
 5%    -.6367424      -1.130177
10%    -.5305484      -1.103174       Obs               1,000
25%    -.3444118      -1.017748       Sum of Wgt.       1,000

50%    -.1253898                      Mean          -.1340506
                        Largest       Std. Dev.      .3053147
75%      .063599       .7306181
90%     .2565345       .7385662       Variance       .0932171
95%     .3583316       .7686451       Skewness      -.0333423
99%     .6108425       .8620657       Kurtosis       3.120044
-------------------------------------------------------------

```

直方图如下。我们可以看到，与 Opler et al. (1999, JFE, [PDF]) 的图 3 是非常类似的，汇报了资本动态结构调整速度的直方图。

![coefficient1.png](https://upload-images.jianshu.io/upload_images/18387775-c3d05f6b8a16ccff.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


## 参考文献
Opler, T., Pinkowitz, L., Stulz, R., & Williamson, R. (1999). The determinants and implications of corporate cash holdings. *Journal of financial economics*, 52(1), 3-46.





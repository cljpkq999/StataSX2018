# 多层级 Tobit 模型及 Stata 应用

> **Source：** [Stata Tips #19 - Multilevel Tobit regression models in Stata 15](https://www.timberlake.co.uk/news/stata-tips-19-multilevel-tobit-regression-models-stata-15/)

Tobit模型用于删失因变量，其数值只能用一个范围来表示。为了了解我们期望考察的关系，本文利用了模拟的数据集，删掉了高于上限的值，即右删失。
Stata之前的版本就可以运算Tobit模型，但Stata15版本可以执行具有随机截距和随机斜率的多级版本。我们先来看简化的数学表达如下：

$y_{i j}=\beta_{0}+u_{j}+\left(\beta_{1}+v_{j}\right) x_{i j}+\epsilon_{i j}$

$\epsilon \sim N\left(0, \sigma_{\epsilon}^{2}\right)$

$u \sim N\left(0, \sigma_{u}^{2}\right)$

$v \sim N\left(0, \sigma_{v}^{2}\right)$

![输入图片说明](https://images.gitee.com/uploads/images/2019/0812/105630_6d77c709_5089143.png "倔强的公式.png")

在这些等式中，$i$ 表示个体，$j$ 表示多级结构的多个个体的集合。分析多层模型优势的一种简洁的方式是，将无法解释的方差分解为观察值层面和聚类层面的方差，通过此种方式，可以对数据给出更深入的理解、得出更为细致的结论。

x 是因变量，y 是自变量，但是我们不能直接观测到 y，而仅能观测到在临界值在 h 水平上的删失值 y*。Β 为简单回归系数。然后，多层结构的数据中的随机截距项 u，在不同的聚类中不同，另外随机斜率 v，是 x 对 y 的影响。一般而言，u 和 v 的值在 0 周围呈现正态分布。

## 1. 模拟数据

本文以截肢患者学习使用无线电动假肢手臂为例，进行虚拟的调查研究。本文所使用的数据是虚假的，但是以现实的研究为基础。

在这个数据集中，有两个需要实验参与者完成的训练项目（变量：**train**），并且在随后的几周内（变量：**week**）将对参与者（以变量 **id** 来识别）进行追踪。他们必须通过执行难度升级的四项任务（变量：**task**）来完成测试，并且他们在完成任务的过程中将被计时（变量：**time**），我们希望时间越短越好。但是**time**变量的上限是 30 秒，也就是说，如果他们在 30 秒内无法完成任务，那么时间将被记录为 30 秒。

因变量是右删失的。我们必须考虑在多层级模型中数据的重复性特征（也就是经济学家们所说的“面板数据”）。每个 **id** 都有随机截距项，每个 **week** 都有随机斜率，本研究主要关注 **week** 和 **train** 的交互项，因为这说明不同的训练项目对恢复率影响的区别。

如下图示表示被分为四项任务和两个训练组的每个“病人”随时间的变化情况。

![图1.png](https://images.gitee.com/uploads/images/2019/0715/121703_877929dc_5089143.png)

![图2.png](https://images.gitee.com/uploads/images/2019/0715/121703_0d2b5b80_5089143.png)

## 2. 获得因变量实际值的情况

我们可以拟合一个多层线性模型，称之为“模型1”。


```stata
mixed y c.week##i.train i.task || id: week, covariance(unstructured)
margins train, at(week = (1(1)5))
marginsplot, name(model1, replace) scheme(s1mono) ylabel(,grid) ytitle("") title("Model 1")
``` 

接下来，我们用高于 30 秒的删失因变量进行了相同的线性回归。唯一的变化是，用 y* 替代 y，即用 30 来替代高于 30 的任何值，这是模型 2 。因为它忽略了删失值，所以我们不应该太惊讶于产生的偏差结果。

![图3.png](https://images.gitee.com/uploads/images/2019/0715/121703_1112a63a_5089143.png)

在上表中，高亮的单元格显示了模型 1 中位于模型 2 的 95% 的置信区间外的参数。

```stata
gen ystar = y
replace ystar = ${h} if y >= ${h}
mixed ystar c.week##i.train i.task || id: week, covariance(unstructured)
margins train, at(week = (1(1)5))
marginsplot, name(model2, replace) scheme(s1mono) ylabel(,grid) ytitle("") title("Model 2")
``` 

这些偏差产生的主要来源于人为将数据控制在上图中的红色虚线以下，并且线性模型试图对此进行补偿。但是，当我们用`metobit`（与`mixed`的语法基本相同）来拟合“模型 3 ”，令 Stata 对在删失的临界值以上部分的删失值的数据分布求积分。这要求模型中建立一些假设，当然，这一线性、正态分布的数据集很好的满足了这些假设条件。如表所示，模型2中的偏差都消失了。

```stata
metobit ystar c.week##i.train i.task || id: week, ul(30) covariance(unstructured)
margins train, at(week = (1(1)5))
marginsplot, name(model3, replace) scheme(s1mono) ylabel(,grid) ytitle("") title("Model 3")
``` 

我们可以使用 `margins` 和` marginsplot` 来帮助我们解释以上模型。你可能会注意到，当使用 `margins`命令时，`metobit` 比 `mixed` 更为耗时，即对计算机提出的要求更高。

![图4.png](https://images.gitee.com/uploads/images/2019/0715/121703_699cb4e5_5089143.png) 


### 参考文献
**1**.Fluegge K R. Assessing the impact of patient self-selection on the costs to treat latent tuberculosis infection (LTBI) with isoniazid and  transitional rifampin [J]. Journal of Evaluation in Clinical Practice, 2015, 20(5):685-691.

**2**.Belasco A S, Trivette M J, Webber K L. Advanced Degrees of Debt: Analyzing the Patterns and Determinants of Graduate Student Borrowing[J]. Review of Higher Education, 2014, 37(4):469-497.

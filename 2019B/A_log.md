#### 团队工作日志

> 选题情况 `2019/8/8 11:41`

1. 潘星宇 2 篇 rddsg 和 一刀切did政策
2. 何庆红 2 篇 多期DID和Synth_Runner命令
3. 胡雨霄 2 篇 dominance analysis和 statsby命令
4. 万  莉 4 篇（1篇stata绘制地图，1篇RDD）
5. 陈勇吏 4 篇（1篇 Monte Carlo 模拟，一篇 boottest 命令介绍，一篇 Wild Bootstrap 简介，一篇组合检验 permute test）


#### 尚未完成任务

> `2019/8/3`

1. 胡雨霄 2篇；
2. 万莉 4篇
3. 潘星宇 2篇 rddsg 和 一刀切did政策
4. 陈勇吏 4篇
5. 李珍 1篇。                              
6. 何庆红 4篇
7. 黄俊凯 4篇
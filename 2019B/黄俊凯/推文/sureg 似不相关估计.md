> 作者：黄俊凯       
> 单位：中国人民大学财政金融学院

<br />

`sureg` 执行似不相关回归 *seemingly unrelated regression* (SUR)    

<br />

### 1 似不相关估计
#### 1.1 系统估计的利弊
> - **利：** 如果多个方程之间有某种联系，对方程组进行联合估计可能提高估计的效率   
> - **弊：** 如果方程组中某个方程误差较大，系统估计会将该方程的误差带入其他方程
#### 1.2 多方程系统的分类
> - **联立方程组：** 不同方程之间的变量存在内在联系，一个方程的解释变量是另一个方程的被解释变量
> - **似不相关回归：** 不同方程之间的变量不存在联系，但各方程的扰动项之间相关     
#### 1.3 进一步研究 
> - **Greene** (2005 P340-351)
> - **陈强** (2014 P468-479)

<br />

### 2 理论说明
#### 2.1 同期不相关假设

$$ read =\beta_{0}+\beta_{1} \cdot \text { math }+\beta_{2} \cdot \text { write }+\beta_{3} \cdot \operatorname{socst}+\varepsilon_{r} $$    

> 其中 $ \beta_0 $ 是方程 ***read*** 的截距项，$\beta_1$, $ \beta_2 $是方程 ***read*** 的回归系数，$ \varepsilon_{r} $ 是方程 ***read*** 的残差项

$$ science = \gamma_{0} + \gamma_{1} \cdot \text { female } + \gamma_{1} \cdot \text { math } + \varepsilon_{s} $$

> 其中 $ \gamma_0 $ 是方程 ***science*** 的截距项，$\gamma_1$ 是方程 ***science*** 的回归系数，$ \varepsilon_{s} $ 是方程 ***science*** 的残差项

由于方程 ***read*** 和方程 ***science*** 的预测变量不完全相同，若满足同期不相关假设，即残差项 $ \varepsilon_{r} $ 和 $ \varepsilon_{s} $ 不相关，则方程系统 ***read*** 和 ***science*** 的联合协方差矩阵 $ V $ 是单位矩阵，此时用 **OLS** 方法估计是有效的。     
但若不满足同期不相关假设，即残差项 $ \varepsilon_{r} $ 和 $ \varepsilon_{s} $ 相关，则方程系统 ***read*** 和 ***science*** 的联合协方差矩阵 $ V $ 不是单位矩阵，由于联合协方差矩阵是未知的，此处应使用 **FGLS** 方法。首先估计联合协方差矩阵 $ V $：

$$ V = S \otimes I_{n} $$

> 其中 $ S $ 是 **OLS** 回归残差的方差协方差矩阵(同期协方差矩阵)，$ I_{n} $ 是一个维数为 $ n $ 的单位矩阵。回归系数的 **FGLS** 估计量为：

$$ \hat{\beta}_{\text {sur}} = \left(X^{\prime} V^{-1} X\right)^{-1} X^{\prime} V^{-1} Y $$

> 其中 $ X $ 时预测变量矩阵，$ Y $ 是结果变量向量，$ V $ 是 $ S $ 和 $ I $ 的克罗内克乘积。
> 综上，**SUR** 估计本质是一个 **FGLS** 估计

#### 2.2 手动复制 `sureg` 命令
[原始方法](https://stats.idre.ucla.edu/stata/code/fitting-a-seemingly-unrelated-regression-sureg-manually/)

```stata
use https://stats.idre.ucla.edu/stat/data/hsb2, clear

//    用 OLS 回归估计方程系统中所有方程的残差项
regress read write math socst
predict r_read, resid
regress science female math
predict r_sci, resid

//    同期协方差矩阵
cor r_read r_sci, cov

//    联合协方差矩阵
mat s = r(C)
mat i = I(200)
mat v = s#i

//    构造 X
mkmat math write socst cons, matrix(x_read)
mat x_read = x_read , J(200,3,0)
mkmat female math cons, matrix(x_sci)
mat x_sci = J(200,4,0) , x_sci
mat x = x_read  \x_sci

//    构造 Y
mkmat read, matrix(y_read)
mkmat science, matrix(y_sci)
mat y = y_read  \y_sci

//    计算回归系数
mat b = inv(x'*inv(v)*x)*x'*inv(v)*y

//    读取回归系数
st_matrix("b",b)

//    展示回归系数
mat list b0
mat list b1
```

<br />

### 3 语法结构
#### 3.1 基本语法
```stata
sureg (depvar1 varlist1) (depvar2 varlist2) ... (depvarN varlistN) [if] [in] [weight]
```
#### 3.2 完整语法
```stata
sureg ([eqname1:] depvar1a [depvar1b ... =] varlist1 [, noconstant])
      ([eqname2:] depvar2a [depvar2b ... =] varlist2 [, noconstant])
      ...
      ([eqnameN:] depvarNa [depvarNb ... =] varlistN [, noconstant])
      [if] [in] [weight] [,options]
```
> #### 参数     
> - `eqname` 方程
> - `depvar` 被解释变量
> - `varlist` 控制变量    
> #### 选项      
> - `corr` 展示方程组残差的相关系数矩阵，并执行 Breusch-Pagan LM 检验判断是否 SUR 估计更有效
> - `isure` 迭代直到估计收敛
> - `tolerance(#)` 默认忍耐度为 1e-6，设定迭代停止时前后两次迭代的系数向量之间的最大相对变动
> - `constraints(constraints)` 线性约束
> - 可以与 `by` 等 Stata 前缀连用

<br />

### 4 一个实例
```stata
sysuse auto, clear

sureg (price foreign weight length) (mpg foreign weight) (displ foreign weight)
//    它等价于 
sureg (price foreign weight length) (mpg displ = foreign weight)
//    还等价于 
global price (price foreign weight length)
global mpg (mpg foreign weight)
global displ (displ foreign weight)
sureg $price $mpg $displ
//    运行结果
Seemingly unrelated regression
--------------------------------------------------------------------------
Equation             Obs   Parms        RMSE    "R-sq"       chi2        P
--------------------------------------------------------------------------
price                 74       3    1967.769    0.5488      89.74   0.0000
mpg                   74       2    3.337283    0.6627     145.39   0.0000
displacement          74       2    39.60002    0.8115     318.62   0.0000
--------------------------------------------------------------------------

------------------------------------------------------------------------------
             |      Coef.   Std. Err.      z    P>|z|     [95% Conf. Interval]
-------------+----------------------------------------------------------------
price        |
     foreign |    3575.26   621.7961     5.75   0.000     2356.562    4793.958
      weight |   5.691462   .9205043     6.18   0.000     3.887307    7.495618
      length |  -88.27114    31.4167    -2.81   0.005    -149.8467   -26.69554
       _cons |   4506.212   3588.044     1.26   0.209    -2526.225    11538.65
-------------+----------------------------------------------------------------
mpg          |
     foreign |  -1.650029   1.053958    -1.57   0.117    -3.715748    .4156902
      weight |  -.0065879   .0006241   -10.56   0.000     -.007811   -.0053647
       _cons |    41.6797   2.121197    19.65   0.000     37.52223    45.83717
-------------+----------------------------------------------------------------
displacement |
     foreign |   -25.6127   12.50621    -2.05   0.041    -50.12441   -1.100984
      weight |   .0967549   .0074051    13.07   0.000     .0822411    .1112686
       _cons |  -87.23548   25.17001    -3.47   0.001    -136.5678   -37.90317
------------------------------------------------------------------------------


//    corr 选项
sureg (price foreign weight length) (mpg foreign weight) (displ foreign weight), corr
--------------------------------------------------------------------
Correlation matrix of residuals:

                     price           mpg  displacement
       price        1.0000
         mpg       -0.0220        1.0000
displacement        0.1765        0.0229        1.0000

Breusch-Pagan test of independence: chi2(3) =     2.379, Pr = 0.4976
--------------------------------------------------------------------
//    Pr = 0.4976 不拒绝无同期相关假设，应用 OLS 比 SUR 更有效

//    加入约束条件
constraint 1 [price]foreign = [mpg]foreign
constraint 2 [price]foreign = [displacement]foreign
sureg (price foreign length) (mpg displacement = foreign weight), const(1 2)
```

<br />

### 5 引用   
陈强. 高级计量经济学及 Stata 应用[M]. 高等教育出版社, 2014.     
Greene, William H. (2005). Econometric Analysis. Fifth edition. Pearson Education.       
[Stata FAQ: What is seemingly unrelated regression and how can I perform it in Stata?](https://stats.idre.ucla.edu/stata/faq/what-is-seemingly-unrelated-regression-and-how-can-i-perform-it-in-stata/)    
[Stata Code Fragments: Fitting a seemingly unrelated regression(sureg)manually](https://stats.idre.ucla.edu/stata/code/fitting-a-seemingly-unrelated-regression-sureg-manually/)


> 作者：黄俊凯 (中国人民大学财政金融学院)      
>     
> Stata 连享会： [知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn) | [CSDN](https://blog.csdn.net/arlionn)

&emsp;
  


[2019暑期论文班-连玉君-江艇主讲](https://gitee.com/arlionn/Course/blob/master/2019Papers.md)

[![2019暑期论文班-连玉君-江艇主讲](https://images.gitee.com/uploads/images/2019/0622/184256_8d116938_1522177.jpeg)](https://gitee.com/arlionn/Course/blob/master/2019Papers.md)


&emsp;


缺失值就像灭霸，不可避免，我们能做的只有迎难而上      
今天我们介绍一个外部命令`carryforward`，用于简单的填充缺失值  

顾名思义，`carryforward`用非空前值填充缺失的观测值  
*carryforward will carry values forward from one observation to the next, filling in missing values with the previous value*      
`carryforward`特别适用于以下三个场景：
> 1. 根据变动事件扩展数据，见 4.3
> 2. 用于填补数据横向合并（`merge`）造成的缺失值
> 3. 其他面板数据缺失值问题，因此特别适用于使用面板数据较多的研究领域，如：区域经济增长，财政，公司金融，会计等

<br />

## 1 一个简单的例子
```stata
carryforward x, gen(y)

//  按顺序用 **x** 的非空前值填充 **y** 的缺失值
-----------
   x    y 
-----------
  12   12 
   4    4 
   .    4 
   .    4 
   .    4 
   3    3 
   .    3 
   7    7 
   .    7 
   .    7 
-----------
```
如果不使用 `carryforward` 命令，[你可以借助系统变量 **_n** ](https://www.stata.com/support/faqs/data-management/replacing-missing-values/)，但效率低且易出错

<br />

## 2 下载安装

```stata
ssc install carryforward, replace
```


&emsp;


## 3 语法结构

### 3.1 基本语法

```stata
carryforward varlist [if] [in], gen(newvarlist) | replace
```

**Notes：** `carryforward` 命令支持分组计算 
         
### 3.2 进阶语法

```stata
carryforward varlist [if] [in], {gen(varlist1) | replace}  ///
   [cfindic(varist2) back carryalong(varlist3) strict ///
    nonotes dynamic_condition(dyncond) extmiss]
```

各个选项简要解释如下：

- `gen(varlist1)` 生成填充后的变量列表 varlist1，varlist 和 varlist1 是一一对应的关系，因此必须包含相同数量的变量
- `cfindic(varist2)` 生成指示变量列表 varlist2，若指示变量等于 1，对应的是原始数据；若等于 0，对应的是填充数据
- `carryalong(varlist3)` 对已存在的变量列表 varlist3，应用与 varlist 相同的填充规则，这意味着：   
  - varlist3 中的非缺失值可能被填充
  - varlist3 中的缺失值可能不被填充
  - 必须配合 `replace` 选项一起使用
- `extmiss` 选项仅限于数值型变量，它视扩展型缺失值 extended missing values (.a, .b, etc.) 为实际数值
  - Stata 中只有一种字符型缺失值，即`""`
  - Stata 中有27种数值型缺失值，默认的缺失值为sysmiss(.)
  - 另外26种数值型缺失值是：.a, .b,.c, ..., .z，即 `extended missing values`
  - 数值型缺失值在stata中被定义为无穷大，大小关系为：`nonmissing numbers` < `.` < `.a` < `.b` < ... < `.z`


&emsp;



## 4 实例

### 4.1 向后填充

```stata
sort id year 
carryforward id year, replace
```

### 4.2 向前填充

```stata
gsort id -year 
carryforward id year, replace
```

### 4.3 广义缺失值

```stata
carryforward x ,gen(x1)
carryforward y ,gen(y1)
carryforward x ,gen(x2) extmiss
carryforward y ,gen(y2) extmiss

* extmiss 选项指定 extended missing values (.a, .b, etc.) 为实际数值
* y1 是没有 extmiss 选项的填充结果，.z 等扩展型缺失值不参与向后填充
* y2 是有 extmiss 选项的填充结果，.z 等扩展型缺失值参与向后填充
  +----------------------------+
  | x    y   x1   y1   x2   y2 |
  |----------------------------|
  | 1    1    1    1    1    1 |
  | .    .    1    1    1    1 |
  | .   .z    1    1    1   .z |
  | .    .    1    1    1   .z |
  | 3    3    3    3    3    3 |
  | .   .b    3    3    3   .b |
  | .   .c    3    3    3   .c |
  | .    .    3    3    3   .c |
  | 3    3    3    3    3    3 |
  | .   .d    3    3    3   .d |
  | .    .    3    3    3   .d |
  | .    .    3    3    3   .d |
  +----------------------------+
```

### 4.4 根据 **CSMAR** 股本变动文件生成日股本数据

中南财经政法大学的李春涛教授有一个借助 [`post`](https://wenku.baidu.com/view/38f7fbfcff00bed5b8f31d17.html?from=search) 命令实现的版本     
在这里我尝试借助 `carryforward` 命令，仅用十行核心代码

```stata
//  原始数据（ CSMAR 数据库股本变动文件）
---------------------------------------------------
clear
input long stkcd float date double(nshrttl nshra)
1 11415    48500171    26500000
1 11535    89751643    44988921
1 11770   134696625    67518049
1 12197   269417899   145138500
1 12599   269417899   179122251
1 12610   431068638   286595634
1 12663   431068638   297371634
1 13051   517282365   356851292
1 13296  1034564728   713934984
1 13751  1551847092  1070902476
1 14920  1945822149  1393124764
1 15263  1945822149  1409361965
1 16435  1945822149  1409361965
1 17337  2086758345  1550187787
1 17527  2086758345  1550186587
1 17528  2293407145  1756820549
1 17708  2293407145  1756791401
1 17709  2293407145  2046520536
1 17710  2388795202  2142004409
1 17836  3105433762  2784605731
1 18067  3105433762  2784593862
1 18070  3105433762  2923674599
1 18184  3105433762  2923756824
1 18185  3105433762  2924114263
1 18390  3105433762  2924114263
1 18441  3105433762  3105369975
1 18442  3485013762  3105367602
1 18627  3485013762  3105358511
1 18827  3485013762  3105358672
1 18828  5123350416  3105358672
1 19358  5123350416  3105358022
1 19523  5123350416  3105358672
1 19529  8197360665  4968573875
1 19674  8197360665  5575901875
1 19731  9520745656  5575901875
1 19880  9520745656  5575882183
1 19886 11424894787  6691058620
1 19964 11424894787  6691105775
1 19967 11424894787  9836712150
1 20191 13709873744 11804054580
1 20228 14308676139 11804054579
1 20597 14308676139 12192650323
1 20621 17170411366 14631180387
1 20828 17170411366 16917989651
1 21184 17170411366 16917983372
1 21322 17170411366 16917998790
1 21325 17170411366 17170246773
2 11351    41246700    28000000
2 11481    76970509    56518173
2 11767    92364611    67821807
end
format %tdCCYY-NN-DD date
label var stkcd "证券代码" 
label var date "股本变动日期" 
label var nshrttl "总股数 股" 
label var nshra "A股流通股数 股" 
---------------------------------------------------

//  十行代码
  sort stkcd date
  bys stkcd: keep if _n == 1
  gen last = mdy(6,29,2019)		//	今天的日期
  gen duration = last - date
  
  expand duration
  keep stkcd date
  sort stkcd date
  
  bys stkcd: replace date = date + _n - 1
  merge 1:1 stkcd date using $im, keep(1 3) nogen
  carryforward nshrttl, cfindic(change)  ///
      carryalong(nshra) replace

//  结果如下
//  cfindic(change) 生成 change 变量，当 change == 1 时代表原值为空值，被前值填充
//  carryalong(nshra) 代表 nshra 变量的缺失值填充规则跟随 nshrttl
  +---------------------------------------------------+
  | stkcd         date    nshrttl      nshra   change |
  |---------------------------------------------------|
  |     1   1991-04-03   48500171   26500000        0 |
  |     1   1991-04-04   48500171   26500000        1 |
  |     1   1991-04-05   48500171   26500000        1 |
  |     1   1991-04-06   48500171   26500000        1 |
  |     1   1991-04-07   48500171   26500000        1 |
  |     1   1991-04-08   48500171   26500000        1 |
  |     1   1991-04-09   48500171   26500000        1 |
  |     1   1991-04-10   48500171   26500000        1 |
  |     1   1991-04-11   48500171   26500000        1 |
  |     1   1991-04-12   48500171   26500000        1 |
  |     1   1991-04-13   48500171   26500000        1 |
  |     1   1991-04-14   48500171   26500000        1 |
  |     1   1991-04-15   48500171   26500000        1 |
  |     1   1991-04-16   48500171   26500000        1 |
  |     1   1991-04-17   48500171   26500000        1 |
  |     1   1991-04-18   48500171   26500000        1 |
  |     1   1991-04-19   48500171   26500000        1 |
  |     1   1991-04-20   48500171   26500000        1 |
  |     1   1991-04-21   48500171   26500000        1 |
  |     1   1991-04-22   48500171   26500000        1 |
  +---------------------------------------------------+

```

<br />

## 7 参考资料
- [How can I “fill down”/expand observations with respect to a time](https://stats.idre.ucla.edu/stata/faq/how-can-i-fill-downexpand-observations-with-respect-to-a-time-variable/)
- [Carryforward data for a limited number of rows - Statalist](https://www.statalist.org/forums/forum/general-stata-discussion/general/1291938-carryforward-data-for-a-limited-number-of-rows)
- [carryforward——填充缺失值| Stata-Club](https://stata-club.github.io/%E6%8E%A8%E6%96%87/20161216/)
- [李春涛：Post 命令处理股本变动](https://wenku.baidu.com/view/38f7fbfcff00bed5b8f31d17.html?from=search)
- [CARRYFORWARD: Stata module to carry forward previous observations](https://ideas.repec.org/c/boc/bocode/s444902.html)
- [How can I replace missing values with previous or following nonmissing values or within sequences?](https://www.stata.com/support/faqs/data-management/replacing-missing-values/)


&emsp;


>#### 关于我们

- 【**Stata 连享会(公众号：StataChina)**】由中山大学连玉君老师团队创办，旨在定期与大家分享 Stata 应用的各种经验和技巧。
- 公众号推文同步发布于 [CSDN-Stata连享会](https://blog.csdn.net/arlionn) 、[简书-Stata连享会](http://www.jianshu.com/u/69a30474ef33) 和 [知乎-连玉君Stata专栏](https://www.zhihu.com/people/arlionn)。可以在上述网站中搜索关键词`Stata`或`Stata连享会`后关注我们。
- 点击推文底部【阅读原文】可以查看推文中的链接并下载相关资料。
- Stata连享会 [精品专题](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文](https://github.com/arlionn/stata/blob/master/README.md)

>#### 联系我们

- **欢迎赐稿：** 欢迎将您的文章或笔记投稿至`Stata连享会(公众号: StataChina)`，我们会保留您的署名；录用稿件达`五篇`以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。您也可以从 [连享会选题平台](https://gitee.com/Stata002/StataSX2018/wikis/Home) &rarr; [002_备选主题] 中选择感兴趣的题目来撰写推文。
- **意见和资料：** 欢迎您的宝贵意见，您也可以来信索取推文中提及的程序和数据。
- **招募英才：** 欢迎加入我们的团队，一起学习 Stata。合作编辑或撰写稿件五篇以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。
- **联系邮件：** StataChina@163.com

>#### 往期精彩推文
- [Stata连享会推文列表](https://www.jianshu.com/p/de82fdc2c18a) 
- Stata连享会 [精品专题](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文](https://github.com/arlionn/stata/blob/master/README.md)

[![点击此处-查看完整推文列表](https://images.gitee.com/uploads/images/2019/0630/215504_0346d148_1522177.png "连享会(公众号: StataChina)推文列表")](https://gitee.com/arlionn/Course/blob/master/README.md)




---
> [![image](http://upload-images.jianshu.io/upload_images/7692714-e52cb14e1928d3f1.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "扫码关注 Stata 连享会")](https://gitee.com/arlionn/Course/blob/master/README.md)


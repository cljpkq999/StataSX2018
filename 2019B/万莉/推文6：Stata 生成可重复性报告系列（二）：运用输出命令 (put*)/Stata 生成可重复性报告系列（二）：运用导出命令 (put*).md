> #### [连享会：内生性问题及估计方法专题](https://mp.weixin.qq.com/s/FWpF5tE68lAtCEvYUOnNgw)
[![连享会-内生性专题现场班-2019.11.14-17](https://images.gitee.com/uploads/images/2019/1030/120141_b5f34b76_1522177.png)](https://gitee.com/arlionn/Course/blob/master/Done/2019Endog.md)


> 作者：万莉 (北京航空航天大学)       
>    &emsp;   
> Stata 连享会：([知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn) | [CSDN](https://blog.csdn.net/arlionn) | [公众号 StataChina](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw))  


[![点击查看完整推文列表](https://images.gitee.com/uploads/images/2019/1030/120141_f9209091_1522177.png)](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw)

> Stata连享会 &emsp; [计量专题](https://gitee.com/arlionn/Course/blob/master/README.md)  || [精品课程](https://mp.weixin.qq.com/s/hWtncj56PeFNL4yg2-va0Q) || [简书推文](https://www.jianshu.com/p/de82fdc2c18a)  || [公众号合集](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw)





&emsp;

## 引言

本文是 [「Stata  可重复性报告系列 A：动态文档命令 (dyn*)」](https://www.jianshu.com/p/733361801722) 的姊妹篇。

在 Stata 16 发布页面， "[**truly reproducible reporting（可重复性报告）**](https://www.stata.com/new-in-stata/truly-reproducible-reporting/#dhtml)" 成为亮点之一。继 Stata 15 之后， Stata16 进一步引入和完善一系列相关命令。

对于 Stata 用户而言，我们可以用这些命令直接编写 dofile， 将报告正文、Stata 生成的图表等整合在一个文档中，自定义生成各种格式的文档（text，Word，PDF，Excel 或 HTML）。

更为重要的是，我们可实现可重复性研究，即报告中的数据、模型发生变化后，我们只需稍加修改 Stata 命令，便可输出更新后的文档，而不必逐个修改变动后的结果。

生成可重复性报告（Reproducible and automated reporting）的命令可分为两类：
- **第一类：dyn 类——生成文档：** 各种带 `dyn` 前缀的动态文档生成命令 (如 `dyntext` |  `dyndoc`)，用于生成 text、HTML 和 Word 文档。更重要的是，该命令支持 **Markdown** 文本格式语言。

- **第二类：put 类——输出文档：** 各种带 `put` 前缀的文档输出命令 (`putdocx` | `putpdf` | `putexcel` )。此类命令可自定义生成 Word、PDF 和 Excel 文件。

本文着重讲解第二类命令的使用方法。第一类命令的使用方法参考推文 [「Stata  可重复性报告系列 A：动态文档命令 (dyn*)」](https://www.jianshu.com/p/733361801722) 。

&ensp;
## 1. 命令一： putdocx 

```stata
help putdocx // 输出 Word 文档
```

### 1.1 语法结构

该命令包含一系列子命令，主要分为一下四类：
（需要特别说明的是：继 Stata 15 后，Stata 16 对该命令进行了完善，带 `[*]` 的选项不适用于 Stata 15。）

#### Step1. 新建、保存以及合并 .docx 文件
	- `putdocx begin` 新建一个空白 Word 文档。
	- `putdocx describe` 描述当前 Word 文档相关信息（比如有几个表格，有几段）。
	- `putdocx save` 保存并关闭 Word 文档。
	- `putdocx clear`  关闭 Word 文档时不保存更改。 
	- `putdocx append` 对多个 Word 文档的内容进行合并。

	```stata
 	putdocx begin [, pagesize(psize) landscape font(fspec)]

 	* 描述当前 Word 文档相关信息（比如有几个表格，有几段）。
 	putdocx describe
  
 	* 描述当前 Word 文档中的表格信息（比如有几行几列）。
 	putdocx describe tablename
  
 	putdocx save filename [, replace|append]
  
 	putdocx clear
  
 	putdocx append filename1 filename2 [filename3 [...]] 
                       [, saving(filename[, replace])]
 	```

#### Step2. 在 .docx 文件中插入分页符 (page break)
	- `putdocx pagebreak` 在 Word 文档中添加分页符 (page break)。
	- `putdocx sectionbreak` 在 Word 文档中添加分节符 (section break)。
	
	```stata
	putdocx pagebreak
  
	putdocx sectionbreak [, pagesize(psize) landscape]
	```
#### Step3. 在 .docx 文件中添加段落（包括文本、图像）
	- `putdocx paragraph` 另起新段落。 
	- `putdocx text` 在当前段落添加文本。
	- [*]`putdocx textblock` 在当前段落或新段落添加文本块。 
	- [*]`putdocx textfile`  在新的段落里添加预格式化的文本块。
	- `putdocx image`     在当前段落插入图像。
	- [*]`putdocx pagenumber` 在页眉/页脚设置页码。

	```stata
	putdocx paragraph [, paragraph_options]
  
	putdocx text (exp) [, text_options]
  
	putdocx image filename [, image_options]
  
	putdocx textblock begin 
	// 另起一段添加文本块。
  
	putdocx textblock append
	// 在当前段落添加文本块。
  
	putdocx textblock end
	// 该命令是 putdocx textblock begin 或 putdocx textblock append 的结尾。
  
	putdocx textfile textfile [, append stopat(string [, stopatopt])]
	```
#### Step4. 在 .docx 文件中插入表格
	- `putdocx table` 创建新表格（估计结果/描述性统计/变量）  

	```stata
	* 添加表格
	putdocx table tablename = (nrows, ncols) [, table_options]

	putdocx table tablename = data(varlist) [if] [in] [, varnames obsno table_options]

	putdocx table tablename = matrix(matname) [, nformat(%fmt) rownames colnames table_options]

	putdocx table tablename = mata(matname) [, nformat(%fmt) table_options]

	putdocx table tablename = etable[(#1 #2 ... #n)] [, table_options]

	putdocx table tablename = returnset [, table_options]

	* 修改表格和行列
	1. 在单元格里添加内容 (Add content to cell)

	putdocx table tablename(i,j) = (exp) [, cell_options]

	putdocx table tablename(i,j) = image(filename) [, image_options cell_options]

	putdocx table tablename(i,j) = table(mem_tablename) [, cell_options]

	2. 修改表格的布局 (Alter table layout)

	putdocx table tablename(i,.), row_col_options

	putdocx table tablename(.,j), row_col_options

	3. 自定义表格或单元格的形式 (Customize format of cells or table)

	putdocx table tablename(i,j), cell_options

	putdocx table tablename(numlist_i,.), cell_fmt_options

	putdocx table tablename(.,numlist_j), cell_fmt_options

	putdocx table tablename(numlist_i,numlist_j), cell_fmt_options

	putdocx table tablename(.,.), cell_fmt_options
	```


&emsp;
> #### [连享会计量方法专题……](https://mp.weixin.qq.com/s/hWtncj56PeFNL4yg2-va0Q)


### 1.2 范例 1：生成 Cover Page

我们先新建一个 do 文件，命名为 `putdocx_example1`。**注意：**我们也可以不新建 do 文件，直接在已有 do 文件中输入并执行以下命令。

```stata
*--------------------------begin putdocx_example1.do------------------------
putdocx begin, pagesize(A4)
* 声明使用 putdocx 命令，新建 Word 文档
* 我们可这样理解：一个 .docx 文件包含很多段落，
* 每一次写入一段必须声明。

putdocx paragraph, halign(left) ///
  font("微软雅黑",28,black) 
* 写入一段，将段落内容左对齐,并提前预设好字体格式

putdocx text ("这只是个简单的例子。"), ///
  bold underline(dotted) linebreak
* 注意 text 和 ( 之间要留空格！  
* 输入文本，加粗字体，添加点式下划线，并在结尾处换行。

putdocx text (""), linebreak 
* 不输入文本内容，直接换行

putdocx paragraph, halign(center) 
* 新写入一段，将段落内容居中

putdocx text ("这是封面 Cover Page"), ///
  bold linebreak underline(dash) ///
  font("华文楷体",30,black)  
* 输入文本，设置字体格式（黑体，华文楷体，30 号，黑色），
* 在文本内容下添加虚下划线，并在结尾处换行。
  
putdocx text ("这是标题"),  ///
  bold linebreak ///
  font("华文楷体",75,black)    

putdocx text ("这是作者xx"),  ///
  bold linebreak ///
  font("华文楷体",30,purple)      

putdocx text ("这是学校xx"),  ///
  bold linebreak ///
  font("华文楷体",30,red) 
  
putdocx text ("2019.09.04"),  ///
  bold linebreak ///
  font("华文楷体",30,green)    
putdocx save CoverPage, replace	
*--------------------------end putdocx_example1.do---------------------------
```
我们可以通过命令 `shellout` 查看输出结果，也可以在当前路径直接打开 `CoverPage.docx`。

```stata
ssc install outreg2 // 安装此命令，调用shellout命令
shellout "CoverPage.docx"
```

输出结果如下：

![CoverPage.png](https://images.gitee.com/uploads/images/2019/1030/120141_e0f57de0_1522177.png)
&emsp;
> #### [连享会计量方法专题……](https://mp.weixin.qq.com/s/hWtncj56PeFNL4yg2-va0Q)


### 1.3 范例 2：输出包含文字、图表的 Word 文档

我们先新建一个 do 文件，命名为 `putdocx_example2`。**注意：**我们也可以不新建 do 文件，直接在已有 do 文件中输入并执行以下命令。

```stata
*--------------------------begin putdocx_example2.do------------------------
webuse nhanes2, clear

putdocx begin // 声明使用 putdocx 命令，新建 Word 文档
// 我们可这样理解：一个 .docx 文件包含很多段落，每一次写入一段必须声明。

// 添加段落标题，样式为 "标题 (Title)"
putdocx paragraph, style(Title) 
putdocx text ("Blood pressure report")
// 查看相关样式的可选项: help putdocx_appendix

// 添加文本块
putdocx textblock begin
We use data from the Second National Health and Nutrition Examination Survey
 to study the incidence of high blood pressure.
putdocx textblock end

// 添加段落标题，样式为 "标题1 (Heading1)" 
putdocx paragraph, style(Heading1)
putdocx text ("Logistic regression results")

// 添加文本块
putdocx textblock begin
We fit a logistic regression model of high blood pressure on
 weight, age group, and the interaction between age group and sex.
putdocx textblock end

logistic highbp weight agegrp##sex, nopvalues vsquish

// 添加上述命令生成的估计表格
putdocx table results = etable

// 添加段落标题，样式为 "Heading1" 
putdocx paragraph, style(Heading1)
putdocx text ("Interaction plot")

// 进行边际效应分析，并画图
margins agegrp#sex
marginsplot, title(Age Group and Sex Interaction) ///
   ytitle(Expected Probability of High Blood Pressure) ///
   legend(ring(0) bplacement(seast) col(1))
graph export interaction.png, replace

// 另起一段，插入上述命令生成的图
putdocx paragraph, halign(center)
putdocx image interaction.png

// 保存 Word 文档
putdocx save report1, replace	

// 若出现错误：document already open in memory
// 可能是执行 putdocx begin 后遇到错误中断后，再重新执行。
// 解决该错误可尝试该命令：putdocx clear
// 再重新执行上述命令。
*--------------------------end putdocx_example2.do---------------------------
```

我们可以通过命令 `shellout` 查看输出结果，也可以在当前路径直接打开 `report1.docx`。

```stata
ssc install outreg2 // 安装此命令，调用shellout命令
shellout "report1.docx"
```

输出结果如下

![report1.png](https://images.gitee.com/uploads/images/2019/1030/120141_97ff5aea_1522177.png)

&emsp;
> #### [连享会计量方法专题……](https://mp.weixin.qq.com/s/hWtncj56PeFNL4yg2-va0Q)


### 1.4 范例 3：美化输出的 Word 文档

我们对范例 2 的输出结果进行进一步的美化，比如设置页眉、页脚、页码，美化图表等。我们新建一个 do 文件，命名为 `putdocx_example3`。
**注意：**我们也可以不新建 do 文件，直接在已有 do 文件中输入并执行以下命令。

```stata
*--------------------------begin putdocx_example3.do------------------------
webuse nhanes2, clear

// 声明新建带页眉的 Word 文档
putdocx begin, header(head)

// 自定义页眉的内容，并加上页码
putdocx paragraph, toheader(head) font(,14)
putdocx text ("Blood pressure report: ")
putdocx pagenumber

// 添加段落标题，样式为 "Heading1" 
putdocx paragraph, style(Heading1)
putdocx text ("Data")

// 添加文本块
putdocx textblock begin
We use data from the Second National Health and Nutrition Examination Survey
 to study the incidence of high blood pressure.
putdocx textblock end

// 添加段落标题，样式为 "Heading1" 
putdocx paragraph, style(Heading1)
putdocx text ("Logistic regression results")

// 添加文本块
putdocx textblock begin
We fit logistic regression models of high blood pressure on
 weight, age group, and the interaction between age group and sex.
putdocx textblock end

// 添加估计表格
logistic highbp weight agegrp##sex, nopvalues vsquish
putdocx table results = etable

// 对表格的某几行，添加背景色
putdocx table results(3 5 7 9 11 13 15 17,.), shading(lightgray)

// 设置表格中数字的显示格式，为：共占5个空格，小数位占三个空格
putdocx table results(2/17,2/5), nformat(%5.3f)

// 改变单元格中的内容
putdocx table results(1,1) = ("High BP")

// 添加段落标题，样式为 "Heading1" 
putdocx paragraph, style(Heading1)
putdocx text ("Interaction plot")

// 进行边际效应分析，并画图
margins agegrp#sex
marginsplot, title(Age Group and Sex Interaction) ///
   ytitle(Expected Probability of High Blood Pressure) ///
   legend(ring(0) bplacement(seast) col(1))
graph export interaction.png, replace

// 另起一段，插入上述命令生成的图，并设置图高度
putdocx paragraph, halign(center)
putdocx image interaction.png, height(5 in)

// 保存 Word 文档
putdocx save report2, replace	
*--------------------------end putdocx_example3.do---------------------------
```

我们可以通过命令 `shellout` 查看输出结果，也可以在当前路径直接打开 `report2.docx`。

```stata
ssc install outreg2 // 安装此命令，调用shellout命令
shellout "report2.docx"
```

输出结果如下：

![report2.png](https://images.gitee.com/uploads/images/2019/1030/120142_072f6e10_1522177.png)

&emsp;
> #### [连享会计量方法专题……](https://mp.weixin.qq.com/s/hWtncj56PeFNL4yg2-va0Q)


### 1.5 讨论：如何用 putdocx 写论文

参考[「君生我未生！Stata - 论文四表一键出」](https://www.jianshu.com/p/97c4f291ee1e)和  [「Stata 生成可重复性报告系列（一）：运用动态文档命令 (dyn*)」](https://blog.csdn.net/arlionn/article/details/100530490)，我们可以使用 `putdocx` 命令写论文。

为了更有条理地管理我们的论文，方便日后修改，建议：

1. 为每篇论文建立一个文件夹，比如命名为 **Mypaper_2019**;

2. 在这个文件夹里新建一些子文件夹，如 **Data**，**Prog**，**Output**，**Figs**， **Table** 等，用于存储不同类型的文件。

3. 将论文拆分成三部分：
**My_Paper.docx**，即论文主文档，包含引言、文献综述、理论分析、参考文献等；
**My_Table.docx**，即表格文档，包含统计和回归表格；
**My_Fig.docx**，即图形文档，包含各种图。
每部分都可以用 `putdocx` 或 `dyndoc` 命令进行编写 do 文件，生成相应的 Word 文档。然后把这些 do 文件放到 **Prog** 子文件夹。如果某部分不想用命令，也可以直接在 Word 文档上码字，贴上图表。

   框架如下：

   >![论文模板.png](https://images.gitee.com/uploads/images/2019/1030/120141_4a41ebd3_1522177.png "论文模板.png")

## 2. 命令二：putpdf

```stata
help putpdf // 输出 PDF 文档
```
`putpdf` 的语法结构与 `putdocx` 几乎完全相同，故不展开说明，直接看一个例子。该例子与 `putdocx` 的范例二几乎完全相同，只是将 `putdocx` 变成 `putpdf`。

### 2.1 范例：输出包含文字、图表的 PDF 文档

我们先新建一个 do 文件，命名为 `putpdf_example1`。**注意：**我们也可以不新建 do 文件，直接在已有 do 文件中输入并执行以下命令。

```stata
*--------------------------begin putpdf_example1.do------------------------
webuse nhanes2, clear

putpdf begin // 声明使用 putpdf 命令，新建 PDF 文档
// 我们可这样理解：一个 .pdf 文件包含很多段落，每一次写入一段必须声明。

// 添加段落标题，段落内容居中，并设置字体大小
putpdf paragraph, font(,20) halign(center)
putpdf text ("Blood pressure report")

// 另起一段，添加文本
putpdf paragraph
putpdf text ("We use data from the Second National Health and Nutrition ")
putpdf text ("Examination Survey to study the incidence of high blood pressure.")

// 另起一段，添加文本
putpdf paragraph, font(,16)
putpdf text ("Logistic regression results")

// 另起一段，添加文本
putpdf paragraph
putpdf text ("We fit a logistic regression model of high blood pressure on ")
putpdf text ("weight, age group, and the interaction between age group and sex.")

logistic highbp weight agegrp##sex, nopvalues vsquish

// 添加上述命令生成的估计表格
putpdf table results = etable

// 另起一页，生成新的段落
putpdf pagebreak
putpdf paragraph, font(,16)
putpdf text ("Interaction plot")

// 进行边际效应分析，并画图
margins agegrp#sex
marginsplot, title(Age Group and Sex Interaction) ///
   ytitle(Expected Probability of High Blood Pressure) ///
   legend(ring(0) bplacement(seast) col(1))
graph export interaction.png, replace

// 另起一段，插入上述命令生成的图
putpdf paragraph, halign(center)
putpdf image interaction.png

// 保存 PDF 文档
putpdf save report-pdf, replace		
*--------------------------end putpdf_example1.do---------------------------
```

我们可以通过命令 `shellout` 查看输出结果，也可以在当前路径直接打开 `report.pdf`。

```stata
ssc install outreg2 // 安装此命令，调用shellout命令
shellout "report.pdf"
```

输出结果如下：

![report-pdf.png](https://images.gitee.com/uploads/images/2019/1030/120141_115cffa9_1522177.png)

&emsp;
> #### [连享会计量方法专题……](https://mp.weixin.qq.com/s/hWtncj56PeFNL4yg2-va0Q)


## 3. 命令三：putexcel

```stata
help putexcel // 输出 Excel 文档
```
`putexcel` 的语法结构与 `putdocx` 类似，可粗略地将段落 (paragraph) 的概念替换成单元格 (cell)。故不展开说明，直接看一个例子。该例子的输出结果与上述例子基本相同。

### 3.1 范例：输出包含文字、图表的 Excel 文档

我们先新建一个 do 文件，命名为 `putexcel_example1`。**注意：**我们也可以不新建 do 文件，直接在已有 do 文件中输入并执行以下命令。

```stata
*--------------------------begin putexcel_example1.do------------------------
webuse nhanes2, clear

// 新建 Excel 文档，命名为 report.xlsx。
putexcel set report.xlsx, replace  

// 添加标题
putexcel A1 = "Blood pressure report", bold
putexcel A1:E1, border(bottom, thick) merge hcenter

// 添加文本内容
putexcel A3 = "We use data from the Second National Health and "
putexcel A4 = "Nutrition Examination Survey to study the "
putexcel A5 = "incidence of high blood pressure."

// 为回归结果添加标题
putexcel A7 = "Logistic regression results"
putexcel A7:E7, border(bottom, double)

// 添加文本内容
putexcel A9 = "We fit a logistic regression model of high blood"
putexcel A10 = "pressure on weight, age group, and the "
putexcel A11 = "interaction between age group and sex." 

*--------------------------------------------
// *设置文字格式（字体和字号）
putexcel A1:A11, font("Arial Narrow",11)
*--------------------------------------------

logistic highbp weight agegrp##sex, nopvalues vsquish

// 添加上述命令生成的估计表格
putexcel A13 = etable

*--------------------------------------------
// *将表中数字保留到小数点后两位
putexcel B14:E29, nformat(number_d2)

// *设置文字格式（字体和字号）以及单元格格式
putexcel A13:E29, font("Arial Narrow",9)
putexcel A15, left
putexcel A21, left
putexcel A23, left
*--------------------------------------------

// 进行边际效应分析，并画图
putexcel D13:E13, merge

putexcel G7 = "Interaction plot"
putexcel G7:I7, border(bottom, double)

margins agegrp#sex
marginsplot, title(Age Group and Sex Interaction) ///
   ytitle(Expected Probability of High Blood Pressure) ///
   legend(ring(0) bplacement(seast) col(1))
graph export interaction.png, replace width(550) height(400)

// 插入上述命令生成的图
putexcel G9 = image(interaction.png)

// 保存 Excel 文档
putexcel save			
*--------------------------end putexcel_example1.do---------------------------
```

**注意：**  \*---- 之间的代码是为了进一步美化输出结果，可删掉对比差异。

我们可以通过命令 `shellout` 查看输出结果，也可以在当前路径直接打开 `report.xlsx`。

```stata
ssc install outreg2 // 安装此命令，调用shellout命令
shellout "report.xlsx"
```

输出结果如下：

![report-excel.png](https://images.gitee.com/uploads/images/2019/1030/120142_98459eba_1522177.png)

&ensp;
## 4. 结语
可通过 `help putdocx`，`help putpdf`和 `help putexcel` 参考更多官方范例，熟悉相关用法。

为了减少平时写报告的工作量，我们可根据自身需求选择合适的工具。比如，你比较习惯 Markdown 语法，且对输出文档的格式要求不那么高，则可以选择用 `dyndoc`，而不是 `putdocx`；如果你比较习惯使用 Word 直接排版，且期望通过命令达到类似效果，则可以试试 `putdocx`。

正所谓 “熟能生巧”！我们勤用这些命令，就能摸索出最适合自己的方法！

&ensp;
### 5. 相关链接

**特别说明：** 文中包含的链接在微信中无法生效。请点击本文底部左下角的【阅读原文】。

- 官方帮助文件
  - [Reproducible and automated reporting](https://www.stata.com/new-in-stata/truly-reproducible-reporting/)
  - [Stata Reporting Reference Manual](https://www.stata.com/manuals/rpt.pdf)
  - [RPT Dynamic documents intro](https://www.stata.com/manuals/rptdynamicdocumentsintro.pdf)
  - [Create Word documents with embedded Stata results](https://www.stata.com/features/overview/create-word-documents/)
  - [Convert dynamic Markdown documents to Word or HTML](https://www.stata.com/features/overview/markdown/)
  - [putdocx](https://www.stata.com/manuals/rptputdocxintro.pdf)
  - [putpdf](https://www.stata.com/manuals/rptputpdfintro.pdf)
  - [putexcel](https://www.stata.com/manuals/rptputexcel.pdf)
- 其他参考资料
  - [【putdocx：输出结果 so easy】【李春涛】【中南财经政法大学金融学院】](https://bbs.pinggu.org/linkto.php?url=https%3A%2F%2Fwww.stata.com%2Fmeeting%2Fchina17%2Fslides%2Fchina17_Chuntao.pdf)（putdocx makes it easy: exporting results to Word document）
  - [君生我未生！Stata - 论文四表一键出](https://www.jianshu.com/p/97c4f291ee1e)
  - [玩转stata15—putdocx生成word文档 ](https://mp.weixin.qq.com/s?__biz=Mzg2NzE1NTQwNQ==&mid=2247488361&idx=1&sn=8b44355140ebc7cf3a58227ce335ac2e&chksm=cebebfcbf9c936dd4878a72c5b8bf46a296be3314fafc4f11d2ea47a176086af21e1314b855d&scene=21#wechat_redirect)
  - [Stata16新功能之putdocx生成页眉页脚 ](https://mp.weixin.qq.com/s/gJZzUf22GaxM7mxPnFwoYA)
  - [朝花夕拾——putdocx批量输出变量观测值](https://mp.weixin.qq.com/s/YbFG_bqQAZ_iiZJthaVR1w)
- 本文所用文件及数据
[----点此下载----](https://pan.baidu.com/s/1q-g8zQ1cPaQpE3ZIAq88gQ)
&emsp;
> #### [连享会计量方法专题……](https://mp.weixin.qq.com/s/hWtncj56PeFNL4yg2-va0Q)



&emsp;

>#### 关于我们

- **「Stata 连享会」** 由中山大学连玉君老师团队创办，定期分享实证分析经验， 公众号：**StataChina**。
- 公众号推文同步发布于 [CSDN](https://blog.csdn.net/arlionn) 、[简书](http://www.jianshu.com/u/69a30474ef33) 和 [知乎Stata专栏](https://www.zhihu.com/people/arlionn)。可在百度中搜索关键词 「Stata连享会」查看往期推文。
- 点击推文底部【阅读原文】可以查看推文中的链接并下载相关资料。
- **欢迎赐稿：** 欢迎赐稿。录用稿件达 **三篇** 以上，即可 **免费** 获得一期 Stata 现场培训资格。
- **E-mail：** StataChina@163.com
- 往期推文：[计量专题](https://gitee.com/arlionn/Course/blob/master/README.md)  || [精品课程](https://mp.weixin.qq.com/s/hWtncj56PeFNL4yg2-va0Q) || [简书推文](https://www.jianshu.com/p/de82fdc2c18a)  || [公众号合集](https://mp.weixin.qq.com/s/AWxDPvTuIrBdf6TMUzAhWw) 

[![点击此处-查看完整推文列表](https://images.gitee.com/uploads/images/2019/1030/120141_ddd5b27b_1522177.png)](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw)

---
![欢迎加入Stata连享会(公众号: StataChina)](https://images.gitee.com/uploads/images/2019/1030/120142_0dcc3fc4_1522177.jpeg "扫码关注 Stata 连享会")3.jpg "扫码关注 Stata 连享会")
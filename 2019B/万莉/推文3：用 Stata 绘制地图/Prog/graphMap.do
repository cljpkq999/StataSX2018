*===============================================================================
* 推文：如何用 spmap 绘制地图
* 作者：万莉（北京航空航天大学）
*===============================================================================
cd "D:\2019年Stata连享会\推文3：用Stata绘制地图\Data"
* 设置工作路径

********************************************************************************
* ### 1. 命令简介
********************************************************************************

*####  1.1 绘制步骤

*-Step 1: Obtain and install the spmap, shp2dta, and mif2dta commands

ssc install spmap, replace // 画地图的命令
  
ssc install shp2dta   //将外部的 dbf 文件和 shp 文件转化为 dta 文件

ssc install mif2dta   //将外部的 mif 文件和 mid 文件转化为 dta 文件


help spmap // 获得帮助文件
help shp2dta
help mif2dta

*-------------------------------------------------------------------------------
*-Step 2: Find a map (an ESRI shapefile or a MapInfo Interchange Format file)
/*
本例子中，我们使用美国地图  shapefile 数据。获得 shapefile 文件步骤如下：
* 在 [谷歌](https://www.google.com.hk/) 或 [雅虎](https://www.google.com.hk/) 
  上搜 "United States shapefile"
* 在搜索结果中找到 https://www.weather.gov/gis/USStates
* 下载 [s_11au16.zip](https://www.weather.gov/source/gis/Shapefiles/County/s_11au16.zip) 
（注：文件名会随数据的更新而改变。）
* 解压文件，我们只需两个文件：s_11au16.shp 和 s_11au16.dbf
*/

*-------------------------------------------------------------------------------
*-Step 3: Translate the files
* note:将数据放在当前工作路径

shp2dta using s_11au16, database(usdb) ///
        coordinates(uscoord) genid(id)

/*注：
database(usdb) 将 database file 转换成 usdb.dta；
coordinates(uscoord) 将 coordinate file 转换成 uscoord.dta；
genid(id) 将 usdb.dta 中的 ID 变量命名为 id。

转换 MapInfo files 时，语法一致，只需将 shp2dta 换成 mif2dta。
*/

*-------------------------------------------------------------------------------
*-Step 4: Determine the coding used by the map
* note:将数据放在当前工作路径

use usdb, clear
describe
list id NAME in 1/5

use stats, clear
merge 1:1 scode using trans

drop _merge
*仔细检查合并的结果，确认无误后可删掉_merge
*- _merge 变量的含义：
* _merge==1 obs. from master data
* _merge==2 obs. from only one using dataset
* _merge==3 obs. from at least two datasets, master or using

*-------------------------------------------------------------------------------
*-Step 5: Merge datasets
* note:将数据放在当前工作路径

merge 1:1 id using usdb

drop if _merge!=3
drop _merge
*仔细检查合并的结果，确定无误后可删掉多余观测值

*-------------------------------------------------------------------------------
*-Step 6: Draw the graph

* 图1：不完美示范
spmap pop1990 using uscoord, ///
id(id) fcolor(Blues)

* 图2：进一步美化
replace pop1990 = pop1990/1e+6
* 改变单位，避免图例里显示的数字过大
format pop1990 %4.2f
* 变量数据格式（整数与小数部分长度）

spmap pop1990 using uscoord ///
if id !=1  & id !=54 & ///
   id !=39 & id !=56, ///
id(id) fcolor(Blues)


*-------------------------------------------------------------------------------
*### 2. Stata 范例


*#### 2.1 如何获得地图的矢量数据文件

*-获得中国地图数据
copy "http://fmwww.bc.edu/repec/bocode/c/china_map.zip" china_map.zip, replace
unzipfile china_map, replace  // 解压压缩文件

*-获得世界地图数据
use  "http://fmwww.bc.edu/repec/bocode/w/world-c.dta",clear
save world-c.dta
use  "http://fmwww.bc.edu/repec/bocode/w/world-d.dta",clear   
save world-d.dta

*-检查地图轮廓
use "china_map.dta", clear
scatter _Y _X, msize(vtiny)

*下述例子均使用官方提供的数据集，下载方式如下：
net get spmap.pkg, from(http://fmwww.bc.edu/RePEc/bocode/s)


*#### 2.2 Examples 1: Choropleth maps

use "Italy-RegionsData.dta", clear
#d ;
spmap relig1 using "Italy-RegionsCoordinates.dta", id(id)         
  title("Pct. Catholics without reservations", size(*0.8))      
  subtitle("Italy, 1994-98" " ", size(*0.8));  
#d cr

use "Italy-RegionsData.dta", clear
#d ;
spmap relig1m using "Italy-RegionsCoordinates.dta", id(id)  
  ndfcolor(red)         
  clmethod(eqint) clnumber(5) eirange(20 70)                      
  title("Pct. Catholics without reservations", size(*0.8))         
  subtitle("Italy, 1994-98" " ", size(*0.8))                       
  legstyle(2) legend(region(lcolor(black)));                        
#d cr

*- ndfcolor(red) 用红色表示缺失数据区域
*- clmethod(...) 设置作图数据分组方式，默认是根据分位数分为 4 组。
*  括号里可选填 quantile, boxplot, eqint, stdev, kmeans, custom, unique。
*  clmethod(eqint) clnumber(5) eirange(20 70)：将作图数据按20-70范围等分为5组。
*- clunmber(#) 设置分组数
*- legstyle(#) 设置图例显示模式，默认为1，可选2,3。
*  1显示为 (num,num]；2显示为 num-num; 3为色卡。
*- legend(...) 设置图例是否带框线、摆放位置等。

use "Italy-RegionsData.dta", clear
#d ;
spmap relig1 using "Italy-RegionsCoordinates.dta", id(id)         
  clnumber(20) fcolor(Reds2) ocolor(none ..)                       
  title("Pct. Catholics without reservations", size(*0.8))         
  subtitle("Italy, 1994-98" " ", size(*0.8))                       
  legstyle(3) legend(ring(1) position(3));   
#d cr


use "Italy-RegionsData.dta", clear
#d ;
spmap relig1 using "Italy-RegionsCoordinates.dta", id(id)          
clnumber(20) fcolor(Greens2) ocolor(white ..) osize(thin ..)     
title("Pct. Catholics without reservations", size(*0.8))         
subtitle("Italy, 1994-98" " ", size(*0.8))                       
legstyle(3) legend(ring(1) position(3))                         
plotregion(icolor(stone)) graphregion(icolor(stone))             
polygon(data("Italy-Highlights.dta") ocolor(white)               
osize(medthick))
scalebar(units(500) scale(1/1000) xpos(-100) label(Kilometers));
#d cr

*- fcolor(...) 设置填充颜色
*- icolor(...) 设置背景色
*- ocolor(...) 设置区域边界颜色
*- osize(...) 设置区域边界线宽度
*- polygon(...) 在底图(basemap)上添加其他多边形
*- scalebar(...) 设置比例尺


*#### 2.3 Examples 2: Proportional symbol maps

use "Italy-OutlineData.dta", clear
#d;
spmap using "Italy-OutlineCoordinates.dta", id(id)                 
title("Pct. Catholics without reservations", size(*0.8))        
subtitle("Italy, 1994-98" " ", size(*0.8))                      
point(data("Italy-RegionsData.dta") xcoord(xcoord)              
ycoord(ycoord) deviation(relig1) fcolor(red) dmax(30)            
legenda(on) leglabel(Deviation from the mean));
#d cr


use "Italy-OutlineData.dta", clear
#d;
spmap using "Italy-OutlineCoordinates.dta", id(id)                 
title("Pct. Catholics without reservations", size(*0.8))         
subtitle("Italy, 1994-98" " ", size(*0.8))                       
point(data("Italy-RegionsData.dta") xcoord(xcoord)               
ycoord(ycoord) proportional(relig1) fcolor(green)                  
ocolor(white) size(*3))                                          
label(data("Italy-RegionsData.dta") xcoord(xcoord)               
ycoord(ycoord) label(relig1) color(white) size(*0.7));
#d cr


use "Italy-OutlineData.dta", clear
#d ;
spmap using "Italy-OutlineCoordinates.dta", id(id) fcolor(white)   
  title("Catholics without reservations", size(*0.9) box bexpand   
  span margin(medsmall) fcolor(sand)) subtitle(" ")                
  point(data("Italy-RegionsData.dta") xcoord(xcoord)               
  ycoord(ycoord) proportional(relig1) prange(0 70)                 
  psize(absolute) fcolor(red) ocolor(white) size(*0.6))            
  plotregion(margin(medium) color(stone))                          
  graphregion(fcolor(stone) lcolor(black))                         
  name(g1, replace) nodraw
spmap using "Italy-OutlineCoordinates.dta", id(id) fcolor(white)   
  title("Catholics with reservations", size(*0.9) box bexpand      
  span margin(medsmall) fcolor(sand)) subtitle(" ")                
  point(data("Italy-RegionsData.dta") xcoord(xcoord)               
  ycoord(ycoord) proportional(relig2) prange(0 70)                
  psize(absolute) fcolor(green) ocolor(white) size(*0.6))          
  plotregion(margin(medium) color(stone))                          
  graphregion(fcolor(stone) lcolor(black))                         
  name(g2, replace) nodraw
spmap using "Italy-OutlineCoordinates.dta", id(id) fcolor(white)  
  title("Other", size(*0.9) box bexpand                            
  span margin(medsmall) fcolor(sand)) subtitle(" ")                
  point(data("Italy-RegionsData.dta") xcoord(xcoord)               
  ycoord(ycoord) proportional(relig3) prange(0 70)                 
  psize(absolute) fcolor(blue) ocolor(white) size(*0.6))           
  plotregion(margin(medium) color(stone))                          
  graphregion(fcolor(stone) lcolor(black))                         
  name(g3, replace) nodraw
graph combine g1 g2 g3, rows(1) title("Religious orientation")     
  subtitle("Italy, 1994-98" " ") xsize(5) ysize(2.6)              
  plotregion(margin(medsmall) style(none))                       
  graphregion(margin(zero) style(none))                          
  scheme(s1mono);
#d cr

*- point(...) 在底图(basemap)上添加点
*- label(...) 在底图(basemap)上添加标签（比如地名，数字等）



*#### 2.4 Examples 3: Other maps

use "Italy-RegionsData.dta", clear
#d ;
spmap using "Italy-RegionsCoordinates.dta", id(id) fcolor(stone)   
  title("Pct. Catholics without reservations", size(*0.8))        
  subtitle("Italy, 1994-98" " ", size(*0.8))                       
  diagram(variable(relig1) range(0 100) refweight(pop98)          
  xcoord(xcoord) ycoord(ycoord) fcolor(red));
#d cr


use "Italy-RegionsData.dta", clear
#d ;
spmap using "Italy-RegionsCoordinates.dta", id(id) fcolor(stone)   
diagram(variable(relig1 relig2 relig3) proportional(fortell)    
xcoord(xcoord) ycoord(ycoord) legenda(on))                       
legend(title("Religious orientation", size(*0.5) bexpand        
justification(left)))                                            
note(" "                                                         
"NOTE: Chart size proportional to number of fortune tellers per million population", 
size(*0.75));
#d cr


use "Italy-OutlineData.dta", clear
#d ;
spmap using "Italy-OutlineCoordinates.dta", id(id) fc(bluishgray)  
  ocolor(none)                                                     
  title("Provincial capitals" " ", size(*0.9) color(white))        
  point(data("Italy-Capitals.dta") xcoord(xcoord)                  
  ycoord(ycoord) by(size) fcolor(orange red maroon) shape(s ..)    
  legenda(on))                                                     
  legend(title("Population 1998", size(*0.5) bexpand               
  justification(left)) region(lcolor(black) fcolor(white))         
  position(2))                                                     
  plotregion(margin(medium) icolor(dknavy) color(dknavy))          
  graphregion(icolor(dknavy) color(dknavy));
#d cr


use "Italy-RegionsData.dta", clear
#d ;
spmap relig1 using "Italy-RegionsCoordinates.dta" if zone==1,      
  id(id) fcolor(Blues2) ocolor(white ..) osize(medthin ..)         
  title("Pct. Catholics without reservations", size(*0.8))         
  subtitle("Northern Italy, 1994-98" " ", size(*0.8))              
  polygon(data("Italy-OutlineCoordinates.dta") fcolor(gs12)        
  ocolor(white) osize(medthin)) polyfirst;
#d cr


use "Italy-OutlineData.dta", clear
#d ;
spmap using "Italy-OutlineCoordinates.dta", id(id) fc(sand)        
title("Main lakes and rivers" " ", size(*0.9))                   
polygon(data("Italy-Lakes.dta") fcolor(blue) ocolor(blue))       
line(data("Italy-Rivers.dta") color(blue) )                      
freestyle aspect(1.4) xlab(400000 900000 1400000, grid);
#d cr

*- polygon(...) 在底图 (basemap) 上添加其他多边形。
*- point(...) 在底图 (basemap) 上添加点。
*- line(...) 在底图 (basemap) 上添加线条。
*- 绘制比较复杂的地图时，常用上述三个命令在底图 (basemap) 上叠加图层。


*#### 其它相关命令
*- `help tmap` 
*外部命令安装：ssc install tmap, replace
*该命令是 `spmap` 的前身。
*- `help grmap`
*该命令源自 `spmap`，注意仅适用于 Stata 15 及更高版本。
 

*#### 相关链接
*- [How do I graph data onto a map with spmap?](https://www.stata.com/support/faqs/graphics/spmap-and-maps/)
*- [How do I graph data onto a map with tmap? ](https://www.stata.com/support/faqs/graphics/tmap-and-maps/)
*- [Pisati, M. 2004.  Simple thematic mapping.  Stata Journal 4: 361-378.](https://www.stata-journal.com/sjpdf.html?articlenum=gr0008)
*- [Pisati, M. 2007.  spmap: Stata module to visualize spatial data. Statistical Software Components S456812, Department of Economics, Boston College.](https://ideas.repec.org/c/boc/bocode/s456812.html)
*- [ShapeFile 格式说明](https://www.xuebuyuan.com/3200043.html)
*- [【地理信息】GIS 之 shapefile 文件趣解（上） ](https://mp.weixin.qq.com/s/GJRcRYfwjntavQo7jFm_Ww)
*- [一文看尽 Stata 绘图](https://www.jianshu.com/p/9ab00282d5a8)
*- [[Stata] 地图绘制（一）：中国地图与世界地图](https://bbs.pinggu.org/thread-3786220-1-1.html)
*- [[Stata] 地图绘制（二）: spkde/spgrid/spmap](https://bbs.pinggu.org/thread-3821854-1-1.html)
*- [【盘点】这些经典地图可视化你一定要知道！](http://www.sohu.com/a/271271477_610696)


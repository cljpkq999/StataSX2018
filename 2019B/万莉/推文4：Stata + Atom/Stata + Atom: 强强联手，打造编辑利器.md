> #### [连享会：内生性问题及估计方法专题](https://mp.weixin.qq.com/s/FWpF5tE68lAtCEvYUOnNgw)
> #### 报名 / 助教招聘 中……
[![连享会-内生性专题现场班-2019.11.14-17](https://images.gitee.com/uploads/images/2019/0909/145950_7be1ff88_1522177.png)](https://gitee.com/arlionn/Course/blob/master/2019Spatial.md)

> 作者：万莉 (北京航空航天大学)

![image](https://images.gitee.com/uploads/images/2019/0910/162344_5ccf5c75_1522177.png)
&ensp;

> #### [2019金秋十月-空间计量专题班，杨海生主讲，成都](https://mp.weixin.qq.com/s/aU6B9HZaf2BSHF7lSLe6LA)

[![image](https://images.gitee.com/uploads/images/2019/0910/162344_c8eff302_1522177.png)](https://mp.weixin.qq.com/s/aU6B9HZaf2BSHF7lSLe6LA)

&emsp;
> Stata连享会 &emsp; [计量专题](https://gitee.com/arlionn/Course/blob/master/README.md)  || [精品课程](https://mp.weixin.qq.com/s/hWtncj56PeFNL4yg2-va0Q) || [简书推文](https://www.jianshu.com/p/de82fdc2c18a)  || [公众号合集](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw)

[![点击查看完整推文列表](https://images.gitee.com/uploads/images/2019/0910/162344_45675369_1522177.png)](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw)



## 引言

我们可以借助外部编辑器编写并运行 dofile，从而提高输入效率和语法高亮功能。那我们可以选择哪些外部编辑器呢？

参考[「Stata+Sublime：美观高效的dofile编辑器」](https://www.jianshu.com/p/fbbab36080de)和[「珠联璧合：Jupyter Notebook 与 Stata 之融合」](https://www.jianshu.com/p/2f52064865c4)，我们可以选用 Sublime 和 Jupyter Notebook。相较 Sublime，Jupyter Notebook 能使运行的结果直接显示在代码下方，更方便查看。

本文将介绍 Atom 这一编辑神器——被开发团队称为“一个为 21 世纪所创造的可配置编辑器 (A hackable text editor for the 21st Century)”。通过下载相应插件 (Package)，我们既能实现 Stata + Sublime 的效果，也能实现 Stata + Jupyter Notebook 的效果。

我们先看看效果图 （安装插件 `Hydrogen`、`language-stata` 和 `Stata Jupyter kernel`），是不是很炫酷！

![](https://img-blog.csdnimg.cn/20190910220309422.gif)
   
&ensp;
## 1. 什么是 Atom ？

[Atom](https://atom.io/)  是一个免费的跨平台编辑器，已在 [GitHub](https://github.com/) 上开放了全部源代码。它拥有非常精致细腻的界面和丰富的可配置项。

它提供了与 SublimeText 类似的 Package Control (包管理)功能。我们可以非常方便地安装、管理各种插件，并将 Atom 打造成真正适合自己的开发工具。

最大的亮点是，该编辑器由 GitHub（目前全球范围内影响力最大的代码仓库/开源社区） 打造，有着很大的发展潜力。开源社区非常非常活跃，能及时解决各种问题。



&ensp;
## 2. Atom 的安装

- 方法一
在官网上 [Atom](https://atom.io/) 直接点击 **Download**，便可下载最新版本。**注意**：缺点是双击 exe 文件便直接安装在 C 盘，无法自定义安装路径。

- 方法二
在官网上 [Atom](https://atom.io/) 点击 **Other platforms** 或者打开 https://github.com/atom/atom/releases，根据自己的系统，选择对应的压缩包。该压缩包为便携版，可以解压到你想安装的目录。

  >![安装.png](https://images.gitee.com/uploads/images/2019/0910/162344_1172551a_1522177.png)

- 注意
运行 atom.exe 时软件会自动在 C 盘用户目录产生 .atom 文件夹。下载的插件便放在这个文件夹里，具体为 C:\Users\Users\.atom\packages。卸载 Atom 不会删掉该文件夹，不会影响配置和插件。
  
  当我们第一次启动 Atom 时，可以看到如下所示的屏幕：

  >![界面-.png](https://images.gitee.com/uploads/images/2019/0910/162344_c16f3da8_1522177.png)

&emsp;
> #### [连享会计量方法专题……](https://mp.weixin.qq.com/s/hWtncj56PeFNL4yg2-va0Q)


&ensp;
## 3. Atom + Stata

本文主要参考 [「Atom + Stata」](https://acarril.github.io/posts/atom-stata) 和 [「 stata_kernel 」](https://kylebarron.dev/stata_kernel/getting_started/)，以 Windows 操作系统为例，介绍如何将 Atom 和 Stata 结合起来，利用 Atom 的编辑器功能来写 dofile，提高输入效率和语法高亮功能。

我们有两种方法将 Atom 和 Stata 结合起来。 第一种方法是安装插件 [`language-stata`](https://github.com/kylebarron/language-stata) 和 [`stata-exec`](https://atom.io/packages/stata-exec)。该方法的效果和 Stata + Sublime 类似。

相较第一种方法，第二种方法更简单，也更好用。具体操作是安装插件 [`hydrogen`](https://atom.io/packages/Hydrogen) 和 [`language-stata`](https://github.com/kylebarron/language-stata)，此外还得安装[`Stata Jupyter kernel`](https://kylebarron.github.io/stata_kernel/)，才能使运行结果直接呈现在代码下方。该方法的效果和 Stata + Jupyter Notebook 类似。

------------------------
### 3.1 第一种方法：[language-stata](https://github.com/kylebarron/language-stata) + [stata-exec](https://atom.io/packages/stata-exec)。

#### 第一步：安装 `language-stata`

- 方法一：点击 `File > Preferences/Settings > Install > Search packages`, 搜索到 `language-stata` 后点击 `Install`。

- 方法二：打开 cmd 命令行，输入并执行 `apm install language-stata`。

- 注意：如何打开 cmd，可参考 https://jingyan.baidu.com/article/f96699bbf01097894e3c1bc7.html。

#### 第二步：安装 `stata-exec`

1. 点击 **File &rarr; Preferences/Settings &rarr; Install &rarr; Search packages**, 搜索到 `stata-exec` 后点击 **Install**；或者打开 cmd 命令行，输入并执行 `apm install stata-exec`。

2. 下载 [Node.js](https://nodejs.org/en/)。安装时，只需选择安装路径，其他不用设置。

3. 以**管理员**身份打开 cmd 或者 PowerShell 命令行。注意是要以管理员身份打开。 在命令行中输入并执行以下内容：
     ```stata
    npm install --global --production windows-build-tools
    ```
    界面如下图：

    >![windows-build-tools.png](https://images.gitee.com/uploads/images/2019/0910/162344_1e9534f1_1522177.png)
    > - 该过程可能会持续 5-10 分钟，安装 Python 及其他工具，用于下一步操作。
    > - 如何打开 cmd, 可参考 https://jingyan.baidu.com/article/f96699bbf01097894e3c1bc7.html； 
    > - 如何打开 PowerShell，可参考 https://jingyan.baidu.com/article/b907e62769217346e7891c8c.html 

4. 打开 cmd 命令行，输入并执行以下内容：
    ```stata
    cd %USERPROFILE%\.atom\packages\stata-exec
    npm install winax --python=%USERPROFILE%\.windows-build-tools\python27\python.exe
    atom -v
    ```
    接着我们继续输入并执行：
    ```stata
    npm rebuild winax --runtime=electron --target=ELECTRON_VERSION --disturl=https://atom.io/download/atom-shell --build-from-source
    ```
    **注意：**将上述内容中的 **ELECTRON_VERSION**，替换成 `atom -v` 输出结果中的 **Electron**。比如，我在命令行中输入 `atom -v` ，结果如下图：

    ![atom -v.png](https://images.gitee.com/uploads/images/2019/0910/162344_84eabb20_1522177.png)

    那我们就将 **ELECTRON_VERSION** 替换成 **3.1.10**。

5. [Link the Stata Automation library](https://www.stata.com/automation/#install).

    > 1. 在 Stata 的安装目录中，右键点击 stata 运行程序（例如 StataMP-64 或 StataSE.exe，具体选择取决于你的安装版本），选择“创建快捷方式”。
    > 2. 右键点击创建的快捷方式 > 属性 > 目标，将路径用英文引号括起来，然后在后面加上`  /Register`，如将 `E:\STATA\Stata14\StataMP-64.exe` 改成 `"E:\STATA\Stata14\StataMP-64.exe" /Register`。改完后点击确定，再打开时目标路径上的引号消失，表示成功。
注意：在 Register 前面是一道斜杠，而且斜杠之前还有一个空格。
    > 3. 右键点击快捷方式，“**以管理员身份运行**”一次即可。
    > 
    >    具体操作见下图：![注册 Stata Automation type library.png](https://images.gitee.com/uploads/images/2019/0915/163110_dcc0c98b_3042808.png)

6. 重新启动 Atom 即可使用。

    然后我们就可以 `File > New File > Save As > 文件名.do` 新建 do 文件或者 `File > Open File` 打开 do 文件，编写命令。如要运行选中命令，则可使用 `ctrl + enter`（注意不是 `ctrl + D`）。
   
   如何使用或自定义快捷键，请参考 [stata-exec：Usage](https://github.com/kylebarron/stata-exec/blob/master/README.md#usage)。
&emsp;
> #### [连享会计量方法专题……](https://mp.weixin.qq.com/s/hWtncj56PeFNL4yg2-va0Q)


------------------------
### 3.2 第二种方法：[Hydrogen](https://atom.io/packages/Hydrogen) + [language-stata](https://github.com/kylebarron/language-stata) + [stata_kernel](https://kylebarron.github.io/stata_kernel/)

1. 安装 Python。推荐安装 [Anaconda Python](https://www.anaconda.com/distribution/)。注意 Anaconda 的安装文件比较大，若平时并不需要使用 Python，则可以安装 [Miniconda](https://docs.conda.io/en/latest/miniconda.html)。
    
    若纠结安装 Python 2.x 还是 3.x，推荐安装 Python 3.x。 

2. [Link the Stata Automation library](https://kylebarron.dev/stata_kernel/getting_started/)
      > 1. 在 Stata 的安装目录中，右键点击 stata 运行程序（例如 StataMP-64 或 StataSE.exe，具体选择取决于你的安装版本），选择“创建快捷方式”。
    > 2. 右键点击创建的快捷方式 > 属性 > 目标，将路径用英文引号括起来，然后在后面加上`  /Register`，如将 `E:\STATA\Stata14\StataMP-64.exe` 改成 `"E:\STATA\Stata14\StataMP-64.exe" /Register`。改完后点击确定，再打开时目标路径上的引号消失，表示成功。
注意：在 Register 前面是一道斜杠，而且斜杠之前还有一个空格。
    > 3. 右键点击快捷方式，“**以管理员身份运行**”一次即可。
    >   示意图见 3.1 第一种方法。

3. 安装 `stata_kernel`。打开 cmd 命令行，输入并执行以下内容：
    ```stata
    pip install stata_kernel
    python -m stata_kernel.install
    ```
    若你的系统默认使用  Python2，则在命令行中输入并执行以下内容：  
   ```stata
    pip3 install stata_kernel
    python3 -m stata_kernel.install
    ```
4. 配置语法高亮功能。打开 cmd 命令行，输入并执行以下内容：
   ```stata
    conda install -c conda-forge nodejs -y
    jupyter labextension install jupyterlab-stata-highlight
    ```
    若你未安装 Anaconda Python，则 `conda` 命令无效。这时需要安装 [Node.js](https://nodejs.org/en/)。安装时，只需选择安装路径，其他不用设置。安装完成后，在命令行中运行 `jupyter labextension install jupyterlab-stata-highlight`。

5. 安装 `hydrogen` 插件。点击 `File > Preferences/Settings > Install > Search packages`, 搜索到 `hydrogen` 后点击 `Install`；或者打开 cmd 命令行，输入 `apm install hydrogen`。

6. 安装 `language-stata` 插件。点击 `File > Preferences/Settings > Install > Search packages`, 搜索到 `language-stata` 后点击 `Install`；或者打开 cmd 命令行，输入并执行 `apm install language-stata`。

7. 重新启动 Atom 即可使用。

    然后我们就可以 `File > New File > Save As > 文件名.do` 新建 do 文件或者 `File > Open File` 打开 do 文件，编写命令。如要运行选中命令，则可使用 `ctrl + enter`（注意不是 `ctrl + D`）。

    在运行 Stata 命令时，请注意页面右下角，应显示 Stata。若没有则按下快捷键 `ctrl+shif+L`，将文本类型选择为 `Stata`。正在运行 Stata 命令时，页面左下角会出现 `Stata | busy`；命令运行完成后，左下角会变成 `Stata | idle`。若左下角一直出现 `Stata | busy`，可以鼠标左键点击此处，选择 `Restart Stata Kernel`。
    
    ![图片.png](https://images.gitee.com/uploads/images/2019/0915/170227_c4fe7de3_3042808.png)
   
    具体使用方法请参考 [Example](https://nbviewer.jupyter.org/github/kylebarron/stata_kernel/blob/master/examples/Example.ipynb)。

## 4. Atom 小技巧和 FAQs

最后，总结下使用 Atom 的小技巧以及配置过程中可能遇到的问题。

### 4.1 Atom 使用小技巧

#### Tip1： 显示左侧侧边栏
使用插件 **Tree View** (**Packages &rarr;  Tree View &rarr; Toggle** 或者快捷键 `ctrl + \`)，即可打开左侧侧边栏，显示文件夹及文件。效果图如下：

>![Tree View.png](https://images.gitee.com/uploads/images/2019/0910/162346_73975dd5_1522177.png)

#### Tip2：Atom 的汉化  
使用插件 **Simplified Chinese Menu** 可支持汉化。具体做法如下：
    
依次点击 **File &rarr; Preferences/Settings &rarr; Install &rarr; Search packages**, 搜索到 **Simplified Chinese Menu** 后点击 「**Install**」；或者打开 `cmd` 命令行，输入并执行 `apm install Simplified Chinese Menu`。

### 4.2 Atom 设定常见问题 (FAQs)
-  **FAQ 1:**  打开 Atom 时，右上角提示错误：Cannot load the system dictionary for zh-CN.

   - A: 点击 **File  &rarr; Preferences/Settings &rarr;  Packages** 搜索 **spell-check** 后，点击 **Settings**，将 Use Locales 前的勾去掉，并在下面填入 en-US。具体请参考 https://www.jianshu.com/p/dcb758424e07。

-  **FAQ 2:**   如何卸载或关闭插件？
    
   - A: 点击 `File > Preferences/Settings > Packages`，选中想要卸载（关闭）的插件，点击 `Uninsall`(`Disable`)。

-  **FAQ 3:**   在 Atom 中利用 `File > Preferences/Settings > Install` 或 `apm` 下载插件很慢怎么办？

   - A: 此时可以手动安装插件。此处以安装 `hydrogen` 插件为例进行说明。

    1.  进入 [Atom](https://atom.io/) ，点击左上角的 **「Packages」** 按钮，在搜索框中输入插件的名称，再点击 **「Repo」**，跳转到 gitHub 页面。也可直接在 [GitHub](https://github.com/) 中搜索插件。
    2. 在跳转到的 gitHub 页面中下载插件。 [`hydrogen` 的 gitHub 页面](https://github.com/nteract/hydrogen)如下:
     ![image](https://images.gitee.com/uploads/images/2019/0910/162345_2176ffcb_1522177.png)      
 
       点击右侧的 `Clone or download` 下载压缩包或者点击上方的 `releases`，选择相应的版本进行下载压缩包。
     3. 将压缩包解压放到 `C:\Users\lily\.atom\packages` 路径下（注意：lily 为我的用户名，在操作中改成你的用户名即可），再将文件名重命名为插件名称，不要包含 - 后的后缀。

     4.  打开 cmd 命令行，输入并执行以下命令，进入 `C:\Users\Users\.atom\packages` 这个路径：
          ```dos
         cd %USERPROFILE%\.atom\packages\hydrogen
         ```
     5. 在 cmd 命令行，输入并执行以下命令，便完成了插件的安装：
         ```dos
         apm install
         ```
         或者
         ```dos
         npm install
         ```

         具体请参考 https://www.jianshu.com/p/d0e86b9607c8。

-  **FAQ 4:**    为何在 cmd 命令行中，输入 `apm ...` 后会报错 「**不是内部或外部命令，也不是可运行的程序或批处理文件。**」？

   - A: 将 `apm` 的路径添加到环境变量 path 中。
    1. 找到 `apm` 的路径。我将 Atom 安装在 E 盘，路径为 `E:\Atom\Atom x64\resources\app\apm\bin`；若将 Atom 的安装路径在 C 盘，路径类似如下：`C:\Users\XXXX\AppData\Local\atom\app- 1.40.0\resources\app\apm\bin`。
    
    2. 将此路径添加环境变量。在“我的电脑”上右击鼠标，在弹出菜单中选择“属性”。在“系统”界面选择“高级系统设置”，点开“环境变量”。最后在“用户变量”中添加环境变量 path。

       >![添加环境变量.png](https://images.gitee.com/uploads/images/2019/0910/162345_38318d90_1522177.png)
 
-  **FAQ 5:**  在 cmd 命令行中，输入 `atom -v`，为何报错 「**不是内部或外部命令，也不是可运行的程序或批处理文件**」？
    
   - A: 安装插件 `atom-shell-commands` 即可。依次点击 `File > Preferences/Settings > Install > Search packages`, 搜索到 `atom-shell-commands` 后点击 `Install`；或者打开 cmd 命令行，输入并执行 `apm install atom-shell-commands`。

------------------------

&emsp;
## 5. 结语

合理借助外部编辑器编写并运行 dofile，我们可以大幅度提高效率和。Atom + Stata 编辑模式是很不错的选择。
&emsp;

### 相关链接
- [Atom + Stata](https://acarril.github.io/posts/atom-stata)
- [language-stata](https://github.com/kylebarron/language-stata)
- [stata-exec](https://github.com/kylebarron/stata-exec)
- [stata_kernel](https://kylebarron.dev/stata_kernel/getting_started/)
- [Atom 更为先进的文本代码编辑器](https://www.jianshu.com/p/8fe692769f62)
- [Stata+Sublime：美观高效的dofile编辑器](https://www.jianshu.com/p/fbbab36080de)
- [珠联璧合：Jupyter Notebook 与 Stata 之融合](https://www.jianshu.com/p/2f52064865c4)
- [How to install anaconda](https://docs.anaconda.com/anaconda/install/windows/)
- [How to use anaconda](https://docs.anaconda.com/anaconda/user-guide/getting-started/)


&emsp;
> #### [2019金秋十月-空间计量专题班，杨海生主讲，成都](https://mp.weixin.qq.com/s/aU6B9HZaf2BSHF7lSLe6LA)

[![](https://images.gitee.com/uploads/images/2019/0910/220527_d2bb4956_1522177.png)](https://mp.weixin.qq.com/s/aU6B9HZaf2BSHF7lSLe6LA)

&emsp;

>#### 关于我们

- **Stata连享会** 由中山大学连玉君老师团队创办，定期分享实证分析经验。
- 公众号推文同步发布于 [CSDN](https://blog.csdn.net/arlionn) 、[简书](http://www.jianshu.com/u/69a30474ef33) 和 [知乎Stata专栏](https://www.zhihu.com/people/arlionn)。可在百度中搜索关键词 「Stata连享会」查看往期推文。
- 点击推文底部【阅读原文】可以查看推文中的链接并下载相关资料。
- **欢迎赐稿：** 欢迎赐稿。录用稿件达 **三篇** 以上，即可 **免费** 获得一期 Stata 现场培训资格。
- **E-mail：** StataChina@163.com
- [**计量专题**：因为专注，所以专业](https://mp.weixin.qq.com/s/hWtncj56PeFNL4yg2-va0Q)
- [**往期精彩推文**：一网打尽](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw)

[![点击此处-查看完整推文列表](https://images.gitee.com/uploads/images/2019/0910/220502_98c6cf43_1522177.png "连享会(公众号: StataChina)推文列表")](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw)



---
![欢迎加入Stata连享会(公众号: StataChina)](https://images.gitee.com/uploads/images/2019/0910/162345_8e4881a7_1522177.jpeg "扫码关注 Stata 连享会")

In this analysis, we are going to discover 
   the mean miles per gallon of cars in 
   1978!

. sysuse auto.dta, clear
(1978 Automobile Data)

. quietly summarize mpg

. dis r(mean)
21.297297


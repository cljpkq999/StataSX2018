*===============================================================================
* 推文：一文看尽 Stata 绘图
* 作者：万莉（北京航空航天大学）
*===============================================================================

/* 一个简单的例子 */
cd "D:\2019年Stata连享会\推文1：一文看尽Stata绘图\Prog" // 设置工作路径
sysuse sp500, clear // 导入软件自带数据文件
twoway (line high date) (line low date),  ///
        title("这是图选项：标题", box) ///
		subtitle("这是副标题" "图1：股票最高价与最低价时序图")  ///
        xtitle("这是 x 轴标题：交易日期", margin(medsmall)) ///
        ytitle("这是 y 轴标题：股票价格") ///
        ylabel(900(200)1400) ymtick(##5)  ///
        legend(title("图例")label(1 "最高价") label(2 "最低价"))  ///
        note("这是注释：数据来源于 Stata 公司") ///
        caption("这是说明：欢迎加入 Stata 连享会！ ") ///
        saving(myfig.gph, replace) 
/*一些解释：
line 是曲线选项，表示线图
设置副标题时，用 "" 达到换行效果
ylabel 设置 y 轴标签及刻度  
saving() 表示保存图像
/// 表示断行，也可以用 #delimit;  #delimit cr 
*/


/* 图形管理 */

*- 图形保存
sysuse sp500, clear
twoway line high low date, saving(fig1.gph, replace) // 保存方式1
graph use fig1.gph // 重现图形

twoway line high low date, scheme(s1mono)
graph save fig2.gph, replace  // 保存方式2，scheme 是设定绘图模板

*- 图形合并
graph combine fig1.gph fig2.gph, col(1) // 以单列形式合并

help graph combine

*- 图形导出
graph export "fig3.png", replace // 导出格式有 png，tif，wmf 等
graph export "fig4.png", width(3200) height(1800) replace 
// 调整输出图片的分辨率，仅适用于.png 和 .tif 格式的图片

help graph export


********************************************************************************
******* 让我们开始吧
********************************************************************************
use "http://www.princeton.edu/~otorres/wdipol.dta", clear  // 导入数据
browse

describe
summarize
/* 了解数据结构
year:    年份
country: 国家名称
gdppc:   人均GDP
unempf:  女性失业率（%）
unempm:  男性失业率（%）
unemp:   失业率（%）
export:  出口额
import:  进口额
polity:  Polity IV数据库中的政体类型变量（polity）,衡量政体的民主程度，
         数值越大表示政体民主程度越高。
polity2: Polity IV数据库中的政体类型变量（polity2）,衡量政体的民主程度，
         数值越大表示政体民主程度越高。
trade:   进出口总额（进口+出口） 
id:      可通过命令group(country)得到，将国家名称与数字相对应
*/

********************************************************************************
******* 线图
********************************************************************************
line unemp unempf unempm year if country=="United States" // 美国失业率的时序图

summarize unemp unempf unempm
replace unemp=. if unemp==0
replace unempf=. if unempf==0
replace unempm=. if unempm==0 // 将变量unemp，unempf，unempm中为0的值变成缺失值
summarize unemp unempf unempm
line unemp unempf unempm year if country=="United States"


twoway line unemp unempf unempm year if country=="United States", ///
       title("Unemployment rate in the US, 1980-2012") /// 
       legend(label(1 "Total") label(2 "Females") label(3 "Males")) ///
       lpattern(solid dash dot) ///
       ytitle("Percentage")
/*
命令依次为：指定画线形图、添加标题、调整图例、改变线条类型、添加y轴标题
*/
	   
	   
twoway connected unemp unempf unempm year if country=="United States", ///
       title("Unemployment rate in the US, 1980-2012") ///
       legend(label(1 "Total") label(2 "Females") label(3 "Males")) ///
       msymbol(circle diamond square) ///
       ytitle("Percentage")
/*
命令依次为：指定点连线图、添加标题、调整图例、改变点的标记符号（实心圆圈、
             实心菱形、实心正方形）、添加y轴标题
			 标记符号的各种代号可参考help symbolstyle 
*/	   
	   
twoway connected unemp year if country=="United States" | ///
                               country=="United Kingdom" | ///
                               country=="Australia" | ///
                               country=="Qatar", ///
       by(country, title("Unemployment")) ///
       msymbol(circle_hollow)
	   
twoway connected unemp year if country=="United States" | ///
                               country=="United Kingdom" | ///
                               country=="Australia" | ///
                               country=="Qatar", ///
       by(country) ///
	   title("Unemployment") ///
       msymbol(circle_hollow)	   
/*
利用by(varname)同时做多个图。 
注意：by(varname)功能适用于 matrix 和 star 以外所有图形,该选项使 graph 
按照指定的分组变量分别绘制图形。
title("")放在by()里面，画出的图共用一个标题；title("")放在by()外面，每个图一个标题
*/

twoway (connected unemp year if country=="United States", msymbol(diamond_hollow)) ///
       (connected unemp year if country=="United Kingdom", msymbol(triangle_hollow)) ///
       (connected unemp year if country=="Australia", msymbol(square_hollow)) ///
       (connected unemp year if country=="Qatar", ///
       title("Unemployment") ///
       msymbol(circle_hollow) ///
       legend(label(1 "USA") label(2 "UK") label(3 "Australia") label(4 "Qatar")))
/*
将四个国家的失业率时序图放在同一张图里
*/	   

twoway connected gdppc year if gdppc>40000, by(country) msymbol(diamond)
/* 保留人均GDP高于40000美元的子样本，再按国家分组绘制时序图*/


bysort year: egen gdppc_mean=mean(gdppc)
bysort year: egen gdppc_median=median(gdppc)
// 利用bysort命令，计算出人均GDP每年的平均数和中位数
twoway connected gdppc gdppc_mean year if country=="United States" | ///
                                          country=="United Kingdom" | ///
                                          country=="Australia" | ///
                                          country=="Qatar", ///
       by(country, title("GDP pc (PPP, 2005=100)")) ///
       legend(label(1 "GDP-PC") label(2 "Mean GDP-PC")) ///
       msymbol(circle_hollow)
/*
将四个国家的人均GDP与平均水平进行比较
*/

help twoway line // 查看线图的帮助文件
help twoway connected // 查看点线图的帮助文件

palette symbolpalette // 图示标记符号及对应代号
palette linepalette // 图示线型及对应代号
palette color green // 图示颜色
help palette 


********************************************************************************
*******  条形图
********************************************************************************
graph hbar (mean) gdppc // 绘制横向条形图，平均值mean选项是默认值
graph hbar (mean) gdppc, over(country, sort(1) descending)
graph hbar (mean) gdppc, over(country, sort(1) descending label(labsize(*0.5)))


graph hbar (mean) gdppc (median) gdppc if gdppc>40000, ///
      over(country, sort(1) descending label(labsize(*1))) ///
      legend(label(1 "GDPpc (mean)") label(2 "GDPpc (median)"))
/*
over() 设定分组变量，这里表示按国家分组
sort(1) 选项表示根据第一个变量，即 gdppc 的柱体高度进行升序排列
descending 表示降序排列
labsize(*0.5) 表示标签字体大小缩放 0.5 倍
*/

help graph bar // 查看条形图的帮助文件


********************************************************************************	  	  
******* 箱形图
********************************************************************************
sort id year  // 排序，规范样本
recode polity2 (-10/-6=1 "Autocracy") ///
               (-5/6=2 "Anocracy") ///
               (7/10=3 "Democracy") ///
               (else=.), ///
               gen(regime) label(polity_rec)
/*
利用recode命令，将polity2变量转换为类别变量regime:
1 "威权政体" if -10<=polity2<=-6
2 "中间政体" if -5<=polity2<=6
3 "民主政体" if 7<=polity2<=10
*/
			   
tab regime // 对类别变量 regime 列表统计，结果包括频数，频率
tab regime, nolabel // 列表统计，不显示类别变量的标签
tab country regime // 二维列表
tab country regime, row // row 选项表示行末增加 Total 统计量

help tab

graph hbox gdppc // 绘制横向箱型图
graph hbox gdppc if gdppc<40000
graph box gdppc, over(regime) yline(4517.94) marker(1,mlabel(country))
/*
over(regime) 表示按regime（政体类型）分组
yline(4517.94) 添加附加线，即 y=4517.94 的直线
marker(1,mlabel(country)) 以变量country的值作为标签，标识出第1个变量的特异值
*/

help graph box // 查看箱型图的帮助文件


********************************************************************************
******* 散点图
********************************************************************************
scatter import export // 进口额与出口额的散点图

twoway (scatter import export if export>1000000, mlabel(country)) ///
       (scatter import export), legend(off)
	   
twoway (scatter import export) ///
       (scatter import export if export>1000000, mlabel(country)), legend(off)
/*
mlabel(country) 标上国家名称
legend(off) 不显示图例
注意： 理解图层的概念，图层2会覆盖图层1。
上述两个 twoway 命令由于图层顺序不一样，画出的图的效果不一样。
*/			   

twoway (scatter import export, ytitle("Imports") xtitle("Exports")) ///
       (scatter import export if export>1000000, mlabel(country) legend(off)) ///
       (lfit import export, note("Constant values, 2005, millions US$"))
/*lfit 线性回归拟合图*/
	   
capture bysort year: egen gdppc_mean=mean(gdppc)
// 利用bysort命令，计算出人均GDP每年的平均数;capture避免程序因错误而中断

twoway (scatter gdppc year, jitter(13)) ///
       (connected gdppc_mean year, msymbol(diamond)) , xlabel(1980(1)2012, angle(90))
/*
jitter(#) 选项表示添加白噪声数据点，数值越大，添加的的白噪声越多
xlabel(1980(1)2012) 设定横坐标刻度标签，x轴的刻度从1980开始，到2012结束，每隔1添加一个刻度
angle(90) 设定刻度标签的角度
*/
	   	   
help twoway scatter // 查看散点图的帮助文件


********************************************************************************
******* 矩阵图
********************************************************************************
graph matrix gdppc unemp unempf unempm export import trade polity2, ///
             maxis(ylabel(none) xlabel(none))

graph matrix gdppc unemp unempf unempm export import trade polity2, ///
             half maxis(ylabel(none) xlabel(none))
/*
graph matrix 命令用矩阵的形式同时画出多个变量之间的相互关系。
比如，第1行第1列的图画出了人均GDP和失业率的关系。
half maxis 表示只显示矩阵的一半，即左下角。
ylabel(none) xlabel(none) 表示不显示y轴刻度标签、x轴刻度标签。
*/
			 
help graph matrix // 查看矩阵图的帮助文件


********************************************************************************
******* 直方图
********************************************************************************
hist gdppc // 频率分布
hist gdppc, frequency // 频数分布 
hist gdppc, kdensity // 将直方图和核密度曲线绘制在一起
hist gdppc, kdensity normal // 同时显示频率直方图、核密度曲线和正态分布图
hist gdppc, kdensity normal bin(20) // bin(#)指定分为几个组别
hist gdppc if country=="United States" | country=="United Kingdom", bin(10) by(country)
// 利用by(varname)设定分组，同时画多个图。 

twoway hist gdppc if country=="United States", bin(10) || ///
       hist gdppc if country=="United Kingdom", bin(10) ///
       fcolor(none) lcolor(black) lwidth(medium) lpattern(dash) ///
	   legend(label(1 "USA") label(2 "UK"))
/*
fcolor(none) 设定柱子的填充颜色，none表示无填充颜色
lcolor(black) 设定柱子的轮廓颜色
legend()设置图例
lwidth(medium) 设定外边缘线的宽度
lpattern(dash) 设定外边缘线的类型
*/

help linewidthstyle   
help linepatternstyle
help hist // 查看直方图的帮助文件


********************************************************************************
******* 面板数据时间趋势图
********************************************************************************
*xtset country year // 会报错，'country' 为字符串变量*/
encode country, gen(country1) 
xtset country1 year // 声明数据是面板数据

xtline gdppc 
xtline gdppc if gdppc>39000, overlay // overlay 将所有国家放在同一图中

help xtline // 查看面板数据时间趋势图的帮助文件


********************************************************************************
******* 点图
********************************************************************************
graph dot (mean) gdppc if gdppc>40000, over(country, sort(1) descending)

graph dot (mean) gdppc (median) gdppc if gdppc>40000, ///
      over(country, sort(1) descending label(labsize(*1))) ///
      legend(label(1 "GDPpc (mean)") label(2 "GDPpc (median)"))
// * 事实上是柱状图的另一种表示方法, 比较省墨	  

help graph dot // 查看点图的帮助文件
	
	
********************************************************************************
******* 饼图
********************************************************************************
graph pie export if (country=="Brazil" | ///
                     country=="Russia" | ///
                     country=="India" | ///
				     country=="China") & year == 2010, ///
				     over(country) noclockwise // noclockwise 逆时针排序

graph pie export if (country=="Brazil" | ///
                     country=="Russia" | ///
                     country=="India" | ///
				     country=="China") & year == 2010, ///
				     over(country) sort descending 			

graph pie export if (country=="Brazil" | ///
                     country=="Russia" | ///
                     country=="India" | ///
				     country=="China") & year == 2010, ///
				     over(country) sort descending ///
					 plabel(_all percent,format("%5.2f")) ///
					 pie(1,explode)
					  					 
/*
sort descending 降序排列; sort 升序排列
plabel(_all percent,format("%7.2f")) 为所有饼块按%7.2f格式显示百分比
pie(1,explode)  突出/分离第1块饼块
*/

help graph pie // 查看饼图的帮助文件


********************************************************************************
******* 双变量的回归拟合图
********************************************************************************
use "http://dss.princeton.edu/training/students.dta", clear
twoway (lfitci sat age) ///
       (scatter sat age, mlabel(lastname)), ///
	   title("SAT scores by age") ytitle("Sat")
/* 
lfitci 线性回归拟合图，包括置信区间
lfit 线性回归拟合图，不包括置信区间
qfitci 非线性回归拟合图，包括置信区间
qfit 非线性回归拟合图，不包括置信区间
*/
help twoway lfit
help twoway lfitci

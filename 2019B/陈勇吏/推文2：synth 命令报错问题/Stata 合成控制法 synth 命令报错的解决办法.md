
&emsp;

> 作者：陈勇吏 (上海交通大学安泰经济与管理学院)      
> &emsp;       
> Stata 连享会： [知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn) | [CSDN](https://blog.csdn.net/arlionn)

&emsp;

合成控制法的常用命令 `synth` 在使用过程中，可能会出现一些问题，导致命令无法正常运行。本文列出两种经常出现的问题，并给出解决方案。

## 1. 第一种出现的报错 
运行 `synth` 命令以后，显示无法加载 `synthopt.plugin` 。
```stata
  use "smoking.dta",clear
  xtset state year
  
  synth cigsale  lnincome age15to24 retprice beer(1984(1)1988) ///
	    cigsale(1988) cigsale(1980) cigsale(1975),             ///
	    trunit(3) trperiod(1989) xperiod(1980(1)1988)          ///
		fig nested allopt
*===================== 出现下面报错信息 ======================*
Could not load plugin: D:\stata15/ado\plus\s\synthopt.plugin
(error occurred while loading synth.ado)
r(9999);
```
这是旧版本文件可能存在的问题，如果电脑里面装了多个版本的 synth 命令相关文件，可能会导致错误调用。

解决方案：
使用 `ssc install synth, replace` 更新 synth 命令。
```stata
. ssc install synth, replace

*========================== 显示结果如下： ========================
the following files will be replaced:
    D:\stata15/ado\plus\s\synth.sthlp
    D:\stata15/ado\plus\s\synthopt.plugin

installing into D:\stata15/ado\plus\...
installation complete.
```

## 2. 第二种出现的报错
使用 `ssc install synth, replace` 更新 synth 命令以后，依然无法加载 `synthopt.plugin`。
![报错信息.png](https://images.gitee.com/uploads/images/2019/0728/211011_2279524d_1333498.png)

这是 Stata 搜索 adopath 的路径设置问题。如果 `adopath + "path"` 添加的 adopath 路径中存在其他版本的 synth 命令文件，调用 synth 就会报错。

三种解决办法：
1、从 `D:\stata15\ado\plus\s` 路径下找到正确的文件 `synthopt.plugin` ，复制到报错中显示的路径 `D:\stata15\ado\personal\PX_C_2019b\C6_synth\adofiles\plus\s` 下。
2、删除 `D:\stata15\ado\personal\PX_C_2019b\C6_synth\adofiles\plus\s` 中的 synth.ado 文件。
3、运行 `adopath - D:\stata15\ado\personal\PX_C_2019b\C6_synth\adofiles` 命令，将这一路径从 ado 文件的搜索路径中剔除（这一方法将导致该路径下的其他外部命令也无法再使用）。


## 小结
Stata 使用合成控制法的外部命令 `synth` 时，如果遇到报错，可尝试运行下列命令解决问题：
```stata
ssc install synth, replace
```
如果依然报错，有两种建议的解决办法：

1、从 `D:\stata15\ado\plus\s` 文件夹中找到 `synthopt.plugin` 文件，复制到 `D:\stata15\ado\personal\PX_C_2019b\C6_synth\adofiles\plus\s` 路径中（替换该路径下的 `synthopt.plugin` 文件）。 

也可以直接运行如下命令达到相同的效果（第二行代码的 PLUS 后面填入 synth 命令报错返回的路径）：
```stata
local path `c(sysdir_plus)'
sysdir set PLUS "D:\stata15\ado\personal\PX_C_2019b\C6_synth\adofiles" //引号中是报错信息显示的路径
ssc install synth, replace
sysdir set PLUS "`path'"
```
2、删除 `D:\stata15\ado\personal\PX_C_2019b\C6_synth\adofiles\plus\s` 路径下的 synth 相关文件。




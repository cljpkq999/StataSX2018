
>作者：李珍
>
>单位：厦门大学

本文以系统自带数据 **auto.dta** 进行介绍，如何识别一个变量或者一个数据集中有多少种不同的观察结果。例如，公司 i 在第 t 年发生了 30 多笔借贷业务，我们想知道这些业务涉及了多少家银行。

本文对命令的介绍基于 [The Stata Journal (2008) 8, Number 4, pp. 557–568 Speaking Stata: Distinct observations Nicholas J. Cox Department of Geography Durham University Durham City, UK n.j.cox@durham.ac.uk Gary M. Longton](https://journals.sagepub.com/doi/pdf/10.1177/1536867X0800800408)。

目前在Stata种有不少命令都可以回答这个问题，比如 `contract` , `duplicates` ,但这些命令在实现过程都破坏了原有的数据结构。在这里，我们推荐两种方法，一种是使用 `egen` 命令自带的 `tag()` 函数来标记非重复值的出现与否，进而使用 `total()` 或 `sum()` 函数进行加总；另一种方法是直接使用外部命令 `egenmore` 提供的 `nvals()` 函数。

## 方法一： 

 ``` 
sysuse auto.dta, clear
drop nvals
by foreign rep78, sort: generate nvals = _n == 1 //“等式_n==1”表示如果foreign和rep78的组合第一次出现，则nvlas就赋值为1，否则为0
replace nvals = sum(nvals)    //对nvals进行加总
replace nvals = nvals[_N]	//将nvals的值替换为nvals的总数。
dis nvals
10
 ``` 
可以看到，**foreign** 和 **rep78** 一共有 10 种非重复组合。当然，如果使用 ```tab foreign rep78,mis ``` 能够更直接看到这 10 种类型的分布情况。

更进一步，我们可以可以使用分组命令 `by` 引导，计算不同组的非重复值个数。

```
sysuse "auto.dta", clear
drop nvals
by foreign rep78, sort: generate nvals = _n == 1
by foreign: replace nvals = sum(nvals)	//计算不同foreign类别下，对 rep788 的非重复值进行计数
by foreign: replace nvals = nvals[_N]	//在不同foreign组下，计算 rep78 的非重复值。
 ``` 
这里当然也可以使用 `by foreign: tab rep78,mis` 观察样本的分布。


以上计数命令，我们可以使用 `tag（）` 对 **varlist** 的观察值进行标记。用 `tag` 可以识别出相同的观察值，并将第一次出现的观察记为 1 ，其余所有相同观察值赋值为 0 ，进而识别出所有的非重复值。
 ``` 
sysuse "auto.dta", clear
egen tag = tag(foreign rep78)	//识别出不同的 foreign 和 rep78 组合
egen nvals = total(tag), by(foreign)	//计算不同 foreign 类别情况下， rep78 各有多少个非重复值
tabdisp foreign, cell(nvals)	//列表展示出结果
**注意 tabdisp 利用表格展示数据，不计算统计结果。
----------------------
 Car type |      nvals
----------+-----------
 Domestic |          5
  Foreign |          3
----------------------


 ``` 

需要注意的是，使用 `tag()` 计算出的类别比使用“等式 `_n==1` ”有所不同，这是因为 `tag()` 忽略了变量中缺失值，即不将缺失值样本作为非重复样本进行计算。


## 方法二：
使用命令 `egenmore` 提供的 `nvals()` 函数，仅一步就可以提供上述分解的简单的步骤的操作结果。

#### 1 下载安装
```
Ssc install egenmore
```

#### 2 语法结构
```
egen newvar=nvals(varname) [, by(byvarlist) missing]
```
**newvar** 的返回值即为 **varname** 变量非重复值的数量。除非指定了 `missing` ，否则 `nvals` 将不会把缺失值作为非重复值的一类。

#### 3 命令使用
```
sysuse "auto.dta", clear
egen a1=nvals(rep78)	//使用 nvals() 得到 rep78 的非重复值个数
egen a2=nvals(rep78), by(foreign)	//不同 foreign 类别下 rep78 的非重复值个数
egen a2=nvals(rep78), by(foreign) mis	//考虑缺失值的情况下，不同 foreign 类别下 rep78 的非重复值个数
```
同样地，使用命令 `distinct` 也可以得到不同观察值的数量。 `distinct` 可以单独报告每个变量包含的非重复值的数量，也可以报告多个变量联合的组中非重复值的数量。变量既可以是数值型，也可以是字符型。注意，默认情况下， `distinct` 也不将缺失值记为一类，除非将 `missing` 选项列出。

#### 1 下载安装
```
Ssc install distinct
```
#### 2 语法结构
```
distinct [varlist] [if] [in] [, missing abbrev(#) joint minimum(#) maximum(#) ]
```
可以搭配 `by` 语句使用

#### 3 命令使用

```
sysuse "auto.dta", clear

distinct	//列出所有变量的样本数和非重复值个数

              |        Observations
              |      total   distinct
--------------+----------------------
         make |         74         74
        price |         74         74
          mpg |         74         21
        rep78 |         69          5
     headroom |         74          8
        trunk |         74         18
       weight |         74         64
       length |         74         47
         turn |         74         18
 displacement |         74         31
   gear_ratio |         74         36
      foreign |         74          2
          tag |         74          2
        nvals |         74          2
           a1 |         69          2


by foreign, sort: distinct rep78	//列出不同foreign类别下，rep的样本数和非重复值数量
----------------------------------------------------------------------
-> foreign = Domestic

       |        Observations
       |      total   distinct
-------+----------------------
 rep78 |         48          5

----------------------------------------------------------------------
-> foreign = Foreign

       |        Observations
       |      total   distinct
-------+----------------------
 rep78 |         21          3

```
## 结语
识别一个变量或者联合多个变量时非重复值的个数，一方面可以把握样本的数据分布，另一方面，在绘图中，识别出变量重复值是很有用的，因为没有必要一次又一次地绘制完全相同的坐标。

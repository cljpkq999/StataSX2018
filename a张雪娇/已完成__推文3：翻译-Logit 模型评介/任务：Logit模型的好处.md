> **任务：** 翻译如下两篇推文，最好能整合成一篇。

- 1. 文中涉及到的参考文献要在文末列出，并通过 百度学术 或 google学术，找到可用的 PDF 链接，采用 `[[PDF]](链接地址)` 的形式放在参考文献末尾。
  - 例如，- Leary M T, Roberts M R. Do Peer Firms Affect Corporate Financial Policy?. **Journal of Finance**, 2014, 69(1):139–178. [[PDF]](http://finance.wharton.upenn.edu/~mrrobert/resources/Publications/PeerEffectsJF2014.pdf)
  - 对应的代码是 `- Leary M T, Roberts M R. Do Peer Firms Affect Corporate Financial Policy?. **Journal of Finance**, 2014, 69(1):139–178. [[PDF]](http://finance.wharton.upenn.edu/~mrrobert/resources/Publications/PeerEffectsJF2014.pdf)`


&emsp;

#### [In Defense of Logit – Part 1](https://statisticalhorizons.com/in-defense-of-logit-part-1)
#### [In Defense of Logit – Part 2](https://statisticalhorizons.com/in-defense-of-logit-part-2)


&emsp;

---
> 下面开始写正文。


# 验前和验后数据的横向合并/纵向追加

[**Source：** Pre and Post Test Data Merge/Append (By Francis at 11/29/2012)](http://www.econometricsbysimulation.com/2012/11/pre-and-post-test-data-mergeappend.html)

假设现在有以下2组数据

- 预测试数据(**Pre-test**）

- 当前数据(**Current Data**)

**注意：** 当前数据是指当前在 Stata 界面的数据，又称主要数据（ Master Data ）窗口数据或记忆空间数据。


若想要将这两种数据合并在一起，可以有以下两种选择

**（1）创建纵向数据集（使用 `append` 命令）**

**（2）创建横向数据集（使用 `merge` 命令）**


这两种数据合并方法都是很常见的，具体实现步骤如下：

  ## 1. 数据的生成   
  ### 1.1 预测试数据的生成（Pre-test Data）
```stata
clear
set obs 1000   
gen ID = _n                // unique identifier on each person/student
gen score = rnormal()

forv i=11/99 {             // generate say 88 variables (v11 through v99)
gen v`i' = runiform()
label var v`i' "Random uniform variable `i' - pretest"
}
save pretest, replace
```
- 通过  `rnormal`  以及 `runiform` 命令生成包含 **ID** **score** **v11** 等 91 个变量在内 1000 个观测值，并保存为预测试数据。

### 1.2 当前数据的生成（Current Data）
```stata
gen treatment = rbinomial(1,.3)      //imagine there is some kind of treatmens
replace score = score+.4*treatment

forv i=11/99 {
local change = rbinomial(1,.1)      //There is a 10% chance that one of your other variables will be changed
if `change'==1 replace v`i' = v`i'+runiform()
label var v`i' "Random uniform variable `i' - current"
}
save current, replace
clear
```
- 通过 `rbinomial` `runiform` 命令在预测试数据基础上对变量进行一定改变，新增 **treatment** 变量后保存为当前数据。现在我们拥有不同变量的两个数据集。
******************************************************************

  ## 2. 数据文件的合并   
  ### 2.1 数据的纵向合并 -- `append` 命令
``` stata
use pretest, clear   
gen phase = "Pretest"                       // Generating a variable to indicate pretest

append using current                        // append: Appending the data
sum
bysort ID: egen treat = mean(treatment)     //Make sure the "treatment" variable is duplicated for every observation.
drop treatment                             // Drop the old treatment variable
rename treat treatment
clear 
```
部分结果如下：
```					
-------------+---------------------------------------------------------
         v94 |      2,000    .5158958    .2936431   .0002596   .9973774
         v95 |      2,000    .4983512    .2921214   .0008446   .9998845
         v96 |      2,000    .5235505    .2834689   .0023575   .9992944
         v97 |      2,000     .503752    .2869586   .0008776   .9996563
         v98 |      2,000    .4861402    .2938694   .0000774   .9997599
-------------+---------------------------------------------------------
         v99 |      2,000    .5110351    .2851361   .0003785    .999744
       phase |          0
   treatment |      1,000        .299    .4580489          0          1
``` 
  **注意事项：**
1. 在绝大部分的纵向合并中，观察值可以是独立的来自于同一调查的不同样本，也可能不是完全独立的来自于不同调查时间的相同样本
2. 合并后，数据的变量基本不变，但观测个案增加了 （例如这里的观测值由原来的 1000 变为 2000 ）。
3. 该方法可以将两个或多个数据文件进行上下对接，简单明了不易出错.
  ### 2.2 数据的横向合并 -- `merge` 命令
  代码实现如下： 
```stata
use pretest, clear
foreach v of varlist * {
rename `v' `v'_0                           // Rename variables so that they will not be overwritten
}
rename ID_0 ID                             // Make sure only the merging variable keeps the same name.
save pretest_rename, replace
use current, clear                         // Loading the current test data.
foreach v of varlist * {
rename `v' `v'_1                           // Rename the variables in the current as well  
}
rename ID_1 ID                             // Making sure to change ID_1 back to ID

merge 1:1 ID using pretest_rename          //Merge the data together
order *, alphabetic
```
   结果如下：
```
. merge 1:1 ID using pretest_rename          

    Result                           # of obs.
    -----------------------------------------
    not matched                             0
    matched                             1,000  (_merge==3)
    -----------------------------------------
```
**注意事项：**
1. 故横向合并也是变量的合并，新数据的变量增加但观察个案可以不变（例如，这里合并之后观测值依然为 1000 个）。
2. 实现数据文件的横向合并的前提是，主要数据和使用数据必须有一个（或多个）相同的关键变量
3. 横向合并中，除关键变量外，其它变量的名称都不能相同。若两个数据包含其它的同名变量，则使用数据中的变量数值将被主要数据的同名变量数值取代。


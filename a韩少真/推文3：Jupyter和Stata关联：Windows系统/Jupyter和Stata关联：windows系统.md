# Jupyter和Stata关联：windows系统
> 作者：韩少真(西北大学) || 展金永（对外经济贸易大学）
## 1. 准备工作-关联前提
首先，请按助教提供教程安装Anaconda和Stata，并检验Anaconda和Stata是否已经成功安装在本机电脑上。
## 2. 将stata添加到命令行注册
- 根据下图步骤，以管理员身份运行`Windows PowerShell`。
划重点：请务必以管理员身份运行。
![](https://upload-images.jianshu.io/upload_images/17516282-b881b1fb5198ad15.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

-打开stata的安装的文件夹，根据下图获取stata的安装路径：
![](https://upload-images.jianshu.io/upload_images/17516282-ab7e95119f89f815.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

-在`Windows PowerShell`执行cd命令，以进入stata程序安装的路径。cd命令后接上步所获取的stata安装路径。根据个人电脑安装路径不同有所差异。路径请以英文引号包围，这样可以避免路径文件夹名称中包含空格导致无法顺利进入目标路径。
```
cd "D:\Stata15"
```
-执行上述命令后，请根据下图提示，确认是否已进入stata安装路径：
![](https://upload-images.jianshu.io/upload_images/17516282-7c7573ab3124ebd9.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

-还可以在`Windows PowerShell`中执行`dir`命令，然后根据下图，观察当前路径的文件内容和stata安装路径的内容是否一致。如果两者一致，说明已成功进入stata安装路径。如果不一致，则应仔细检查操作流程，重新执行以确定进入stata安装路径。
![](https://upload-images.jianshu.io/upload_images/17516282-71012cde6a3488d1.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

-在`Windows PowerShell`中执行`.\StataMP-64.exe /Register`命令，则可以成功将stata添加到命令行注册。需要注意的是，`.\StataMP-64.exe /Register`中的`.\StataMP-64.exe`部分，根据个人电脑安装stata15版本有所差异。我电脑安装的是MP版，所以为`.\StataMP-64.exe`。如果安装的是SE版，应该为`.\StataSE-64.exe`。
```
.\StataMP-64.exe /Register
```
#如果是SE版，则为:
```
.\StataSE-64.exe /Register
```
如果依然担心出错，还有一种简单办法可以确定`.\StataMP-64.exe /Register`中的`.\StataMP-64.exe`部分。打开个人电脑中stata安装路径文件夹，找到stata程序的执行文件，然后按下图所示，将完整的带.exe的文件名复制下来，然后将其替换`StataMP-64.exe'部分。再进行命令行注册。
![](https://upload-images.jianshu.io/upload_images/17516282-6bd0ba72da78fced.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

## 3. 在Anaconda中安装stata_kernel包。
- 根据下图打开`Anaconda prompt`
![](https://upload-images.jianshu.io/upload_images/17516282-8f46df8ff49c5080.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

- 在`Anaconda prompt`逐行执行以下代码，第一行是为了升级conda，第二行是升级pip包。根据`Anaconda prompt`窗口的提示，可能要输入`y`来进行包的升级。如果已经是最新版本，则不会提醒升级，略过此步骤即可。升级包管理模块的目的是保证anaconda中的包管理模块是最新版本，这样可以降低后续安装stata_kernel包出错的概率。
```python
conda update conda

python -m pip install --upgrade pip
```


- 在`Anaconda prompt`逐行执行以下代码,安装stata_kernel包。安装结束后可根据`Anaconda prompt`窗口的提示，判断是否成功安装。
```python
pip install stata_kernel
python -m stata_kernel.install
```
- 为检验stata_kernel包是否已经成功安装，请在`Anaconda prompt`执行`conda list`代码，这会在`Anaconda prompt`窗口展示当前python环境下安装的所有包，以英文字母排序。请查看是否包含stata_kernel包。如果包含，这说明stata_kernel包已经成功安装。
```python
conda list
```
具体如下图所示：
![](https://upload-images.jianshu.io/upload_images/17516282-ca9ad1cdff80aeba.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

## 4. 打开jupyter notebook，新建stata语法格式的notebook，执行stata代码，检验是否关联成功。

- 根据下图，打开`Anaconda Navigator`
![](https://upload-images.jianshu.io/upload_images/17516282-a0c28dea2d9cfb08.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

- 根据下图，在`Anaconda Navigator`窗口打开`jupyter notebook`。这会在浏览器中弹出一个类似于网页的窗口，就是`jupyter notebook`。
![](https://upload-images.jianshu.io/upload_images/17516282-6fcd445190ae7a7a.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

- 根据下图，在`jupyter notebook`中执行以下操作，新建一个支持stata语法的notebook。这会弹出一个新的网页标签。
![](https://upload-images.jianshu.io/upload_images/17516282-5c58d4df65373e7a.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

- 在新建的notebook中，通过下图可以初步判断是否关联成功。
![](https://upload-images.jianshu.io/upload_images/17516282-c0a1880ab695006c.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

- 在命令行输入stata命令，并点击`运行`执行。如果关联成功，则会在命令的下方显示stata结果窗口的结果。具体如下图所示：

```stata
dis 1+3
sysuse auto,clear
reg price weight
scatter price weight
```
![](https://upload-images.jianshu.io/upload_images/17516282-b566cbff21ac6eaf.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)





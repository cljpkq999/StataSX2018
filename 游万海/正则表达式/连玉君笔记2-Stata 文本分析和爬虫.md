[toc]

# Stata 文本分析和爬虫


## 基础知识

### ASCII

[ASCII](https://baike.baidu.com/item/ASCII/309296?fr=aladdin)（American Standard Code for Information Interchange，美国信息交换标准代码）是基于拉丁字母的一套电脑编码系统，主要用于显示现代英语和其他西欧语言。它是现今最通用的**单字节**编码系统，并等同于国际标准ISO/IEC 646。

### Unicode

详情参见：[Unicode 和 UTF-8 有何区别？](https://www.zhihu.com/question/23374078)

[Unicode](https://baike.baidu.com/item/Unicode/750500?fr=aladdin)（统一码、万国码、单一码）是计算机科学领域里的一项业界标准，包括字符集、编码方案等。Unicode 是为了解决传统的字符编码方案的局限而产生的，它为每种语言中的每个字符设定了统一并且唯一的二进制编码，以满足跨语言、跨平台进行文本转换、处理的要求。1990年开始研发，1994年正式公布。

Unicode 编码包含了不同写法的字，如“ɑ/a”、“户/户/戸”。然而在汉字方面引起了一字多形的认定争议。
在文字处理方面，统一码为每一个字符而非字形定义唯一的代码（即一个整数）。换句话说，统一码以一种抽象的方式（即数字）来处理字符，并将视觉上的演绎工作（例如字体大小、外观形状、字体形态、文体等）留给其他软件来处理，例如网页浏览器或是文字处理器。

Unicode 是国际组织制定的可以容纳世界上所有文字和符号的字符编码方案。目前的 Unicode 字符分为 17 组编排，0x0000 至 0xFFFF，每组称为平面（Plane），而每平面拥有 65536 个码位，共 1114112 个。然而目前只用了少数平面。**UTF-8**、UTF-16、UTF-32 都是将数字转换到程序数据的编码方案。


### UTF-8

[UTF-8](https://baike.baidu.com/item/UTF-8/481798?fr=aladdin)（8-bit Unicode Transformation Format）是一种针对 Unicode 的可变长度字符编码，又称万国码。由 Ken Thompson 于 1992 年创建。现在已经标准化为 RFC 3629。UTF-8 用 1 到 6 个字节编码 Unicode 字符。用在网页上可以统一页面显示中文简体繁体及其它语言（如英文，日文，韩文）。


## 文档导入

- `help intext`
- `help infile`
- `help infix`

## Stata 文本处理函数

### Stata中的正则表达式

- [Stata中的正则表达式的基本定义](https://www.stata.com/support/faqs/data-management/regular-expressions/)
- [这个帖子介绍的不错](http://www.bruunisejs.dk/StataHacks/Datamanagement/regex/regular_expressions/)
- [Stata FAQ: 正则表达式使用范例](https://stats.idre.ucla.edu/stata/faq/how-can-i-extract-a-portion-of-a-string-variable-using-regular-expressions/)
- [数据处理中的正则表达式](https://www.stata.com/meeting/wcsug07/medeiros_reg_ex.pdf)
- [一份简单的讲义和小例子-整理到我们的讲义后删除即可](http://jearl.faculty.arizona.edu/sites/jearl.faculty.arizona.edu/files/Regular%20Expressions%2C%20Year%202.pdf)
- [一个新命令+Stata 范例](https://www.statalist.org/forums/forum/general-stata-discussion/general/1327564-new-program-for-regular-expressions)

## 文档处理(一整份文档为分析对象)

### subinfile 命令

### filefilter 命令

参见：Riley, A. R. 2008. Stata tip 60: Making fast and easy changes to files with filefilter. *Stata Journal*, 8: 290–292. [++PDF++](http://www.stata-journal.com/sjpdf.html?articlenum=pr0039)


## 相关应用

- [爬取网页中的表格 Website tables into Stata](http://statadaily.com/2011/05/09/website-tables-into-stata/)


## 其他软件和工具
- [黄页爬虫 Yellow Pages Spider](http://www.softwaredownloadcentre.com/software/yellow-pages-spider.php)
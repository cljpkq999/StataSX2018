>作者：游万海 || 连玉君 ||  ([知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn))    
>
> Stata连享会 [精彩推文1](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文2](https://github.com/arlionn/stata/blob/master/README.md)
  
![Stata外部命令一览-连享会](https://upload-images.jianshu.io/upload_images/7692714-acb4e76ec9dfde32.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

Stata 软件的一大特点是其开放性。用户可以修改 Stata 官方提供的命令 (其实就是一些以 **.ado** 为后缀的文本文件)，为己所用。同时，全球的 Stata 用户也在日夜耕耘，共享他们编写的新命令——**外部命令**，比如大家熟悉的  `outreg2 ` (Roy Wada)，` estout`  (Ben Jann) 和  `winsor2`  (Lian Yu-jun ) 等热门命令。

遗憾的是，这些外部命令分散在互联网的各个角落，我们需要一些海纳百川的本领。这就是本文地主要目的：大家介绍外部命令的搜索、安装及使用方法。

大家在演示教材中的例子时，经常发现为何相同的命令，教材可以得到结果，而自己运行却出现错误，如最常见是扎眼的红色信息 **_command fuzzydid is unrecognized_**
![](https://upload-images.jianshu.io/upload_images/7692714-c20bea97159eb5d0.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)



>常用的外部命令来源：
        
+ Statistical Software Components (SSC) archive (http://www.repec.org)
+ Stata Journal ( https://www.stata-journal.com/)
+ Stata Technical Bulletin (STB) (https://www.stata.com/products/stb/)
+ github (https://github.com/)

> 一些最热门的命令：

输入 `ssc hot, n(10)` 可以查看 [SSC](http://www.repec.org) 上最热门的 10 个命令：
>![SSC 上最热门的 10 个 Stata 外部命令](https://upload-images.jianshu.io/upload_images/7692714-807e59dcf92faf5d.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
输入 `github hot, number(5)` 可以查看 [Github](https://github.com/) 上最热门的 5 个命令。 
>![Github 上最热门的 5 个 Stata 外部命令](https://upload-images.jianshu.io/upload_images/7692714-24bed1880034b85a.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

            
### 1. 路径设置

为了方便命令的管理，下面先介绍一些基本的设置。

下载的外部命令通常为 **.ado** 格式，为了将下载的命令与自编的命令放在不同的路径下，找到 Stata 的安装路径，可以看到一个 【ado 】文件夹。里面有【base】 文件、【plus】 文件及 【personal】 文件（后两个文件夹不一定有自带，可以自己手动创建）。
- 【base】文件夹用于存储 Stata 自带的基础命令
- 【plus】文件夹用于存储外部命令
- 【personal】用于存储自己编写的命令和 dofiles。

为了达到这一目的，需要对系统默认路径进行一些简单的设置。在 Stata 的安装路径下（如：【D:\stata15】）新建一个 **profile.do** 文件，将如下命令放到此文件中：
```
adopath + "D:\stata15\ado\plus"
sysdir set PLUS "D:\stata15\ado\plus"          // 外部命令的存放位置
sysdir set PERSONAL "D:\stata15\ado\personal"  // 个人文件夹位置
cd `c(sysdir_personal)'
```
（**Note:** 连老师在这里-[连玉君的 profile.do 文档](https://gitee.com/arlionn/StataProfile)  分享了他的 **profile.do** 文档。https://gitee.com/arlionn/StataProfile ）

###  2. 外部命令的下载

 根据外部命令的来源不同，所使用的命令也不尽一致，这里主要介绍  `ssc` ， `search，net` ，`findit` 和  `github`  等命令。

### 2.1 ssc 命令

`ssc` 是 Statistical Software Components ([http://www.repec.org](http://)) 的缩写，用于操作存放在该网站上的外部命令，包括安装 ( `ssc install `)、移除 ( `ssc uninstall` )、描述 ( `ssc describe `)、显示最近更新 ( `ssc new` )、显示最热门 ( `ssc hot` )。例如：

```stata
. ado dir  //显示已安装ado文件
[1] package outreg2 from http://fmwww.bc.edu/repec/bocode/o
      'OUTREG2': module to arrange regression outputs into an illustrative table
[2] package estout from http://fmwww.bc.edu/repec/bocode/e
      'ESTOUT': module to make regression tables

. ssc install winsor2, all replace   //安装winsor2，基本用法：ssc install newprogramname
checking winsor2 consistency and verifying not already installed...
installing into D:\stata15\ado\plus\...
installation complete.
```
若要查看某一具体命令是否安装，可以利用
```
. ado, find(winsor2)

[3] package winsor2 from http://fmwww.bc.edu/repec/bocode/w
      'WINSOR2': module to winsorize data
```
要查看 `ssc` 上最热门的命令，可以通过
```
. ssc hot, n(10)  //显示排名前10的命令
Top 10 packages at SSC
        Aug 2018   
  Rank   # hits    Package       Author(s)
  ----------------------------------------------------------------------
     1  331271.0    findname      Nicholas J. Cox                         
     2  19504.6    outreg2       Roy Wada                                
     3  18223.6    estout        Ben Jann                                
     4  11066.7    distinct      Gary Longton, Nicholas J. Cox           
     5   7746.3    winsor        Nicholas J. Cox                         
     6   6881.7    winsor2       Lian Yu-jun                             
     7   6598.7    ivreg2        Mark E Schaffer, Steven Stillman,  Christopher F Baum                      
     8   6579.3    ivreg210      Mark E Schaffer, Steven Stillman, Christopher F Baum                      
     9   6571.1    ivreg28       Mark E Schaffer, Steven Stillman,  Christopher F Baum                      
    10   6561.8    ivreg29       Christopher F Baum, Mark E Schaffer, Steven Stillman                         
  ----------------------------------------------------------------------
  (Click on package name for description)
```
 从上面结果可以看出，连老师编写的 `winsor2` 也榜上有名 (^^此处应有掌声)。另外，还有  `ssc new` ,  `ssc describe` ,  `ssc type`  和  `ssc copy`  等用法，具体可以通过  `help ssc ` 查看相关例子。

### 2.2 search 命令

与 `ssc `命令相比，`search` 搜索的范围更广，该命令语法如下：
```
.search word [word ...] [, search_options]
```
 word 表示待搜索的关键词，比如想了解面板数据模型的相关命令，可以 `search panel data model`。对于怎么取关键词，可以 `help searchadvice`。可以看出，搜索结果中既有 Stata 手册中的命令，也有外部命令，甚至还包括 Stata 官方的培训、书籍、视频和 FAQ 等信息。
![Stata 中的 search 命令-Stata连享会](https://upload-images.jianshu.io/upload_images/7692714-446a42f80a96b2bc.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

**search** 命令的选项 **search_options** 主要包括：`all`，`net`，`faq`，`manual`，`sj` 等，对应上文提到的各种类型的信息，以便于我们实现精准搜索。

- (1) **search + all**: 通常为默认选项，根据帮助文件的描述：
   _**`search, all` is the best way to search for information on a topic across all sources, including the system help, the FAQs at the Stata website, the Stata Journal,and all Stata-related Internet sources including user-written additions**_ 

  从上面的表述可以发现，当定义 `all` 时，其搜索范围很广，包括软件自带的系统文件，Stata 网站的常见问题集，Stata journal 期刊及其他网络相关资料。因此，与 `ssc` 不同，`search` 命令不仅可以搜索外部命令，也能搜索其他相关文档资料。

- (2) **search + net:** 与 `net search` 等价，根据帮助文件描述：

   _**`net search` searches the Internet for user-written additions to Stata, including, but not limited to, user-written additions published in the Stata Journal (SJ) and in the Stata Technical Bulletin (STB)**_ 

  从上述描述可知，通过该命令可以搜索发布在 Stata Journal (SJ) 和Stata Technical Bulletin (STB)上的相关资料。资料不仅包括 ado 命令
文件，还包括帮助文件 (help files) 和数据集 (datasets).

- (3) **search + sj:** 仅搜索 Stata Journal 和 Stata Technical Bulletin 上的资源。
- (4) **search + faq:** 仅搜索到发布在 Stata 官网 http://www.stata.com 中的 FAQS 条目下的资源。
- (5) **search + manual:** 仅搜索 Stata 电子手册文档上的资源。
例子：
```
. search linear regression, all
. search linear regression, net
. search linear regression, sj
. search linear regression, faq
. search linear regression, manual
```

### 2.3 net 命令

`net` 命令的用法与 `search` 相似，功能也较多，详细可以 `help net` 查看。本文主要介绍以下几种：
- (1) `net search word [word ...] [, search_options]`
  该命令与上面介绍的 `search + net` 等价；
(2) `net install`
  语法如下：
```
. net install pkgname [, all replace force from(directory_or_url)]
```
该命令可以用于从特定的网站安装外部 ado 文件，比如
```
.net install github, from("https://haghish.github.io/github/")
```
运行上述命令得到
```
. net install github, from("https://haghish.github.io/github/")
checking github consistency and verifying not already installed...
installing into D:\stata15\ado\plus\...
installation complete.
```
使用 `ado describe` 命令可以查看已安装命令详情：
```
. ado describe github

--------------------------------------------------------
[4] package github from https://haghish.github.io/github
--------------------------------------------------------

TITLE
      'GITHUB': search, install, and uninstall Stata packages with a particular    {break}

DESCRIPTION/AUTHOR(S)
      version (release) as well as their dependencies from
      {browse "http://www.github.com/haghish/github":GitHub} website
      Distribution-Date: 20161214
INSTALLATION FILES
      f\findall.ado
      f\findall.sthlp
      g\github.ado
      g\github.dlg
      g\github.sthlp
      g\githubcheck.ado
      g\githubconfirm.ado
      g\githubdependency.ado
      g\githublist.ado
      g\githubmake.ado
      g\githuboutput.ado
      g\githubquery.ado
      g\githubsearch.ado
      g\githubsearchsteps.ado
      m\make.ado
      m\make.dlg

INSTALLED ON
      1 Nov 2018
--------------------------------------------------------
```
- (3)  `net sj vol-issue [insert]`
这个用法很强大，有时候我们想把 Stata Journal 某一期所涉及的外部命令都下载到本地，比如想安装 2018年第 3 期文中的所有文件，可以使用如下命令：
```
. net sj 18-3
```
这等价于
```
. net from "http://www.stata-journal.com/software/sj18-3"
```

### 2.4 findit 命令

`findit + keyword` 等价于 `search keyword [keyword ...], all`      

可以搜索的资料包括： 系统文件 **the system help, the FAQs, the Stata Journal, and all Stata-related Internet sources including user-written additions**. 如我们想了解 Stata 中有关面板单位根检验方面命令与资料，可以执行如下命令：
```
. findit panel unit root
```
![Stata 中的 findit 命令-Stata连享会](https://upload-images.jianshu.io/upload_images/7692714-317f7aa4df7fc8d2.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

  
### 2.5 github命令

gitHub 是一个面向开源及私有软件项目的托管平台，因为只支持 git 作为唯一的版本库格式进行托管，故名 gitHub。在 GitHub 中，用户可以十分轻易地找到海量的开源代码。

目前，越来越多学者将程序托管到该平台，包括 Python，R 和 Stata 等各种软件。为了更方便地安装托管在github 上的命令，E. F. Haghish 开发了 `github` 命令。`github` 可以实现搜索、安装、移除等功能。

为了使用这一外部命令，首先要通过以下命令进行安装
```
. net install github, from("https://haghish.github.io/github/")
```
完成 github 安装，通过 `help github`，可以发现其语法如下
```
. github [ subcommand ] [ keyword | username/repository ] [, options]
```
这里主要介绍 `github search` 和 `github install`：

- (1) **`github search`:** 该命令可以对托管到 github 平台的 Stata 相关命令进行搜索，比如我们想知道在 github 平台上有哪些面板数据模型相关命令，可以输入：
```
. github search panel data model, in(all)
```
![github search 命令 - Stata连享会](https://upload-images.jianshu.io/upload_images/7692714-8fa0b757734c81c0.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

根据上面返回的结果，点击相应蓝色部分的命令可以在 github 上查看相应的项目，包括：仓库、作者主页等，点击 **Install** 可以在线安装该命令的相关文件。

- (2) **`github install`:** 该命令主要用于安装托管于 github 平台的外部命令。比如我们想了解在 Stata 中是否有实现动态报告的相关命令(类似于 R 里面的 knitr 包)，可以输入命令
```
. github search dynamic report, in(all)
```
![image](http://upload-images.jianshu.io/upload_images/7692714-7647aea0521acbac.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "dynamic1.png")
此时，点击 **Install** 会自动安装最新版本，若想安装此前的某个版本，则可以使用 `github install` 命令的 `version()` 选项加以控制：
```
. github install haghish/MarkDoc , version("3.8.0")
```
之后在界面上可以看到
```
. github install haghish/MarkDoc , version("3.8.0")
checking markdoc consistency and verifying not already installed...
installing into D:\stata15\ado\plus\...
installation complete.

Checking package dipendencies
markdoc package has no dependency
```
这也是发布于 github 上的命令区别于 SSC 上的命令的主要区别：Github 可以非常高效地进行版本控制。

### 3. 总结
可将本部分内容归结为两点：
- 其一，当知道外部包的具体命令写法时，通常可以利用 `ssc install`, `net install` ，`github install` 等命令直接安装；
- 其二，若只知道该命令的大体功能或关键词，而不知道具体名称，可以通过 `findit`，`search`，`github search` 等命令进行搜索，在返回的结果中查找安装。
- 其三，为了保证外部命令能够被 Stata 自动检索到，需要在 **profile.do** 文档中设定文件路径。



>#### 关于我们

- 【**Stata 连享会(公众号：StataChina)**】由中山大学连玉君老师团队创办，旨在定期与大家分享 Stata 应用的各种经验和技巧。
- 公众号推文同步发布于 [CSDN-Stata连享会](https://blog.csdn.net/arlionn) 、[简书-Stata连享会](http://www.jianshu.com/u/69a30474ef33) 和 [知乎-连玉君Stata专栏](https://www.zhihu.com/people/arlionn)。可以在上述网站中搜索关键词`Stata`或`Stata连享会`后关注我们。
- 点击推文底部【阅读原文】可以查看推文中的链接并下载相关资料。
- Stata连享会 [精彩推文1](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文2](https://github.com/arlionn/stata/blob/master/README.md)

>#### 联系我们

- **欢迎赐稿：** 欢迎将您的文章或笔记投稿至`Stata连享会(公众号: StataChina)`，我们会保留您的署名；录用稿件达`五篇`以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。
- **意见和资料：** 欢迎您的宝贵意见，您也可以来信索取推文中提及的程序和数据。
- **招募英才：** 欢迎加入我们的团队，一起学习 Stata。合作编辑或撰写稿件五篇以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。
- **联系邮件：** StataChina@163.com

>#### 往期精彩推文
- [Stata连享会推文列表1](https://www.jianshu.com/p/de82fdc2c18a) 
- [Stata连享会推文列表2](https://github.com/arlionn/Stata_Blogs/blob/master/README.md)
- Stata连享会 [精彩推文1](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文2](https://github.com/arlionn/stata/blob/master/README.md)

![Stata连享会推文集锦-StataChina](https://upload-images.jianshu.io/upload_images/7692714-8b1fb0b5068487af.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


---
![欢迎加入Stata连享会(公众号: StataChina)](http://upload-images.jianshu.io/upload_images/7692714-e8f30f5a27a2122b.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "扫码关注 Stata 连享会")

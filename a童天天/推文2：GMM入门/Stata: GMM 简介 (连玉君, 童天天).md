### 主要参考资料
- Stata 手册 GMM，[[PDF]](https://www.stata.com/manuals13/rgmm.pdf)，写得非常清楚。
- Zsohar, P., 2010, Short introduction to the generalized method of moments, Hungarian statistical review, 16: 150-170. [[PDF]](http://www.ksh.hu/statszemle_archive/2012/2012_K16/2012_K16_150.pdf), 写的非常清楚，与我想要的思路也很一致
-  Drukker, 2010, PPT, [An introduction to GMM estimation using Stata](https://www.stata.com/meeting/germany10/germany10_drukker.pdf)，介绍了 GMM 的基本思想，以及 GMM 与 MLE 的差别
- Mora，Ricardo, 2010，PPT，
  - GMM 原理介绍，[Generalized Method of Moments](http://www.eco.uc3m.es/~ricmora/mei/materials/Session_12_GMM.pdf)，最后一部分介绍了过度识别检验，整体上非常清晰。
  - GMM 范例，[GMM: Examples](http://www.eco.uc3m.es/~ricmora/mei/materials/Session_13_Examples.pdf)，包括：非线性理性预期模型，非线性IV估计，持有收入假说等。
  - GMM 的 Stata 实操，[GMM Estimation in Stata](http://www.eco.uc3m.es/~ricmora/mei/materials/Session_14_GMM_estimation.pdf)，提供了几个 Stata 中实操的范例，包括：线性回归，非线性回归。简单明了，但深度不足，要配合上一个 PPT 看才好。
  - GMM 习题：[ProblemSet4_GMM](http://www.eco.uc3m.es/~ricmora/mei/materials/ProblemSet4_GMM.pdf)
- Guilkey, David, 2011, [(GMM) Estimation with Applications using STATA](https://www.cpc.unc.edu/training/Guilkey%20Talk.pdf). 提供了多个在 Stata 中实现 GMM 估计的范例，包括：2SLS, Poisson，Logit，SUR 等。


### 写作提纲

1. GMM 简介
2. MM 
2.1  PMC 和 SMC 
  样本均值的估计 - MM
2.2 MM 估计的一般形式
  OLS 估计
  IV 估计
3. GMM
  - 为何要使用 GMM？
  - 两阶段最小二乘法
  - Euler 方程范例
  - 过度识别检验
4. Stata 实现
4.1 gmm 命令
  - 简单例子：样本均值，OLS，IV，两阶段最小二乘法
  - 进阶例子：Euler 方程，动态面板等
  - 高级例子：xxx
4.2 其他内置 GMM 估计的 Stata 命令
  - `help ivregress` // gmm 选项
  - `help xtabond` //
4.3 过度识别检验 
  

&emsp;

&emsp;

---
> Note: 如下是我随便记录的一些内容，你写的时候自行留用 
---


### MM  和 GMM 的区别
其实，要先引入两阶段最小二乘法的问题，介绍清楚恰好识别，识别不足和过度识别 这三个概念。Stata 手册中 GMM 部分对此问题讲解的比较清楚。我上课讲的那个面试的例子其实更容易理解。

- The Method of Moments (MM) is a particular type of GMM
- In the Method of Moments (MM), we have the same number of sample moment conditions as we have parameters
- In GMM, we may have more sample moment conditions than
we have parameters



## 1. GMM 简介

参考 ，[[Stata 手册 GMM]](https://www.stata.com/manuals13/rgmm.pdf) 中的介绍，可以直接翻译后调整即可。

广义矩估计 (Generalized Method of Moment, 简称 GMM) 是一种构造估计量的方法，类似于极大似然法 (MLE) 。MLE 通过假设随机变量服从特定的分布，进而将待估参数嵌入似然函数，通过极大化联合概率密度函数得到参数的估计值。GMM 则是以随机变量遵循特定矩的假设，而不是对整个分布的假设，这些假设被称为矩条件。这使得 GMM 比 MLE 更稳健，但会导致估计量的有效性有所降低 (估计出的标准误比较大)。

GMM 得到的广泛的应用。最为典型的是动态面板数据模型的估计，参见 。[GMM: Examples](http://www.eco.uc3m.es/~ricmora/mei/materials/Session_13_Examples.pdf) 中也列举了不少经典范例，包括：非线性理性预期模型，非线性IV估计，持有收入假说等。

## MM 估计量
### 1.1 MM 估计量

#### 母体矩条件 (PMC) 与样本矩条件 (SMC)

$$
E(y-\mu)=0 \longrightarrow \frac{1}{N} \sum_{i=1}^{N}\left(y_{i}-\widehat{\mu}\right)=0 \longrightarrow \widehat{\mu}=\frac{1}{N} \sum_{i=1}^{N} y_{i}
$$

其中，$N$ 表示样本数，$y_{i}$ 表示 $y$ 的第 $i$ 个观察值 $y$。此处，估计量
$\widehat{\mu}$ 被称为 **矩估计量** (the method of moments estimator)，简称 MM 估计量。这是因为，该估计量的构造以母体矩条件 (population moment
condition) 为基础，进而用其样本矩条件 (依赖于我们使用的数据) 做等价代换。
 because we started with a population moment
condition and then applied the analogy principle to obtain an estimator that depends on the observed
data.


假设待估参数的个数为 $k$，矩条件的个数为 $l$。当 $k=l$ 时，称为“**恰好识别**”，当 $k<l$ 时，称为 “**过度识别**”。

GMM 是矩估计（MM）的推广。在恰好识别情况下，目标函数的最小值等于 0 ，GMM 估计量与 MM 估计量等价；然而在过度识别情况下，MM 不再适用，GMM 可以有效地组合矩条件，使 GMM 比 MM 更有效。

GMM 建立在期望值和样本平均值的基础上。矩条件是用真实矩指定模型参数的期望值。

自由度为 d 的 χ2 随机变量的均值为 d，其方差为 2d，因此两个 **母体矩条件**  (Population Moment Condition, 简称 **PMC**) 如下：
$$
\begin{array}{c}
{E[Y-d]=0}  \qquad\qquad \qquad (1a) \\
{E\left[(Y-d)^{2}-2 d\right]=0} \qquad (1b)
\end{array} 
$$

对应的 **样本矩条件** (Sample Moment Condition, 简称 **SMC**) 为：
$$
\begin{array}{c}
{\frac{1}{N} \sum_{i=1}^{N} \left(y_{i}-\hat{d}\right)=0}   \qquad\qquad\quad (2a) \\ 
{\frac{1}{N} \sum_{i=1}^{N}\left[\left(y_{i}-\hat{d}\right)^{2}-2 \hat{d}\right]=0 \quad(2 b)}
\end{array}
$$

我们可以使用样本矩条件 (2a) 或 (2b) 来估计参数 $d$，得到都是 MM 估计量。然而，如果我们同时使用 (2a) 和 (2b)，则会有两个样本矩条件，但待估参数却只有一个，致使我们无法得到唯一的参数估计。

例如，假设基于 (2a) 得到的参数估计值为 $\hat{d}_1=0.9$，而基于 (2b) 得到的参数估计值为 $\hat{d}_2=0.7$。从理论上来讲，这两个 MM 估计量都是 $d$ 的无偏估计量，我们到底使用哪个呢？

显然，我们无法找到一个参数让两个矩条件 (记为 $m_1$ 和 $m_2$) 同时得到满足，但可以设定一个权重 $W=(w_1, w_2)$，进而得到让 ($w_1\times m_1+w_2\times m_2)$ 极小化时参数 $d$ 的估计值，称之为 $\hat{d}_{GMM}$。

GMM 寻找最接近于求解加权样本矩条件的参数。

Uniform weights 和 optimal weights 是加权样本矩条件的两种方法。Uniform weights 使用单位矩阵对矩条件进行加权。optimal weights 使用矩条件协方差的逆矩阵来加权。



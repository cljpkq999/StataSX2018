> version 1：`2017/11/8 22:27`

> version 2: `2019/4/30 23:08`

## 推文目的   
- 介绍如何设定 Stata dofile 编辑器的`配色方案`，以便呈现语法高亮，光标高亮等功能，提高 dofile 编辑器的舒适性。
- 一些经典的配色方案可以参考 [highlightjs经典配色方案](https://highlightjs.org/static/demo/)。其中，`rainbow`, `VS 2015`，`Xcode`，`Tomorrow系列`，`GoogleCode` 等风格都不错。


## Stata 中配色方案(亦称为“个人设置 (Preference)”) 的设定方式

### 通用设置

1. 在 Stata 的任何一个窗口中都可以通过 **右击鼠标** 来设定`字体属性`；
1. 在 Stata 结果窗口中，**右击鼠标** &rarr; **Preference** 可以进一步设定`结果窗口(Results)`、`文件浏览器(Viewer)`、`数据编辑器(Data Editor)`等窗口的属性，主要涉及**字体是否加粗**、**颜色**、**窗口背景色**等。
1. 上述设定，以及如下设定，都可以保存到一个模板中。Stata 支持多种模板，可以让你在早、中、晚使用不同的模板，既能调节心情，又能保护视力。
**很重要：** 模板的保存方法为如下。在设置了某些特征后，可以在 Stata 主界面依次点击 **Edit** &rarr;  **Preference** &rarr; **save preference set**，自行定义模板名称，然后 **确认** 即可。若想调用某个模板，方法同上，只是上述最后一步中的 **save preference set** 改为 **load preference set** 即可。
1. 打开 dofile 编辑器 (快捷键为：Ctrl+9)，依次点击 **Edit** &rarr; **Preferences**，会弹出如下 `Do-file Editor Preferences`&rarr; `通用属性 (General)` 设定的对话框：  
![通用设置](https://i.loli.net/2019/07/23/5d3688d859e2c58979.png)
   - **Display** 下拉菜单中的条目含义如下：  
     - Syntax highlighting 语法高亮；
     - Line numbers 在左侧显示行号；
     - Code folding 点击即可将代码块折叠起来，通常是循环语句
     - Bookmarks 在行号的右侧会显示书签的标志
     - Highlighting current line 将光标所在行高亮显示
     - Indentation guide 缩进指引
     - Enable page guide  at column 虚线代表此处是第20列，默认是80
     - Wrap lines 当一行不能完全容纳时，会换行，这是换行的标记

1. Do-file 编辑器配色方案主要是通过调整 **`Colors`** 对话框中的参数来实现的：

首先展示几个常用的配色方案，如`monokai`、`one dark`、`spring`和`coffee`等经典主题。

![monokai、one dark、spring和coffee四种主题展示](https://i.loli.net/2019/07/23/5d3689a22788f57080.gif)


对于主题配色大致分为两种类别，即 dark 和 light，分别是黑色和浅色主题。上述展示中 `monokai` 和 `one dark` 是黑色主题，而 `spring` 和  `coffee` 是浅色主题。

为了实现上述效果，下面介绍具体设置方法。

![输入图片说明](https://gitee.com/uploads/images/2017/1108/225601_8150b7d8_1522177.png "屏幕截图.png")


通过对 `plain text`、`keywords`、`comments`、`functions`、`macros`、`strings`、`numbers and operators`、`background`、`selection`、`current line`等颜色的设置来配色。为了避免出现色差，使用RGB的方式来设定颜色。对上图中的元素具体解释如下：

- Text styles
  - plain text 普通文本
  - keywords 关键字
  - comments 注释
  - functions 函数
  - macros 暂元
  - strings 字符串
  - numbers 数字
  - operators 运算符
- Display items
  - background 背景（用于区别深色、浅色主题）
  - selection 选中区域
  - current line 当前行

![输入图片说明](https://i.loli.net/2019/07/23/5d36c6ed98aa142178.png)

可以通过点击 `colors` 选项中的 Text styles 和 Display items ，来自定义颜色样式。步骤如下：

- 点击右侧区域，调出设置颜色的窗口
- 点击自定义颜色区域中的一个小正方形来存储即将设置的颜色
- 根据给出的颜色代码分别设置红绿蓝
- 点击添加到自定义颜色区域，并点击确定
- 点击OK，即配置成功，并存储下了该颜色 

![颜色配置步骤.png](https://i.loli.net/2019/07/23/5d368956f011387537.png)


```java
颜色配置说明
主题：monakai
string:(230,219,116)
comment:(117,113,94)
number and operators:(174,129,255)
keyword:(249,38,114)
function:(102,217,239)
background:(39,40,34)
selection:(73,72,62)
current line:(62,61,50)
macros:(253,151,31)
plain text:(white)

主题：Coffee flavor
background:(227,221,197)
selection:(234,238,240)
comment:(188,148,88)
keyword:(204,120,51)
string:(39,143,137)
number and operators:(165,194,97)
function:(218,73,57)
current line:(255,255,255)
plain text:(black)
macros:(163,21,21)

主题：spring
background:(240,248,255)
selection:(153,197,255)
comment:(255,140,0)
keyword:(0,128,0)
string:(220,20,60)
number and operators:(46,139,87)
function:(124,124,124)
current line:(174,208,255)
plain text:(black)
macros:(65,105,225)

主题：one dark
background:(40,44,52)
selection:(61,67,80)
comment:(92,99,112)
keywords:(198,120,221)
string:(152,195,121)
numbers and operators:(209,154,102)
function:(97,175,239)
current line:(76,87,103)
plain text:(171,178,191)
macros:(224,108,117)
```

设定好配色方案后，可以按如下方式保存之：在 Stata 主界面中，依次点击 **Edit** &rarr; **Preference** &rarr; **Save Preference Set**，填入容易记忆和区分的名字即可。在不同的配色方案之间切换时，可以在 Stata 主界面中，依次点击  **Edit** &rarr; **Preference** &rarr; **Load Preference Set**，继而选择你中意的配色方案名称。

### 选择合适的字体

在 Stata dofile 编辑器中，目前体验最好的是 **Courier New** 字体，主要是因为这种字体等宽呈现。


## 参考资料

- [让代码看起来更舒服](https://www.cnblogs.com/xiaoshatian/archive/2009/11/20/1606440.html)
- [为革命保护视力 --- 给 Visual Studio 换颜色 - stg609 - 博客园](https://www.cnblogs.com/stg609/p/3723968.html)，这一篇写的很细致，也提供了颜色代码方案。采用颜色代码可以准确控制配色方案。
- [这个配色方案也不错](https://bbs.csdn.net/topics/391940967)
- [一些 Matlab 中的配色方案 - 黑色背景](https://www.cnblogs.com/mat-wu/p/7419746.html)
- [灰色系列配色方案](https://www.bbsmax.com/A/kPzOO7kezx/)
- [TmTheme网站中Monokai方案颜色展示](https://tmtheme-editor.herokuapp.com/#!/editor/theme/Monokai)
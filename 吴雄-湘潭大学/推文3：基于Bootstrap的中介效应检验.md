> 任务：在翻译 [Stata code for performing the Preacher and Hayes bootstrapped test of mediation](http://ederosia.byu.edu/blog/Eric_DeRosia/using-stata-to-perform-the-preacher-and-hayes-1994-bootstrapped-test-of-mediation/)
的基础上，加上对中介效应的介绍。

具体而言：

1. 介绍中介效应检验方法（可以参考上述推文中介绍的文献）；
2. 插入上述推文中的内容；
3. 结合作者提供的命令，找一个实例进行介绍（数据要选用 Stata 网站提供的数据，可以使用 `sysuse` 或 `webuse` 命令直接掉入，以便读者可以演练）。
4. 搜索一下，使用 `findit` 命令，看看是否有相似功能的命令，一并在推文中进行介绍。
**如何解决工具变量回归中的弱工具变量问题---以AER论文中的弱工具变量为例**
## 1 工具变量的相关概念
### 1.1 内生性问题的定义与其产生的原因
- 遗漏变量 (Omitted Variable Bias)
- 反向因果 (Reverse Causality)
- 衡量偏误 (Measurement Error)

### 1.2 工具变量的定义与用途
- 工具变量模型: 2SLS、 GMM
- 同方差性与异方差性 (Homoskedastic and Nonhomoskedastic Cases)
- 控制变量的设置 (Control Variables)

### 1.3 工具变量的相关检验及统计量
- Hausman Test
- First-stage Cragg and Donald test
- Over-identification test
- Under-identification
- F统计量
- Wald 或 t 统计量
- Anderson-Rubin (AR) 置信区间

## 2 什么是弱工具变量问题
### 2.1 异方差情形下的弱工具变量问题
### 2.2 *AER* 论文中的弱工具变量举例
### 2.3 弱工具变量带来的估计问题

## 3 弱工具变量的检验方法
### 3.1 同方差标准误的弱工具变量检验
### 3.2 异方差标准误的弱工具变量检验
### 3.3 以第一阶段 (First-Stage) 回归的F统计量为筛选依据

## 4 弱工具变量的推理
### 4.1 对于恰好识别 (Just-identified) 的模型进行 Anderson-Rubin 检验 
### 4.1 对于过度识别 (Overidentified ) 模型的弱工具变量处理
- 同方差情形
- 异方差情形

## 5 文献中尚未解决的问题

### 参考文献

- [Wei Jiang. "Have Instrumental Variables Brought Us Closer to the Truth" *The Review of Corporate Finance Studies*, Volume 6, Issue 2, September 2017, Pages 127–140](https://quqi.gblhgk.com/s/3829824/IErPgh0EboszqIY2)
- [Angrist, Joshua, D., and Alan B. Krueger. 2001. "Instrumental Variables and the Search for Identification: From Supply and Demand to Natural Experiments." *Journal of Economic Perspectives*, 15 (4): 69-85.](https://quqi.gblhgk.com/s/3829824/DDGkd47XFGIz2dUX)
- [Andrews I, Stock JH, Sun L. Weak Instruments in IV Regression: Theory and Practice. *Annual Review of Economics*. Forthcoming.](https://quqi.gblhgk.com/s/3829824/2FzSawHqWHDLPSoR)
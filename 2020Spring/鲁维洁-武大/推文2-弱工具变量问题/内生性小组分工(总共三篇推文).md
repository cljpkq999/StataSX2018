## 杨柳分工(解决"**是什么**"问题)

## 1 工具变量的相关概念
### 1.1 内生性问题的定义与其产生的原因
- 遗漏变量 (Omitted Variable Bias)
- 反向因果 (Reverse Causality)
- 衡量偏误 (Measurement Error)

### 1.2 IV 的定义与用途
这一部分请一句话叙述 IV 的定义与主要应对何种内生性问题，建议参考《统计学的世界》第 15 章类似“母女 BMI ”的例子通俗易懂地解释(参考书已放置底部链接)。

## 2 IV 估计法的基本思想与 IV 估计式
### 3 2SLS
#### 3.1 2SLS 原理
#### 3.2 2SLS 在 Stata 中的命令与举例
### 4. 小结：使用 2SLS 的分析步骤

### 参考文献
- [《统计学的世界》第15章](https://quqi.gblhgk.com/s/3829824/TltgnHdRSegBtv2l)
- [Andrews I, Stock JH, Sun L. Weak Instruments in IV Regression: Theory and Practice. *Annual Review of Economics*. Forthcoming.](https://quqi.gblhgk.com/s/3829824/2FzSawHqWHDLPSoR)

## 鲁维洁分工(解决“为什么”问题)

### 1 工具变量的相关检验及统计量
- Hausman Test
- First-stage Cragg and Donald test
- Over-identification test
- Under-identification
- F统计量
- Wald 或 t 统计量
- Anderson-Rubin (AR) 置信区间

### 2 什么是弱工具变量问题
#### 2.1 异方差情形下的弱工具变量问题
#### 2.2 *AER* 论文中的弱工具变量举例
这部分应该是推文读者阅读的兴趣核心点 借用表格的形式
#### 2.3 弱工具变量带来的估计问题

#### 参考文献

- [Wei Jiang. "Have Instrumental Variables Brought Us Closer to the Truth" *The Review of Corporate Finance Studies*, Volume 6, Issue 2, September 2017, Pages 127–140](https://quqi.gblhgk.com/s/3829824/IErPgh0EboszqIY2)
- [Angrist, Joshua, D., and Alan B. Krueger. 2001. "Instrumental Variables and the Search for Identification: From Supply and Demand to Natural Experiments." *Journal of Economic Perspectives*, 15 (4): 69-85.](https://quqi.gblhgk.com/s/3829824/DDGkd47XFGIz2dUX)


## 沙莎分工(解决“怎么办”问题)
建议主要参考连老师发在主教群里的论文Section 4, 5 & 6 (已将链接放在参考文献)；当然也可以参考以前你做的笔记

### 1 弱工具变量的检验方法
#### 1.1 同方差标准误的弱工具变量检验
#### 1.2 异方差标准误的弱工具变量检验
#### 1.3 以第一阶段 (First-Stage) 回归的F统计量为筛选依据

### 2 弱工具变量的推理
#### 2.1 对于恰好识别 (Just-identified) 的模型进行 Anderson-Rubin 检验 
#### 2.2 对于过度识别 (Overidentified) 模型的弱工具变量处理
- 同方差情形
- 异方差情形

### 3 文献中尚未解决的问题

### 主要参考文献
- [Andrews I, Stock JH, Sun L. Weak Instruments in IV Regression: Theory and Practice. *Annual Review of Economics*. Forthcoming.](https://quqi.gblhgk.com/s/3829824/2FzSawHqWHDLPSoR)
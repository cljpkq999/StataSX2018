> **Version**: `2020/1/22 11:51`

&emsp;


> 作者：鲁维洁(武汉大学) || 何美玲 (西南财经大学) || 连玉君 (中山大学)
>    
> Stata 连享会： [知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn)

> Stata连享会 &emsp; [计量专题](https://gitee.com/arlionn/Course/blob/master/README.md)  || [精品课程](https://mp.weixin.qq.com/s/hWtncj56PeFNL4yg2-va0Q) || [简书推文](https://www.jianshu.com/p/de82fdc2c18a)  || [公众号合集](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw)

[![点击查看完整推文列表](https://file.lianxh.cn/images/20191111/3ed0c73d48c0046f04c502458b4e1c0b.png)](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw)


&emsp;

> #### [2020连享会-文本分析与爬虫-现场班](http://mp.weixin.qq.com/s?__biz=MzAwMzk4ODUzOQ==&mid=2247486963&idx=1&sn=ed6e2b77be6977bcb723f12bebaa3e26&chksm=9b3380a7ac4409b186fa06a2133d3799e8697893caef53fc0f07c204b2f0d850d671d340d9a5&scene=21#wechat_redirect)    
> (西安, 3月26-29日，司继春-游万海 主讲；&ensp; 内附助教招聘)

![连享会-文本分析与爬虫专题班，西北工业大学，2020.3.26-29](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会2020.3文本分析海报.png "连享会-文本分析现场班，西北工业大学，2020.3.26-29")



&emsp;

[[toc]]

&emsp;

## 1. 短期事件研究法概览

### 1.1 短期事件研究法的定义
金融领域的研究学者常使用事件研究法 (Event Study) 的分析框架，研究某一特定事件发生对公司股票价格或收益率的影响，并以此检验金融市场对新信息披露的反应程度。按照事件影响持续时间的长短，文献中通常将事件研究法分为短期事件研究与长期事件研究 (Brown and Warner,1980; Fama,1991) 。在公司金融领域，短期事件研究法 (Dailey Event Study) 为衡量某一事件对公司股东财富的影响提供了一个良好的度量指标，即累计异常收益率 (Cumulative Abnormal Returns, CARs) 。
### 1.2 短期事件研究法的理论框架
短期事件研究法依赖的三个基本假设：首先，根据有效市场假说 (Efficient Markets Hypothesis, EMH) , 金融市场是有效的, 即股票价格反映所有已知的公共信息；其次，所研究的事件是市场未预期到的，因此这种异常收益可以度量股价对事件发生或信息披露异常反应的程度；第三，在事件发生的窗口期间无其他事件的混合效应。
### 1.3 短期事件研究法的应用范围
短期事件研究法主要关注事件公布日周围数天内的公告效应，这为投资者理解公司的分红配股、兼并收购等决策提供了相关证据 (e.g., Edmans, 2011; Deng et al., 2013) ; 此外，短期事件研究作为检验市场效率的一种方法，在资本市场研究中也起着重要的作用。除了金融经济学，短期事件研究也被广泛运用于其它相关领域，例如在会计学领域的文献中，公司的发布盈余公告对其股票价格的影响备受关注；在法律和经济领域中，短期事件研究被用于度量监管政策 (如中组部18号文的出台的出台对企业政治关联的影响) 的效果，以及评估法律责任案件中的损害赔偿等 (如D&O保险) 。

## 2 短期事件研究法简介
### 2.1 短期事件研究法的相关概念
短期事件研究法的主要概念如下：
- **事件日 (Event date)：** 特定事件发生的日期 ($t=0$) 。
- **事件窗口期 (Event window) ：** 对事件涉及到的股票价格进行考察的时间段 ($[t_1,t_2]$ ， 通常 $ t_1<0$ 且 $ t_2>0$ ) 。例如，如果某课题是研究企业兼并收购 (M&A) 对其股票价格的影响时，事件日期就是企业发布并购公告的当天，事件窗口从发布并购公告的当日开始，依据研究目的包含公告发布的前一天和公告发布的后一天。有时候也会包含事件发生的前后数日。
- **估计窗口 (Estimation window) ：** 估计窗口一般选择为事件发生前一段时间，通常是事件发生前210个交易日至前11个交易日  $[-210,-10]$ 。估计窗口与事件窗口不可有交集。
- **正常收益 (Normal Returns) ：** 为了考察特定事件对公司股价的影响，我们首先需要知道，如果没有该事件发生，股票收益率应该是多少，这也就是我们需要估计正常收益率的原因。
- **异常收益率 (Abnormal Returns, ARs)：** 每只股票实际收益率与正常收益率的差值，异常收益率能够反映该事件的经济影响。
- **累积异常收益率 (Cumulative abnormal returns，CARs)** : 每只股票在事件窗口内异常收益率的简单加总，最常见的是 $ CAR(-1,+1) $ 和 $ CAR(-2,+2) $ ，正负的天数可根据研究需要进行相应调整。  

![事件研究法事件窗口界定示意图](https://images.gitee.com/uploads/images/2019/1122/155031_cc2ee1e7_1522177.png)

### 2.2 短期事件研究法的估计模型 

如果市场是有效的、某一事件是意料之外的，并且该事件的发生与市场中某些公司的价值相关，那么通过将事后公司股票的实际收益 (ARs) 减去统计模型估计的正常收益率便能得到股票的异常收益率。依据 MacKinlay (1997) 的估计步骤，在进行异常收益率估计之前，首先需要定义事件发生的窗口期。通常，包括事件发生日期当天与事件发生日的前后数天。接着定义估计窗口 ，并选择估计模型计算预期收益率。以市场模型  (Market Model) 估计预期收益率为例，异常收益率 (AR) 可表示为：

$$
AR_{i,t} = R_{i,t}-(\alpha_i+\beta_iR_{m,t}) $$
其中 $R_{i,t}$ 表示股票的实际收益率, $(\alpha_i+\beta_iR_{m,t})$ 表示由超额收益 $\alpha_i$、市场性风险 $\beta_i$ 以及市场收益率 $R_{m,t}$ 决定的预期收益率。包括市场模型在内，预期收益率可由以下五种估计模型中进行估计：
- 市场模型 (Market Model) : $ R_{i,t}=\alpha_i+\beta_iR_{m,t}+\epsilon_{i,t}$
- 市场调整模型 (Market-adjused Model) : $R_{i,t}=R_{m,t}+\epsilon_{i,t}$
- Fama三因素模型 (Fama-French Three Factor Model) : $ R_{i,t}-R_{f,t}=\alpha_i+\beta_1[R_{m,t}-R_{f,t}]+\beta_2SMB_t+\beta_3(HML_t)+\epsilon_{i,t} $
- Carhart四因素模型 (Fama-French Plus Momentum) :  $ R_{i,t}-R_{f,t}=\alpha_i+\beta_1[R_{m,t}-R_{f,t}]+\beta_2SMB_t+\beta_3(HML_t)+\beta_4(MOM_t)+\epsilon_{i,t} $
- 历史平均模型 (HMM) ，即事件窗口期的正常收益率使用估计窗口的期望值代替:
$$
E(R_{i,t}|X_t)=\mu_i
$$


### 2.3 短期事件研究法的局限性
1. 证券市场可能是无效率的，或者属于非半强式有效市场。
2. 短期事件的公告信息与公司价值是无关的。
3. 事件信息已经被完全预期。
4. 由于内幕交易 (Insider trading) 的存在，信息只会影响普通投资者而不会影响内部知情人士。
5. 难于精确识别事件日期。 
6. 估计正常收益率的模型存在设定偏误。

&emsp;
> #### [连享会计量方法专题……](https://gitee.com/arlionn/Course/blob/master/README.md)，<https://gitee.com/arlionn/Course>

## 3. 短期事件研究法的估计步骤
### 3.1 定义事件与事件窗口
在公司金融的研究文献中，通常可以将事件分为以下三种：
1. 有关公司活动的公告，包括资产出售公告、兼并收购更以及独立董事突然死亡等;
2. 有关宏观经济事件的公告，例如各国政府的破产和信用评级变化、中央银行的公告、或者与黄金和石油等重要商品有关的新闻。
3. 来自政府监管与政策层面的公告，例如公司董事会女性配额的提议或某项法律的意外通过。

如2.1定义所述，**事件窗口期** 为包含事件日的一段时间。为检测该事件是否被预期或泄密，通常事件窗口不仅包含事件发生后的一段时间，也会包含事件发生前几天。

### 3.2 数据准备：合并事件数据与股票数据
定义好事件发生的日期与事件窗口期后，我们需要进行短期事件研究的数据准备，一般包括公司个股与股票市场的历史收益率数据 (Stock return data) 以及事件发生的日期数据 (Event dates)。如果每个公司的一组股票收益率数据可以匹配到单个事件日期，那么短期事件研究将会简单得多。然而，在许多情况下，一个公司在研究样本期间可能匹配到多次事件。在这种情况下，我们需要研究与同一公司有关的所有事件对其股票收益率的影响，即需要为每个“单个公司股票收益率-事件发生日期”的配对组合创建一组重复的股票收益率数据。简言之，在进行短期事件研究之前，我们需要理清每个公司在样本期间内究竟有几次事件发生。

### 3.3 模型估计正常收益率与计算异常收益率
如前所述，我们可以使用四种估计模型计算正常收益率，以最常见的市场模型 (Market model) 为例，$ R_{i,t}$ 为股票 $i$ 在第 $t$ 天的收益率，$R_{m,t}$ 是市场在第 $t$ 天的收益率，分别针对每一只股票执行如下最小二乘法 OLS 回归： 


$$
R_{i,t}=\alpha_i+\beta_iR_{m,t}+\epsilon_{i,t}
$$
得到估计系数 $\hat{\alpha}_{i}$ 和 $\hat{\beta}_{i}$ 后，可使用以下公式计算异常收益率：

$$
AR_{i,t}=R_{i,t}- ({\hat{\alpha}_{i} + \hat{\beta}_{i} R_{m,t}})
$$

在使用市场调整模型 (Market-adjusted model) 估计正常收益率时，模型设定为 $ \alpha_i=0 $ 与 $ \beta_i=1 $ 。同理，使用其它两种估计模型计算正常收益率的方法也相似，只不过在 OLS 回归的时候将单个股票收益率与市场收益率同时减去无风险利率 $R_{f,t}$ 分别作为因变量和主要的自变量进行估计，并加入更多定价因子 $SMB_t$ 和 $HML_t$ 以及 $MOM_t$ 作控制变量，不同频率 (日度数据、周度数据以及月度数据) 的无风险利率以及定价因子数据都可以在 [Fama-French的数据图书馆](http://mba.tuck.dartmouth.edu/pages/faculty/ken.french/data_library.html) 免费下载。

&emsp;

### 3.4 计算异常收益率并加总异常收益率(CAR)
$AR_{i,t}$ 计算的是股票 $i$ 在第 $t$ 天的异常收益率，为了研究事件对整体证券定价的影响，还需要计算平均异常收益率 $AAR_{i,t}$ 和累积异常收益率 $CAR_{i,t}$ 。通常而言，平均异常收益率是针对某一时点、对所有公司的异常收益率求平均，计算方式如下所示：


$$
AAR_{i,t} = \frac{1}{N}\sum_i^N {AR_{i,t}}
$$
其中，$t$ 为事件窗口期的某时间，$N$ 为公司个数。
累积异常收益率是计算公司 $i$ 在 $t_1$ 到 $t_2$ 某一段时期内，平均异常收益率的总和，计算方式如如下所示：

$$
CAR_i(t_1,t_2) =\sum_{t=t_1}^{t_2}{AR_{i,t}}
$$
在此基础上，计算平均累积异常收益率，也即在特定时点 $t$ ,对所有公司的累计异常收益率求平均，计算方式如下所示：

$$
CAAR_i(t_1,t_2) =\sum_{t=t_1}^{t_2} {AAR_{i,t}}
$$
以上公式中，$t_1$ 和 $t_2$ 是之前已经定义好的事件窗口的左右两端。
另外，在 Kothari and Warner (2007) 的文章中，还介绍了另一种资产组合法计算异常收益率，即将所有证券按一定权重组合，按照整体的资产组合计算异常收益率。

### 3.5 检验CAR的显著性
计算出累积异常收益率之后，最后需要检验每只股票的累积异常收益是否在统计上异于零，以便判断事件的发生是否对股价产生了显著的影响。

&emsp;
> #### [连享会计量方法专题……](https://gitee.com/arlionn/Course/blob/master/README.md)，<https://gitee.com/arlionn/Course>

## 4. 短期事件研究法的Stata命令之 `estudy ` 应用
### 4.1 `estudy `的简介与基本语法
`estudy` 是由 LIUC Università Carlo Cattaneo 的三位作者贡献的 Stata 外部命令，它作为一个集成的事件研究法估计程序，简洁清晰，方便使用，主要用于分析已知发生日期的某一特定事件或公告消息对公司股价的影响。但是，也因为 `estudy` 程序的执行简单方便，导致了它只能分析**单一确定事件**对公司股价的影响而**无法对不同时间段发生的多次事件** (如一年中公司发布两次甚至两次以上的并购公告) 进行分析。在运用 `estudy` 命令进行短期事件研究前，可通过在 Stata 命令对话框输入 `findit estudy` 查找到同名安装包与示例数据 `data_estudy.dta` 进行命令安装与数据试运行， `estudy ` 命令的基本语法如下所示：
```Stata 
estudy varlist1 [(varlist2) ... (varlistN)], datevar(varname)  ///
       evdate(date) dateformat(string) lb1(#) ub1(#) ///
	   [options]
```
`estudy ` 命令的简单解释：

- `varlist1 [ (varlist2) ... (varlistN) ]` ：每个varlist中填入的是样本内某一公司的变量存储名，变量中存放的是该公司的历史股票收益率时间序列数据。 `estudy` 将区分不同的 varlist (公司变量名) ,分别汇报累积异常收益率 (CAR) 与平均累积异常收益率 (CAAR) 。
- `datevar` : 定义日期变量 (date) ，设为时间格式。
- `evdate` : 定义事件发生的日期，如07092015。
- `dateformat` : 定义 evdate 中 “年月日” 的格式，比如 dateformat (MDY) 则表示事件的日期格式按照月份(M)、日(D)、年(Y)的顺序进行排列。
- `lb1(#)` 和 `ub1(#)` 分别表示事件窗口期的起点 $t_1$ 和终点 $t_2$ 。例如，设定 `lb1(-3)` 与 `ub1(2)` 代表，从事件发生前三天起，到事件发生后两天止是事件窗口期，即计算 $CAR(-3,+2)$ , $CARR(-3,+2)$。可根据研究目的设置多个时间窗口期，通过改变事件窗口的起点和终点 `lb2(#)` 和 `ub2(#)` 来完成设置。

其他 `options` 设置： 
- `eswlbound(#)` : 设定估计窗口的起始日期，如有缺省，则自动设定第一个交易日为估计窗口的起始日期。
- `eswubound(#)` : 设定估计窗口的截止日期，如有缺省，则自动设定事件发生日前一个月的30日为估计窗口的截止日期。
- `modtype` ：设置估计股票正常收益率的估计模型。其中， `modtype(SIM)` 表示使用市场模型， `modtype(MAM)` 表示使用市场调整模型， `modtype(MFM)` 表示多因素模型， `modtype(HMM)` 表示历史平均模型。
- `indexlist(varlist)` : 用于存放用于估计股票正常收益率的各因子。
- `diagnosticsstat(string)` : 设定检验显著性的方法。可选择的有参数法 `diagnosticsstat(Norm)` 、 `diagnosticsstat(Patell)` 、 `diagnosticsstat(ADJPatell)` 、 `diagnosticsstat(BMP)` 、 `diagnosticsstat(KP)` 、 `diagnosticsstat(KP)` 以及非参数法 `diagnosticsstat(Wilcoxon)` 、`diagnosticsstat(GRANK)`。显著性检验方法具体说明可在Stata命令窗口输入 `help estudy` 了解更多。
- `estudy`还有多个设置结果输出的选项，例如 `suppress(string)` 、 `decimal(#)` 等。suppress(group)表示报告每家公司的CAR, suppress(ind)表示报告整体CAAR。decimal可设置保留的小数点精确位数。

### 4.2 `estudy `命令实战
在确认给 Stata 13.0 及以上版本的 Stata 安装好 `estudy` 安装包与示例数据 `data_estudy.dta` 之后，我们首先通过 `cd` 命令将当前工作路径所在的文件夹设置为保存示例数据的文件夹以方便调用 (当然，也可以通过菜单操作找到数据存放的文件夹直接打开数据) 。示例数据 `data_estudy.dta` 内储存的是 IBM 与可口可乐等公司的日度股票时间序列数据，并假设**有且仅有一确定事件**于2015年7月9日发生，研究这一事件对所有样本内公司股票收益率的影响。接下来，利用 `estudy` 命令，我们可以方便地按照以下步骤进行短期事件研究分析：
1. 打开数据
```stata
*-下载数据  https://gitee.com/arlionn/data/tree/master/data01
copy "https://gitee.com/arlionn/data/raw/master/data01/data_estudy.dta"  "data_estudy.dta"
use "data_estudy.dta", clear 
```
2. 利用市场模型估计正常收益率，并以 $(-1,+1)$ 以及 $(-3,+3)$ 为事件窗口期，同时计算多家公司两个事件窗口期的累积异常收益率与平均累积异常收益率，精确到小数点后四位数，可运行以下语句：
```stata 
estudy boa ford boeing (apple netflix amazon facebook google),  ///
       datevar(date) evdate(07092015) dateformat(MDY)  ///
	   lb1(-1) ub1(1) lb2(-3) ub2(3) ///
	   indexlist(mkt) decimal(4)
```
结果输出如下：
```
By default the upper bound of the estimation window has been set to (-30)
Event date: 09jul2015, with 2 event windows specified, under the Normality assumption
SECURITY                      CAAR[-1,1]       CAAR[-3,3]
Bank of America Corporation     0.3647%         -1.1494%
Ford Motor Company             -2.1413%         -1.8529%
The Boeing Company              0.9844%          3.4777%
Ptf CARs n 1 (3 securities)    -0.2641%          0.1585%
CAAR group 1  (3 securities)   -0.2641%          0.1585%
-------------------------------------------------------------------------
Apple Inc                      -1.9051%         -2.1204%
Netflix Inc                     2.8084%          2.9968%
Amazon com Inc                  1.7837%          4.1678%
Facebook Inc                    0.8082%          0.0043%
Alphabet Inc                    1.1752%          5.3336%*
Ptf CARs n 2 (5 securities)     0.9341%          2.0764%
CAAR group 2  (5 securities)    0.9341%          2.0764%
-------------------------------------------------------------------------
*** p-value < .01, ** p-value <.05, * p-value <.1
```

3. 利用Fama三因子模型估计正常收益率，并以 $(-1,+1)$ 、 $(-3,+3)$ 、 $(-1,0)$ 以及 $(0,+3)$ 为事件窗口期，同时计算多家公司四个时间窗口期的累积异常收益率与平均累积异常收益率，并使用 Kolari 和 Pynnonen (2010) 的方法检验显著性，可运行以下语句：

```stata 
estudy boa ford boeing (apple netflix amazon facebook google) (boa ford boeing apple netflix amazon facebook google), datevar(date) evdate(07092015) dateformat(MDY) lb1(-1) ub1(1) lb2(-3) ub2(3) lb3(-1) ub3(0) lb4(0) ub4(3) modtype(MFM) indexlist(mkt smb hml) diagnosticsstat(KP)
```
结果输出如下：
```
By default the upper bound of the estimation window has been set to (-30)
Event date: 09jul2015, with 4 event windows specified, using the Boehmer, Musumeci, Poulsen test, with the Kolari and Pynnonen adjustment
SECURITY                      CAAR[-1,1]       CAAR[-3,3]       CAAR[-1,0]       CAAR[0,3]
Bank of America Corporation     0.95%            1.01%            0.41%            2.67%
Ford Motor Company             -2.06%           -1.31%           -1.87%           -0.13%
The Boeing Company              1.00%            3.40%            1.07%            0.87%
Ptf CARs n 1 (3 securities)    -0.04%            1.04%           -0.13%            1.14%
CAAR group 1  (3 securities)   -0.04%            1.04%           -0.13%            1.14%
-------------------------------------------------------------
Apple Inc                      -2.11%           -2.93%           -3.50%*          -0.51%
Netflix Inc                     2.30%            2.07%            2.81%            1.91%
Amazon com Inc                  1.37%            2.97%            1.28%            3.40%
Facebook Inc                   -0.07%           -2.04%           -0.01%           -0.93%
Alphabet Inc                    0.91%            4.53%            0.68%            4.53%**
Ptf CARs n 2 (5 securities)     0.48%            0.92%            0.25%            1.68%
CAAR group 2  (5 securities)    0.48%            0.92%            0.25%            1.68%
-------------------------------------------------------------
Bank of America Corporation     0.95%            1.01%            0.41%            2.67%
Ford Motor Company             -2.06%           -1.31%           -1.87%           -0.13%
The Boeing Company              1.00%            3.40%            1.07%            0.87%
Apple Inc                      -2.11%           -2.93%           -3.50%*          -0.51%
Netflix Inc                     2.30%            2.07%            2.81%            1.91%
Amazon com Inc                  1.37%            2.97%            1.28%            3.40%
Facebook Inc                   -0.07%           -2.04%           -0.01%           -0.93%
Alphabet Inc                    0.91%            4.53%            0.68%            4.53%**
Ptf CARs n 3 (8 securities)     0.29%            0.96%            0.11%            1.47%
CAAR group 3  (8 securities)    0.29%            0.96%            0.11%            1.47%*
-------------------------------------------------------------
*** p-value < .01, ** p-value <.05, * p-value <.1
```
4. 利用历史均值模型估计正常收益率，并以 $(-1,+1)$ 、 $(-3,+3)$ 以及 $(-1,0)$ 为事件窗口期，同时计算多家公司三个时间窗口期的累积异常收益率与平均累积异常收益率，并使用 Wilcoxon (1945) 的正负秩检验方法检验显著性。输出表将显示p值，而不是显着性星号，可运行以下语句：

```stata
estudy boa ford boeing (apple netflix amazon facebook google) (boa ford boeing apple netflix amazon facebook google), datevar(date) evdate(09072015) dateformat(DMY) lb1(-1) ub1(1) lb2(-3) ub2(3) lb3(-1) ub3(0) modtype(HMM) diagnosticsstat(Wilcoxon) showpvalues nostar
```

结果输出如下：

```
By default the upper bound of the estimation window has been set to (-30)
Event date: 09jul2015, with 3 event windows specified, using the Generalised SIGN test by Wilcoxon
SECURITY                      CAAR[-1,1]       CAAR[-3,3]       CAAR[-1,0]
Bank of America Corporation    -0.12%            0.16%           -1.39%
                                (0.9609)         (0.9662)         (0.4940)  
Ford Motor Company             -2.61%           -0.60%           -3.62%
                                (0.2819)         (0.8718)         (0.0670)  
The Boeing Company              0.61%            4.50%           -0.46%
                                (0.7865)         (0.1904)         (0.8026)  
Ptf CARs n 1 (3 securities)    -0.71%            1.35%           -1.82%
                                (0.6986)         (0.6289)         (0.2215)  
CAAR group 1  (3 securities)   -0.71%            1.35%           -1.82%
                                (0.0000)         (0.0000)         (0.1088)  
---------------------------------------------------------------------------------------
Apple Inc                      -2.21%           -1.30%           -4.76%
                                (0.4272)         (0.7609)         (0.0362)  
Netflix Inc                     2.33%            4.29%            1.09%
                                (0.6769)         (0.6163)         (0.8112)  
Amazon com Inc                  1.27%            5.54%           -0.71%
                                (0.7082)         (0.2879)         (0.7969)  
Facebook Inc                    0.28%            1.42%           -1.91%
                                (0.9450)         (0.8219)         (0.5680)  
Alphabet Inc                    0.76%            6.46%           -0.97%
                                (0.7451)         (0.0701)         (0.6089)  
Ptf CARs n 2 (5 securities)     0.49%            3.28%           -1.45%
                                (0.8318)         (0.3495)         (0.4364)  
CAAR group 2  (5 securities)    0.49%            3.28%           -1.45%
                                (0.0000)         (0.0000)         (0.0000)  
---------------------------------------------------------------------------------------
Bank of America Corporation    -0.12%            0.16%           -1.39%
                                (0.9609)         (0.9662)         (0.4940)  
Ford Motor Company             -2.61%           -0.60%           -3.62%
                                (0.2819)         (0.8718)         (0.0670)  
The Boeing Company              0.61%            4.50%           -0.46%
                                (0.7865)         (0.1904)         (0.8026)  
Apple Inc                      -2.21%           -1.30%           -4.76%
                                (0.4272)         (0.7609)         (0.0362)  
Netflix Inc                     2.33%            4.29%            1.09%
                                (0.6769)         (0.6163)         (0.8112)  
Amazon com Inc                  1.27%            5.54%           -0.71%
                                (0.7082)         (0.2879)         (0.7969)  
Facebook Inc                    0.28%            1.42%           -1.91%
                                (0.9450)         (0.8219)         (0.5680)  
Alphabet Inc                    0.76%            6.46%           -0.97%
                                (0.7451)         (0.0701)         (0.6089)  
Ptf CARs n 3 (8 securities)     0.04%            2.56%           -1.59%
                                (0.9834)         (0.3705)         (0.2954)  
CAAR group 3  (8 securities)    0.04%            2.56%           -1.59%
                                (0.0000)         (0.0000)         (0.0000)  
---------------------------------------------------------------------------------------
p-values in parentheses
```

5. 利用市场模型估计正常收益率，并以 $(-1,+1)$ 以及 $(-3,+3)$ 为事件窗口期，同时计算多家公司两个事件窗口期的累积异常收益率与平均累积异常收益率，将结果存储在 `my_output_tables.xlsx` 和 `my_ar_dataset.dta` 文件中，可运行以下语句：
```stata 
estudy boa ford boeing (apple netflix amazon facebook google), ///
       datevar(date) evdate(07092015) dateformat(MDY)          ///
	   lb1(-1) ub1(1) lb2(-3) ub2(3)  ///
	   indexlist(mkt) outputfile(my_output_tables)
```
结果输出如下：
```
By default the upper bound of the estimation window has been set to (-30)
Event date: 09jul2015, with 2 event windows specified, under the Normality assumption
SECURITY                      CAAR[-1,1]       CAAR[-3,3]
Bank of America Corporation     0.36%           -1.15%
Ford Motor Company             -2.14%           -1.85%
The Boeing Company              0.98%            3.48%
Ptf CARs n 1 (3 securities)    -0.26%            0.16%
CAAR group 1  (3 securities)   -0.26%            0.16%
-------------------------------------------------------------------------
Apple Inc                      -1.91%           -2.12%
Netflix Inc                     2.81%            3.00%
Amazon com Inc                  1.78%            4.17%
Facebook Inc                    0.81%            0.00%
Alphabet Inc                    1.18%            5.33%*
Ptf CARs n 2 (5 securities)     0.93%            2.08%
CAAR group 2  (5 securities)    0.93%            2.08%
-------------------------------------------------------------------------
*** p-value < .01, ** p-value <.05, * p-value <.1
```
```stata 
estudy boa ford boeing (apple netflix amazon facebook google),  ///
       datevar(date) evdate(07092015) dateformat(MDY)  ///
	   lb1(-1) ub1(1) lb2(-3) ub2(3) ///
	   indexlist(mkt) mydataset(my_ar_dataset)
```

结果输出如下：

```stata
By default the upper bound of the estimation window has been set to (-30)
Event date: 09jul2015, with 2 event windows specified, under the Normality assumption
SECURITY                      CAAR[-1,1]       CAAR[-3,3]
Bank of America Corporation     0.36%           -1.15%
Ford Motor Company             -2.14%           -1.85%
The Boeing Company              0.98%            3.48%
Ptf CARs n 1 (3 securities)    -0.26%            0.16%
CAAR group 1  (3 securities)   -0.26%            0.16%
-------------------------------------------------------------------------
Apple Inc                      -1.91%           -2.12%
Netflix Inc                     2.81%            3.00%
Amazon com Inc                  1.78%            4.17%
Facebook Inc                    0.81%            0.00%
Alphabet Inc                    1.18%            5.33%*
Ptf CARs n 2 (5 securities)     0.93%            2.08%
CAAR group 2  (5 securities)    0.93%            2.08%
-------------------------------------------------------------------------
*** p-value < .01, ** p-value <.05, * p-value <.1
(note: file my_ar_dataset.dta not found)
file my_ar_dataset.dta saved
```

运行以上命令之后，Stata会展示每个不同的事件窗口期的累积异常收益率与平均累积异常收益率的值及其显著性，通过正负符号及显著性分析，我们可以判断某一特定事件对不同公司价值的影响。

&emsp;
> #### [连享会计量方法专题……](https://gitee.com/arlionn/Course/blob/master/README.md)，<https://gitee.com/arlionn/Course>


## 5. 短期事件研究法的Stata命令之 `eventstudy2` 应用

### 5.1 `eventstudy2`的简介与基本语法

`eventstudy2` 是由 Thomas Kaspereit (2019) 贡献的可用于进行事件研究的 Stata 外部命令，允许用户使用包括市场模型、 Fama 三因子模型等在内的估计模型。相对于 `estudy` 命令而言， `eventstudy2` 命令不仅让用户自由选择估计窗口和事件窗口的长度，而且能够同时计算超过10个事件窗口的累积(平均)异常收益率 CARs 和 CCARs ，并描绘图形。简言之，`eventstudy2` 命令是 Stata 执行事件研究中处理复杂的统计数据的程序。 `eventstudy2` 命令 的基本语法如下: 

```stata
eventstudy2 security_id date using security_returns_file,  
     returns(security_returns) 
    [model(abnormal_return_model) 
	 marketfile(file_with_market/factor_returns) 
	 marketreturns(market_returns) 
	 idmarket(market_id) 
	 factor1(factor_return1) ... factor12(factor_return12) 
	 riskfreerate(risk_free_rate) 
	 prices(prices) 
	 tradingvolume(trading_volume) 
	 evwlb(event_window_lower_boundary) 
	 evwub(event_window_upper_boundary) 
	 eswlb(estimation_window_lower_boundary) 
	 eswub(estimation_window_upper_boundary) 
	 minevw(minimum_observations_event_window) 
	 minesw(minimum_observations_estimation_window) 
	 aarfile(output_average_abn_ret_file) 
	 carfile(output_cum_average_abn_ret_file) 
	 arfile(output_abn_ret_file) 
	 crossfile(output_cross_sec_file) 
	 diagnosticsfile(output_diag_file) 
	 graphfile(output_graph_file) 
	 replace 
	 logreturns 
	 thin(thin_trading_threshold) 
	 fill nokolari delweekend 
	 datelinethreshold(dateline_threshold) 
	 shift(maximum_event_date_shift) 
	 garch archoption(arch_option) 
	 garchoption(garch_option) 
	 architerate(iterations) 
	 parallel pclusters(clusters) 
	 processors(processors_StataMP) 
	 prapath(path_eventstudy2_parallel) 
	 arfillevent arfillestimation 
	 car1LB(CAR_window_1_lower_boundary) car1UB(CAR_window_1_upper_boundary) 
	 car2LB(CAR_window_2_lower_boundary) car2UB(CAR_window_2_upper_boundary) 
	 ... ...
	 car10LB(CAR_window_10_lower_boundary) car10UB(CAR_window_10_upper_boundary)]
``` 

其中， 
- `security_id` 是数据中识别每只股票或每家公司的 id 代码，以美国 WRDs-Compustat/CRSP 为例， permno 和 GVKEY 通常是每只股票或每家公司的识别代码。 `date` 是事件数据中每件事件发生的日期。
- `security_returns_file` 是包含存储在工作目录中的所估计股票收益率的返回值(参见选项 `returns(security_returns)` )， `security_returns_file` 还必须包含变量 `security_id` 和` date` ， 而这里 `date` 指的是股票收益率观察值返回的日期，而不是事件日期。
- `returns(security_returns)` 用于指定 `security_returns_file` 中表示事件研究法中与所发生事件可能相关的公司股票收益率的变量名称。
- ` model(abnormal_return_model) ` 用于指定估计正常收益率的估计模型并计算异常收益率，估计模型包括市场模型 (MA) 、 Fama (FM) 三因子模型等。
- `marketfile(file_with_market/factor_returns)` 用于指定用于存放股票收益率的股票数据。
- `marketreturns(market_returns)` 用于指定存放市场收益率的数据。
- `idmarket(market_id)` 用于识别在内存中存储的事件列表(或者股票收益率文件)。
- `factor1(factor_return1)` 到 `factor12(factor_return12)` 是 Fama三因子模型中各种因子的名称，如 $ HML_t $ 与 $ MOM_t $ 。
- `riskfreerate(risk_free_rate)` 用于识别数据中无风险利率的存储名称。 
- ` prices(prices) ` 在文件 `file_with_market/factor_returns` 中指定股票价格的变量名称。
- `tradingvolume(trading_volume)` 在文件中存放所估计股票的交易量。
- `evwlb(event_window_lower_boundary)` 与 `evwub(event_window_upper_boundary)` 分别表示事件窗口的下限与上限， Stata 默认分别等于 -20 与 20 。
- `eswlb(estimation_window_lower_boundary)` 与 `eswub(estimation_window_upper_boundary)` 分别表示估计窗口的下限与上限， Stata 默认分别等于 -270 与 -20 。
- `minevw(minimum_observations_event_window)` 与 `minesw(minimum_observations_estimation_window) ` 分别表示事件窗口与估计窗口的最少样本数量。在 Stata 中前者的默认设置为 1， 即事件窗口中至少放置 1 天用于估计；后者对股票收益率数据的长度有要求，如果股票数据观测值的天数不够，那么该股票的收益率历史数据将不参与事件研究的模型估计，默认设置为 30 天。
- `aarfile(output_average_abn_ret_file)` 用于指定存放历史平均异常收益率的文件名称。
- `carfile(output_cum_average_abn_ret_file)` 用于指定存放累积异常收益率的文件名称。
- `arfile(output_abn_ret_file)` 用于指定存放异常收益率的文件名称。
- `crossfile(output_cross_sec_file)` 用于指定保存用于横截面分析的累积异常收益率dta文件的文件名。
- `diagnosticsfile(output_diag_file)` 用于存放统计显著性的文件。
- `graphfile(output_graph_file)` 用于指定存放异常收益率图形的位置。
- `logreturns` 用于表明数据中的股票收益率及市场收益率是否是连续复利。
- `thin_trading_threshold` 用于指定将股票收益率观察值分类为 “Arising from Thin Trading” (针对交易量少的股票命名) 的分界值（阈值）。
- `fill` 用于指定对于缺失的股票收益率历史数据是按照缺失值还是按照 0 代替缺失值进行处理。
- `car1LB(CAR_window_1_lower_boundary)` 至 `car10LB(CAR_window_10_lower_boundary)` 用于指定计算异常收益率估计窗口的下限值，可以同时指定 10 个，默认值是 -20 。
- `car1UB(CAR_window_1_upper_boundary)` 至 ` car10UB(CAR_window_10_upper_boundary)` 用于指定计算异常收益率的估计窗口的上限值，也可以同时指定 10 个，默认值是 20 。

其它命令部分的详情请参看 `help eventstudy2` 。

&emsp;

### 5.2 `eventstudy2`命令实战

在确认给 Stata 安装好 `eventstudy2` 、 `moremata` 、 `nearmrg` 、 `distinct` 、 `_gprod` 、 `rmse` 和 `parallel` 安装包 ( `ssc install eventstudy2` 与 `ssc install moremata/nearmrg/distinct/_gprod/rmse/parallel`) 与示例数据 `Earnings_surprises.dta` 之后，我们首先通过 `cd` 命令将当前工作路径所在的文件夹设置为保存示例数据的文件夹以方便调用 (当然，也可以通过菜单操作找到数据存放的文件夹直接打开数据) 。示例数据 `Earnings_surprises.dta` 内储存的分别是 `Date Earnings_surprise` 、`Market_reference` 及 `Security_id` 三个变量。接下来，利用 `eventstudy2` 命令，我们可以方便地按照以下步骤进行短期事件研究分析：

1. 打开数据
```
*-下载数据  https://gitee.com/arlionn/data/tree/master/data01
copy "https://gitee.com/arlionn/data/raw/master/data01/earnings_surprises.dta" earnings_surprises.dta

use "Earnings_surprises.dta", clear 
```

2. 使用市场调整模型估计股票异常收益率和并使用默认参数的进行基本示例事件研究
```
eventstudy2 Security_id Date using Security_returns  ///
            if Earnings_surprise>0.05, returns(Return)
```

结果输出如下：

```
Generating dateline ...
...succeeded
Preparation of event list ...
...succeeded
Preparation of security return data...
...succeeded
Merging event dates and stock market data...
...succeeded
Calculating abnormal returns...
1 out of 39 events completed.
2 out of 39 events completed.
(output omitted)
38 out of 39 events completed.
39 out of 39 events completed.
...succeeded
Assessing statistical significance of abnormal returns...
...succeeded
Diagnosing events that are excluded from the analysis...
...succeeded
-----------------------------------------------------
      name:  <unnamed>
       log:  E:\draft\diagnosticsfile.smcl
  log type:  smcl
 opened on:  23 Dec 2019, 15:55:16


Number of events in the event file: 45
-- thereof: Number of events for which security identifiers and event dates are available: 45
-- thereof: Number of events for which event dates are in the range of dates in the security file: 45
-- thereof: Number of events in the analysis (not deleted because of any insufficient data in the estimation or event period): 39

List of security identifiers for which no security market data was available: 101 102 103 104

ANALYSIS OF ESTIMATION PERIOD

Number of events with insufficient security return data: 1

ANALYSIS OF EVENT PERIOD

Number of events with insufficient security return data: 0
Events for which the IPO (deletion) date of the event firm is later (earlier) than the first (last) day of the event window: 1

      name:  <unnamed>
       log:  E:\draft\diagnosticsfile.smcl
  log type:  smcl
 closed on:  23 Dec 2019, 15:55:16
-----------------------------------------------------
-----------------------------------------------------
      name:  <unnamed>
       log:  E:\draft\diagnosticsfile.smcl
  log type:  smcl
 opened on:  23 Dec 2019, 15:55:16

      t   NoFirms         AAR   t_test   CDA   Patell   PatellADJ   Boehmer   Kolari   Corrado   Zivney   GenSign   Wilcox  
    -20        38     .004729                                                                                  **        *  
    -19        39    .0029191                                                                                               
    -18        36   -.0023795                                                                                               
    -17        36    .0049581                                             *                                                 
    -16        37    .0019446                                                                                               
    -15        38   -.0020521                                                                                               
    -14        39    .0066838                                                                                               
    -13        37   -.0059705                                                                                               
    -12        36    .0101652                                                                                               
    -11        36    .0002356                                                                                               
    -10        38   -.0054185                                                                                               
     -9        36   -.0025805                                                                                               
     -8        36    .0045793                                                                                               
     -7        35   -.0100787                                                                                               
     -6        37   -.0019002                                                                                               
     -5        37    .0071357                                                                                               
     -4        38    .0000734                                                                                               
     -3        37     .008936                                                                                               
     -2        38   -.0109795                                                               **       **       ***       **  
     -1        38     .012602                                            **                 **       **       ***       **  
      0        39     .025763      ***   ***      ***         ***       ***       **        **       **         *       **  
      1        39   -.0095898                                                                                               
      2        36   -.0033064                                                                                               
      3        37   -.0040544                                                                                               
      4        37   -.0083442                                                                                               
      5        39     .001478                                                                                               
      6        38   -.0091507                                                                                               
      7        38    .0034253                                                                                               
      8        37   -.0024559                                                                                               
      9        36    .0053815                                                                                               
     10        38   -.0003621                                                                                   *           
     11        39    .0017001                                                                                   *           
     12        39    .0045353                                                                                               
     13        38    .0018392                                                                                               
     14        38    .0018035                                                                                               
     15        37    .0027843                                                                                               
     16        37    .0107235                                                                                               
     17        38   -.0145819        *     *                                                 *        *                  *  
     18        39    .0078064                                                                         *       ***       **  
     19        36   -.0002348                                                                                               
     20        39    .0011819                                                                                               
      name:  <unnamed>
       log:  E:\draft\diagnosticsfile.smcl
  log type:  smcl
 closed on:  23 Dec 2019, 15:55:16
-----------------------------------------------------
-----------------------------------------------------
      name:  <unnamed>
       log:  E:\draft\diagnosticsfile.smcl
  log type:  smcl
 opened on:  23 Dec 2019, 15:55:16

           t   NoFirms       CAAR   t_test   CDA   Patell   PatellADJ   Boehmer   Kolari   Corrado_Cowan   Zivney_Cowan   GenSign   GRANKT   Wilcox  
    [-20;20]        37   .0313972                      **           *                                                         ***      ***       **  
    [-20;20]        37   .0313972                      **           *                                                         ***      ***       **  
    [-20;20]        37   .0313972                      **           *                                                         ***      ***       **  
    [-20;20]        37   .0313972                      **           *                                                         ***      ***       **  
    [-20;20]        37   .0313972                      **           *                                                         ***      ***       **  
    [-20;20]        37   .0313972                      **           *                                                         ***      ***       **  
    [-20;20]        37   .0313972                      **           *                                                         ***      ***       **  
    [-20;20]        37   .0313972                      **           *                                                         ***      ***       **  
    [-20;20]        37   .0313972                      **           *                                                         ***      ***       **  
    [-20;20]        37   .0313972                      **           *                                                         ***      ***       **  
      name:  <unnamed>
       log:  E:\draft\diagnosticsfile.smcl
  log type:  smcl
 closed on:  23 Dec 2019, 15:55:16
-----------------------------------------------------
-----------------------------------------------------
      name:  <unnamed>
       log:  E:\draft\diagnosticsfile.smcl
  log type:  smcl
 opened on:  23 Dec 2019, 15:55:16
The following result files are available in the evenstata directory and are loaded into memory by clicking.
Graph of cumulative average abnormal returns:  graphfile
Average abnormal returns (daily basis):  aarfile
Cumulative average abnormal returns:  carfile
Abnormal returns:  arfile
Cumulative abnormal returns for cross-sectional analyses:  crossfile
Diagnostic of events that are excluded:  diagnosticsfile
Logfile:  diagnosticsfile
      name:  <unnamed>
       log:  E:\draft\diagnosticsfile.smcl
  log type:  smcl
 closed on:  23 Dec 2019, 15:55:16
-----------------------------------------------------
```
eventstudy2不仅完成了累积超额收益的计算与显著性检验，并且将相关数据自动储存在当前读取的文件夹里，可直接点击 "graphfile" 、" aarfile" 等阅览相应文件。

![graphfile 文件示意图](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/20200122110239.png)

3. 使用Fama三因子模型估计股票异常收益率进行示例事件研究 
```
eventstudy2 Security_id Date using Security_returns if Ea>0.05,  ///
   ret(Return) car1LB(-1) car1UB(1) mod(FM) ///
   marketfile(Factor_returns) mar(MKT)      ///
   idmar(Market_reference)   ///
   factor1(SMB) factor2(HML) ///
   risk(risk_free_rate)
```

结果输出如下：
```
Generating dateline ...
...succeeded
Preparation of event list ...
...succeeded
Preparation of security return data...
...succeeded
Preparation of market and/or factor return data...
...succeeded
Merging event dates and stock market data...
...succeeded
Calculating abnormal returns...
1 out of 38 events completed.
2 out of 38 events completed.
(output omitted)
37 out of 38 events completed.
38 out of 38 events completed.
...succeeded
Assessing statistical significance of abnormal returns...
...succeeded
Diagnosing events that are excluded from the analysis...
...succeeded
------------------------------------------------
      name:  <unnamed>
       log:  E:\draft\diagnosticsfile.smcl
  log type:  smcl
 opened on:  23 Dec 2019, 16:29:49


Number of events in the event file: 45
-- thereof: Number of events for which security identifiers and event dates are available: 45
-- thereof: Number of events for which event dates are in the range of dates in the security file: 44
-- thereof: Number of events in the analysis (not deleted because of any insufficient data in the estimation or event period): 38

List of security identifiers for which no security market data was available: 102 103 104

ANALYSIS OF ESTIMATION PERIOD

Number of events with insufficient security return data: 2
Number of events with insufficient market index/factor return data: 2

ANALYSIS OF EVENT PERIOD

Number of events with insufficient security return data: 0
Number of events with insufficient market/index factor return data: 0
Events for which the IPO (deletion) date of the event firm is later (earlier) than the first (last) day of the event window: 1

      name:  <unnamed>
       log:  E:\draft\diagnosticsfile.smcl
  log type:  smcl
 closed on:  23 Dec 2019, 16:29:49
---------------------------------------
---------------------------------------
      name:  <unnamed>
       log:  E:\draft\diagnosticsfile.smcl
  log type:  smcl
 opened on:  23 Dec 2019, 16:29:49

      t   NoFirms         AAR   t_test   CDA   Patell   PatellADJ   Boehmer   Kolari   Corrado   Zivney   GenSign   Wilcox  
    -20        37    .0049708                                                                                               
    -19        38    .0030984                                                                                               
    -18        35   -.0038899                                                                                               
    -17        35    -.000733                                                                                               
    -16        36   -.0003419                                                                                               
    -15        37      .00481                                                                         *        **           
    -14        38    .0043478                                                                                               
    -13        36   -.0074897                                             *                  *       **         *        *  
    -12        35    .0062511                                                                                               
    -11        35    .0013786                                                                                               
    -10        37   -.0112272                                             *        *                                        
     -9        35     .002375                                                                                               
     -8        35    .0068386                       *                                                                       
     -7        34   -.0116341                                                                                               
     -6        36    -.003284                                                                                               
     -5        36    .0092179                                                                                               
     -4        37    .0008256                                                                                               
     -3        36     .006481                                                                                               
     -2        37   -.0035439                                                                                               
     -1        37    .0076635                                                                                               
      0        38    .0293003      ***   ***      ***         ***       ***      ***       ***      ***       ***      ***  
      1        38   -.0035208                                                                                               
      2        36   -.0005523                                                                                               
      3        36   -.0063949                                                                                               
      4        36   -.0054967                                                                                               
      5        38   -.0005514                                                                                               
      6        37   -.0020611                                                                                               
      7        37   -.0024591                                                                                               
      8        36   -.0018874                                                                                               
      9        35   -.0034669                                                                                               
     10        37   -.0040193                                                                         *                     
     11        38   -.0010819                                                                                               
     12        38    .0036728                                                                                               
     13        37   -.0023843                                                                                               
     14        37    .0008085                                                                                               
     15        36    .0022357                                                                                               
     16        36   -.0015717                                                                                               
     17        37   -.0131729        *     *        *                    **       **         *       **         *      ***  
     18        38   -.0011146                                                                                               
     19        35   -.0051176                                                                                               
     20        38    -.004766                                                                                               
      name:  <unnamed>
       log:  E:\draft\diagnosticsfile.smcl
  log type:  smcl
 closed on:  23 Dec 2019, 16:29:49
-----------------------------------------------------
-----------------------------------------------------
      name:  <unnamed>
       log:  E:\draft\diagnosticsfile.smcl
  log type:  smcl
 opened on:  23 Dec 2019, 16:29:49

           t   NoFirms        CAAR   t_test   CDA   Patell   PatellADJ   Boehmer   Kolari   Corrado_Cowan   Zivney_Cowan   GenSign   GRANKT   Wilcox  
      [-1;1]        37    .0360421      ***   ***      ***         ***       ***       **             ***            ***        **      ***       **  
    [-20;20]        36   -.0114423                                                                                                                    
    [-20;20]        36   -.0114423                                                                                                                    
    [-20;20]        36   -.0114423                                                                                                                    
    [-20;20]        36   -.0114423                                                                                                                    
    [-20;20]        36   -.0114423                                                                                                                    
    [-20;20]        36   -.0114423                                                                                                                    
    [-20;20]        36   -.0114423                                                                                                                    
    [-20;20]        36   -.0114423                                                                                                                    
    [-20;20]        36   -.0114423                                                                                                                    
      name:  <unnamed>
       log:  E:\draft\diagnosticsfile.smcl
  log type:  smcl
 closed on:  23 Dec 2019, 16:29:49
-----------------------------------------------------
-----------------------------------------------------
      name:  <unnamed>
       log:  E:\draft\diagnosticsfile.smcl
  log type:  smcl
 opened on:  23 Dec 2019, 16:29:49
The following result files are available in the evenstata directory and are loaded into memory by clicking.
Graph of cumulative average abnormal returns:  graphfile
Average abnormal returns (daily basis):  aarfile
Cumulative average abnormal returns:  carfile
Abnormal returns:  arfile
Cumulative abnormal returns for cross-sectional analyses:  crossfile
Diagnostic of events that are excluded:  diagnosticsfile
Logfile:  diagnosticsfile
      name:  <unnamed>
       log:  E:\draft\diagnosticsfile.smcl
  log type:  smcl
 closed on:  23 Dec 2019, 16:29:49
```

&emsp;

## 6. 相关资料链接

### 事件研究法原理参考资料

- [事件研究法概览 - eventstudymetrics.com](https://eventstusdymetrics.com/index.php/event-studies/)
- [Eventus-Guide-8-Public.pdf](http://www.eventstudy.com/Eventus-Guide-8-Public.pdf) Eventus 软件的说明书，附录部分可参考
- [Event Studies: Assessing the Market Impact of Corporate Policy (PDF)](http://www1.american.edu/academic.depts/ksb/finance_realestate/rhauswald/fin673/673mat/Hauswald%20%282002%29,%20Event%20Studies%20Note%20%28FIN-02-005%29.pdf)
- [Lecture Notes on Event Study Analysis Jin-Lung Lin (PDF)](http://faculty.ndhu.edu.tw/~jlin/files/EventStudy.pdf)
- [经典论文 - MacKinlay (1997), Event Studies in Economics and Finance.pdf](http://www1.american.edu/academic.depts/ksb/finance_realestate/rhauswald/fin673/673mat/MacKinlay%20%281997%29,%20Event%20Studies%20in%20Economics%20and%20Finance.pdf)
- [参数和非参数检验，正常回报的估计等 eventstudymetrics.com](https://eventstudymetrics.com/index.php/event-study-methodology/)
- [MIT Event Study Webpage](http://web.mit.edu/doncram/www/eventstudy.html) 提供了大量有关 Event Study 的参考文献和数据来源说明

### 短期事件研究法资源
- 沃顿商学院数据库（付费数据库，需购买）帮你一键操作 Event Study Tool by WRDS [(U.S Daily event study)](https://wrds-www.wharton.upenn.edu/) 
- 一网打尽事件研究法 [(EventStudyTools)](https://www.eventstudytools.com/)
- 参数和非参数检验，正常回报的估计等 [(eventstudymetrics.com)](https://eventstudymetrics.com/index.php/event-study-methodology/)
- [MIT Event Study Webpage](http://web.mit.edu/doncram/www/eventstudy.html) 提供了大量有关 Event Study 的参考文献和数据来源说明


### Stata 短期事件范例
- [Princeton Stata 范例](http://dss.princeton.edu/online_help/stats_packages/stata/eventstudy.html) 解读了 Event Study 中最关键的编程问题
- [StackOverFlow `atuo.dta` 模拟 Stata 范例](https://stackoverflow.com/questions/41622943/stata-event-study-graph-code)
- [StackOverFlow Event Study 问题汇总](https://stackoverflow.com/search?q=Event+Study)
- [GitHub - Stata Event Study](https://github.com/arlionn/Stata-Event_Study) 提供了一些自己编写的命令(Fama-Macbech 1973，Rolling beta 等)

### Stata短期事件外部命令
使用这些命令，可以一次性完成基本的 Event Study 估计和检验工作，非常便捷。  
需要事先用 `ssc install cmdname, replace` 下载最新版；亦可使用 `findit` 命令搜索相应的命令，以便查看完整的 Stata 范例数据和 dofile。 

- `help eventstudy` //基本命令
- `help eventstudy2` //基本命令
- `help st0532_1`  // Stata Journal 19-2，只能指定单一的事件日期，但可以分析特定公司的事件效果


&emsp;


## 7. 参考文献
- 陈汉文，陈向民 (2002). "证券价格的事件性反应——方法、背景和基于中国证券市场的应用" 经济研究 1: 40-47.[[PDF]](https://quqi.gblhgk.com/s/3829824/T24VKRAlcu9PoooV)
- 王永钦，刘思远，杜巨澜 (2014). "信任品市场的竞争效应与传染效应:理论和基于中国食品行业的事件研究" 经济研究 2: 141-154. [[PDF]](https://quqi.gblhgk.com/s/3829824/ls6bAD7pgtiXiQwL)
- 金宇超，靳庆鲁，严青蕾 (2018). "合谋与胁迫:作为经济主体的媒体行为——基于新闻敲诈曝光的事件研究" 管理科学学报 21(3): 1-22. [[PDF]](https://quqi.gblhgk.com/s/3829824/iQduhjxmPLTZYGaU)
- Brown, S., and J. Warner (1980) . "Measuring security price performance." Journal of Financial Economics 8: 205-258.. [[PDF]](https://quqi.gblhgk.com/s/3829824/84xPtiOppX6veVEN)
- Fama, E., (1991) . "Efficient capital markets: II." Journal of Finance 46: 1575-1617.. [[PDF]](https://quqi.gblhgk.com/s/3829824/jNdwiDEboKkWWMus)
- Edmans, A. (2011) . "Does the stock market fully value intangibles? Employee satisfaction and equity prices." Journal of Financial Economics 101(3): 621-640.. [[PDF]](https://quqi.gblhgk.com/s/3829824/PCpc9ea0StrzUvyl)
- Deng, X., et al. (2013) . "Corporate social responsibility and stakeholder value maximization: Evidence from mergers." Journal of Financial Economics 110(1): 87-109.. [[PDF]](https://quqi.gblhgk.com/s/3829824/VdCCglVVNqo0G3tl)
- Kothari, S. P., and J. B. Warner. 2007. Econometrics of event studies. In Handbook of Empirical Corporate Finance, vol. 1, ed. B. E. Eckbo, 3–36. Amsterdam: Elsevier.. [[PDF]](https://quqi.gblhgk.com/s/3829824/34Rn338etJFMgxeD)



&emsp;

>#### 关于我们
- **Stata连享会** 由中山大学连玉君老师团队创办，定期分享实证分析经验。
- **欢迎赐稿：** 欢迎赐稿至StataChina@163.com。录用稿件达 **三篇** 以上，即可 **免费** 获得一期 Stata 现场培训资格。
- **往期精彩推文：**
 [Stata绘图](https://mp.weixin.qq.com/s/xao8knOk0ulGfNc7vasfew) | [时间序列+面板数据](https://mp.weixin.qq.com/s/8yP1Dijylgreg59QIkqnMg) | [Stata资源](https://mp.weixin.qq.com/s/Kdeoi5uJyNtwwwptdQDQDQ) | [数据处理+程序](https://mp.weixin.qq.com/s/_3DQacFyy7juRjgFedp9WQ) |  [回归分析-交乘项-内生性](https://mp.weixin.qq.com/s/61qJNWnL4KRp0fbLxuDGww)
---
![欢迎加入Stata连享会(公众号: StataChina)](https://file.lianxh.cn/images/20191111/ec83ed2baf9c93494e4f71c9b0f5d766.png)






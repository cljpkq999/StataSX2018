use https://stats.idre.ucla.edu/stat/data/hsbdemo, clear

*数据结构描述
tab prog ses, chi2
table prog, con(mean write sd write) //展示在prog的各类别中write变量的均值和标准误

*多元线性回归
label list
mlogit prog i.ses write, base(2) //以第二类academic type作为基准组 列示出的是log ratio对应的系数

mlogit, rrr //  列示出ratio对应的系数

*假设检验
*Wald Test
test 2.ses 3.ses // 分别检验社会地位分类2和社会地位分类3对被解释变量的影响是否显著
test [general]3.ses = [vocation]3.ses // 组间差异检验 社会地位对组别general和组别vocation的差异是否显著

*LR检验
qui mlogit prog i.ses write, base(2)
est store mFull
qui mlogit prog write, base(2)
est store m0
lrtest mFull m0

*概率预测ses
margins ses, atmeans predict(outcome(1))
margins ses, atmeans predict(outcome(2))
margins ses, atmeans predict(outcome(3))

*绘图1
margins ses, atmeans predict(outcome(1))
marginsplot, name(general) 
margins ses, atmeans predict(outcome(2))
marginsplot, name(academic) 
margins ses, atmeans predict(outcome(3))
marginsplot, name(vocational) 
graph combine general academic vocational, ycommon

*概率预测write
margins, at(write = (30(10) 70)) predict(outcome(1)) vsquish
margins, at(write = (30(10) 70)) predict(outcome(2)) vsquish
margins, at(write = (30(10) 70)) predict(outcome(3)) vsquish


*绘图2
predict p1 p2 p3
sort write
twoway (line p1 write if ses ==1) (line p1 write if ses==2) (line p1 write if ses ==3), ///
	legend(order(1 "ses = 1" 2 "ses = 2" 3 "ses = 3") ring(0) position(7) row(1)) name(one)
twoway (line p2 write if ses ==1) (line p2 write if ses==2) (line p2 write if ses ==3), ///
        legend(order(1 "ses = 1" 2 "ses = 2" 3 "ses = 3") ring(0) position(7) row(1)) name(two)
twoway (line p3 write if ses ==1) (line p3 write if ses==2) (line p3 write if ses ==3), ///
	legend(order(1 "ses = 1" 2 "ses = 2" 3 "ses = 3") ring(0) position(7) row(1)) name(three)
graph combine one two three, ycommon

*拟合优度统计量
fitstat

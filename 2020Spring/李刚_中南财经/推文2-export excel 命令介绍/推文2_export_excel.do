* 2.2 介绍 firstrow 和 sheet 的使用
sysuse auto, clear
*设定Excel文件的Sheet名
export excel using “auto.xls", replace sheet("auto")
//结果：注意观察 Excel 文件的 Sheet 名

*导出Excel的表头为变量名
preserve 
keep make price mpg rep78
export excel using "auto_varname.xls",firstrow(variable) replace
restore
/*结果：auto_varname.xls 的前三行
make	price	mpg	rep78
AMC Concord	4,099	22	3
AMC Pacer	4,749	17	3
*/

*设定导出Excel的表头为变量名
preserve 
keep make price mpg rep78
export excel using "auto_varlabel.xls", firstrow(varlabel) replace
restore

/*结果：auto_varlabel.xls 的前三行
Make and Model	Price	Mileage (mpg)	Repair Record 1978
AMC Concord	4,099	22	3
AMC Pacer	4,749	17	3

*/

* ## 3.2 案例二
sysuse nlsw88, clear

*获取变量名
preserve
export excel using "nlsw88_varname.xls" in 1,firstrow(variable) replace
import excel using  "nlsw88_varname.xls",clear
keep in 1
save "nlsw88_varname.dta", replace
restore

*获取变量标签
export excel using "nlsw88_varlab.xls" in 1, firstrow(varlabels) replace
import excel using  "nlsw88_varlab.xls",clear
keep in 1
save "nlsw88_varlab.dta", replace

*合并、转置
use "nlsw88_varname.dta", clear
append using "nlsw88_varlab.dta"
sxpose, clear
rename _var1 varname
rename _var2 varlabel
list varname  varlab, noobs
export excel using "nlsw88_varname_varlab.xls",firstrow(variable) replace
/*
+-----------------------------------------+
  |          name                  varlabel |
  |-----------------------------------------|
  |        idcode                    NLS id |
  |           age       age in current year |
  |          race                      race |
  |       married                   married |
  | never_married             never married |
  |-----------------------------------------|
  |         grade   current grade completed |
  |      collgrad          college graduate |
  |         south            lives in south |
  |          smsa             lives in SMSA |
  |        c_city     lives in central city |
  |-----------------------------------------|
  |      industry                  industry |
  |    occupation                occupation |
  |         union              union worker |
  |          wage               hourly wage |
  |         hours        usual hours worked |
  |-----------------------------------------|
  |       ttl_exp     total work experience |
  |        tenure        job tenure (years) |
  +-----------------------------------------+
*/


 
* 3.2.1 小彩蛋
sysuse nlsw88, clear
describe, replace
des
list name varlab, noobs
keep name varlab
export excel using "nlsw88_varname_varlab.xls",firstrow(variable) replace
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/-front-命令：连享会文章封面自动生成.png)

# 1. 用途

`front` 命令用于快捷生成连享会公众号、码云、简书和知乎等平台的文章封面图。支持自定义标题、Logo 和网址链接，以及随机插入短语等。

安装时请注意：将 `front.ado` 和 `frontQuotes.txt` 放置在 `ado/plus/f` 路径下。其中，`frontQuotes.txt` 为图片中插入的短语，可以自行收录添加。

# 2. 语法

```Stata
front platform, title(string) ///
[logo(string) url(string) quotes(string) ///
bc(string) path(string)]
```

注意：`platform` 请选择 `lxh`, `gitee`, `zh` 或 `js` 其中一种，分别对应连享会 、码云、知乎和简书。选择 `platform` 的好处在于可以自动对应到相应平台的 `url` 。

# 3. 选项

- `platform`: `lxh`, `gitee`, `zh` 或 `js` 其中一种。指定平台后，`url()` 会自对应相应平台的主页网址。
- `titile()`: 为排版美观，title 字符限定在 1-59 字符之间。若超出 59 字符，会报错并提示当前字符长度和建议缩减字符数。
- `logo()`：默认为 连享会 ，可修改。
- `url()`：用于设置默认与所选 `platform` 对应，可修改。
- `quotes()`：默认从初始化的文档中随机选取励志名言，可以使用 `quotes()` 选项自行设置。
- `bc()`：用于调节图片底色，默认为 墨绿色 (**g**reen) 还有 深蓝色(**b**lue) 和 褐色(**y**ellow) 可选。若需更换，只用在 `bc()` 中填写相应颜色的首字母。
- `path()`：设置图片保存路径，默认为当前路径，可修改。导出图片的名称为标题 `title` 。

# 4. 示例

## 4.1 默认用法

```Stata
front lxh, title(公众号封面)
front gitee, title(码云封面) logo(码云-连享会)
front zh,title(知乎封面) logo(知乎-连享会)
```

![默认用法-公众号封面](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/公众号封面.png)
![默认用法-码云封面](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/码云封面.png)
![默认用法-知乎封面](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/知乎封面.png)

## 4.2 title 字符数过多

```Stata
. front lxh, title(凑字数凑字数凑字数凑字数凑字数凑字数凑字数凑字数凑字数凑字数凑字数凑字数凑字数)

* 当前标题 117 字符，建议再精简 58 个字符哦
* r(198);
```

## 4.3 修改 logo 和 url

```Stata
front lxh, title(测试一下) ///
logo(猜猜我是谁) url(www.youseesee.com)
```

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/测试一下.png)

## 4.4 修改短语

```Stata
front lxh, title(更换名言警句) ///
quotes(好好学习，天天向上) bc(b)
```

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/更换短语.png)

## 4.5 修改底色

```Stata
front lxh, title(修改底色) logo(褐色) bc(y)
```

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/修改底色.png)

## 4.6 修改图片保存地址

```Stata
front lxh, title(更换保存地址) ///
logo(User) url(www.noidea.com) bc(y) ///
path(C:\Users\...)
```

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/更换保存地址.png)
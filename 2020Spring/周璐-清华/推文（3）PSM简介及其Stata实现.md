# 倾向得分匹配(PSM)简介与Stata实现
> 周璐，清华大学，zhouluthu@126.com
## 1.PSM原理简介
倾向得分匹配（Propensity Score Matching, PSM）是一种统计校正方法，用于更准确地评估政策实施效果或药物治疗效果。在随机实验中，当样本容量较大时可能可以实现对效果的无偏估计；但实际的观察性研究中，实验组和对照组的选择往往不是随机的。那么，我们如何得知效果是来源于政策/药物等干预措施，还是实验组本身的特质呢？

倾向得分匹配法的思路是：根据 logit 回归获得的预测因素，使用预测的群体成员概率(如该样本属于实验组还是对照组)，来从非实验组中选择部分数据，创造一个反事实（counterfactual）的对照组。举个通俗的例子：我们希望判断小张攻读在职硕士学位是否对他的薪水增长有帮助。于是我们获得一批未攻读在职硕士学位的员工样本，根据他们的工作绩效、职位类型、入职年份、毕业院校、性别等因素，预测这些员工加薪的概率。然后选择这些员工样本中与小张加薪概率最相近但没有读在职硕士的小李，与小张匹配。当我们对实验组中的所有“小张”都匹配了“小李”之后，就完成了对照组的创造，可以分析自变量在职硕士学位对因变量薪水的影响。

常用的匹配方法包括最近邻匹配、k近邻匹配、核函数匹配、半径匹配、局部线性回归等，读者可以根据自己的研究需要进行选择。不同方法的 Stata 命令语法详见相关命令的 help 文件。对数理推导过程感兴趣的读者也可以阅读参考文献【1】中相关内容。

## 2.Stata实现范例
### 2.1 数据准备
我们以经典的 NSW-PSID 数据集为例，用 Stata 实现倾向得分匹配。NSW 是美国国家支持工作示范项目（National Supported Work），实施于上世纪70年代。我们尝试观察接受该项目培训与不接受该项目培训对工资的影响。

![NSW-PSID数据集](https://images.gitee.com/uploads/images/2020/0130/194522_caf92454_5486007.png)

数据集中各变量含义如下图所示。根据研究问题，我们感兴趣的输出是 **RE78** （1978年实际工资，NSW 项目在1975和1978年之间实施），处理指示变量是 **TREAT** ，控制变量包括年龄、受教育年限、种族、民族、婚姻状况等，以及可能相关的交乘项。

![变量及其含义](https://images.gitee.com/uploads/images/2020/0130/194522_69dd42c8_5486007.png)

### 2.2 相关命令
完成数据准备后，我们还需要下载 PSM 分析所需的命令  `psmatch2`。在联网状态下，在命令窗口中输入以下代码即可完成下载。
> ssc install psmatch2

`psmatch2` 命令的格式如下，选项十分繁杂，具体细节可以参阅帮助文件。首先必选项 *depvar* 为因变量，这里即为是否被处理的指示变量 **TREAT**。*indepvars* 为自变量，这里即为模型中可能影响该样本是否被处理的控制变量如年龄、婚姻状况等。逗号后的可选项中，*outcome* 为感兴趣的结果变量，*pscore* 后指定作为倾向值得分的变量，或者不指定由程序根据自变量进行计算。默认使用 probit 模型计算，亦可标明 logit 后使用 logit 模型计算。*neighbor* 指定每个样本的匹配对象个数，默认值为1，默认计算方法为最近邻匹配。*ate*表示除报告实验组的平均处理效应 ATT 外，还要报告总体样本平均处理效应 ATE 和未处理组平均处理效应 ATU。

还有一些可选项为指定其他的匹配值计算方法。*radius caliper* 用于确定半径匹配时的最大半径。*mahalanobis* 用于马氏度量匹配，*kernel* 用于核函数方法，*spline* 用于样条平滑匹配。读者可以根据自己的需要选用这些方法。

```  
psmatch2 depvar [indepvars] [if exp] [in range] 
[, outcome(varlist) pscore(varname) neighbor(integer) radius caliper(real) 
mahalanobis(varlist) ai(integer) kernel llr kerneltype(type) bwidth(real) spline 
nknots(integer) common trim(real) noreplacement descending odds 
index logit ties quietly w(matrix) ate]
```

### 2.3 匹配过程
首先，我们需要判断：哪些因素会影响某样本接受 NSW 项目培训呢？我们所列出的控制变量是否都对某样本属于实验组/对照组有影响呢？
于是，我们先对总体样本执行 logit 回归。
> logit TREAT AGE EDUC BLACK HISP MARR RE74 RE75 NODEGREE

从下图中可以看出，控制变量中，教育年限 **EDUC** 和学位 **NODEGREE** 对 处理指示变量 **TREATMENT** 的影响不显著，可以考虑从模型中去除。
![logit回归结果](https://images.gitee.com/uploads/images/2020/0130/194522_b99df8f5_5486007.png)

接下来我们尝试进行倾向得分匹配。
>psmatch2 TREAT AGE BLACK HISP MARR RE74 RE75,outcome(RE78) neighbor(1) common caliper(.05) ties

我们这里采用的是比较简单的一种写法，没有增加太多选项。首先 **TREAT** 是区分实验组与对照组的指示变量， **AGE**等6个变量是显著影响 **TREAT** 的控制变量，逗号之后为可选选项。在可选选项中，*outcome(RE78)* 表示感兴趣的结果变量为 **RE78** ，*neighbor(1)* 表示匹配对象个数为1个（如果需要实验组:对照组=1:2，可以把参数改成*neighbor(2)*），*common* 表示去除实验组中的极端值（大于对照组最大值或小于对照组最小值），*caliper(.05)* 表示实验组与匹配的对照组样本最大倾向得分距离为0.05， *ties* 表示不止一个最佳匹配值时需要同时记录。

匹配结果如下图所示。我们可以观察数据表中新增的若干个变量：**_pscore** 表示各样本的倾向得分值，**_treated** 标记该样本是实验组/对照组，**_id** 是自动生成的样本唯一 id ，按照倾向值得分 **_pscore** 排序， **_n1** 是该样本被匹配到的对象的 **_id** （1对多匹配时，可以有多个匹配对象存储在 **_n2**、**_n3** 中），**_pdif** 表示已经匹配的一组实验组样本-对照组样本的倾向值得分之差。
![匹配后的数据表](https://images.gitee.com/uploads/images/2020/0130/194522_914b850c_5486007.png)

### 2.4 结果分析
完成匹配后，我们对匹配结果进行均衡性检验。
> pstest, both

从下图均衡性检验结果可以看出，匹配后各变量的 bias 都显著减小。匹配后实验组与对照组的**RE74**、**RE75** 仍存在显著差异，模型需要进一步优化。关于 PSM 中控制变量的选择问题可以参考连享会之前的文章 
[https://www.jianshu.com/p/cdf1ef2c664e?utm_campaign=maleskine](https://www.jianshu.com/p/cdf1ef2c664e?utm_campaign=maleskine)

![均衡性检验结果](https://images.gitee.com/uploads/images/2020/0130/194522_d705e197_5486007.png)


将 **RE74**、**RE75** 从模型中去除后再进行匹配，均衡性检验结果如下：匹配后实验组与控制组的匹配变量均没有显著差异，满足平衡性假设条件。
![均衡性检验结果（修正模型后）](https://images.gitee.com/uploads/images/2020/0130/194522_05d0d85f_5486007.png)

为了更直观地查看匹配结果，可以将其可视化。
>psgraph

![匹配结果可视化](https://images.gitee.com/uploads/images/2020/0130/194523_48d5e14e_5486007.png)

### 参考文献

【1】Becker S O, Ichino A. Estimation of average treatment effects based on propensity scores[J]. The stata journal, 2002, 2(4): 358-377.

【2】Cameron A C, Trivedi P K. Microeconometrics using stata[M]. College Station, TX: Stata press, 2009.

【3】Leuven E, Sianesi B. PSMATCH2: Stata module to perform full Mahalanobis and propensity score matching, common support graphing, and covariate imbalance testing[J]. 2003.





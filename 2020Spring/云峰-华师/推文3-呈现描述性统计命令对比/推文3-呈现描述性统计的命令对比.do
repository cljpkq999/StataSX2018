*2.计算基本统计量的命令汇总
*2.1 table
*2.1.1 命令展示与描述
*一维列表
sysuse auto,clear
table foreign, ///
  contents(mean price sd price ///
  min price max price n price) ///
  format(%9.2f) center row
 
 *二维列表
 sysuse auto,clear
table foreign rep78,      ///
  c(mean price sd price   ///
  min price max price     ///
  n price)  format(%9.2f) ///
  center row col
  
*三维列表
sysuse "nlsw88.dta",clear
table race married coll, ///
  c (mean wage) format(%9.2f)
  
*四维列表
sysuse "nlsw88.dta",clear
table race married coll,  ///
  by(union) c (mean wage) ///
  format(%9.2f)
  
*2.2 tabulate
*单频表
sysuse census
tabulate region


*双频表
webuse citytemp2
tabulate region agecat



*双向频率表

*2.3 summtab
sysuse auto,clear
summtab, by(foreign)    ///
  contvars(price mpg weight length) ///
  catvars(rep78) mean median range  ///
  total medfmt(1) mnfmt(2)          ///
  excel excelname(summart_table1) title(My Table 1) replace
  
*2.4 summarize
sysuse auto
summarize mpg weight 
 
*2.5 tabstat
*一般描述性统计
sysuse auto,clear
tabstat price weight mpg   ///
  rep78, by(foreign) stat(mean sd min max) 
  
*复杂描述性统计  
sysuse auto
tabstat price weight mpg ///
  rep78, by(foreign)     ///
  stat(mean sd min max)  ///
  nototal long col(stat)
  
*2.6 fsum
*一般表格
sysuse "nlsw88.dta",clear
fsum wage hours age      ///
  union race,cat(union) mcat(race)

*分组统计
sysuse "nlsw88.dta",clear
bysort married: fsum wage hours ///
  age union race

*2.6 baselinetable
*一维列表
```Stata
sysuse "nlsw88.dta",clear
baselinetable wage(cts) hours(cts) ///
  age(cts) union race

*二维列表
sysuse "nlsw88.dta",clear
baselinetable wage(cts) hours(cts) ///
  union race, by( married,totalcolumn)

*汇报缺漏样本
sysuse "nlsw88.dta",clear
baselinetable wage(cts) hours(cts)     ///
  union race, by( married,totalcolumn) ///
  reportmissing

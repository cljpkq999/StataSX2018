*1.描述性分析
*1.1 全部变量一般统计指标

*基本代码

outreg2 using table1.doc,replace sum(log) title(Decriptive statistics) bdec(3)

*实例

sysuse auto, clear 

outreg2 using table1.doc, replace        ///
  sum(log) title(Decriptive statistics)  ///
  bdec(3)

*1.2 部分变量一般统计指标输出

*基本代码

outreg2 using table1.doc, replace sum(log) keep(var1 var2 var3) title(Decriptive statistics) bdec(3)

*实例

sysuse auto, clear

outreg2 using table1.doc, replace        ///
  sum(log) keep(price weight foreign)    ///
  title(Decriptive statistics) bdec(3)

*1.3 全部统计指标输出

*基本代码

outreg2 using table1.doc, replace sum(detail) title(Decriptive statistics) bdec(3)

*实例

sysuse auto, clear

outreg2 using table1.doc, replace          ///
  sum(detail) title(Decriptive statistics) ///
  bdec(3)

*1.4 部分变量部分统计指标输出

*基本代码

outreg2 using table1.doc, replace sum(detail) keep(var1 var2 var3) eqkeep(N min max) title(Decriptive statistics) bdec(3)     

*实例

sysuse auto, clear

outreg2 using table1.doc, replace          ///
  sum(detail) keep(price weight foreign)   ///
  eqkeep(N min max)                        ///
  title(Decriptive statistics) bdec(3)

*1.5分组描述

*基本代码

bysort var1: outreg2 using table1.doc, replace sum(log) title(Decriptive statistics) bdec(3)

*实例

sysuse auto, clear

bysort foreign:outreg2 using               ///
  table1.doc, replace                      ///
  sum(log) title(Decriptive statistics)    ///
  bdec(3)

*1.6 频数

*基本代码

outreg2 var1 using table1.doc, replace cross title(Decriptive statistics)

*实例

sysuse auto, clear

outreg2 foreign using                      ///
  table1.doc, replace cross                ///
  title(Decriptive statistics)

*2. 回归结果输出
*2.1线性回归结果输出

*基本代码

outreg2 using table1.doc,replace tstat bdec(3) tdec(2) ctitle(y)

*实例
sysuse auto, clear

reg price mpg

outreg2 using table1.doc,replace            ///
  tstat bdec(3) tdec(2) ctitle(y) 

*2.2 面板固定效应回归结果输出

*基本代码

outreg2 using table1.doc,replace            ///
  tstat ctitle(y) bdec(3) tdec(2)           ///
  keep(var1 var2 var3)                      ///
  addtext(Company FE, YES )

*实例 1 .个体固定效应

webuse grunfeld,clear

xtset company year

xtreg invest mvalue kstock,fe robust

outreg2 using table1.doc,replace             ///
  tstat bdec(3) tdec(2) ctitle(y)            ///
  keep(invest mvalue kstock)                 ///
  addtext(Company FE, YES )

*实例 2 个体和时间效应

webuse grunfeld,clear

xtset company year

xtreg invest mvalue kstock i.year,fe robust

outreg2 using table1.doc,replace             ///
  tstat bdec(3) tdec(2) ctitle(y)            ///
  keep(invest mvalue kstock)                 ///
  addtext(Company FE, YES,Year FE, YES)

*2.3 逐步增加变量回归合并输出

*基本代码

outreg using table1.doc,replace tstat bdec(3) tdec(2) ctitle(y)
outreg using table1.doc,append tstat bdec(3) tdec(2) ctitle(y)

*实例
sysuse auto, clear

reg price mpg

outreg2 using table1.doc,replace             ///
  tstat bdec(3) tdec(2) ctitle(y)
  
reg price mpg rep78

outreg2 using table1.doc,append              ///
  tstat bdec(3) tdec(2) ctitle(y)

*2.4 不同模型回归结果合并输出

*基本代码

outreg2 using table1.doc,replace tstat bdec(3) tdec(2) ctitle(model1)
outreg2 using table1.doc,append tstat bdec(3) tdec(2) ctitle(model2) 
outreg2 using table1.doc,append tstat bdec(3) tdec(2) ctitle(model3)

*实例

webuse grunfeld,clear

xtset company year

reg invest mvalue kstock,robust

outreg2 using table1.doc,replace              ///
  tstat bdec(3) tdec(2) ctitle(OLS)

xtreg invest mvalue kstock,fe robust

outreg2 using table1.doc,append               ///
  tstat bdec(3) tdec(2) ctitle(FE)            ///
  addtext(Company FE, YES)

xtreg invest mvalue kstock i.year,fe robust

outreg2 using table1.doc,append               ///
  tstat bdec(3) tdec(2) ctitle(FE)            ///
  keep(invest mvalue kstock)                  ///
  addtext(Company FE, YES,Year FE, YES)

*2.5 包含两阶段回归的结果输出——以 2SLS 为例
*第一阶段回归结果输出代码

ivregress2 2sls y var3 (var1=var2), first

est restore first

outreg2 using table1.doc,cttop(first)         ///
  tstat bdec(3) tdec(2) replace

*第二阶段回归结果输出代码

ivregress2 2sls y var3 (var1=var2),first

outreg2 using table1.doc,cttop(two)          ///
  tstat bdec(3) tdec(2) replace

*实例

sysuse auto

ivregress2 2sls mpg                          ///
  weight (length=displacement),first

est restore first

outreg2 using table1.doc,cttop(first)        ///
  tstat bdec(3) tdec(2) replace

ivregress2 2sls mpg                          ///
  weight (length=displacement), first
  
outreg2 using table1.doc,cttop(two)          ///
  tstat bdec(3) tdec(2) 

*2.6自定义统计量的输出
*更改有效数字位数

sysuse auto

reg price mpg rep78

outreg2 using table1.doc,replace             ///
  tstat ctitle(y) bdec(4) tdec(5)

*在 logit 估计之后报告伪 R 平方实例

sysuse auto, clear

logit foreign price trunk weight

outreg2 using table1.doc,replace             ///
  tstat bdec(3) tdec(2)                      ///
  addstat(Pseudo R-squared, `e(r2_p)')

*回归后表格中添加 F 值、调整后 R 方 和 F 检验 P 值输出实例

sysuse auto,clear

reg price length  rep78 weight

outreg2  using table1.doc,replace           ///
  tstat bdec(3) tdec(2) e(r2_a,F)           ///
  addstat(F test,e(p)) 


&emsp;

> 作者：云锋 (华南师范大学)     
> 邮箱：leonfengyun@163.com      

> Stata 连享会：  [知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn) | [CSDN](https://blog.csdn.net/arlionn) | [StataChina公众号](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw) 

> Stata连享会 &emsp; [计量专题](https://gitee.com/arlionn/Course/blob/master/README.md)  || [精品课程](https://mp.weixin.qq.com/s/hWtncj56PeFNL4yg2-va0Q) || [简书推文](https://www.jianshu.com/p/de82fdc2c18a)  || [公众号合集](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw)

[![点击查看完整推文列表](https://images.gitee.com/uploads/images/2019/1220/081230_54d0f0b7_1522177.png)](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw)


&emsp;
> #### [2020寒假Stata现场班](http://www.peixun.net/view/1224.html)       
>  北京, 1月8-17日，连玉君-江艇主讲
![](https://images.gitee.com/uploads/images/2019/1220/081427_f4694a1a_1522177.jpeg)

> #### [2020连享会-文本分析与爬虫-现场班](https://zhuanlan.zhihu.com/p/93858899)    
> 西安, 3月26-29日，司继春-游万海 主讲；&ensp; (附助教招聘)
![](https://images.gitee.com/uploads/images/2019/1220/081427_cffbf70e_1522177.jpeg)


## 1. 引言
 一篇实证论文中，最基本也是最重要的部分就是展示 Stata 中得出的统计分析、回归结果等表格。但自己动手做表格往往非常繁琐，word 排版也常常令人抓狂。而 `outreg2` 命令可以让 Stata 自动输出我们想要的表格，为你解决所有结果输出的烦恼。熟练掌握 `outreg2` 命令对我们快速导出 Stata 结果，一步到位的完成实证结果展示有莫大帮助。
## 2. outreg2 命令代码及实例
### 2.1 描述性分析
首先我们介绍描述性统计分析表格的输出，一般实证论文都选择展示全部变量的一般统计指标，这里我们将各种情况都罗列出来，并利用 Stata 自带的 1978 年美国汽车交易数据为大家进行实例演示，方便大家全面了解 `outreg2` 命令。

#### 2.1.1 全部变量一般统计指标
- 代码

`outreg2 using table1.doc,replace sum(log) title(Decriptive statistics) bdec(3)`

- 实例
```Stata
sysuse auto, clear

outreg2 using table1.doc,      ///
  replace sum(log)             ///
  title(Decriptive statistics) ///
  bdec(3) 
```
![全部统计变量一般统计指标](https://images.gitee.com/uploads/images/2019/1228/132022_2d052139_5480865.png)




> **命令解析**
>1. table1.doc 为输出文件名为 table1 的 word 文档命令。类似得，如果需要输出为 excel 表格则更改代码为 table1.xsl 即可。
>2. `sum(log)` 即输出一般统计指标命令，一般统计指标包括样本数、中值、标准误、最大值和最小值。
>3. `title(Decriptive statistics)` 为自定义输出表格的名称命令，可在括号内自行编辑表格名称。
>4. 一般论文要求保存三位有效数字，因此我们利用`bdec(3)`代码进行规范。

#### 2.1.2 部分变量一般统计指标输出
- 代码

`outreg2 using table1.doc, replace sum(log) keep(var1 var2 var3) title(Decriptive statistics) bdec(3)`

- 实例
```Stata
sysuse auto, clear

outreg2 using table1.doc,      ///
  replace sum(log)             ///
  keep(price weight foreign)   ///
  title(Decriptive statistics) ///
  bdec(3)
```
![部分变量一般统计指标](https://images.gitee.com/uploads/images/2019/1228/132022_c0599f1d_5480865.png)


>**命令解析**
>1. `keep(var1 var2 var3)` 为保留部分变量输出命令，其中 **var1** **var2** **var3** 是我们所需要保留的变量名称。
>2. table1.doc 为输出文件名为 table 的 word 文档命令。类似得，如果需要输出为 excel 表格则更改代码为 table1.xsl 即可。
>3. `title(Decriptive statistics)` 为自定义输出表格的名称命令，可在括号内自行编辑表格名称。
>4. 一般论文要求保存三位有效数字，因此我们利用`bdec(3)`代码进行规范。
#### 2.1.3 全部统计指标输出
- 代码

`outreg2 using table1.doc, replace sum(detail) title(Decriptive statistics) bdec(3)`

- 实例
```Stata
sysuse auto, clear

outreg2 using table1.doc,      ///
  replace sum(detail)          ///
  title(Decriptive statistics) ///
  bdec(3)
```



>**命令解析**
>1. `sum(detail)` 为输出全部统计指标命令，除去一般统计指标外还包括方差、偏度、峰度、P50、P10、P95等指标，如果论文中需要非一般统计指标则按照下文所示部分统计量输出代码输出所需指标即可。
>2. table1.doc 为输出文件名为 table1 的 word 文档命令。类似得，如果需要输出为 excel 表格则更改代码为 table1.xsl 即可。
>3. `title(Decriptive statistics)` 为自定义输出表格的名称命令，可在括号内自行编辑表格名称。
>4. 一般论文要求保存三位有效数字，因此我们利用`bdec(3)`代码进行规范。

#### 2.1.4 部分变量部分统计指标输出
- 代码

`outreg2 using table1.doc, replace  sum(detail) keep(var1 var2 var3) eqkeep(N min max) title(Decriptive statistics) bdec(3)`

- 实例
```Stata
sysuse auto, clear

outreg2 using table1.doc,      ///
  replace sum(detail)          ///
  keep(price weight foreign)   ///
  eqkeep(N min max)            ///
  title(Decriptive statistics) ///
  bdec(3)
```
![部分变量部分统计指标](https://images.gitee.com/uploads/images/2019/1228/132022_2150c413_5480865.png)



>**命令解析**
>1. `eqkeep(N min max)` 为保留部分统计指标输出命令，这里我们以样本数、最大值和最小值为例。
>2. `keep(var1 var2 var3)` 为保留部分变量输出命令，同时利用 `keep()` 和 `eqkeep()` 命令进行限定即可进行部分变量和部分统计指标输出。
>3. table1.doc 为输出文件名为 table1 的 word 文档命令。类似得，如果需要输出为 excel 表格则更改代码为 table1.xsl 即可。
>4. `title(Decriptive statistics)` 为自定义输出表格的名称命令，可在括号内自行编辑表格名称。
>5. 一般论文要求保存三位有效数字，因此我们利用`bdec(3)`代码进行规范。
#### 2.1.5 分组描述
- 代码

`bysort var1: outreg2 using table1.doc, replace sum(log) title(Decriptive statistics) bdec(3)`
- 实例
```Stata 
sysuse auto, clear

bysort foreign:outreg2         ///
  using table1.doc,replace     /// 
  sum(log)                     ///
  title(Decriptive statistics) ///
  bdec(3)
```
![分组描述](https://images.gitee.com/uploads/images/2019/1228/132023_abea1cb9_5480865.png)


>**命令解析**
>1. `bysort var1` 为按照变量 **var1** 进行分组命令，在实例中我们以 car type 进行分组描述。
>2. 若论文中需要描述部分变量或部分统计指标时，同样可以添加前文所述 `keep()` 和 `eqkeep()` 命令，此处不再赘述。
>3. table1.doc 为输出文件名为 table1 的 word 文档命令。类似得，如果需要输出为 excel 表格则更改代码为 table1.xsl 即可。
>4. `title(Decriptive statistics)` 为自定义输出表格的名称命令，可在括号内自行编辑表格名称。
>5.  一般论文要求保存三位有效数字，因此我们利用`bdec(3)`代码进行规范。

#### 2.1.6 频数
频数是指落入不同组的个体数。

- 代码

`outreg2 var1 using table1.doc, replace cross title(Decriptive statistics)`
- 实例
```Stata
sysuse auto, clear

outreg2 foreign                       ///
  using table1.doc,replace            ///
  cross title(Decriptive statistics)
```
![频数输出结果](https://images.gitee.com/uploads/images/2019/1228/132022_0545a9f5_5480865.png)
>**命令解析**
>1. 代码中 **var1** 即所需描述频数的分组变量。
>2. table1.doc 为输出文件名为 table1 的 word 文档命令。类似得，如果需要输出为 excel 表格则更改代码为 table1.xsl 即可。
>3. `title(Decriptive statistics)` 为自定义输出表格的名称命令，可在括号内自行编辑表格名称。

### 2.2 回归结果输出
#### 2.2.1 线性回归结果输出
- 代码

`outreg2 using table1.doc,replace tstat bdec(3) tdec(2) ctitle(y) `
- 实例
 ```Stata
sysuse auto, clear

reg price mpg

outreg2 using              ///
  table1.doc,replace        ///
  tstat bdec(3) tdec(2)     ///
  ctitle(y) 
```
![线性回归结果输出](https://images.gitee.com/uploads/images/2019/1228/132023_62575a0e_5480865.png)


>**命令解析**
>1. `ctitle` 为自定义表格内标题命令，如果不进行设定则直接输出为被解释变量名。
>2. table1.doc 为输出文件名为 table1 的 word 文档命令。类似得，如果需要输出为 excel 表格则更改代码为 table1.xsl 即可。
>3. 按照 `outreg2` 命令输出的表格内相关系数下括号内数字为标准误，因此我们一般利用命令 `tstat` 将其更改为 t 值。
>4. `outreg2` 命令输出时默认相关系数和 t 值都保留 3 位有效数字，而一般期刊要求相关系数保留 3 位有效数字，t 值保留 2 位有效数字，因此我们利用 `bdec(3)` 和 `tdec(2)` 命令限定。

#### 2.2.2 面板固定效应回归结果输出
- 代码

`outreg2 using table1.doc,replace tstat ctitle(y) bdec(3) tdec(2) keep(var1 var2 var3) addtext(Company FE, YES )`

- 实例

```Stata
webuse grunfeld,clear

xtset company year

xtreg invest mvalue kstock,   ///
  fe robust

webuse grunfeld,clear

xtset company year

xtreg invest mvalue kstock,   ///
  fe robust

outreg2 using                 ///
  table1.doc,replace          ///
  tstat bdec(3) tdec(2)       ///
  ctitle(y)                   ///
  keep(invest mvalue kstock)  ///
  addtext(Company FE, YES )
 ```
![面板固定效应回归结果输出](https://images.gitee.com/uploads/images/2019/1228/132023_de09df2e_5480865.png)


同理，若要同时控制时间固定效应代码如下：
- 代码

`outreg2 using table1.doc,replace tstat bdec(3) tdec(2) ctitle(y) keep(var1 var2 var3) addtext(Company FE, YES,Year FE, YES)`
- 实例
```Stata
webuse grunfeld,clear

xtset company year

xtreg invest mvalue kstock   ///
  i.year,fe robust

outreg2 using                ///
  table1.doc,replace         ///
  tstat bdec(3) tdec(2)      ///
  ctitle(y)                  ///
  keep(invest mvalue kstock) ///
  addtext(Company FE, YES,Year FE, YES)
```
![同时控制时间固定效应结果输出](https://images.gitee.com/uploads/images/2019/1228/132023_7b5afd6c_5480865.png)


>**命令解析**
>1. `addtext` 为在表中增加信息命令，由于 Stata 进行固定效应回归后单纯利用 `outreg2` 命令输出不会展示是否控制固定效应，因此我们需要利用 `addtext` 命令追加。
>2. `keep()` 命令的作用是保持输出表格简洁，括号中是所有需要展示的变量。
>3. `ctitle` 为自定义表格内标题命令，如果不进行设定则直接输出为被解释变量名。
>4. table1.doc 为输出文件名为 table1 的 word 文档命令。类似得，如果需要输出为 excel 表格则更改代码为 table1.xsl 即可。
>5. 按照 `outreg2` 命令输出的表格内相关系数下括号内数字为标准误，因此我们一般利用命令 `tstat` 将其更改为 t 值。
>6. `outreg2` 命令输出时默认相关系数和 t 值都保留 3 位有效数字，而一般期刊要求相关系数保留 3 位有效数字，t 值保留 2 位有效数字，因此我们利用 `bdec(3)` 和 `tdec(2)` 命令限定。
#### 2.2.3 逐步增加变量回归合并输出
- 代码

```Stata
outreg using         ///
  table1.doc,replace ///
  tstat bdec(3) tdec(2) ctitle(y)
  
outreg using        ///
  table1.doc,append ///
  tstat bdec(3) tdec(2) ctitle(y)
```
- 实例
```Stata
sysuse auto, clear

reg price mpg

outreg2 using        ///
  table1.doc,replace ///
  tstat bdec(3) tdec(2) ctitle(y)

reg price mpg rep78

outreg2 using       ///
  table1.doc,append ///
  tstat bdec(3) tdec(2) ctitle(y)
```
![逐步增加变量回归结果合并输出](https://images.gitee.com/uploads/images/2019/1228/132023_5f84a659_5480865.png)


>**命令解析**
>1. `append` 为合并命令，在实例中我们先进行了 **price** 和 **mpg** 两个变量回归，之后增加变量 **rep78** 回归后再利用 `outreg2` + `append` 命令进行合并输出。
>2. `ctitle` 为自定义表格内标题命令，如果不进行设定则直接输出为被解释变量名。
>3. table1.doc 为输出文件名为 table1 的 word 文档命令。类似得，如果需要输出为 excel 表格则更改代码为 table1.xsl 即可。
>4. 按照 `outreg2` 命令输出的表格内相关系数下括号内数字为标准误，因此我们一般利用命令 `tstat` 将其更改为 t 值。
>5. `outreg2` 命令输出时默认相关系数和 t 值都保留 3 位有效数字，而一般期刊要求相关系数保留 3 位有效数字，t 值保留 2 位有效数字，因此我们利用 `bdec(3)` 和 `tdec(2)` 命令限定。

#### 2.2.4 不同模型回归结果合并输出
- 代码

```Stata
outreg2 using           ///
  table1.doc,replace    ///
  tstat bdec(3) tdec(2) ///
  ctitle(model1)

outreg2 using           ///
  table1.doc,append     ///
  tstat bdec(3) tdec(2) ///
  ctitle(model2) 

outreg2 using           ///
  table1.doc,append     ///
  tstat bdec(3) tdec(2) ///
  ctitle(model3)
```
- 实例
 ```Stata
webuse grunfeld,clear

xtset company year

reg invest mvalue kstock,robust

outreg2 using               ///
  table1.doc,replace         ///
  tstat bdec(3) tdec(2) ctitle(OLS)

xtreg invest mvalue         ///
  kstock,fe robust

outreg2 using               ///
  table1.doc,append          ///
  tstat bdec(3) tdec(2)      ///
  ctitle(FE)                 ///
  addtext(Company FE, YES)

xtreg invest mvalue         ///
  kstock i.year,fe robust

outreg2 using               ///
  table1.doc,append          ///
  tstat bdec(3) tdec(2)      ///
  ctitle(FE)                 ///
  keep(invest mvalue kstock) ///
  addtext(Company FE, YES,Year FE, YES)
```
![不同模型回归结果合并输出](https://images.gitee.com/uploads/images/2019/1228/132023_aa0ab56f_5480865.png)


>**命令解析**
>1. 与逐步增加变量合并输出相类似，不同模型回归结果输出也是利用 `append` 命令。
>2. 利用 `ctitle` 可将标题命名为模型名称。
>3. 固定效应回归后需利用 `addtext` 增加是否控制固定效应信息。
>4. table1.doc 为输出文件名为 table1 的 word 文档命令。类似得，如果需要输出为 excel 表格则更改代码为 table1.xsl 即可。
>5. 按照 `outreg2` 命令输出的表格内相关系数下括号内数字为标准误，因此我们一般利用命令 `tstat` 将其更改为 t 值。
>6. `outreg2` 命令输出时默认相关系数和 t 值都保留 3 位有效数字，而一般期刊要求相关系数保留 3 位有效数字，t 值保留 2 位有效数字，因此我们利用 `bdec(3)` 和 `tdec(2)` 命令限定。

#### 2.2.5 包含两阶段回归的结果输出——以 2SLS 为例
- 第一阶段回归结果输出代码
```Stata
ivregress2 2sls y          ///
var3 (var1=var2), first

est restore first

outreg2 using              ///
  table1.doc,cttop(first)  ///
  tstat bdec(3) tdec(2) replace
```
- 第二阶段回归结果输出代码
```Stata
ivregress2 2sls y        ///
  var3 (var1=var2), first

outreg2 using            ///
  table1.doc, cttop(two) ///
  tstat bdec(3) tdec(2) replace
```
- 实例
```Stata
sysuse auto

ivregress2 2sls mpg        ///
  weight (length=displacement),first

est restore first

outreg2 using              ///
  table1.doc,cttop(first)  ///
  tstat bdec(3) tdec(2) replace
 
ivregress2 2sls mpg        ///
  weight (length=displacement), first

outreg2 using              ///
  table1.doc,cttop(two)    ///
  tstat bdec(3) tdec(2) 
```
![2SLS 两阶段回归结果输出](https://images.gitee.com/uploads/images/2019/1228/132023_5d4c3523_5480865.png)



- 命令解析
>1. 两阶段回归输出时，若只展示第二阶段回归，则直接利用第二阶段回归输出代码即可。如需展示第一阶段回归结果，则可以利用实例中所示方法，利用命令先将第一阶段回归结果保存输出，再输出第二阶段回归结果。实例中去掉了第二阶段输出时的 `replace` 这样就可以将两阶段结果输出在一张表上。
>2. 按照 `outreg2` 命令输出的表格内相关系数下括号内数字为标准误，因此我们一般利用命令 `tstat` 将其更改为 t 值。
>3. `outreg2` 命令输出时默认相关系数和 t 值都保留 3 位有效数字，而一般期刊要求相关系数保留 3 位有效数字，t 值保留 2 位有效数字，因此我们利用 `bdec(3)` 和 `tdec(2)` 命令限定。

#### 2.2.6 自定义统计量的输出
前文所介绍的输出命令下 Stata 默认输出相关系数、标准误、样本数和 R 平方，并保留 3 位有效数字。但根据实际情况，有些论文需要报告伪 R 平方、F值，或者更改相关系数和t值有效数字位数，这里我们进一步为大家介绍自定义统计量输出命令。
- 更改有效数字位数实例
```Stata
sysuse auto

reg price mpg rep78
 
outreg2 using        ///
  table1.doc,replace ///
  tstat ctitle(y)    ///
  bdec(4) tdec(5)
```
![更改有效数字位数](https://images.gitee.com/uploads/images/2019/1228/132023_9a00a9fd_5480865.png)

>**命令解析**
>`bdec(4)` 是指相关系数保留 4 位有效数字，`tdec(5)` 指 t 值保留 5 位有效数字。

- 在 logit 估计之后报告伪 R 平方实例
```Stata
sysuse auto, clear

logit foreign price trunk weight

outreg2 using           ///
  table1.doc,replace    ///
  tstat bdec(3) tdec(2) ///
  addstat(Pseudo R-squared, `e(r2_p)')
```
![增加报告伪 R 方回归结果输出](https://images.gitee.com/uploads/images/2019/1228/132023_dc0e52eb_5480865.png)


>**命令解析**
>1. logit 模型回归是没有 R 方的，因此在报告时我们需要输出伪 R 方的值，在正常输出回归表格代码后增加 `addstat()` 命令可以增加报告伪 R 方。
>2. `outreg2` 命令输出时默认相关系数和 t 值都保留 3 位有效数字，而一般期刊要求相关系数保留 3 位有效数字，t 值保留 2 位有效数字，因此我们利用 `bdec(3)` 和 `tdec(2)` 命令限定。
- 回归后表格中添加 F 值、调整后 R 方 和 F 检验 P 值输出实例
```Stata
sysuse auto,clear

reg price length  rep78 weight

outreg2  using          ///
  table1.doc,replace    ///
  tstat bdec(3) tdec(2) ///
  e(r2_a,F)             ///
  addstat(F test,e(p)) 
```
![增加报告调整后 R 方 F 值等结果输出](https://images.gitee.com/uploads/images/2019/1228/132023_f0d438f7_5480865.png)



>**命令解析**
>1. 与增加伪 R 方类似，增加 F 检验同样是利用 `addstat` 命令，括号内代码更换为 F test,e(p)  即可。
>2. `outreg2` 命令输出时默认相关系数和 t 值都保留 3 位有效数字，而一般期刊要求相关系数保留 3 位有效数字，t 值保留 2 位有效数字，因此我们利用 `bdec(3)` 和 `tdec(2)` 命令限定。
## 3.小结
通过对 `outreg2` 命令的学习，我们不难发现它可以输出除相关系数矩阵外，一般实证论文所需几乎全部的表格，实用性非常强，可谓一招鲜吃遍天。最后我们整理一下利用 `outreg2` 输出主要表格的代码，方便大家理清 `outreg2` 命令语法。
```Stata
*描述性统计表格输出
*部分变量部分统计指标输出
outreg2 using          ///
  table1.doc, replace  ///
  sum(detail)          ///
  keep(var1 var2 var3) ///
  eqkeep(N min mix)    ///
  title(Decriptive statistics)               

*回归结果输出
*1.普通 OLS 回归结果输出
outreg2 using            ///
  table1.doc, replace    ///
  sum(detail)            ///
  keep(var1 var2 var3)   ///
  eqkeep(N min mix)      ///
  title(Decriptive statistics) ///
  bdec(3)
 
 outreg2 using           ///
  table1.doc,replace     ///
  tstat bdec(3)          ///
  tdec(2) ctitle(y)  

 *2.面板固定效应回归结果输出（个体固定效应和时间固定效应）
outreg2 using             ///
  table1.doc,replace      ///
  tstat bdec(3) tdec(2)   ///
  ctitle(y)               ///
  keep(var1 var2 var3)    ///
  addtext(Company FE, YES,Year FE, YES) 


*3.逐步增加变量回归结果合并输出
outreg2 using             ///
  table1.doc,replace      ///
  tstat bdec(3) tdec(2)   ///
  ctitle(y)

outreg2 using             ///
  table1.doc,append       ///
  tstat bdec(3) tdec(2)   ///
  ctitle(y)

*4.不同模型回归结果合并输出
outreg2 using             ///
  table1.doc,replace      ///
  tstat bdec(3) tdec(2)   ///
  ctitle(model1)

outreg2 using             ///
  table1.doc,append       ///
  tstat bdec(3) tdec(2)   ///
  ctitle(model2) 

outreg2 using             ///
  table1.doc,append       ///
  tstat bdec(3) tdec(2)   ///
  ctitle(model3)

*5. 2SLS 两阶段模型回归结果输出
ivregress2 2sls y         ///
var3 (var1=var2), first

est restore first

outreg2 using             ///
  table1.doc,cttop(first) ///
  tstat bdec(3) tdec(2) replace

ivregress2 2sls y         ///
  var3 (var1=var2), first

outreg2 using             ///
  table1.doc, cttop(two)  ///
  tstat bdec(3) tdec(2) replace 
```

## 附录：本文示例 do 文档
https://gitee.com/Stata002/StataSX2018/blob/master/2020Spring/%E4%BA%91%E5%B3%B0-%E5%8D%8E%E5%B8%88/%E6%8E%A8%E6%96%871-outreg2%E5%91%BD%E4%BB%A4%E8%AF%A6%E8%A7%A3/outreg2%E5%91%BD%E4%BB%A4%E8%AF%A6%E8%A7%A3.do

















>#### 关于我们
- **Stata连享会** 由中山大学连玉君老师团队创办，定期分享实证分析经验。
- **欢迎赐稿：** 欢迎赐稿至StataChina@163.com。录用稿件达 **三篇** 以上，即可 **免费** 获得一期 Stata 现场培训资格。
- **往期精彩推文：**
 [Stata绘图](https://mp.weixin.qq.com/s/xao8knOk0ulGfNc7vasfew) | [时间序列+面板数据](https://mp.weixin.qq.com/s/8yP1Dijylgreg59QIkqnMg) | [Stata资源](https://mp.weixin.qq.com/s/Kdeoi5uJyNtwwwptdQDQDQ) | [数据处理+程序](https://mp.weixin.qq.com/s/_3DQacFyy7juRjgFedp9WQ) |  [回归分析-交乘项-内生性](https://mp.weixin.qq.com/s/61qJNWnL4KRp0fbLxuDGww)
---
![欢迎加入Stata连享会(公众号: StataChina)](https://images.gitee.com/uploads/images/2019/1220/081400_32fef61a_1522177.png)







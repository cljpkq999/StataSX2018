> **任务：** 这篇推文王俊基本上写完了，但我还是很不满意，觉得写得干巴巴的，想进一步完善一下。特邀请第二作者。


&emsp;
> 前期相关推文：
*   [Stata：runby - 一切皆可分组计算！](https://www.jianshu.com/p/68af54db9ecb)
*   [盈余管理、过度投资怎么算？分组回归获取残差](https://www.jianshu.com/p/6ea640fe68f1)



&emsp;

> 作者：王俊 (中山大学)，xx (xx大学)    
>     
> Stata 连享会：  [知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn) | [CSDN](https://blog.csdn.net/arlionn) | [StataChina公众号](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw) 
  

---

## 1. 滚动回归 (Rolling Regression)

在时间序列分析中，我们经常分析变量之间的动态关系。

滚动回归通常是用来分析变量之间随时间而变动的关系。

Fama and MacBeth (1973) 为了检验风险与平均收益的关系需要估计出组合的各个时点的$\hat{\beta}_{pt}$。而$\hat{\beta}_{pt}$是 $\hat{\beta}_{it}$的加权平均，因而需要估计出每个时点单个资产的$\hat{\beta}_{it}$。

具体做法是：
- 确定滚动回归的窗口大小
- 对$\tilde{R}_{it}=\alpha_i+\beta_i\tilde{R}_{mt}+\tilde{\epsilon}_{it}$进行滚动回归得到每个资产T个时点$\hat{\beta}_{it}$的估计值
- 加权平均得到T个时点$\hat{\beta}_{pt}$的估计值

## 2. Stata实现命令

### 2.1 rolling 命令

```stata
 rolling [exp_list] [if] [in] , window(#) [options] :  command
*- window(#)        滚动的窗口，相当于每次滚动过程中需要的连续数据个数
*- recursive 设定起始点不变，每次滚动窗口加1
*- rrecursive 设定结束点不变，每次滚动窗口加1
*- clear 用滚动的结果来替换原内存，即使原来内存里的数据没有被保存
*- stepsize(#)        每次滚动窗口增加的时期个数
*- start(time_constant) 滚动的起始时间点
*- end(time_constant)  滚动的结束时间点
```

在滚动回归之前，必须要 `tsset` 声明时间变量。当使用 `rolling` 命令时，`window(#)` 选项是必须的，其含义是每次回归的窗口大小。

```stata
clear
webuse lutkepohl2
tsset qtr
rolling _b _se, window(30) rrecursive: regress dln_inv dln_inc dln_consump
```

使用 `help rolling` 中的第一个例子，发现观察值一共有92个，而滚动回归的窗口是30，所以回归的总次数是 `92-30+1=63`。

当设定 `recursive` 选项时，起始时点被固定，结束时点每次滚动向后一个单位，那么 `window` 的大小也就会从30依次加1。

```stata
. list start end in 1/10
     +-----------------+
     |  start      end |
     |-----------------|
  1. | 1960q1   1967q2 |
  2. | 1960q1   1967q3 |
  3. | 1960q1   1967q4 |
  4. | 1960q1   1968q1 |
  5. | 1960q1   1968q2 |
     |-----------------|
  6. | 1960q1   1968q3 |
  7. | 1960q1   1968q4 |
  8. | 1960q1   1969q1 |
  9. | 1960q1   1969q2 |
 10. | 1960q1   1969q3 |
     +-----------------+
```

从1960q1到1967q2共有30个时间单位，而后每次回归窗口加1，起始点不变。

当设定 `rrecursive` 选项时，结束时点被固定，起始时点每次滚动向前推一个单位。

```stata
. list start end in -10/-1

     +-----------------+
     |  start      end |
     |-----------------|
 54. | 1973q2   1982q4 |
 55. | 1973q3   1982q4 |
 56. | 1973q4   1982q4 |
 57. | 1974q1   1982q4 |
 58. | 1974q2   1982q4 |
     |-----------------|
 59. | 1974q3   1982q4 |
 60. | 1974q4   1982q4 |
 61. | 1975q1   1982q4 |
 62. | 1975q2   1982q4 |
 63. | 1975q3   1982q4 |
     +-----------------+
```

可以看出来1975q3到1982q4是30个时间单位，每次回归窗口加1，结束时点不变。

`stata` 默认会以 `_b` 开头来命名估计出的系数，并将结果保存下来。另外，可以在 `rolling` 后加上 `_se`，那么 `stata` 会将每个系数的标准差以 `_se` 为开头存下来。

| 变量名        |    估计系数      | 标准差  |
| ------------- |:-------------:| -----:|
| dln_inc   | _b_dln_inc| _se_dln_inc |
| dln_con~p       | _b_dln_con~p     |  _se_dln_co~p |
| cons |  _b_cons     |   _se_cons|

### 2.2 rolling2 命令

另外一个命令是 `rolling2` 。这个命令的做法其实和 `rolling` 是差不多的，但最大的一个不同点是，`rolling`在对面板数据进行滚动回归时，会分组进行滚动回归；而 `rolling2` 就是 pool regression。

举个例子，利用 `help rolling2` 的示例数据。

```stata
*- rolling滚动回归
webuse grunfeld
rolling _b _se, window(8): regress invest mvalue kstock
count
*-130
```

`grunfeld` 是面板数据，共有10家公司，每家公司有20年数据，即总共200个观察值。当使用上述命令进行滚动回归时，每家公司会被分开分别进行滚动回归，那么总的回归次数就是`(20-8+1)*10=130`。

```stata
webuse grunfeld
rolling2 _b _se, window(8) onepanel: regress invest mvalue kstock
count
*- 13
```

当使用 `rolling2` 的时候，它会将整个面板数据混合回归，也就是说，从整个面板数据中滚动找出8年的数据来回归。那么总的回归次数就是 `20-8+1=13` 次。

### 2.3 asreg 命令

滚动回归会需要大量的循环。但在运行的时候会遇到很多数据结构问题，诸如非平衡面板数据、重复值、缺失值等。而 `asreg` 对不同的数据结构并不会采用一种固定的方式估计，恰恰相反，它能识别出不同类别的数据集结构，并采用合适的方法。除此之外，`asreg` 命令是利用 `Mata` 语言来完成的。

因而 `asreg` 在数据集特别大的时候，会非常有用，能够节约大量的时间。同时，`asreg` 可以将估计结果输出到现有内存中，这也就减少了我们自己再去将估计结果与原数据集合并的麻烦。

```stata
 asreg depvar indepvars [if] [in] [, window([rangevar] # ) recursive minimum( # ) by(varlist) statistics_options]
*- window(#): 设定滚动窗口长度
*- recursive:起始点固定，结束点和滚动窗口增长
*- by:分组变量
*-  minimum(#)：用来回归的最小的观察值个数，但应至少大于回归元的个数
```

展示结果与 `rolling` 类似，也更加好理解，就不再赘述。可以 `help asreg` 。

## 3. 参考资料：
- [Fama E F , Macbeth J D . Risk, Return, and Equilibrium: Empirical Tests[J]. Journal of Political Economy, 1973, 81(3):607-636.](http://finpko.faculty.ku.edu/myssi/FIN938/Fama%20&%20Macbeth.JPE_1973.pdf)
- [https://fintechprofessor.com/2017/09/08/rolling-window-regressions-in-stata/](https://fintechprofessor.com/2017/09/08/rolling-window-regressions-in-stata/)
- [获取分组回归的残差：盈余管理、过度投资的计算](https://www.jianshu.com/p/6ea640fe68f1)



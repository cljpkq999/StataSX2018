> 作者：连玉君 ( [知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn) | [github](http://github.com/StataChina) )

> Stata连享会 &emsp; [计量专题](https://gitee.com/arlionn/Course/blob/master/README.md)  || [精品课程](https://mp.weixin.qq.com/s/hWtncj56PeFNL4yg2-va0Q) || [简书推文](https://www.jianshu.com/p/de82fdc2c18a)  || [公众号合集](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw)

[![点击查看完整推文列表](https://upload-images.jianshu.io/upload_images/7692714-77595f33f776ad2e.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw)

&emsp;
> #### [2020寒假Stata现场班](http://www.peixun.net/view/1224.html) (北京, 1月8-17日，连玉君-江艇主讲)，[「+助教招聘」](https://zhuanlan.zhihu.com/p/91621242)
[![2020寒假Stata现场班](https://file.lianxh.cn/images/20191111/6913c0874f78fc6f176876b66999c401.jpg "2020寒假Stata现场班-扫码了解详情")](https://mp.weixin.qq.com/s/-dFPrGQMhH3JzA4tEc35kQ)



[原文链接1](https://github.com/StataChina/StataTraining/tree/master/images)  |  [原文链接2](https://geocenter.github.io/StataTraining/portfolio/01_resource/)

[完整 PDF 版本下载](https://gitee.com/arlionn/Stata-Jianshu/attach_files)

[单个 PDF 下载](https://gitee.com/arlionn/StataTraining/tree/gh-pages/pdf)


>## 1. Stata 数据处理基础

![图1-1：Stata常用快捷键](http://upload-images.jianshu.io/upload_images/7692714-9bc09188df7d9c4c.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![图1-2：Stata基本设定](http://upload-images.jianshu.io/upload_images/7692714-1aef0719c270c912.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![图1-3：Stata-导入数据常用命令](http://upload-images.jianshu.io/upload_images/7692714-62f2b729c7355ba4.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![图1-4：Stata命令语法结构](http://upload-images.jianshu.io/upload_images/7692714-de9ee64d9f11ee11.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![图1-5：Stata基本运算符](http://upload-images.jianshu.io/upload_images/7692714-1f9c4304419a0f20.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![图1-6：Stata变量存储类型](http://upload-images.jianshu.io/upload_images/7692714-9de38c0d6fea8f95.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![图1-7：Stata数据概览和基本分析](http://upload-images.jianshu.io/upload_images/7692714-5f73a2b7b2763bcb.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![图1-8：Stata基本统计描述分析](http://upload-images.jianshu.io/upload_images/7692714-877d9ffe0a746962.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![图1-9：Stata-创建新变量](http://upload-images.jianshu.io/upload_images/7692714-0dc328de5977f395.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

&emsp;
> #### [连享会计量方法专题……](https://gitee.com/arlionn/Course/blob/master/README.md)

> ## 2. Stata 数据变换

![图2-1：Stata-数据删减和子样本](http://upload-images.jianshu.io/upload_images/7692714-72cda58368368152.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![图2-2：Stata-修改数据](http://upload-images.jianshu.io/upload_images/7692714-d058b405cfbe08b8.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![图2-3：Stata-变量标签](http://upload-images.jianshu.io/upload_images/7692714-adefc8a6d1604eab.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


![图2-4：Stata-数据的纵横变换](http://upload-images.jianshu.io/upload_images/7692714-0dd7b8bef421454d.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![图2-5：Stata-数据合并](http://upload-images.jianshu.io/upload_images/7692714-4db924f44923a6f7.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![](http://upload-images.jianshu.io/upload_images/7692714-2d2c7ceb44d708ec.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![图2-6：Stata-文字变量处理](http://upload-images.jianshu.io/upload_images/7692714-ced7ccd883645038.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![图2-7：Stata-数据的保存和导出](http://upload-images.jianshu.io/upload_images/7692714-5d30240ad0d7d947.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


&emsp;

> Stata连享会 [专题课程](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文](https://github.com/arlionn/stata/blob/master/README.md)

&emsp;
> ## 3. 数据的可视化分析

![图3-1：Stata-绘图命令的基本语法](http://upload-images.jianshu.io/upload_images/7692714-49f79681ff33a75d.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![图3-2：Stata-单变量绘图命令](http://upload-images.jianshu.io/upload_images/7692714-fbc3e1524b28bbde.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![图3-3：Stata-双变量：横轴离散变量-纵轴连续变量](http://upload-images.jianshu.io/upload_images/7692714-52f568f557428842.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![](http://upload-images.jianshu.io/upload_images/7692714-f7e73800688a0b30.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
![](http://upload-images.jianshu.io/upload_images/7692714-93795d0e8e1e8c21.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
![图3-4：Stata-双变量绘图命令](http://upload-images.jianshu.io/upload_images/7692714-91e6fa247983abb6.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


![图3-5：Stata-三变量绘图命令](http://upload-images.jianshu.io/upload_images/7692714-b377df3021e7b2b8.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![图3-6：Stata-散点图、拟合图等](http://upload-images.jianshu.io/upload_images/7692714-1b7310977ce55c59.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

&emsp;
> #### [连享会计量方法专题……](https://gitee.com/arlionn/Course/blob/master/README.md)

> ## 4. Stata绘图

![](http://upload-images.jianshu.io/upload_images/7692714-5a52f2f621e8b38f.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
![图4-1：Stata图形的基本要素](http://upload-images.jianshu.io/upload_images/7692714-ed248308f4d32028.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


![图4-2：Stata绘图模板](http://upload-images.jianshu.io/upload_images/7692714-16e120fe22d39596.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![图4-3：Stata图形编辑器](http://upload-images.jianshu.io/upload_images/7692714-2dac0c92e38d77e2.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


![图4-4：保存stata图片](http://upload-images.jianshu.io/upload_images/7692714-434cfe794bf6c81f.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

> ## 图中使用的 dofile 
请点击 [github-StataChina](https://github.com/StataChina/StataTraining/blob/master/CheatSheet/ProgrammingCheatsheet.do) 下载。



---

&emsp;

>#### 关于我们

- **Stata连享会** 由中山大学连玉君老师团队创办，定期分享实证分析经验。
- 推文同步发布于 [CSDN](https://blog.csdn.net/arlionn) 、[简书](http://www.jianshu.com/u/69a30474ef33) 和 [知乎Stata专栏](https://www.zhihu.com/people/arlionn)。可在百度中搜索关键词 「Stata连享会」查看往期推文。
- 点击推文底部【阅读原文】可以查看推文中的链接并下载相关资料。
- **欢迎赐稿：** 欢迎赐稿。录用稿件达 **三篇** 以上，即可 **免费** 获得一期 Stata 现场培训资格。
- **E-mail：** StataChina@163.com
- [**往期精彩推文**：一网打尽](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw)

---
[![欢迎加入Stata连享会(公众号: StataChina)](https://upload-images.jianshu.io/upload_images/7692714-fce74434516b3e4d?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)](https://mp.weixin.qq.com/s/RwkuPpLS7bI5C5OhRqjkOw)











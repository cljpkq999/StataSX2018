> [一套很好的 PDF 讲义](https://data.princeton.edu/wws509/notes)，可以作为我们写推文的重要参考资料。

[Chapters in PDF Format](https://data.princeton.edu/wws509/notes/#)

[2\. Linear Models for Continuous Data](https://data.princeton.edu/wws509/notes/c2.pdf)

[3\. Logit Models for Binary Data](https://data.princeton.edu/wws509/notes/c3.pdf)

[4\. Poisson Models for Count Data](https://data.princeton.edu/wws509/notes/c4.pdf)

[4a*. Addendum on Overdispersed Count Data](https://data.princeton.edu/wws509/notes/c4a.pdf)

[5\. Log-Linear Models for Contingency Tables](https://data.princeton.edu/wws509/notes/c5.pdf)

[6\. Multinomial Response Models](https://data.princeton.edu/wws509/notes/c6.pdf)

[7\. Survival Models](https://data.princeton.edu/wws509/notes/c7.pdf)

[8*. Panel and Clustered Data](https://data.princeton.edu/wws509/notes/fixedRandom.pdf)

[A. Review of Likelihood Theory](https://data.princeton.edu/wws509/notes/a1.pdf)

[B. Generalized Linear Model Theory](https://data.princeton.edu/wws509/notes/a2.pdf)

